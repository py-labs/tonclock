﻿namespace TonClock
{
    partial class frmMolodimetr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.pnlStress = new System.Windows.Forms.Panel();
            this.pnlPressure = new System.Windows.Forms.Panel();
            this.tlpBottom = new System.Windows.Forms.TableLayoutPanel();
            this.pnlPulse = new System.Windows.Forms.Panel();
            this.pnlBlood = new System.Windows.Forms.Panel();
            this.pnlAge = new System.Windows.Forms.Panel();
            this.pnlLegend = new System.Windows.Forms.Panel();
            this.pnlSosud = new System.Windows.Forms.Panel();
            this.pnlTopTitle = new System.Windows.Forms.Panel();
            this.tlpHistory = new System.Windows.Forms.TableLayoutPanel();
            this.btnHistoryLast = new System.Windows.Forms.Button();
            this.btnHistoryNext = new System.Windows.Forms.Button();
            this.cmbHistory = new System.Windows.Forms.ComboBox();
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.mnuPulseWave = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tlpRight = new System.Windows.Forms.TableLayoutPanel();
            this.pnlPulseWave = new System.Windows.Forms.Panel();
            this.pnlRightBottom = new System.Windows.Forms.Panel();
            this.pnlRepers = new System.Windows.Forms.Panel();
            this.tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.tlpMain.SuspendLayout();
            this.tlpBottom.SuspendLayout();
            this.tlpHistory.SuspendLayout();
            this.menuStripMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tlpRight.SuspendLayout();
            this.pnlRightBottom.SuspendLayout();
            this.tlpButtons.SuspendLayout();
            this.SuspendLayout();
            // 
            // tlpMain
            // 
            this.tlpMain.ColumnCount = 3;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tlpMain.Controls.Add(this.pnlStress, 2, 2);
            this.tlpMain.Controls.Add(this.pnlPressure, 1, 2);
            this.tlpMain.Controls.Add(this.tlpBottom, 0, 3);
            this.tlpMain.Controls.Add(this.pnlSosud, 0, 2);
            this.tlpMain.Controls.Add(this.pnlTopTitle, 0, 1);
            this.tlpMain.Controls.Add(this.tlpHistory, 0, 0);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 5;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpMain.Size = new System.Drawing.Size(449, 440);
            this.tlpMain.TabIndex = 0;
            // 
            // pnlStress
            // 
            this.pnlStress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlStress.Location = new System.Drawing.Point(271, 52);
            this.pnlStress.Name = "pnlStress";
            this.pnlStress.Size = new System.Drawing.Size(175, 208);
            this.pnlStress.TabIndex = 3;
            this.pnlStress.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlStress_Paint);
            // 
            // pnlPressure
            // 
            this.pnlPressure.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPressure.Location = new System.Drawing.Point(182, 52);
            this.pnlPressure.Name = "pnlPressure";
            this.pnlPressure.Size = new System.Drawing.Size(83, 208);
            this.pnlPressure.TabIndex = 2;
            this.pnlPressure.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlPressure_Paint);
            // 
            // tlpBottom
            // 
            this.tlpBottom.ColumnCount = 3;
            this.tlpMain.SetColumnSpan(this.tlpBottom, 3);
            this.tlpBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpBottom.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpBottom.Controls.Add(this.pnlPulse, 2, 0);
            this.tlpBottom.Controls.Add(this.pnlBlood, 1, 0);
            this.tlpBottom.Controls.Add(this.pnlAge, 0, 0);
            this.tlpBottom.Controls.Add(this.pnlLegend, 0, 1);
            this.tlpBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpBottom.Location = new System.Drawing.Point(3, 266);
            this.tlpBottom.Name = "tlpBottom";
            this.tlpBottom.RowCount = 2;
            this.tlpBottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpBottom.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpBottom.Size = new System.Drawing.Size(443, 150);
            this.tlpBottom.TabIndex = 0;
            // 
            // pnlPulse
            // 
            this.pnlPulse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPulse.Location = new System.Drawing.Point(297, 3);
            this.pnlPulse.Name = "pnlPulse";
            this.pnlPulse.Size = new System.Drawing.Size(143, 124);
            this.pnlPulse.TabIndex = 4;
            this.pnlPulse.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlPulse_Paint);
            // 
            // pnlBlood
            // 
            this.pnlBlood.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBlood.Location = new System.Drawing.Point(150, 3);
            this.pnlBlood.Name = "pnlBlood";
            this.pnlBlood.Size = new System.Drawing.Size(141, 124);
            this.pnlBlood.TabIndex = 3;
            this.pnlBlood.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlBlood_Paint);
            // 
            // pnlAge
            // 
            this.pnlAge.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAge.Location = new System.Drawing.Point(3, 3);
            this.pnlAge.Name = "pnlAge";
            this.pnlAge.Size = new System.Drawing.Size(141, 124);
            this.pnlAge.TabIndex = 2;
            this.pnlAge.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlAge_Paint);
            // 
            // pnlLegend
            // 
            this.tlpBottom.SetColumnSpan(this.pnlLegend, 3);
            this.pnlLegend.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlLegend.Location = new System.Drawing.Point(3, 133);
            this.pnlLegend.Name = "pnlLegend";
            this.pnlLegend.Size = new System.Drawing.Size(437, 14);
            this.pnlLegend.TabIndex = 0;
            this.pnlLegend.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlLegend_Paint);
            // 
            // pnlSosud
            // 
            this.pnlSosud.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlSosud.Location = new System.Drawing.Point(3, 52);
            this.pnlSosud.Name = "pnlSosud";
            this.pnlSosud.Size = new System.Drawing.Size(173, 208);
            this.pnlSosud.TabIndex = 1;
            this.pnlSosud.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlSosud_Paint);
            // 
            // pnlTopTitle
            // 
            this.tlpMain.SetColumnSpan(this.pnlTopTitle, 3);
            this.pnlTopTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTopTitle.Location = new System.Drawing.Point(0, 30);
            this.pnlTopTitle.Margin = new System.Windows.Forms.Padding(0);
            this.pnlTopTitle.Name = "pnlTopTitle";
            this.pnlTopTitle.Size = new System.Drawing.Size(449, 19);
            this.pnlTopTitle.TabIndex = 4;
            this.pnlTopTitle.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlTopTitle_Paint);
            // 
            // tlpHistory
            // 
            this.tlpHistory.ColumnCount = 3;
            this.tlpMain.SetColumnSpan(this.tlpHistory, 3);
            this.tlpHistory.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpHistory.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tlpHistory.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpHistory.Controls.Add(this.btnHistoryLast, 0, 0);
            this.tlpHistory.Controls.Add(this.btnHistoryNext, 2, 0);
            this.tlpHistory.Controls.Add(this.cmbHistory, 1, 0);
            this.tlpHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpHistory.Location = new System.Drawing.Point(0, 0);
            this.tlpHistory.Margin = new System.Windows.Forms.Padding(0);
            this.tlpHistory.Name = "tlpHistory";
            this.tlpHistory.RowCount = 1;
            this.tlpHistory.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpHistory.Size = new System.Drawing.Size(449, 30);
            this.tlpHistory.TabIndex = 5;
            // 
            // btnHistoryLast
            // 
            this.btnHistoryLast.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHistoryLast.Location = new System.Drawing.Point(3, 3);
            this.btnHistoryLast.Name = "btnHistoryLast";
            this.btnHistoryLast.Size = new System.Drawing.Size(83, 24);
            this.btnHistoryLast.TabIndex = 0;
            this.btnHistoryLast.Text = "<--";
            this.btnHistoryLast.UseVisualStyleBackColor = true;
            this.btnHistoryLast.Click += new System.EventHandler(this.btnHistoryLast_Click);
            // 
            // btnHistoryNext
            // 
            this.btnHistoryNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnHistoryNext.Location = new System.Drawing.Point(361, 3);
            this.btnHistoryNext.Name = "btnHistoryNext";
            this.btnHistoryNext.Size = new System.Drawing.Size(85, 24);
            this.btnHistoryNext.TabIndex = 1;
            this.btnHistoryNext.Text = "-->";
            this.btnHistoryNext.UseVisualStyleBackColor = true;
            this.btnHistoryNext.Click += new System.EventHandler(this.btnHistoryNext_Click);
            // 
            // cmbHistory
            // 
            this.cmbHistory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbHistory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHistory.FormattingEnabled = true;
            this.cmbHistory.Location = new System.Drawing.Point(92, 3);
            this.cmbHistory.Name = "cmbHistory";
            this.cmbHistory.Size = new System.Drawing.Size(263, 21);
            this.cmbHistory.TabIndex = 2;
            this.cmbHistory.SelectedIndexChanged += new System.EventHandler(this.cmbHistory_SelectedIndexChanged);
            // 
            // menuStripMain
            // 
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuPulseWave,
            this.mnuExit});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.Size = new System.Drawing.Size(625, 24);
            this.menuStripMain.TabIndex = 1;
            this.menuStripMain.Text = "menuStrip1";
            // 
            // mnuPulseWave
            // 
            this.mnuPulseWave.Image = global::TonClock.Properties.Resources.Key_16x16;
            this.mnuPulseWave.Name = "mnuPulseWave";
            this.mnuPulseWave.Size = new System.Drawing.Size(130, 20);
            this.mnuPulseWave.Text = "&Пульсовая волна";
            this.mnuPulseWave.Click += new System.EventHandler(this.mnuPulseWave_Click);
            // 
            // mnuExit
            // 
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Size = new System.Drawing.Size(95, 20);
            this.mnuExit.Text = "&Закрыть окно";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tlpMain);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tlpRight);
            this.splitContainer1.Size = new System.Drawing.Size(625, 440);
            this.splitContainer1.SplitterDistance = 449;
            this.splitContainer1.TabIndex = 2;
            this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
            // 
            // tlpRight
            // 
            this.tlpRight.ColumnCount = 1;
            this.tlpRight.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpRight.Controls.Add(this.pnlPulseWave, 0, 0);
            this.tlpRight.Controls.Add(this.pnlRightBottom, 0, 1);
            this.tlpRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpRight.Location = new System.Drawing.Point(0, 0);
            this.tlpRight.Name = "tlpRight";
            this.tlpRight.RowCount = 2;
            this.tlpRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpRight.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpRight.Size = new System.Drawing.Size(172, 440);
            this.tlpRight.TabIndex = 0;
            // 
            // pnlPulseWave
            // 
            this.pnlPulseWave.AutoScroll = true;
            this.pnlPulseWave.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPulseWave.Location = new System.Drawing.Point(3, 3);
            this.pnlPulseWave.Name = "pnlPulseWave";
            this.pnlPulseWave.Size = new System.Drawing.Size(166, 214);
            this.pnlPulseWave.TabIndex = 7;
            this.pnlPulseWave.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlPulseWave_Paint);
            this.pnlPulseWave.Resize += new System.EventHandler(this.pnlPulseWave_Resize);
            // 
            // pnlRightBottom
            // 
            this.pnlRightBottom.Controls.Add(this.pnlRepers);
            this.pnlRightBottom.Controls.Add(this.tlpButtons);
            this.pnlRightBottom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlRightBottom.Location = new System.Drawing.Point(3, 223);
            this.pnlRightBottom.Name = "pnlRightBottom";
            this.pnlRightBottom.Size = new System.Drawing.Size(166, 214);
            this.pnlRightBottom.TabIndex = 0;
            // 
            // pnlRepers
            // 
            this.pnlRepers.AutoScroll = true;
            this.pnlRepers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlRepers.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlRepers.Location = new System.Drawing.Point(0, 35);
            this.pnlRepers.Name = "pnlRepers";
            this.pnlRepers.Size = new System.Drawing.Size(166, 136);
            this.pnlRepers.TabIndex = 6;
            this.pnlRepers.Scroll += new System.Windows.Forms.ScrollEventHandler(this.pnlRepers_Scroll);
            this.pnlRepers.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlRepers_Paint);
            this.pnlRepers.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlRepers_MouseDown);
            this.pnlRepers.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlRepers_MouseMove);
            this.pnlRepers.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pnlRepers_MouseUp);
            // 
            // tlpButtons
            // 
            this.tlpButtons.ColumnCount = 2;
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpButtons.Controls.Add(this.btnCancel, 1, 0);
            this.tlpButtons.Controls.Add(this.btnOk, 0, 0);
            this.tlpButtons.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tlpButtons.Enabled = false;
            this.tlpButtons.Location = new System.Drawing.Point(0, 171);
            this.tlpButtons.Name = "tlpButtons";
            this.tlpButtons.RowCount = 1;
            this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpButtons.Size = new System.Drawing.Size(166, 43);
            this.tlpButtons.TabIndex = 5;
            // 
            // btnCancel
            // 
            this.btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCancel.Image = global::TonClock.Properties.Resources.DeleteHS;
            this.btnCancel.Location = new System.Drawing.Point(86, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(77, 37);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOk
            // 
            this.btnOk.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnOk.Image = global::TonClock.Properties.Resources.Check_16x16;
            this.btnOk.Location = new System.Drawing.Point(3, 3);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(77, 37);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "ОК";
            this.btnOk.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnOk.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // frmMolodimetr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 464);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStripMain);
            this.Name = "frmMolodimetr";
            this.Text = "Молодиметр";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Shown += new System.EventHandler(this.frmMolodimetr_Shown);
            this.Resize += new System.EventHandler(this.frmMolodimetr_Resize);
            this.tlpMain.ResumeLayout(false);
            this.tlpBottom.ResumeLayout(false);
            this.tlpHistory.ResumeLayout(false);
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tlpRight.ResumeLayout(false);
            this.pnlRightBottom.ResumeLayout(false);
            this.tlpButtons.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpBottom;
        private System.Windows.Forms.Panel pnlLegend;
        private System.Windows.Forms.Panel pnlPressure;
        private System.Windows.Forms.Panel pnlSosud;
        private System.Windows.Forms.Panel pnlStress;
        private System.Windows.Forms.Panel pnlPulse;
        private System.Windows.Forms.Panel pnlBlood;
        private System.Windows.Forms.Panel pnlAge;
        private System.Windows.Forms.Panel pnlTopTitle;
        private System.Windows.Forms.TableLayoutPanel tlpHistory;
        private System.Windows.Forms.Button btnHistoryLast;
        private System.Windows.Forms.Button btnHistoryNext;
        private System.Windows.Forms.ComboBox cmbHistory;
        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.ToolStripMenuItem mnuExit;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TableLayoutPanel tlpRight;
        private System.Windows.Forms.Panel pnlPulseWave;
        private System.Windows.Forms.Panel pnlRightBottom;
        private System.Windows.Forms.Panel pnlRepers;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.ToolStripMenuItem mnuPulseWave;
    }
}