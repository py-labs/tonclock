﻿namespace TonClock
{
    partial class frmPatientAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblTitle = new System.Windows.Forms.Label();
            this.txtPatronymic = new System.Windows.Forms.TextBox();
            this.lblPatronymic = new System.Windows.Forms.Label();
            this.txtFirstname = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.txtLastname = new System.Windows.Forms.TextBox();
            this.lblSurname = new System.Windows.Forms.Label();
            this.lblAge = new System.Windows.Forms.Label();
            this.numTopPressure = new System.Windows.Forms.NumericUpDown();
            this.lblPressure = new System.Windows.Forms.Label();
            this.lblPressure2 = new System.Windows.Forms.Label();
            this.numLowerPressure = new System.Windows.Forms.NumericUpDown();
            this.lblHistory = new System.Windows.Forms.Label();
            this.txtHistory = new System.Windows.Forms.TextBox();
            this.lblComplaints = new System.Windows.Forms.Label();
            this.txtComplaints = new System.Windows.Forms.TextBox();
            this.lblDestination = new System.Windows.Forms.Label();
            this.txtDestination = new System.Windows.Forms.TextBox();
            this.picDoctor = new System.Windows.Forms.PictureBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.numAge = new System.Windows.Forms.NumericUpDown();
            this.lblZvezdaTitle = new System.Windows.Forms.Label();
            this.lblZvezda = new System.Windows.Forms.Label();
            this.lblZvezda1 = new System.Windows.Forms.Label();
            this.lblZvezda2 = new System.Windows.Forms.Label();
            this.lblZvezda3 = new System.Windows.Forms.Label();
            this.lblSex = new System.Windows.Forms.Label();
            this.rdbSexMale = new System.Windows.Forms.RadioButton();
            this.rdbSexFemale = new System.Windows.Forms.RadioButton();
            this.lblGrowth = new System.Windows.Forms.Label();
            this.numGrowth = new System.Windows.Forms.NumericUpDown();
            this.lblWeight = new System.Windows.Forms.Label();
            this.numWeight = new System.Windows.Forms.NumericUpDown();
            this.lblWeight2 = new System.Windows.Forms.Label();
            this.lblWrist = new System.Windows.Forms.Label();
            this.numWrist = new System.Windows.Forms.NumericUpDown();
            this.pcbCasp = new System.Windows.Forms.PictureBox();
            this.grbSizes = new System.Windows.Forms.GroupBox();
            this.lblL5 = new System.Windows.Forms.Label();
            this.lblL4 = new System.Windows.Forms.Label();
            this.lblL3 = new System.Windows.Forms.Label();
            this.lblL2 = new System.Windows.Forms.Label();
            this.lblL1 = new System.Windows.Forms.Label();
            this.numL5 = new System.Windows.Forms.NumericUpDown();
            this.numL4 = new System.Windows.Forms.NumericUpDown();
            this.numL3 = new System.Windows.Forms.NumericUpDown();
            this.numL2 = new System.Windows.Forms.NumericUpDown();
            this.numL1 = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numTopPressure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLowerPressure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDoctor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGrowth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWrist)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbCasp)).BeginInit();
            this.grbSizes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numL5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numL4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numL3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numL2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numL1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(585, 536);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(117, 30);
            this.btnClose.TabIndex = 16;
            this.btnClose.Text = "Отмена";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(456, 536);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(117, 30);
            this.btnOK.TabIndex = 15;
            this.btnOK.Text = "ОК";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(52, 23);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(35, 13);
            this.lblTitle.TabIndex = 7;
            this.lblTitle.Text = "label1";
            // 
            // txtPatronymic
            // 
            this.txtPatronymic.Location = new System.Drawing.Point(145, 104);
            this.txtPatronymic.Name = "txtPatronymic";
            this.txtPatronymic.Size = new System.Drawing.Size(212, 20);
            this.txtPatronymic.TabIndex = 3;
            this.txtPatronymic.TextChanged += new System.EventHandler(this.txtPatronymic_TextChanged);
            this.txtPatronymic.Validating += new System.ComponentModel.CancelEventHandler(this.txtPatronymic_Validating);
            // 
            // lblPatronymic
            // 
            this.lblPatronymic.AutoSize = true;
            this.lblPatronymic.Location = new System.Drawing.Point(12, 107);
            this.lblPatronymic.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblPatronymic.Name = "lblPatronymic";
            this.lblPatronymic.Size = new System.Drawing.Size(57, 13);
            this.lblPatronymic.TabIndex = 11;
            this.lblPatronymic.Text = "Отчество:";
            // 
            // txtFirstname
            // 
            this.txtFirstname.Location = new System.Drawing.Point(145, 78);
            this.txtFirstname.Name = "txtFirstname";
            this.txtFirstname.Size = new System.Drawing.Size(212, 20);
            this.txtFirstname.TabIndex = 2;
            this.txtFirstname.TextChanged += new System.EventHandler(this.txtFirstname_TextChanged);
            this.txtFirstname.Validating += new System.ComponentModel.CancelEventHandler(this.txtFirstname_Validating);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(12, 81);
            this.lblName.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(32, 13);
            this.lblName.TabIndex = 10;
            this.lblName.Text = "Имя:";
            // 
            // txtLastname
            // 
            this.txtLastname.Location = new System.Drawing.Point(145, 52);
            this.txtLastname.Name = "txtLastname";
            this.txtLastname.Size = new System.Drawing.Size(212, 20);
            this.txtLastname.TabIndex = 1;
            this.txtLastname.TextChanged += new System.EventHandler(this.txtLastname_TextChanged);
            this.txtLastname.Validating += new System.ComponentModel.CancelEventHandler(this.txtLastname_Validating);
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.Location = new System.Drawing.Point(12, 55);
            this.lblSurname.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(59, 13);
            this.lblSurname.TabIndex = 12;
            this.lblSurname.Text = "Фамилия:";
            // 
            // lblAge
            // 
            this.lblAge.AutoSize = true;
            this.lblAge.Location = new System.Drawing.Point(12, 155);
            this.lblAge.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblAge.Name = "lblAge";
            this.lblAge.Size = new System.Drawing.Size(118, 13);
            this.lblAge.TabIndex = 11;
            this.lblAge.Text = "Возраст (полных лет):";
            // 
            // numTopPressure
            // 
            this.numTopPressure.Location = new System.Drawing.Point(145, 205);
            this.numTopPressure.Maximum = new decimal(new int[] {
            230,
            0,
            0,
            0});
            this.numTopPressure.Name = "numTopPressure";
            this.numTopPressure.Size = new System.Drawing.Size(58, 20);
            this.numTopPressure.TabIndex = 9;
            this.numTopPressure.Value = new decimal(new int[] {
            120,
            0,
            0,
            0});
            // 
            // lblPressure
            // 
            this.lblPressure.AutoSize = true;
            this.lblPressure.Location = new System.Drawing.Point(12, 207);
            this.lblPressure.Name = "lblPressure";
            this.lblPressure.Size = new System.Drawing.Size(127, 13);
            this.lblPressure.TabIndex = 13;
            this.lblPressure.Text = "Давление с тонометра:";
            // 
            // lblPressure2
            // 
            this.lblPressure2.AutoSize = true;
            this.lblPressure2.Location = new System.Drawing.Point(209, 207);
            this.lblPressure2.Name = "lblPressure2";
            this.lblPressure2.Size = new System.Drawing.Size(12, 13);
            this.lblPressure2.TabIndex = 13;
            this.lblPressure2.Text = "/";
            // 
            // numLowerPressure
            // 
            this.numLowerPressure.Location = new System.Drawing.Point(227, 205);
            this.numLowerPressure.Maximum = new decimal(new int[] {
            230,
            0,
            0,
            0});
            this.numLowerPressure.Name = "numLowerPressure";
            this.numLowerPressure.Size = new System.Drawing.Size(58, 20);
            this.numLowerPressure.TabIndex = 10;
            this.numLowerPressure.Value = new decimal(new int[] {
            80,
            0,
            0,
            0});
            // 
            // lblHistory
            // 
            this.lblHistory.AutoSize = true;
            this.lblHistory.Location = new System.Drawing.Point(9, 355);
            this.lblHistory.Name = "lblHistory";
            this.lblHistory.Size = new System.Drawing.Size(98, 13);
            this.lblHistory.TabIndex = 13;
            this.lblHistory.Text = "История болезни:";
            // 
            // txtHistory
            // 
            this.txtHistory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHistory.Location = new System.Drawing.Point(118, 352);
            this.txtHistory.Multiline = true;
            this.txtHistory.Name = "txtHistory";
            this.txtHistory.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtHistory.Size = new System.Drawing.Size(584, 51);
            this.txtHistory.TabIndex = 12;
            // 
            // lblComplaints
            // 
            this.lblComplaints.AutoSize = true;
            this.lblComplaints.Location = new System.Drawing.Point(9, 412);
            this.lblComplaints.Name = "lblComplaints";
            this.lblComplaints.Size = new System.Drawing.Size(103, 13);
            this.lblComplaints.TabIndex = 13;
            this.lblComplaints.Text = "Жалобы пациента:";
            // 
            // txtComplaints
            // 
            this.txtComplaints.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComplaints.Location = new System.Drawing.Point(118, 409);
            this.txtComplaints.Multiline = true;
            this.txtComplaints.Name = "txtComplaints";
            this.txtComplaints.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtComplaints.Size = new System.Drawing.Size(584, 51);
            this.txtComplaints.TabIndex = 13;
            // 
            // lblDestination
            // 
            this.lblDestination.AutoSize = true;
            this.lblDestination.Location = new System.Drawing.Point(9, 469);
            this.lblDestination.Name = "lblDestination";
            this.lblDestination.Size = new System.Drawing.Size(71, 13);
            this.lblDestination.TabIndex = 13;
            this.lblDestination.Text = "Назначения:";
            // 
            // txtDestination
            // 
            this.txtDestination.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDestination.Location = new System.Drawing.Point(118, 466);
            this.txtDestination.Multiline = true;
            this.txtDestination.Name = "txtDestination";
            this.txtDestination.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtDestination.Size = new System.Drawing.Size(584, 51);
            this.txtDestination.TabIndex = 14;
            // 
            // picDoctor
            // 
            this.picDoctor.Location = new System.Drawing.Point(12, 12);
            this.picDoctor.Name = "picDoctor";
            this.picDoctor.Size = new System.Drawing.Size(24, 24);
            this.picDoctor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picDoctor.TabIndex = 6;
            this.picDoctor.TabStop = false;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // numAge
            // 
            this.errorProvider1.SetIconAlignment(this.numAge, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.numAge.Location = new System.Drawing.Point(145, 153);
            this.numAge.Maximum = new decimal(new int[] {
            152,
            0,
            0,
            0});
            this.numAge.Name = "numAge";
            this.numAge.Size = new System.Drawing.Size(58, 20);
            this.numAge.TabIndex = 6;
            this.numAge.Value = new decimal(new int[] {
            40,
            0,
            0,
            0});
            // 
            // lblZvezdaTitle
            // 
            this.lblZvezdaTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblZvezdaTitle.AutoSize = true;
            this.lblZvezdaTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblZvezdaTitle.Location = new System.Drawing.Point(12, 536);
            this.lblZvezdaTitle.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblZvezdaTitle.Name = "lblZvezdaTitle";
            this.lblZvezdaTitle.Size = new System.Drawing.Size(325, 13);
            this.lblZvezdaTitle.TabIndex = 13;
            this.lblZvezdaTitle.Text = "Поля, обязательные для заполнения, обозначены звёздочкой";
            // 
            // lblZvezda
            // 
            this.lblZvezda.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblZvezda.AutoSize = true;
            this.lblZvezda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblZvezda.ForeColor = System.Drawing.Color.Red;
            this.lblZvezda.Location = new System.Drawing.Point(337, 536);
            this.lblZvezda.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.lblZvezda.Name = "lblZvezda";
            this.lblZvezda.Size = new System.Drawing.Size(12, 13);
            this.lblZvezda.TabIndex = 13;
            this.lblZvezda.Text = "*";
            // 
            // lblZvezda1
            // 
            this.lblZvezda1.AutoSize = true;
            this.lblZvezda1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblZvezda1.ForeColor = System.Drawing.Color.Red;
            this.lblZvezda1.Location = new System.Drawing.Point(71, 55);
            this.lblZvezda1.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.lblZvezda1.Name = "lblZvezda1";
            this.lblZvezda1.Size = new System.Drawing.Size(12, 13);
            this.lblZvezda1.TabIndex = 13;
            this.lblZvezda1.Text = "*";
            // 
            // lblZvezda2
            // 
            this.lblZvezda2.AutoSize = true;
            this.lblZvezda2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblZvezda2.ForeColor = System.Drawing.Color.Red;
            this.lblZvezda2.Location = new System.Drawing.Point(44, 81);
            this.lblZvezda2.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.lblZvezda2.Name = "lblZvezda2";
            this.lblZvezda2.Size = new System.Drawing.Size(12, 13);
            this.lblZvezda2.TabIndex = 13;
            this.lblZvezda2.Text = "*";
            // 
            // lblZvezda3
            // 
            this.lblZvezda3.AutoSize = true;
            this.lblZvezda3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblZvezda3.ForeColor = System.Drawing.Color.Red;
            this.lblZvezda3.Location = new System.Drawing.Point(69, 107);
            this.lblZvezda3.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.lblZvezda3.Name = "lblZvezda3";
            this.lblZvezda3.Size = new System.Drawing.Size(12, 13);
            this.lblZvezda3.TabIndex = 13;
            this.lblZvezda3.Text = "*";
            // 
            // lblSex
            // 
            this.lblSex.AutoSize = true;
            this.lblSex.Location = new System.Drawing.Point(12, 132);
            this.lblSex.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblSex.Name = "lblSex";
            this.lblSex.Size = new System.Drawing.Size(30, 13);
            this.lblSex.TabIndex = 11;
            this.lblSex.Text = "Пол:";
            // 
            // rdbSexMale
            // 
            this.rdbSexMale.AutoSize = true;
            this.rdbSexMale.Checked = true;
            this.rdbSexMale.Location = new System.Drawing.Point(145, 130);
            this.rdbSexMale.Name = "rdbSexMale";
            this.rdbSexMale.Size = new System.Drawing.Size(71, 17);
            this.rdbSexMale.TabIndex = 4;
            this.rdbSexMale.TabStop = true;
            this.rdbSexMale.Text = "Мужской";
            this.rdbSexMale.UseVisualStyleBackColor = true;
            // 
            // rdbSexFemale
            // 
            this.rdbSexFemale.AutoSize = true;
            this.rdbSexFemale.Location = new System.Drawing.Point(222, 130);
            this.rdbSexFemale.Name = "rdbSexFemale";
            this.rdbSexFemale.Size = new System.Drawing.Size(72, 17);
            this.rdbSexFemale.TabIndex = 5;
            this.rdbSexFemale.Text = "Женский";
            this.rdbSexFemale.UseVisualStyleBackColor = true;
            // 
            // lblGrowth
            // 
            this.lblGrowth.AutoSize = true;
            this.lblGrowth.Location = new System.Drawing.Point(6, 21);
            this.lblGrowth.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblGrowth.Name = "lblGrowth";
            this.lblGrowth.Size = new System.Drawing.Size(49, 13);
            this.lblGrowth.TabIndex = 11;
            this.lblGrowth.Text = "Рост (L):";
            // 
            // numGrowth
            // 
            this.numGrowth.DecimalPlaces = 1;
            this.numGrowth.Location = new System.Drawing.Point(99, 19);
            this.numGrowth.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numGrowth.Name = "numGrowth";
            this.numGrowth.Size = new System.Drawing.Size(58, 20);
            this.numGrowth.TabIndex = 7;
            this.numGrowth.Value = new decimal(new int[] {
            175,
            0,
            0,
            0});
            // 
            // lblWeight
            // 
            this.lblWeight.AutoSize = true;
            this.lblWeight.Location = new System.Drawing.Point(12, 181);
            this.lblWeight.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(29, 13);
            this.lblWeight.TabIndex = 11;
            this.lblWeight.Text = "Вес:";
            // 
            // numWeight
            // 
            this.numWeight.Location = new System.Drawing.Point(145, 179);
            this.numWeight.Maximum = new decimal(new int[] {
            590,
            0,
            0,
            0});
            this.numWeight.Name = "numWeight";
            this.numWeight.Size = new System.Drawing.Size(58, 20);
            this.numWeight.TabIndex = 8;
            this.numWeight.Value = new decimal(new int[] {
            70,
            0,
            0,
            0});
            // 
            // lblWeight2
            // 
            this.lblWeight2.AutoSize = true;
            this.lblWeight2.Location = new System.Drawing.Point(209, 181);
            this.lblWeight2.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblWeight2.Name = "lblWeight2";
            this.lblWeight2.Size = new System.Drawing.Size(18, 13);
            this.lblWeight2.TabIndex = 11;
            this.lblWeight2.Text = "кг";
            // 
            // lblWrist
            // 
            this.lblWrist.AutoSize = true;
            this.lblWrist.Location = new System.Drawing.Point(6, 47);
            this.lblWrist.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblWrist.Name = "lblWrist";
            this.lblWrist.Size = new System.Drawing.Size(90, 13);
            this.lblWrist.TabIndex = 11;
            this.lblWrist.Text = "Охват запястья:";
            // 
            // numWrist
            // 
            this.numWrist.DecimalPlaces = 1;
            this.numWrist.Location = new System.Drawing.Point(99, 45);
            this.numWrist.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numWrist.Name = "numWrist";
            this.numWrist.Size = new System.Drawing.Size(58, 20);
            this.numWrist.TabIndex = 11;
            this.numWrist.Value = new decimal(new int[] {
            16,
            0,
            0,
            0});
            // 
            // pcbCasp
            // 
            this.pcbCasp.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pcbCasp.Image = global::TonClock.Properties.Resources.Casp;
            this.pcbCasp.Location = new System.Drawing.Point(402, 12);
            this.pcbCasp.Name = "pcbCasp";
            this.pcbCasp.Size = new System.Drawing.Size(300, 334);
            this.pcbCasp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbCasp.TabIndex = 17;
            this.pcbCasp.TabStop = false;
            // 
            // grbSizes
            // 
            this.grbSizes.Controls.Add(this.lblL5);
            this.grbSizes.Controls.Add(this.lblL4);
            this.grbSizes.Controls.Add(this.lblL3);
            this.grbSizes.Controls.Add(this.lblL2);
            this.grbSizes.Controls.Add(this.lblL1);
            this.grbSizes.Controls.Add(this.numL5);
            this.grbSizes.Controls.Add(this.numL4);
            this.grbSizes.Controls.Add(this.numL3);
            this.grbSizes.Controls.Add(this.numL2);
            this.grbSizes.Controls.Add(this.numL1);
            this.grbSizes.Controls.Add(this.lblGrowth);
            this.grbSizes.Controls.Add(this.numGrowth);
            this.grbSizes.Controls.Add(this.lblWrist);
            this.grbSizes.Controls.Add(this.numWrist);
            this.grbSizes.Location = new System.Drawing.Point(12, 231);
            this.grbSizes.Name = "grbSizes";
            this.grbSizes.Size = new System.Drawing.Size(384, 115);
            this.grbSizes.TabIndex = 18;
            this.grbSizes.TabStop = false;
            this.grbSizes.Text = "Замеры пациента (см)";
            // 
            // lblL5
            // 
            this.lblL5.AutoSize = true;
            this.lblL5.Location = new System.Drawing.Point(280, 49);
            this.lblL5.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblL5.Name = "lblL5";
            this.lblL5.Size = new System.Drawing.Size(22, 13);
            this.lblL5.TabIndex = 11;
            this.lblL5.Text = "L5:";
            // 
            // lblL4
            // 
            this.lblL4.AutoSize = true;
            this.lblL4.Location = new System.Drawing.Point(280, 23);
            this.lblL4.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblL4.Name = "lblL4";
            this.lblL4.Size = new System.Drawing.Size(22, 13);
            this.lblL4.TabIndex = 11;
            this.lblL4.Text = "L4:";
            // 
            // lblL3
            // 
            this.lblL3.AutoSize = true;
            this.lblL3.Location = new System.Drawing.Point(179, 73);
            this.lblL3.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblL3.Name = "lblL3";
            this.lblL3.Size = new System.Drawing.Size(22, 13);
            this.lblL3.TabIndex = 11;
            this.lblL3.Text = "L3:";
            // 
            // lblL2
            // 
            this.lblL2.AutoSize = true;
            this.lblL2.Location = new System.Drawing.Point(179, 47);
            this.lblL2.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblL2.Name = "lblL2";
            this.lblL2.Size = new System.Drawing.Size(22, 13);
            this.lblL2.TabIndex = 11;
            this.lblL2.Text = "L2:";
            // 
            // lblL1
            // 
            this.lblL1.AutoSize = true;
            this.lblL1.Location = new System.Drawing.Point(179, 21);
            this.lblL1.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblL1.Name = "lblL1";
            this.lblL1.Size = new System.Drawing.Size(22, 13);
            this.lblL1.TabIndex = 11;
            this.lblL1.Text = "L1:";
            // 
            // numL5
            // 
            this.numL5.DecimalPlaces = 1;
            this.numL5.Location = new System.Drawing.Point(305, 47);
            this.numL5.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numL5.Name = "numL5";
            this.numL5.Size = new System.Drawing.Size(58, 20);
            this.numL5.TabIndex = 7;
            // 
            // numL4
            // 
            this.numL4.DecimalPlaces = 1;
            this.numL4.Location = new System.Drawing.Point(305, 21);
            this.numL4.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numL4.Name = "numL4";
            this.numL4.Size = new System.Drawing.Size(58, 20);
            this.numL4.TabIndex = 7;
            // 
            // numL3
            // 
            this.numL3.DecimalPlaces = 1;
            this.numL3.Location = new System.Drawing.Point(204, 71);
            this.numL3.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numL3.Name = "numL3";
            this.numL3.Size = new System.Drawing.Size(58, 20);
            this.numL3.TabIndex = 7;
            // 
            // numL2
            // 
            this.numL2.DecimalPlaces = 1;
            this.numL2.Location = new System.Drawing.Point(204, 45);
            this.numL2.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numL2.Name = "numL2";
            this.numL2.Size = new System.Drawing.Size(58, 20);
            this.numL2.TabIndex = 7;
            // 
            // numL1
            // 
            this.numL1.DecimalPlaces = 1;
            this.numL1.Location = new System.Drawing.Point(204, 19);
            this.numL1.Maximum = new decimal(new int[] {
            300,
            0,
            0,
            0});
            this.numL1.Name = "numL1";
            this.numL1.Size = new System.Drawing.Size(58, 20);
            this.numL1.TabIndex = 7;
            // 
            // frmPatientAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(726, 578);
            this.Controls.Add(this.grbSizes);
            this.Controls.Add(this.pcbCasp);
            this.Controls.Add(this.rdbSexFemale);
            this.Controls.Add(this.rdbSexMale);
            this.Controls.Add(this.txtDestination);
            this.Controls.Add(this.txtComplaints);
            this.Controls.Add(this.txtHistory);
            this.Controls.Add(this.numLowerPressure);
            this.Controls.Add(this.numWeight);
            this.Controls.Add(this.numAge);
            this.Controls.Add(this.numTopPressure);
            this.Controls.Add(this.lblZvezda3);
            this.Controls.Add(this.lblZvezda2);
            this.Controls.Add(this.lblZvezda1);
            this.Controls.Add(this.lblZvezda);
            this.Controls.Add(this.lblZvezdaTitle);
            this.Controls.Add(this.lblDestination);
            this.Controls.Add(this.lblComplaints);
            this.Controls.Add(this.lblPressure2);
            this.Controls.Add(this.lblHistory);
            this.Controls.Add(this.lblPressure);
            this.Controls.Add(this.lblWeight2);
            this.Controls.Add(this.lblWeight);
            this.Controls.Add(this.lblSex);
            this.Controls.Add(this.lblAge);
            this.Controls.Add(this.txtPatronymic);
            this.Controls.Add(this.lblPatronymic);
            this.Controls.Add(this.txtFirstname);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.txtLastname);
            this.Controls.Add(this.lblSurname);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.picDoctor);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPatientAdd";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Добавление пациента";
            this.Shown += new System.EventHandler(this.frmPatientAdd_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.numTopPressure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numLowerPressure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picDoctor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numAge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numGrowth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numWrist)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcbCasp)).EndInit();
            this.grbSizes.ResumeLayout(false);
            this.grbSizes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numL5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numL4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numL3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numL2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numL1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.PictureBox picDoctor;
        private System.Windows.Forms.TextBox txtPatronymic;
        private System.Windows.Forms.Label lblPatronymic;
        private System.Windows.Forms.TextBox txtFirstname;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtLastname;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.Label lblAge;
        private System.Windows.Forms.NumericUpDown numTopPressure;
        private System.Windows.Forms.Label lblPressure;
        private System.Windows.Forms.Label lblPressure2;
        private System.Windows.Forms.NumericUpDown numLowerPressure;
        private System.Windows.Forms.Label lblHistory;
        private System.Windows.Forms.TextBox txtHistory;
        private System.Windows.Forms.Label lblComplaints;
        private System.Windows.Forms.TextBox txtComplaints;
        private System.Windows.Forms.Label lblDestination;
        private System.Windows.Forms.TextBox txtDestination;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.NumericUpDown numAge;
        private System.Windows.Forms.Label lblZvezdaTitle;
        private System.Windows.Forms.Label lblZvezda;
        private System.Windows.Forms.Label lblZvezda3;
        private System.Windows.Forms.Label lblZvezda2;
        private System.Windows.Forms.Label lblZvezda1;
        private System.Windows.Forms.RadioButton rdbSexFemale;
        private System.Windows.Forms.RadioButton rdbSexMale;
        private System.Windows.Forms.NumericUpDown numWeight;
        private System.Windows.Forms.NumericUpDown numGrowth;
        private System.Windows.Forms.Label lblWeight2;
        private System.Windows.Forms.Label lblWeight;
        private System.Windows.Forms.Label lblSex;
        private System.Windows.Forms.Label lblGrowth;
        private System.Windows.Forms.NumericUpDown numWrist;
        private System.Windows.Forms.Label lblWrist;
        private System.Windows.Forms.PictureBox pcbCasp;
        private System.Windows.Forms.GroupBox grbSizes;
        private System.Windows.Forms.Label lblL5;
        private System.Windows.Forms.Label lblL4;
        private System.Windows.Forms.Label lblL3;
        private System.Windows.Forms.Label lblL2;
        private System.Windows.Forms.Label lblL1;
        private System.Windows.Forms.NumericUpDown numL5;
        private System.Windows.Forms.NumericUpDown numL4;
        private System.Windows.Forms.NumericUpDown numL3;
        private System.Windows.Forms.NumericUpDown numL2;
        private System.Windows.Forms.NumericUpDown numL1;
    }
}