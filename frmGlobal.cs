﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TonClock
{
    public partial class frmGlobal : Form
    {
        //*****************************************************************
        //                         Базовые поля                           *
        //*****************************************************************

        private GlobalSettings _globalSettings = new GlobalSettings();
        private int _idPatient, _idMeasurement, _idNumber;
        private GlobalCalc _calc;
        private bool _sex;
        private int _age, _weight;
        private float _growth;
        private double _scale;


        //*****************************************************************
        //                     Конструкторы и загрузка                    *
        //*****************************************************************

        public frmGlobal(int id, int idM, ushort[] data, int n, double scale = 1)
        {
            InitializeComponent();
            this.Icon = Icon.FromHandle(Properties.Resources.CalculatorHS.GetHicon());
            ResultChanged = false;
            _idPatient = id;
            _idMeasurement = idM;
            _calc = new GlobalCalc(data);
            _idNumber = n;
            _scale = scale;
        }

        private void frmGlobal_Shown(object sender, EventArgs e)
        {
            try
            {
                using (var myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    OleDbCommand myOleDbCommand = new OleDbCommand(
                        "SELECT lastname, firstname, patronymic, sex, age, growth, weight " +
                        "FROM Patients " +
                        "WHERE id = " + _idPatient,
                        myOleDbConnection);
                    var myReader = myOleDbCommand.ExecuteReader();
                    if (myReader != null)
                    {
                        if (myReader.Read())
                        {
                            this.Text += " - " + myReader[0].ToString() + " " +
                                        myReader[1].ToString()[0] + "." + myReader[2].ToString()[0] + "." +
                                        " - замер " + _idNumber;
                            _sex = myReader.GetBoolean(3);
                            _age = myReader.GetInt16(4);
                            _growth = myReader.IsDBNull(5) ? 0 : myReader.GetFloat(5);
                            _weight = myReader.GetInt16(6);
                            myReader.Close();
                        }
                        else
                        {
                            myReader.Close();
                            throw new Exception("В базе не обнаружен пациент с таким идентификатором.");
                        }
                    }
                    else
                        throw new Exception("Не удалось прочитать информацию о пациенте.");
                    myOleDbCommand.CommandText =
                        "SELECT globalA1, globalA2, globalA3, globalA4, globalA5, " +
                        "       globalTop, globalLower " +
                        "FROM Measurements " +
                        "WHERE id = " + _idMeasurement;
                    myReader = myOleDbCommand.ExecuteReader();
                    if (myReader != null)
                    {
                        if (myReader.Read())
                        {
                            try
                            {
                                var repers = new int[]
                                             {
                                                 myReader.GetInt32(0), myReader.GetInt32(1), myReader.GetInt32(2),
                                                 myReader.GetInt32(3), myReader.GetInt32(4)
                                             };
                                _calc.Repers = repers;
                                lblResult.Text = myReader.GetInt16(5) + " / " + myReader.GetInt16(6);
                            }
                            catch (InvalidCastException)
                            {
                                _calc.AutoRepers();
                                lblResult.Text = String.Empty;
                            }
                        }
                        myReader.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Внимание! При попытке открытия базы данных произошла ошибка:\n" +
                    exc.Message + "\n\n" +
                    "Дальнейшая работа модуля не возможна.\n\n" +
                    "Пожалуйста, обратитесь к администратору.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.DialogResult = DialogResult.Abort;
            }
            if (_calc.Repers == null)
                _calc.AutoRepers();
            lblSex.Text = _sex ? "мужской" : "женский";
            lblAge.Text = DbPulseOperations.PrintAge(_age);
            lblGrowth.Text = _growth.ToString(CultureInfo.InvariantCulture) + " см";
            lblWeight.Text = _weight.ToString() + " кг";
            rdbScaleWidth.Checked = _globalSettings.FlagScaleWidth;
            rdbScaleBuffer.Checked = !_globalSettings.FlagScaleWidth;
            var cultInfo = CultureInfo.CreateSpecificCulture("ru-RU");
            txtJ.Text = _globalSettings.J.ToString("0.####", cultInfo);
            txtN.Text = _globalSettings.N.ToString("0.####", cultInfo);
            txtH.Text = _globalSettings.H.ToString("0.####", cultInfo);
            txtI.Text = _globalSettings.I.ToString("0.####", cultInfo);
            txtR.Text = _globalSettings.R.ToString("0.####", cultInfo);
        }

        private void frmGlobal_Resize(object sender, EventArgs e)
        {
            this.Refresh();
        }

        private void frmGlobal_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Сохранение параметров
            _globalSettings.Save();
        }

        private void frmGlobal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.DialogResult = DialogResult.OK;
            }
        }


        //*****************************************************************
        //                         Проверка ввода                         *
        //*****************************************************************

        private void txtJ_Validating(object sender, CancelEventArgs e)
        {
            float temp;
            if (!float.TryParse(txtJ.Text, out temp))
            {
                errorProvider1.SetError(txtJ, "Число введено не верно.");
                e.Cancel = true;
            }
            else
            {
                _globalSettings.J = temp;
                errorProvider1.SetError(txtJ, String.Empty);
            }
        }

        private void txtN_Validating(object sender, CancelEventArgs e)
        {
            float temp;
            if (!float.TryParse(txtN.Text, out temp))
            {
                errorProvider1.SetError(txtN, "Число введено не верно.");
                e.Cancel = true;
            }
            else
            {
                _globalSettings.N = temp;
                errorProvider1.SetError(txtN, String.Empty);
            }
        }

        private void txtH_Validating(object sender, CancelEventArgs e)
        {
            float temp;
            if (!float.TryParse(txtH.Text, out temp))
            {
                errorProvider1.SetError(txtH, "Число введено не верно.");
                e.Cancel = true;
            }
            else
            {
                _globalSettings.H = temp;
                errorProvider1.SetError(txtH, String.Empty);
            }
        }

        private void txtI_Validating(object sender, CancelEventArgs e)
        {
            float temp;
            if (!float.TryParse(txtI.Text, out temp))
            {
                errorProvider1.SetError(txtI, "Число введено не верно.");
                e.Cancel = true;
            }
            else
            {
                _globalSettings.I = temp;
                errorProvider1.SetError(txtI, String.Empty);
            }
        }

        private void txtR_Validating(object sender, CancelEventArgs e)
        {
            float temp;
            if (!float.TryParse(txtR.Text, out temp))
            {
                errorProvider1.SetError(txtR, "Число введено не верно.");
                e.Cancel = true;
            }
            else
            {
                _globalSettings.R = temp;
                errorProvider1.SetError(txtR, String.Empty);
            }
        }


        //*****************************************************************
        //                 Прорисовка пульсовой волны                     *
        //*****************************************************************

        private void pnlPulse_Paint(object sender, PaintEventArgs e)
        {
            if (_calc != null)
            {
                Rectangle rctPulse = new Rectangle(pnlPulse.HorizontalScroll.Value,
                    pnlPulse.VerticalScroll.Value,
                    pnlPulse.ClientSize.Width, pnlPulse.ClientSize.Height);
                int scroll;
                _calc.DrawPulseWave(e.Graphics, rctPulse, out scroll, _globalSettings.FlagScaleWidth, _scale);
                pnlPulse.AutoScrollMinSize = new Size(scroll, 0);
            }
        }

        private void pnlPulse_Scroll(object sender, ScrollEventArgs e)
        {
            pnlPulse.Refresh();
        }

        private void rdbScaleWidth_CheckedChanged(object sender, EventArgs e)
        {
            _globalSettings.FlagScaleWidth = rdbScaleWidth.Checked;
            pnlPulse.AutoScrollPosition = new Point(0, 0);
            pnlPulse.Refresh();
        }


        //*****************************************************************
        //                       Расстановка реперов                      *
        //*****************************************************************

        private void pnlPulse_MouseMove(object sender, MouseEventArgs e)
        {
            Rectangle rctPulse = new Rectangle(pnlPulse.HorizontalScroll.Value,
                                                    pnlPulse.VerticalScroll.Value,
                                                    pnlPulse.ClientSize.Width,
                                                    pnlPulse.ClientSize.Height);
            if (_calc.MoveReperMode)
            {
                if (_calc.ReperMove(e.Location, rctPulse))
                    pnlPulse.Refresh();
            }
            else
            {
                bool flagRefresh;
                pnlPulse.Cursor = _calc.MouseMove(e.Location, rctPulse,
                                                  out flagRefresh)
                                      ? Cursors.Hand
                                      : Cursors.Default;
                if (flagRefresh)
                    pnlPulse.Refresh();
            }
        }

        private void pnlPulse_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && _calc.MouseDown(e.Location))
                pnlPulse.Refresh();
        }

        private void pnlPulse_MouseUp(object sender, MouseEventArgs e)
        {
            if (_calc.MouseUp())
                pnlPulse.Refresh();
        }


        //*****************************************************************
        //                            Расчеты                             *
        //*****************************************************************

        public int ResultTopPressure { get; private set; }
        public int ResultLowerPressure { get; private set; }
        public bool ResultChanged { get; private set; }
        public int[] ResultRepers { get { return _calc.Repers; } }

        private void btnCalc_Click(object sender, EventArgs e)
        {
            _calc.MakeCalc(_sex, _age, _growth, _weight, _globalSettings.J, _globalSettings.N, _globalSettings.H,
                _globalSettings.I, _globalSettings.R);
            lblP5P2.Text = _calc.ResultStrP5P2;
            lblKp.Text = _calc.ResultStrKp;
            lblKn.Text = _calc.ResultStrKn;
            ResultTopPressure = _calc.ResultTopPressure;
            ResultLowerPressure = _calc.ResultLowerPressure;
            lblResult.Text = ResultTopPressure + " / " + ResultLowerPressure;
            string strError;
            if (!DbPulseOperations.UpdateRepers(_calc.Repers, _idMeasurement, _idPatient, out strError))
            {
                MessageBox.Show(
                    String.Format("При попытке обновления данных после перестановки реперов произошла ошибка:{0}" +
                                  strError + "{0}{0}" +
                                  "Пожалуйста, обратитесь к администратору.", Environment.NewLine),
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            ResultChanged = true;
        }

        private void mnuAutoRepers_Click(object sender, EventArgs e)
        {
            _calc.AutoRepers();
            pnlPulse.Refresh();
        }



    }
}
