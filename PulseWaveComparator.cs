﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;

namespace TonClock
{
    class PulseWaveComparator
    {

        private const int NumberWidth = 35; // ширина и высота квадратика с номером

        public PulseWaveComparator()
        {
            lstWaves = new ArrayList();
            maxValue = 0;
        }

        private struct WaveOnComparatorItem
        {
            public ushort[] pulseWave;  // средняя волна
            private int id;             // номер в базе (в таблице)
            private PointF idPoint;     // координаты номера (X - в единичном масштабе, Y - в долях от всей высоты)
            private Color pulseColor;   // цвет
            public WaveOnComparatorItem(ushort[] data, int id, ArrayList lst)
            {
                this.pulseWave = new ushort[data.Length];
                for (int i = 0; i < data.Length; i++)
                    this.pulseWave[i] = data[i];
                this.id = id;
                //this.idPoint = new PointF(10 + lst.Count * NumberWidth, 1f / 6f);
                this.idPoint = new PointF(10, 1f / 6f);

                ArrayList lstColors = new ArrayList();
                lstColors.Add(Color.MediumBlue);
                lstColors.Add(Color.Green);
                lstColors.Add(Color.Goldenrod);
                bool flagReplaced;
                do
                {
                    flagReplaced = false;
                    foreach (var lstWave in lst)
                    {
                        var item = (WaveOnComparatorItem)lstWave;
                        lstColors.Remove(item.GetColor());
                        if (Math.Abs(this.idPoint.X - item.Point.X) < NumberWidth)
                        {
                            this.idPoint.X += NumberWidth;
                            flagReplaced = true;
                        }
                    }
                } while (flagReplaced);
                if (lstColors.Count > 0)
                    this.pulseColor = (Color)lstColors[0];
                else
                {
                    var rnd = new Random();
                    this.pulseColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
                }
            }
            public int GetId()
            {
                return this.id;
            }
            public PointF Point
            {
                get { return idPoint; }
                set { idPoint = value; }
            }
            public Color GetColor()
            {
                return this.pulseColor;
            }
        };

        private ArrayList lstWaves;

        private ushort maxValue;

        public int GetCount()
        {
            return lstWaves.Count;
        }

        public Color AddWave(ushort[] data, int id)
        {
            WaveOnComparatorItem newStruct = new WaveOnComparatorItem(data, id, lstWaves);
            lstWaves.Add(newStruct);
            for (int i = 0; i < data.Length; i++)
                if (data[i] > maxValue)
                    maxValue = data[i];
            return newStruct.GetColor();
        }

        public void DeleteWave(int id)
        {
            foreach (var lstWave in lstWaves)
            {
                WaveOnComparatorItem item = (WaveOnComparatorItem) lstWave;
                if (item.GetId() == id)
                {
                    lstWaves.Remove(lstWave);
                    break;
                }
            }
            if (lstWaves.Count <= 0)
                maxValue = 0;
        }

        public void ClearList()
        {
            lstWaves.Clear();
            maxValue = 0;
        }

        public void DrawPulseWaves(Graphics gr, Rectangle rct, Point p, out int scroll, int id,
            double scale = 1, bool points = false, bool print = false, bool oneHeight = false)
        {
            gr.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            gr.SmoothingMode = SmoothingMode.AntiAlias;
            scroll = 0;
            gr.FillRectangle(new SolidBrush(Color.White), new Rectangle(p, new Size(rct.Width, rct.Height)));
            int maxNumbersScroll = 0;
            foreach (var lstWave in lstWaves)
            {
                WaveOnComparatorItem item = (WaveOnComparatorItem) lstWave;
                Point[] pulsePoints = new Point[(int)Math.Min(Math.Round(rct.Width / scale),
                    Math.Max(Math.Round(item.pulseWave.Length - rct.Left / scale), 0))];
                Pen penPulse = new Pen(item.GetColor(), 1);
                Font fontWave = new Font("Microsoft Sans Serif", 14);
                if (id == item.GetId())
                {
                    penPulse.Width = 2;
                    fontWave = new Font(fontWave, FontStyle.Bold);
                }
                if (oneHeight)
                {
                    Rectangle rctNumber = new Rectangle((int) (p.X + item.Point.X*scale - rct.Left),
                                                        (int) (p.Y + rct.Height*item.Point.Y - NumberWidth/2f),
                                                        NumberWidth, NumberWidth);
                    string strNumber = item.GetId().ToString();
                    gr.DrawString(strNumber, fontWave, new SolidBrush(penPulse.Color),
                                  new PointF(
                                      rctNumber.X + rctNumber.Width/2 - gr.MeasureString(strNumber, fontWave).Width/2,
                                      rctNumber.Y + rctNumber.Height/2 - gr.MeasureString(strNumber, fontWave).Height/2));
                    if (rct.Left + rctNumber.Right > maxNumbersScroll)
                        maxNumbersScroll = rct.Left + rctNumber.Right;
                    if (_idMouseMove == item.GetId())
                    {
                        Pen penNumber = new Pen(penPulse.Color);
                        if (MoveNumberMode)
                            penNumber.Width = 2;
                        gr.DrawRectangle(penNumber, rctNumber);
                    }
                }
                uint h, H = (uint)(oneHeight ? rct.Height - rct.Height/3 - 20 : rct.Height - 20);
                uint min = item.pulseWave.Min(),
                    max = (oneHeight ? item.pulseWave.Max() : maxValue);
                int iPoint = 0;
                double curX = 0;
                while ((curX < rct.Width) && (iPoint + (int)(rct.Left / scale) < item.pulseWave.Length) &&
                    (iPoint >= 0) && (iPoint < pulsePoints.Length))
                {
                    int iIndex = iPoint + (int)(Math.Round(rct.Left / scale));
                    h = (uint)(item.pulseWave[iIndex] - min);
                    Point curPoint = new Point(p.X + (int)curX, p.Y + (int)(rct.Height - 10 - h * H / (max - min)));
                    pulsePoints[iPoint] = curPoint;
                    // значения
                    if (print && id == item.GetId())
                    {
                        Font fontTextData = new Font("Microsoft Sans Serif", 6);
                        gr.DrawString(item.pulseWave[iIndex].ToString(), fontTextData, new SolidBrush(penPulse.Color),
                            new Point(curPoint.X, curPoint.Y + 2));
                    }
                    curX += scale;
                    iPoint++;
                }
                // точки
                if (points)
                {
                    foreach (var pulsePoint in pulsePoints)
                    {
                        if (id == item.GetId())
                            gr.FillEllipse(new SolidBrush(penPulse.Color), pulsePoint.X - 3, pulsePoint.Y - 3, 6, 6);
                        else
                            gr.FillEllipse(new SolidBrush(penPulse.Color), pulsePoint.X - 2, pulsePoint.Y - 2, 4, 4);
                    }
                }
                if (pulsePoints.Length > 1)
                    gr.DrawCurve(penPulse, pulsePoints, 1.0F);
                if (scroll  < (int)(Math.Ceiling(item.pulseWave.Length * scale)))
                    scroll = (int)(Math.Ceiling(item.pulseWave.Length * scale));
            }
            scroll = Math.Max(scroll, maxNumbersScroll);
        }


        //*****************************************************************
        //           Перемещение цифр на реконструкции волн               *
        //*****************************************************************

        private int _idMouseMove = -1;

        public bool MouseMove(Point loc, Rectangle rct, out bool refresh, double scale = 1)
        {
            //_idMouseMove = -1;
            refresh = false;
            foreach (var lstWave in lstWaves)
            {
                if (PointInNumber((WaveOnComparatorItem)lstWave, loc, rct, scale))
                {
                    int _idMouseMoveNew = ((WaveOnComparatorItem) lstWave).GetId();
                    if (_idMouseMove != _idMouseMoveNew)
                    {
                        refresh = true;
                        _idMouseMove = _idMouseMoveNew;
                    }
                    return true;
                }
            }
            if (_idMouseMove != -1)
                refresh = true;
            _idMouseMove = -1;
            return false;
        }

        // проверка, что курсор над цифрой
        private bool PointInNumber(WaveOnComparatorItem item, Point loc, Rectangle rct, double scale = 1)
        {
            var curPoint = new Point((int) (item.Point.X*scale - rct.Left),
                                     (int) (rct.Height*item.Point.Y - NumberWidth/2f));
            if (loc.X >= curPoint.X && loc.X <= curPoint.X + NumberWidth &&
                loc.Y >= curPoint.Y && loc.Y <= curPoint.Y + NumberWidth)
                return true;
            return false;
        }

        public bool MoveNumberMode = false;
        private Point _mousePressedLocation;

        public bool MouseDown(Point loc, Rectangle rct, double scale = 1)
        {
            foreach (var lstWave in lstWaves)
            {
                var item = (WaveOnComparatorItem) lstWave;
                if (item.GetId() == _idMouseMove)
                {
                    var curPoint = new Point((int) (item.Point.X*scale - rct.Left),
                                             (int) (rct.Height*item.Point.Y - NumberWidth/2f));
                    _mousePressedLocation = new Point(loc.X - curPoint.X, loc.Y - curPoint.Y);
                    MoveNumberMode = true;
                    return true;
                }
            }
            return false;
        }

        public bool MouseUp()
        {
            if (MoveNumberMode)
            {
                MoveNumberMode = false;
                return true;
            }
            return false;
        }

        public bool NumberMove(Point loc, Rectangle rct, double scale = 1)
        {
            for (int i = 0; i < lstWaves.Count; i++)
            {
                var item = (WaveOnComparatorItem)lstWaves[i];
                if (item.GetId() == _idMouseMove)
                {
                    PointF newPointReal = new PointF(loc.X - _mousePressedLocation.X,
                                                     loc.Y - _mousePressedLocation.Y + NumberWidth/2f);
                    item.Point = new PointF((rct.Left + newPointReal.X) / (float)scale, newPointReal.Y / (float)rct.Height);
                    lstWaves[i] = item;
                    return true;
                }
            }
            return false;
        }

    }
}
