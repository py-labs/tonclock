﻿namespace TonClock
{
    partial class frmGlobal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblRepers = new System.Windows.Forms.Label();
            this.pnlPulse = new System.Windows.Forms.Panel();
            this.grpData = new System.Windows.Forms.GroupBox();
            this.lblWeight = new System.Windows.Forms.Label();
            this.lblAge = new System.Windows.Forms.Label();
            this.lblWeightTitle = new System.Windows.Forms.Label();
            this.lblAgeTitle = new System.Windows.Forms.Label();
            this.lblGrowth = new System.Windows.Forms.Label();
            this.lblGrowthTitle = new System.Windows.Forms.Label();
            this.lblSex = new System.Windows.Forms.Label();
            this.lblSexTitle = new System.Windows.Forms.Label();
            this.btnCalc = new System.Windows.Forms.Button();
            this.grpResult1 = new System.Windows.Forms.GroupBox();
            this.lblKn = new System.Windows.Forms.Label();
            this.lblP5P2 = new System.Windows.Forms.Label();
            this.lblKp = new System.Windows.Forms.Label();
            this.grpResult2 = new System.Windows.Forms.GroupBox();
            this.lblResult = new System.Windows.Forms.Label();
            this.grpParams = new System.Windows.Forms.GroupBox();
            this.txtR = new System.Windows.Forms.TextBox();
            this.txtI = new System.Windows.Forms.TextBox();
            this.labelR = new System.Windows.Forms.Label();
            this.txtN = new System.Windows.Forms.TextBox();
            this.lblI = new System.Windows.Forms.Label();
            this.txtH = new System.Windows.Forms.TextBox();
            this.lblN = new System.Windows.Forms.Label();
            this.txtJ = new System.Windows.Forms.TextBox();
            this.lblH = new System.Windows.Forms.Label();
            this.lblJ = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.grpScale = new System.Windows.Forms.GroupBox();
            this.rdbScaleBuffer = new System.Windows.Forms.RadioButton();
            this.rdbScaleWidth = new System.Windows.Forms.RadioButton();
            this.cmsPulseMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuAutoRepers = new System.Windows.Forms.ToolStripMenuItem();
            this.grpData.SuspendLayout();
            this.grpResult1.SuspendLayout();
            this.grpResult2.SuspendLayout();
            this.grpParams.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.grpScale.SuspendLayout();
            this.cmsPulseMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblRepers
            // 
            this.lblRepers.AutoSize = true;
            this.lblRepers.Location = new System.Drawing.Point(12, 9);
            this.lblRepers.Name = "lblRepers";
            this.lblRepers.Size = new System.Drawing.Size(137, 15);
            this.lblRepers.TabIndex = 0;
            this.lblRepers.Text = "Расстановка реперов:";
            // 
            // pnlPulse
            // 
            this.pnlPulse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlPulse.AutoScroll = true;
            this.pnlPulse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPulse.ContextMenuStrip = this.cmsPulseMenu;
            this.pnlPulse.Location = new System.Drawing.Point(12, 27);
            this.pnlPulse.Name = "pnlPulse";
            this.pnlPulse.Size = new System.Drawing.Size(460, 161);
            this.pnlPulse.TabIndex = 3;
            this.pnlPulse.Scroll += new System.Windows.Forms.ScrollEventHandler(this.pnlPulse_Scroll);
            this.pnlPulse.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlPulse_Paint);
            this.pnlPulse.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlPulse_MouseDown);
            this.pnlPulse.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlPulse_MouseMove);
            this.pnlPulse.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pnlPulse_MouseUp);
            // 
            // grpData
            // 
            this.grpData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpData.Controls.Add(this.lblWeight);
            this.grpData.Controls.Add(this.lblAge);
            this.grpData.Controls.Add(this.lblWeightTitle);
            this.grpData.Controls.Add(this.lblAgeTitle);
            this.grpData.Controls.Add(this.lblGrowth);
            this.grpData.Controls.Add(this.lblGrowthTitle);
            this.grpData.Controls.Add(this.lblSex);
            this.grpData.Controls.Add(this.lblSexTitle);
            this.grpData.Location = new System.Drawing.Point(12, 270);
            this.grpData.Name = "grpData";
            this.grpData.Size = new System.Drawing.Size(460, 53);
            this.grpData.TabIndex = 4;
            this.grpData.TabStop = false;
            this.grpData.Text = "Исходные данные:";
            // 
            // lblWeight
            // 
            this.lblWeight.AutoSize = true;
            this.lblWeight.Location = new System.Drawing.Point(251, 32);
            this.lblWeight.Name = "lblWeight";
            this.lblWeight.Size = new System.Drawing.Size(0, 15);
            this.lblWeight.TabIndex = 0;
            // 
            // lblAge
            // 
            this.lblAge.AutoSize = true;
            this.lblAge.Location = new System.Drawing.Point(70, 32);
            this.lblAge.Name = "lblAge";
            this.lblAge.Size = new System.Drawing.Size(0, 15);
            this.lblAge.TabIndex = 0;
            // 
            // lblWeightTitle
            // 
            this.lblWeightTitle.AutoSize = true;
            this.lblWeightTitle.Location = new System.Drawing.Point(207, 32);
            this.lblWeightTitle.Name = "lblWeightTitle";
            this.lblWeightTitle.Size = new System.Drawing.Size(31, 15);
            this.lblWeightTitle.TabIndex = 0;
            this.lblWeightTitle.Text = "Вес:";
            // 
            // lblAgeTitle
            // 
            this.lblAgeTitle.AutoSize = true;
            this.lblAgeTitle.Location = new System.Drawing.Point(6, 32);
            this.lblAgeTitle.Name = "lblAgeTitle";
            this.lblAgeTitle.Size = new System.Drawing.Size(58, 15);
            this.lblAgeTitle.TabIndex = 0;
            this.lblAgeTitle.Text = "Возраст:";
            // 
            // lblGrowth
            // 
            this.lblGrowth.AutoSize = true;
            this.lblGrowth.Location = new System.Drawing.Point(251, 17);
            this.lblGrowth.Name = "lblGrowth";
            this.lblGrowth.Size = new System.Drawing.Size(0, 15);
            this.lblGrowth.TabIndex = 0;
            // 
            // lblGrowthTitle
            // 
            this.lblGrowthTitle.AutoSize = true;
            this.lblGrowthTitle.Location = new System.Drawing.Point(207, 17);
            this.lblGrowthTitle.Name = "lblGrowthTitle";
            this.lblGrowthTitle.Size = new System.Drawing.Size(38, 15);
            this.lblGrowthTitle.TabIndex = 0;
            this.lblGrowthTitle.Text = "Рост:";
            // 
            // lblSex
            // 
            this.lblSex.AutoSize = true;
            this.lblSex.Location = new System.Drawing.Point(70, 17);
            this.lblSex.Name = "lblSex";
            this.lblSex.Size = new System.Drawing.Size(0, 15);
            this.lblSex.TabIndex = 0;
            // 
            // lblSexTitle
            // 
            this.lblSexTitle.AutoSize = true;
            this.lblSexTitle.Location = new System.Drawing.Point(6, 17);
            this.lblSexTitle.Name = "lblSexTitle";
            this.lblSexTitle.Size = new System.Drawing.Size(33, 15);
            this.lblSexTitle.TabIndex = 0;
            this.lblSexTitle.Text = "Пол:";
            // 
            // btnCalc
            // 
            this.btnCalc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCalc.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCalc.Image = global::TonClock.Properties.Resources.Check_16x16;
            this.btnCalc.Location = new System.Drawing.Point(12, 437);
            this.btnCalc.Name = "btnCalc";
            this.btnCalc.Size = new System.Drawing.Size(460, 36);
            this.btnCalc.TabIndex = 0;
            this.btnCalc.Text = "Вычислить";
            this.btnCalc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCalc.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCalc.UseVisualStyleBackColor = true;
            this.btnCalc.Click += new System.EventHandler(this.btnCalc_Click);
            // 
            // grpResult1
            // 
            this.grpResult1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpResult1.Controls.Add(this.lblKn);
            this.grpResult1.Controls.Add(this.lblP5P2);
            this.grpResult1.Controls.Add(this.lblKp);
            this.grpResult1.Location = new System.Drawing.Point(12, 479);
            this.grpResult1.Name = "grpResult1";
            this.grpResult1.Size = new System.Drawing.Size(460, 71);
            this.grpResult1.TabIndex = 4;
            this.grpResult1.TabStop = false;
            this.grpResult1.Text = "Промежуточный результат:";
            // 
            // lblKn
            // 
            this.lblKn.AutoSize = true;
            this.lblKn.Location = new System.Drawing.Point(6, 47);
            this.lblKn.Name = "lblKn";
            this.lblKn.Size = new System.Drawing.Size(0, 15);
            this.lblKn.TabIndex = 0;
            // 
            // lblP5P2
            // 
            this.lblP5P2.AutoSize = true;
            this.lblP5P2.Location = new System.Drawing.Point(6, 17);
            this.lblP5P2.Name = "lblP5P2";
            this.lblP5P2.Size = new System.Drawing.Size(0, 15);
            this.lblP5P2.TabIndex = 0;
            // 
            // lblKp
            // 
            this.lblKp.AutoSize = true;
            this.lblKp.Location = new System.Drawing.Point(6, 32);
            this.lblKp.Name = "lblKp";
            this.lblKp.Size = new System.Drawing.Size(0, 15);
            this.lblKp.TabIndex = 0;
            // 
            // grpResult2
            // 
            this.grpResult2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpResult2.Controls.Add(this.lblResult);
            this.grpResult2.Location = new System.Drawing.Point(12, 556);
            this.grpResult2.Name = "grpResult2";
            this.grpResult2.Size = new System.Drawing.Size(460, 44);
            this.grpResult2.TabIndex = 4;
            this.grpResult2.TabStop = false;
            this.grpResult2.Text = "Вычисленное АД:";
            // 
            // lblResult
            // 
            this.lblResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblResult.Location = new System.Drawing.Point(3, 17);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(454, 24);
            this.lblResult.TabIndex = 0;
            this.lblResult.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // grpParams
            // 
            this.grpParams.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpParams.Controls.Add(this.txtR);
            this.grpParams.Controls.Add(this.txtI);
            this.grpParams.Controls.Add(this.labelR);
            this.grpParams.Controls.Add(this.txtN);
            this.grpParams.Controls.Add(this.lblI);
            this.grpParams.Controls.Add(this.txtH);
            this.grpParams.Controls.Add(this.lblN);
            this.grpParams.Controls.Add(this.txtJ);
            this.grpParams.Controls.Add(this.lblH);
            this.grpParams.Controls.Add(this.lblJ);
            this.grpParams.Location = new System.Drawing.Point(12, 329);
            this.grpParams.Name = "grpParams";
            this.grpParams.Size = new System.Drawing.Size(460, 102);
            this.grpParams.TabIndex = 4;
            this.grpParams.TabStop = false;
            this.grpParams.Text = "Степени полинома:";
            // 
            // txtR
            // 
            this.txtR.Location = new System.Drawing.Point(230, 47);
            this.txtR.Name = "txtR";
            this.txtR.Size = new System.Drawing.Size(57, 21);
            this.txtR.TabIndex = 5;
            this.txtR.Text = "0,1";
            this.txtR.Validating += new System.ComponentModel.CancelEventHandler(this.txtR_Validating);
            // 
            // txtI
            // 
            this.txtI.Location = new System.Drawing.Point(230, 20);
            this.txtI.Name = "txtI";
            this.txtI.Size = new System.Drawing.Size(57, 21);
            this.txtI.TabIndex = 4;
            this.txtI.Text = "0,8";
            this.txtI.Validating += new System.ComponentModel.CancelEventHandler(this.txtI_Validating);
            // 
            // labelR
            // 
            this.labelR.AutoSize = true;
            this.labelR.Location = new System.Drawing.Point(207, 50);
            this.labelR.Name = "labelR";
            this.labelR.Size = new System.Drawing.Size(14, 15);
            this.labelR.TabIndex = 44;
            this.labelR.Text = "r:";
            // 
            // txtN
            // 
            this.txtN.Location = new System.Drawing.Point(29, 47);
            this.txtN.Name = "txtN";
            this.txtN.Size = new System.Drawing.Size(57, 21);
            this.txtN.TabIndex = 2;
            this.txtN.Text = "0,6";
            this.txtN.Validating += new System.ComponentModel.CancelEventHandler(this.txtN_Validating);
            // 
            // lblI
            // 
            this.lblI.AutoSize = true;
            this.lblI.Location = new System.Drawing.Point(207, 23);
            this.lblI.Name = "lblI";
            this.lblI.Size = new System.Drawing.Size(13, 15);
            this.lblI.TabIndex = 44;
            this.lblI.Text = "i:";
            // 
            // txtH
            // 
            this.txtH.Location = new System.Drawing.Point(29, 74);
            this.txtH.Name = "txtH";
            this.txtH.Size = new System.Drawing.Size(57, 21);
            this.txtH.TabIndex = 3;
            this.txtH.Text = "1";
            this.txtH.Validating += new System.ComponentModel.CancelEventHandler(this.txtH_Validating);
            // 
            // lblN
            // 
            this.lblN.AutoSize = true;
            this.lblN.Location = new System.Drawing.Point(6, 50);
            this.lblN.Name = "lblN";
            this.lblN.Size = new System.Drawing.Size(17, 15);
            this.lblN.TabIndex = 43;
            this.lblN.Text = "n:";
            // 
            // txtJ
            // 
            this.txtJ.Location = new System.Drawing.Point(29, 20);
            this.txtJ.Name = "txtJ";
            this.txtJ.Size = new System.Drawing.Size(57, 21);
            this.txtJ.TabIndex = 1;
            this.txtJ.Text = "0,15";
            this.txtJ.Validating += new System.ComponentModel.CancelEventHandler(this.txtJ_Validating);
            // 
            // lblH
            // 
            this.lblH.AutoSize = true;
            this.lblH.Location = new System.Drawing.Point(6, 76);
            this.lblH.Name = "lblH";
            this.lblH.Size = new System.Drawing.Size(17, 15);
            this.lblH.TabIndex = 42;
            this.lblH.Text = "h:";
            // 
            // lblJ
            // 
            this.lblJ.AutoSize = true;
            this.lblJ.Location = new System.Drawing.Point(6, 23);
            this.lblJ.Name = "lblJ";
            this.lblJ.Size = new System.Drawing.Size(13, 15);
            this.lblJ.TabIndex = 45;
            this.lblJ.Text = "j:";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // grpScale
            // 
            this.grpScale.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpScale.Controls.Add(this.rdbScaleBuffer);
            this.grpScale.Controls.Add(this.rdbScaleWidth);
            this.grpScale.Location = new System.Drawing.Point(12, 194);
            this.grpScale.Name = "grpScale";
            this.grpScale.Size = new System.Drawing.Size(460, 70);
            this.grpScale.TabIndex = 8;
            this.grpScale.TabStop = false;
            this.grpScale.Text = "Масштаб по горизонтальной оси:";
            // 
            // rdbScaleBuffer
            // 
            this.rdbScaleBuffer.AutoSize = true;
            this.rdbScaleBuffer.Location = new System.Drawing.Point(9, 45);
            this.rdbScaleBuffer.Name = "rdbScaleBuffer";
            this.rdbScaleBuffer.Size = new System.Drawing.Size(139, 19);
            this.rdbScaleBuffer.TabIndex = 7;
            this.rdbScaleBuffer.Text = "заданный в буфере";
            this.rdbScaleBuffer.UseVisualStyleBackColor = true;
            // 
            // rdbScaleWidth
            // 
            this.rdbScaleWidth.AutoSize = true;
            this.rdbScaleWidth.Checked = true;
            this.rdbScaleWidth.Location = new System.Drawing.Point(9, 20);
            this.rdbScaleWidth.Name = "rdbScaleWidth";
            this.rdbScaleWidth.Size = new System.Drawing.Size(179, 19);
            this.rdbScaleWidth.TabIndex = 7;
            this.rdbScaleWidth.TabStop = true;
            this.rdbScaleWidth.Text = "растянуть по ширине окна";
            this.rdbScaleWidth.UseVisualStyleBackColor = true;
            this.rdbScaleWidth.CheckedChanged += new System.EventHandler(this.rdbScaleWidth_CheckedChanged);
            // 
            // cmsPulseMenu
            // 
            this.cmsPulseMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAutoRepers});
            this.cmsPulseMenu.Name = "cmsPulseMenu";
            this.cmsPulseMenu.Size = new System.Drawing.Size(285, 48);
            // 
            // mnuAutoRepers
            // 
            this.mnuAutoRepers.Name = "mnuAutoRepers";
            this.mnuAutoRepers.Size = new System.Drawing.Size(284, 22);
            this.mnuAutoRepers.Text = "Автоматическая расстановка реперов";
            this.mnuAutoRepers.Click += new System.EventHandler(this.mnuAutoRepers_Click);
            // 
            // frmGlobal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 612);
            this.Controls.Add(this.btnCalc);
            this.Controls.Add(this.grpResult2);
            this.Controls.Add(this.grpParams);
            this.Controls.Add(this.grpResult1);
            this.Controls.Add(this.grpScale);
            this.Controls.Add(this.grpData);
            this.Controls.Add(this.pnlPulse);
            this.Controls.Add(this.lblRepers);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.KeyPreview = true;
            this.MinimizeBox = false;
            this.Name = "frmGlobal";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Глобал";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmGlobal_FormClosing);
            this.Shown += new System.EventHandler(this.frmGlobal_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmGlobal_KeyDown);
            this.Resize += new System.EventHandler(this.frmGlobal_Resize);
            this.grpData.ResumeLayout(false);
            this.grpData.PerformLayout();
            this.grpResult1.ResumeLayout(false);
            this.grpResult1.PerformLayout();
            this.grpResult2.ResumeLayout(false);
            this.grpParams.ResumeLayout(false);
            this.grpParams.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.grpScale.ResumeLayout(false);
            this.grpScale.PerformLayout();
            this.cmsPulseMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRepers;
        private System.Windows.Forms.Panel pnlPulse;
        private System.Windows.Forms.GroupBox grpData;
        private System.Windows.Forms.Button btnCalc;
        private System.Windows.Forms.GroupBox grpResult1;
        private System.Windows.Forms.GroupBox grpResult2;
        private System.Windows.Forms.Label lblWeight;
        private System.Windows.Forms.Label lblAge;
        private System.Windows.Forms.Label lblWeightTitle;
        private System.Windows.Forms.Label lblAgeTitle;
        private System.Windows.Forms.Label lblGrowth;
        private System.Windows.Forms.Label lblGrowthTitle;
        private System.Windows.Forms.Label lblSex;
        private System.Windows.Forms.Label lblSexTitle;
        private System.Windows.Forms.GroupBox grpParams;
        private System.Windows.Forms.TextBox txtI;
        private System.Windows.Forms.TextBox txtN;
        private System.Windows.Forms.Label lblI;
        private System.Windows.Forms.TextBox txtH;
        private System.Windows.Forms.Label lblN;
        private System.Windows.Forms.TextBox txtJ;
        private System.Windows.Forms.Label lblH;
        private System.Windows.Forms.Label lblJ;
        private System.Windows.Forms.TextBox txtR;
        private System.Windows.Forms.Label labelR;
        private System.Windows.Forms.Label lblKn;
        private System.Windows.Forms.Label lblKp;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.GroupBox grpScale;
        private System.Windows.Forms.RadioButton rdbScaleBuffer;
        private System.Windows.Forms.RadioButton rdbScaleWidth;
        private System.Windows.Forms.Label lblP5P2;
        private System.Windows.Forms.ContextMenuStrip cmsPulseMenu;
        private System.Windows.Forms.ToolStripMenuItem mnuAutoRepers;
    }
}