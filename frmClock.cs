﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO.Ports;
using System.Linq;
using System.Net.Sockets;
using System.Windows.Forms;
using TonClock.Properties;
using System.Threading;
using System.IO;
using System.Data.OleDb;
using System.Globalization;
using NAudio.Wave;
using System.Drawing.Imaging;

namespace TonClock
{
    public partial class frmClock : Form
    {

        // текущее состояние часов
        private enum CurStations
        {
            Watch,      // просто часы
            WatchUp,    // открытие часов
            Menu,       // меню
            Control,    // контроль давления тонометром
            Connecting, // подключение к устройству
            Pulse,      // отображение пульсовой волны
            WatchClose, // закрытие часов
            Rec,        // запись фрагмента
            Saving,     // сохранение результатов замера
            History     // история замеров
        };
        private CurStations _watchState;

        private readonly int _idPatient;     // текущий пациент
        private int _idControlMes;           // последнее измерение

        // параметры пациента
        private float _patientA2A1,
                      _patientS1,
                      _patientS2,
                      _patientK1,
                      _patientK2,
                      _patientK,
                      _patientA,
                      _patientB,
                      _patientC,
                      _patientD;
        private int _patientFss, _patientFssOpor;
        private readonly int _patientAge, _patientWeight;
        private readonly float _patientGrowth;
        private readonly bool _patientSex;
        // параметры программы
        private readonly ClockSettings _clockSettings;

        private bool _flagOkResult,         // флаг, что замер прошел успешно
                     _flagFormIsClosing;    // флаг, что форма закрывается
        private readonly string _strTitle;
        private readonly CultureInfo _myDbCultInfo = CultureInfo.InvariantCulture;
        // для расчета глобала
        private readonly GlobalSettings _globalSettings = new GlobalSettings();

        public int GetMesId() { return _idControlMes; }
        
        // конструктор формы
        public frmClock(int idP, bool history = false)
        {
            InitializeComponent();
            this.Icon = Properties.Resources.yagodka;
            // БД
            _idPatient = idP;
            try
            {
                using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    OleDbCommand myOleDbCommand = new OleDbCommand(
                        "SELECT lastname, firstname, patronymic, topPressureControl, lowerPressureControl, " +
                        "       sex, age, growth, weight " +
                        "FROM Patients " +
                        "WHERE id = " + _idPatient.ToString(),
                        myOleDbConnection);
                    using (OleDbDataReader myReader = myOleDbCommand.ExecuteReader())
                    {
                        if (myReader != null && myReader.Read())
                        {
                            _strTitle = ConstGlobals.AssemblyTitle + " - Часы-тонометр - " + myReader[0].ToString() +
                                        " " +
                                        myReader[1].ToString()[0] + "." + myReader[2].ToString()[0] + ".";
                            _topPressureControl = (byte) myReader.GetInt16(3);
                            _lowerPressureControl = (byte) myReader.GetInt16(4);
                            _patientSex = myReader.GetBoolean(5);
                            _patientAge = myReader.GetInt16(6);
                            _patientGrowth = myReader.IsDBNull(7) ? 0 : myReader.GetFloat(7);
                            _patientWeight = myReader.GetInt16(8);
                        }
                        else
                            throw new Exception("В базе не обнаружен пациент с таким идентификатором.");
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Внимание! При попытке открытия базы данных произошла ошибка:\n" +
                    exc.Message + "\n\n" +
                    "Дальнейшая работа программы не возможна.\n\n" +
                    "Пожалуйста, обратитесь к администратору.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            // Инициализация параметров
            _idControlMes = ReadPatientParameters();
            _clockSettings = new ClockSettings();
            SetMenu();
            _devicesList = new DevicesList();
            _devicesList.ReadListFromTextFile(ConstGlobals.DevicesFileFullName);
            // для ускорения отображения графики
            this.SetStyle(ControlStyles.AllPaintingInWmPaint |
                ControlStyles.OptimizedDoubleBuffer |
                ControlStyles.UserPaint |
                ControlStyles.SupportsTransparentBackColor,
                true);
            this.UpdateStyles();
            this.CenterToScreen();
            _watchState = CurStations.Watch;
            _templates = CurTemplates.WatchOnly;
            lblStatus.Text = "Настройте параметры, и нажмите на крышку часов для начала работы.";
            // кнопки
            SetEnableArrows(false);
            _firstShowHistory = history;
            if (_firstShowHistory)
                OpenWatch();
        }

        private void frmClock_Load(object sender, EventArgs e)
        {
            this.Text = _strTitle;
            InitFonDevice(false);
        }

        // чтение параметров пациента из базы
        private int ReadPatientParameters()
        {
            int returnMesId = -1;
            try
            {
                using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    OleDbCommand myOleDbCommand = new OleDbCommand(
                        "SELECT id, pulse, a2a1, s1, s2, fssOpor, k1, k2, k, a, b, c, d " +
                        "FROM Measurements " +
                        "WHERE patient = " + _idPatient.ToString() + " " +
                        "ORDER BY mesDateTime DESC",
                        myOleDbConnection);
                    using (OleDbDataReader myReader = myOleDbCommand.ExecuteReader())
                    {
                        if (myReader != null && myReader.Read())
                        {
                            returnMesId = myReader.GetInt32(0);
                            _patientFss = myReader.GetInt16(1);
                            _patientA2A1 = myReader.GetFloat(2);
                            _patientS1 = myReader.GetFloat(3);
                            _patientS2 = myReader.GetFloat(4);
                            _patientFssOpor = myReader.GetInt16(5);
                            _patientK1 = myReader.GetFloat(6);
                            _patientK2 = myReader.GetFloat(7);
                            _patientK = myReader.GetFloat(8);
                            _patientA = myReader.GetFloat(9);
                            _patientB = myReader.GetFloat(10);
                            _patientC = myReader.GetFloat(11);
                            _patientD = myReader.GetFloat(12);
                        }
                        else
                        {
                            _patientFss = 80;
                            _patientA2A1 = 0.6f;
                            _patientS1 = 0.42f;
                            _patientS2 = 0.75f;
                            _patientFssOpor = 80;
                            _patientK1 = 1.917f;
                            _patientK2 = 2.875f;
                            _patientK = 2;
                            _patientA = 0.7f;
                            _patientB = 2;
                            _patientC = 0.1f;
                            _patientD = 1;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                    exc.Message + "\n\n" +
                    "Пожалуйста, обратитесь к администратору.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return returnMesId;
        }


        // мнимальная ширина шаблона
        private const int MinWaveWidth = 325;

        private void frmMain_Resize(object sender, EventArgs e)
        {
            if (_templates == CurTemplates.WatchOnly)
            {
                picWatch.Location = new Point((this.ClientSize.Width - picWatch.Width) / 2,
                    menuStripMain.Height + (this.ClientSize.Height - menuStripMain.Height - statusStripMain.Height - picWatch.Height) / 2);
                prbRec.Location = new Point(this.ClientSize.Width / 2 - prbRec.Width / 2,
                    picWatch.Location.Y + 20);
            }
            else
            {
                int pnlWaveWidth = this.ClientSize.Width - picWatch.Width - picTemplates.Width + 31;
                picWatch.Location = new Point(this.ClientSize.Width - picWatch.Width,
                    menuStripMain.Height + (this.ClientSize.Height - menuStripMain.Height - statusStripMain.Height - picWatch.Height) / 2);
                picTemplates.Location = new Point(0, picWatch.Location.Y);
                if (pnlWaveWidth < MinWaveWidth)
                {
                    picTemplates.Location = new Point(-Math.Min((MinWaveWidth - pnlWaveWidth) / 2, 45), picTemplates.Location.Y);
                    picWatch.Location = new Point(picWatch.Location.X + Math.Min((MinWaveWidth - pnlWaveWidth) / 2, 160), picWatch.Location.Y);
                }
                pnlWave.Location = new Point(picTemplates.Location.X + picTemplates.Width,
                    picTemplates.Top + 230);
                pnlWave.Width = picWatch.Left - picTemplates.Right + 31;
            }
        }


        //*****************************************************************
        //                              Меню                              *
        //*****************************************************************

        private void SetMenu()
        {
            mnuSoundPulseOn.Checked = _clockSettings.FlagSoundPulse;
            mnuSoundPulseOff.Checked = !_clockSettings.FlagSoundPulse;
            mnuSoundResultOn.Checked = _clockSettings.FlagSoundResult;
            mnuSoundResultOff.Checked = !_clockSettings.FlagSoundResult;
        }

        private void mnuSoundPulseOn_Click(object sender, EventArgs e)
        {
            _clockSettings.FlagSoundPulse = true;
            SetMenu();
            if ((_watchState == CurStations.Pulse) || (_watchState == CurStations.Rec))
                StartSoundPulse();
        }

        private void mnuSoundPulseOff_Click(object sender, EventArgs e)
        {
            _clockSettings.FlagSoundPulse = false;
            SetMenu();
            StopSoundPulse();
        }

        private void mnuSoundResultOn_Click(object sender, EventArgs e)
        {
            _clockSettings.FlagSoundResult = true;
            SetMenu();
            if (_watchState == CurStations.History)
                StartSoundResult();
        }

        private void mnuSoundResultOff_Click(object sender, EventArgs e)
        {
            _clockSettings.FlagSoundResult = false;
            SetMenu();
            StopSoundResult();
        }

        private void mnuBuffer_Click(object sender, EventArgs e)
        {
            using (frmPas myFormPas = new frmPas("буфер"))
            {
                myFormPas.ShowDialog();
                if (myFormPas.DialogResult == DialogResult.OK)
                {
                    int curMesDb = (_watchState == CurStations.History &&
                                    (_dtHistory != null) && (_currentHisrory >= 0) &&
                                    (_currentHisrory < _dtHistory.Rows.Count)
                                        ? Convert.ToInt32(_dtHistory.Rows[_currentHisrory][0])
                                        : 0);
                    using (frmBuffer myFrmBuffer = new frmBuffer(_idPatient, curMesDb))
                    {
                        myFrmBuffer.ShowDialog();
                        if (_watchState == CurStations.History)
                            SetModeHistory(false, false);
                        _idControlMes = ReadPatientParameters();
                    }
                }
            }
        }

        private void mnuMolodimetr_Click(object sender, EventArgs e)
        {
            int curMesDb;
            if (_watchState == CurStations.History &&
                            (_dtHistory != null) && (_currentHisrory >= 0) &&
                            (_currentHisrory < _dtHistory.Rows.Count))
                curMesDb = Convert.ToInt32(_dtHistory.Rows[_currentHisrory][0]);
            else
            {
                MessageBox.Show("Не выбран замер для отображения данных!", "Ошибка",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            //using (frmPas myFormPas = new frmPas("молодиметр"))
            //{
            //    myFormPas.ShowDialog();
            //    if (myFormPas.DialogResult == DialogResult.OK)
            //    {
            //        using (frmMolodimetr myFrmMolodimetr = new frmMolodimetr(curMesDb,
            //                                      _dtHistory.Rows.Count - _currentHisrory))
            //        {
            //            myFrmMolodimetr.ShowDialog();
            //        }
            //    }
            //}

            using (var myFrmMolodimetr = new frmMolodimetr( curMesDb, _dtHistory.Rows.Count - _currentHisrory,
                                                            _dtHistory, _idPatient) )
            {
                myFrmMolodimetr.ShowDialog();
                if (_watchState == CurStations.History)
                    SetModeHistory(false, false);
            }
        }


        // главное меню на экране
        int _mainMenuSelected;

        // показать меню на экране часов
        private void ShowMainMenu()
        {
            _mainMenuSelected = 0;
            _watchState = CurStations.Menu;
            SetEnableArrows(false);
            SetEnableArrows(0);
            SetEnableArrows(4);
            picWatch.Refresh();
            lblStatus.Text = "Выберите пункт меню.";
            StartLightElement(2);
        }


        //*****************************************************************
        //                             Часы                               *
        //*****************************************************************

        int _curWatchNumber; // счетчик смены картинок при поднятии / опускании крышки

        // прорисовка текста и объектов на часах
        private void picWatch_Paint(object sender, PaintEventArgs e)
        {
            Graphics graph = e.Graphics;
            graph.SmoothingMode = SmoothingMode.AntiAlias;
            graph.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            for (int i = 0; i < 5; i++)
            {
                string strResName = String.Empty;
                switch (i)
                {
                    case 0: strResName += "ok_"; break;
                    case 1: strResName += "left_"; break;
                    case 2: strResName += "top_"; break;
                    case 3: strResName += "right_"; break;
                    case 4: strResName += "bottom_"; break;
                }
                strResName += (EnableButton[i] ? (_lightButton == i ? "light" : (_pressButton == i ? "press" : "enable")) : "disable");
                graph.DrawImage((Bitmap)Resources.ResourceManager.GetObject(strResName), RctButton[i]);
            }
            if (_flagLightElement && (_lightElement != 0) && !_lightWatch && (_lightButton == -1) && (_pressButton == -1))
            {
                Rectangle myRct;
                int myRadius;
                if (_lightElement == 1)
                {
                    myRct = new Rectangle(_rctWatch.X, _rctWatch.Y, _rctWatch.Width, _rctWatch.Height);
                    myRadius = 1;
                }
                else
                {
                    myRct = new Rectangle(RctButton[0].X + 7, RctButton[0].Y + 6, 66, 58);
                    myRadius = 6;
                }
                for (int i = 0; i < 11; i++)
                {
                    Pen myPen = new Pen(Color.FromArgb(210 - 20 * i, 140, 140, 140), 1);
                    GraphicsPath myPath = CreateRoundedRectangles(myRct.X + i, myRct.Y + i, myRct.Width - i * 2, myRct.Height - i * 2, myRadius);
                    graph.DrawPath(myPen, myPath);
                }
            }
            switch (_watchState)
            {
                case CurStations.Watch:
                    DateTime curDateTime = DateTime.Now;
                    // пишем время
                    string strHour = curDateTime.Hour.ToString("D2");
                    string strMinute = curDateTime.Minute.ToString("D2");
                    string strTime = (curDateTime.Millisecond < 500 ? strHour + " : " + strMinute : strHour + "   " + strMinute);
                    Font fontTime = new Font("Microsoft Sans Serif", 36, FontStyle.Bold);
                    SolidBrush brushTime = new SolidBrush(Color.FromArgb(251, 221, 56));
                    SizeF timeSize = graph.MeasureString(strTime, fontTime);
                    Rectangle rctTime = new Rectangle(_rctWatch.X + _rctWatch.Width / 2 - (int)timeSize.Width / 2,
                        _rctWatch.Y + _rctWatch.Height / 2 - (int)timeSize.Height / 2,
                        (int)timeSize.Width, (int)timeSize.Height);
                    graph.DrawString(strTime, fontTime, brushTime, new Point(rctTime.X, rctTime.Y));
                    // пишем дату
                    string strDate = curDateTime.ToString("d MMMM yyyy г.");
                    fontTime = new Font("Microsoft Sans Serif", 15, FontStyle.Regular);
                    brushTime = new SolidBrush(Color.FromArgb(177, 150, 1));
                    timeSize = graph.MeasureString(strDate, fontTime);
                    rctTime = new Rectangle(_rctWatch.X + _rctWatch.Width / 2 - (int)timeSize.Width / 2,
                        _rctWatch.Bottom - (int)timeSize.Height - 20,
                        (int)timeSize.Width, (int)timeSize.Height);
                    graph.DrawString(strDate, fontTime, brushTime, new Point(rctTime.X, rctTime.Y));
                    break;
                case CurStations.Menu:
                    Font fontMenuTitle = new Font("Microsoft Sans Serif", 16, FontStyle.Regular),
                        fontMenu = new Font("Microsoft Sans Serif", 14, FontStyle.Regular);
                    SolidBrush brushMenu = new SolidBrush(Color.FromArgb(230, 247, 255)),
                        brushMenuSelected = new SolidBrush(Color.Yellow),
                        brushMenuSelectedFill = new SolidBrush(Color.FromArgb(183, 95, 24));
                    Pen penMenu = new Pen(Color.FromArgb(60, 60, 60)),
                        penMenuSelected = new Pen(Color.FromArgb(247, 172, 112));
                    graph.DrawString("Выберите пункт меню:", fontMenuTitle, brushMenu, new Point(
                        _rctWatch.X + _rctWatch.Width / 2 - (int)graph.MeasureString("Выберите пункт меню:", fontMenuTitle).Width / 2,
                        _rctWatch.Top + 20));
                    Rectangle[] rctMenu = {new Rectangle(_rctWatch.X + _rctWatch.Width / 2 - 180 / 2, _rctWatch.Top + 65, 180, 60),
                        new Rectangle(_rctWatch.X + _rctWatch.Width / 2 - 180 / 2, _rctWatch.Top + 65 + 60, 180, 60),
                        new Rectangle(_rctWatch.X + _rctWatch.Width / 2 - 180 / 2, _rctWatch.Top + 65 + 60 * 2, 180, 60)};
                    string[,] strMenu = {{"Замер", "давления"}, {"Контроль", "тонометром"}};
                    graph.FillRectangle(brushMenuSelectedFill, rctMenu[_mainMenuSelected]);
                    for (int i = 0; i < 3; i++)
                    {
                        SolidBrush curBrush = (_mainMenuSelected == i ? brushMenuSelected : brushMenu);
                        if (i != 2)
                        {
                            graph.DrawString(strMenu[i, 0], fontMenu, curBrush, new Point(
                                rctMenu[i].X + rctMenu[i].Width / 2 - (int)graph.MeasureString(strMenu[i, 0], fontMenu).Width / 2,
                                rctMenu[i].Y + rctMenu[i].Height / 2 - (int)graph.MeasureString(strMenu[i, 0], fontMenu).Height));
                            graph.DrawString(strMenu[i, 1], fontMenu, curBrush, new Point(
                                rctMenu[i].X + rctMenu[i].Width / 2 - (int)graph.MeasureString(strMenu[i, 1], fontMenu).Width / 2,
                                rctMenu[i].Y + rctMenu[i].Height / 2));
                        }
                        else
                            graph.DrawString("Память", fontMenu, curBrush, new Point(
                                rctMenu[i].X + rctMenu[i].Width / 2 - (int)graph.MeasureString("Память", fontMenu).Width / 2,
                                rctMenu[i].Y + rctMenu[i].Height / 2 - (int)graph.MeasureString("Память", fontMenu).Height / 2));
                        graph.DrawRectangle(penMenu, rctMenu[i]);
                    }
                    graph.DrawRectangle(penMenuSelected, rctMenu[_mainMenuSelected]);
                    break;
                case CurStations.Control:
                    Font fontKontrolText = new Font("Microsoft Sans Serif", 18, FontStyle.Regular),
                        fontPressureText = new Font("Microsoft Sans Serif", 28, FontStyle.Regular);
                    SolidBrush brushKontrolText = new SolidBrush(Color.FromArgb(230, 247, 255)),
                        brushKontrolSelected = new SolidBrush(Color.Yellow),
                        brushKontrolSelectedFill = new SolidBrush(Color.FromArgb(183, 95, 24));
                    Pen penPressure = new Pen(Color.FromArgb(60, 60, 60)),
                        penPressureSelected = new Pen(Color.FromArgb(247, 172, 112));
                    graph.DrawString("Введите показания", fontKontrolText, brushKontrolText, new Point(
                        picWatch.Width / 2 - (int)graph.MeasureString("Введите показания", fontKontrolText).Width / 2,
                        _rctWatch.Top + 20));
                    graph.DrawString("тонометра:", fontKontrolText, brushKontrolText, new Point(
                        picWatch.Width / 2 - (int)graph.MeasureString("тонометра:", fontKontrolText).Width / 2,
                        _rctWatch.Top + 20 + fontKontrolText.Height));
                    Point firstNumber = new Point((int)(_rctWatch.X + _rctWatch.Width / 2 - 33 * 3.5), _rctWatch.Y + _rctWatch.Height / 2 - 25);
                    string strNumber = "/";
                    Size sizeNum = new Size((int)graph.MeasureString(strNumber, fontPressureText).Width,
                        (int)graph.MeasureString(strNumber, fontPressureText).Height);
                    Rectangle rctNumber = new Rectangle(firstNumber.X + 3 * 33, firstNumber.Y, 33, 50);
                    graph.DrawString(strNumber, fontPressureText, brushKontrolText, new Point(
                        rctNumber.X + rctNumber.Width / 2 - (int)graph.MeasureString(strNumber, fontPressureText).Width / 2,
                        rctNumber.Y + rctNumber.Height / 2 - (int)graph.MeasureString(strNumber, fontPressureText).Height / 2));
                    graph.FillRectangle(brushKontrolSelectedFill, new Rectangle(firstNumber.X + _selectedNumber * 33 + (_selectedNumber > 2 ? 33 : 0), firstNumber.Y, 33, 50));
                    for (int i = 0; i < 6; i++)
                    {
                        strNumber = _numbersPressure[i].ToString();
                        sizeNum = new Size((int)graph.MeasureString(strNumber, fontPressureText).Width,
                            (int)graph.MeasureString(strNumber, fontPressureText).Height);
                        rctNumber = new Rectangle(firstNumber.X + i * 33 + (i > 2 ? 33 : 0), firstNumber.Y, 33, 50);
                        if (i == _selectedNumber)
                            graph.DrawString(strNumber, fontPressureText, brushKontrolSelected, new Point(
                                rctNumber.X + rctNumber.Width / 2 - (int)graph.MeasureString(strNumber, fontPressureText).Width / 2,
                                rctNumber.Y + rctNumber.Height / 2 - (int)graph.MeasureString(strNumber, fontPressureText).Height / 2));
                        else
                            graph.DrawString(strNumber, fontPressureText, brushKontrolText, new Point(
                                rctNumber.X + rctNumber.Width / 2 - (int)graph.MeasureString(strNumber, fontPressureText).Width / 2,
                                rctNumber.Y + rctNumber.Height / 2 - (int)graph.MeasureString(strNumber, fontPressureText).Height / 2));
                        graph.DrawRectangle(penPressure, rctNumber);
                    }
                    graph.DrawRectangle(penPressureSelected, new Rectangle(firstNumber.X + _selectedNumber * 33 + (_selectedNumber > 2 ? 33 : 0), firstNumber.Y, 33, 50));
                    break;
                case CurStations.Connecting:
                    Font fontConnecting = new Font("Microsoft Sans Serif", 22, FontStyle.Regular);
                    SolidBrush brushConnecting = new SolidBrush(Color.FromArgb(230, 247, 255));
                    string[] strTextConnecting = { "Пожалуйста,", "подождите...", " ", "Идет", "подключение", "к устройству..." };
                    int curYConnecting = _rctWatch.Top + _rctWatch.Height / 2 - (int)graph.MeasureString(strTextConnecting[0], fontConnecting).Height * 3;
                    for (int i = 0; i < 6; i++)
                    {
                        graph.DrawString(strTextConnecting[i], fontConnecting, brushConnecting, new Point(
                            _rctWatch.X + _rctWatch.Width / 2 - (int)graph.MeasureString(strTextConnecting[i], fontConnecting).Width / 2, curYConnecting));
                        curYConnecting += (int)graph.MeasureString(strTextConnecting[i], fontConnecting).Height;
                    }
                    break;
                case CurStations.Pulse:
                case CurStations.Rec:
                    if (_clockSettings.FlagLevel)
                    {
                        Font fontLevel = new Font("Microsoft Sans Serif", 18, FontStyle.Regular);
                        SolidBrush brushLevel = new SolidBrush(Color.FromArgb(230, 247, 255));
                        string strLevel = (_signalLevel >= 0 ? _signalLevel.ToString() : "-");
                        graph.DrawString(strLevel, fontLevel, brushLevel, new Point(
                            _rctWatch.Right - (int)graph.MeasureString(strLevel, fontLevel).Width,
                            _rctWatch.Y + HeaderHeight / 2 - (int)graph.MeasureString(strLevel, fontLevel).Height / 2));
                    }
                    if ((_device != null) && _device.DeviceStarted &&
                        ((_device.DeviceMode == DeviceModes.FtdiNew && ((FtdiNewDevice)_device).ConnectedEndDevice) || _device.DeviceMode == DeviceModes.Bluetooth) &&
                        (_batteryLevel >= 0 && _batteryLevel <= 5))
                    //if ((_clockSettings.DeviceMode == DeviceModes.FtdiNew) && (_device != null) && ((FtdiNewDevice)_device).ConnectedEndDevice &&
                    //    (_batteryLevel >=0 && _batteryLevel <=5))
                    {
                        Bitmap bmpBattery = (Bitmap) Resources.ResourceManager.GetObject("battery_" + _batteryLevel);
                        if (bmpBattery != null)
                        {
                            bmpBattery.SetResolution(graph.DpiX, graph.DpiY);
                            graph.DrawImage(bmpBattery, new Point(_rctBattery.X, _rctBattery.Y));
                        }
                    }
                    if (_watchState == CurStations.Rec)
                    {
                        Font fontRecText1 = new Font("Microsoft Sans Serif", 16, FontStyle.Underline),
                            fontRecText2 = new Font("Microsoft Sans Serif", 22, FontStyle.Bold);
                        SolidBrush brushRecText = new SolidBrush(Color.FromArgb(233, 48, 48));
                        string strRec = "Не говорить, не двигаться!";
                        graph.DrawString(strRec, fontRecText1, brushRecText, new Point(
                            _rctWatch.X + _rctWatch.Width / 2 - (int)graph.MeasureString(strRec, fontRecText1).Width / 2, 45));
                        strRec = "Идет замер.";
                        graph.DrawString(strRec, fontRecText2, brushRecText, new Point(
                            _rctWatch.X + _rctWatch.Width / 2 - (int)graph.MeasureString(strRec, fontRecText2).Width / 2, 75));
                    }
                    break;
                case CurStations.Saving:
                    Font fontSaving = new Font("Microsoft Sans Serif", 22, FontStyle.Regular);
                    SolidBrush brushSaving = new SolidBrush(Color.FromArgb(230, 247, 255));
                    string[] strTextSaving = {"Пожалуйста,", "подождите...", " ", "Результаты", "сохраняются", "в базу..."};
                    int curYSaving = _rctWatch.Top + _rctWatch.Height / 2 - (int)graph.MeasureString(strTextSaving[0], fontSaving).Height * 3;
                    for (int i = 0; i < 6; i++)
                    {
                        graph.DrawString(strTextSaving[i], fontSaving, brushSaving, new Point(
                            _rctWatch.X + _rctWatch.Width / 2 - (int)graph.MeasureString(strTextSaving[i], fontSaving).Width / 2, curYSaving));
                        curYSaving += (int)graph.MeasureString(strTextSaving[i], fontSaving).Height;
                    }
                    break;
                case CurStations.History:
                    if ((_dtHistory != null) && (_currentHisrory >= 0) && (_currentHisrory < _dtHistory.Rows.Count))
                    {
                        Font fontMesNumber = new Font("Microsoft Sans Serif", 20, FontStyle.Regular),
                            fontMesDate = new Font("Microsoft Sans Serif", 14, FontStyle.Regular),
                            fontMesTime = new Font("Microsoft Sans Serif", 20, FontStyle.Regular),
                            fontResultText = new Font("Microsoft Sans Serif", 22, FontStyle.Regular),
                            fontResultNumber = new Font("Microsoft Sans Serif", 28, FontStyle.Bold);
                        SolidBrush brushNumber = new SolidBrush(Color.FromArgb(177, 150, 1)),
                            brushDate = new SolidBrush(Color.FromArgb(177, 150, 1)),
                            brushResult = new SolidBrush(Color.FromArgb(230, 247, 255));
                        int mesNumber = _dtHistory.Rows.Count - _currentHisrory;
                        string strText = mesNumber.ToString();
                        int curY = _rctWatch.Y + (int)graph.MeasureString(strText, fontMesNumber).Height;
                        if (Convert.ToBoolean(_dtHistory.Rows[_currentHisrory][7]))
                        {
                            brushNumber.Color = Color.Red;
                            fontMesNumber = new Font(fontMesNumber, FontStyle.Bold);
                        }
                        int widthNumber = (int)graph.MeasureString(strText, fontMesNumber).Width;
                        graph.DrawString(strText, fontMesNumber, brushNumber, new Point(_rctWatch.X,
                            curY - (int)graph.MeasureString(strText, fontMesNumber).Height));
                        strText = Convert.ToDateTime(_dtHistory.Rows[_currentHisrory][1]).ToShortTimeString();
                        int widthTime = (int)graph.MeasureString(strText, fontMesTime).Width;
                        graph.DrawString(strText, fontMesTime, brushDate, new Point(
                            _rctWatch.Right - widthTime,
                            curY - (int)graph.MeasureString(strText, fontMesTime).Height));
                        strText = Convert.ToDateTime(_dtHistory.Rows[_currentHisrory][1]).ToShortDateString();
                        graph.DrawString(strText, fontMesDate, brushDate, new Point(
                            _rctWatch.X + widthNumber + (_rctWatch.Width - widthNumber - widthTime) / 2 - (int)graph.MeasureString(strText, fontMesDate).Width / 2 + 5,
                            curY - (int)graph.MeasureString(strText, fontMesDate).Height - 3));
                        
                        Rectangle rctHistory = new Rectangle(_rctWatch.X + (_rctWatch.Width / _dtHistory.Rows.Count) * (_dtHistory.Rows.Count - _currentHisrory - 1) + 1,
                            curY - 1, (_rctWatch.Width / _dtHistory.Rows.Count), 3);
                        if (_dtHistory.Rows.Count <= 1)
                            rctHistory = new Rectangle(_rctWatch.X + 1, rctHistory.Y, _rctWatch.Width - 1, 3);
                        else
                        {
                            if (_currentHisrory < (_dtHistory.Rows.Count - 1))
                                graph.DrawLine(new Pen(brushDate), new Point(_rctWatch.X, curY), new Point(rctHistory.X - 2, curY));
                            else
                                rctHistory.Location = new Point(_rctWatch.X + 1, rctHistory.Y);
                            if (_currentHisrory > 0)
                                graph.DrawLine(new Pen(brushDate), new Point(rctHistory.Right + 1, curY), new Point(_rctWatch.Right, curY));
                            else
                            {
                                graph.DrawLine(new Pen(brushDate), new Point(rctHistory.X - 2, curY), new Point(_rctWatch.Right - rctHistory.Width - 2, curY));
                                rctHistory.Location = new Point(_rctWatch.Right - rctHistory.Width, rctHistory.Y);
                            }
                        }
                        graph.FillRectangle(brushDate, rctHistory);
                        curY += 10;
                        strText = "Сосуды:";
                        graph.DrawString(strText, fontResultText, brushResult, new Point(
                            _rctWatch.X + 3, curY + 60 / 2 - (int)graph.MeasureString(strText, fontResultText).Height / 2));
                        int curX = _rctWatch.Right - 10 - 60;
                        Color[] colorsSosud = new Color[] {Color.Green, Color.Blue, Color.Red};
                        Rectangle rctSelected = new Rectangle(curX, curY, 60, 60);
                        bool flagRightSosud = false;
                        for (int i = 1; i <= 3; i++)
                        {
                            Rectangle rctEllipse = new Rectangle(
                                curX, curY + (i - 1) * 10, 60 - (i - 1) * 20, 60 - (i - 1) * 20);
                            if (Convert.ToInt16(_dtHistory.Rows[_currentHisrory][2]) == i)
                            {
                                rctSelected = rctEllipse;
                                flagRightSosud = true;
                            }
                            else
                            {
                                SolidBrush brushSosud = new SolidBrush(Color.FromArgb(
                                    Convert.ToInt16(_dtHistory.Rows[_currentHisrory][2]) == i ? 255 : 128,
                                    colorsSosud[i - 1]));
                                graph.FillEllipse(brushSosud, rctEllipse);
                            }
                            curX -= (60 - i * 20) - 5;
                        }
                        if (flagRightSosud && (rctSelected != null))
                        {
                            graph.FillEllipse(new SolidBrush(colorsSosud[Convert.ToInt16(_dtHistory.Rows[_currentHisrory][2]) - 1]), rctSelected);
                            graph.DrawEllipse(new Pen(brushResult, 2), rctSelected);
                        }
                        curY += 60 + 5;
                        strText = Convert.ToInt16(_dtHistory.Rows[_currentHisrory][3]).ToString() + " / " + Convert.ToInt16(_dtHistory.Rows[_currentHisrory][4]).ToString();
                        graph.DrawString(strText, fontResultNumber, brushResult, new Point(
                            _rctWatch.X + _rctWatch.Width / 2 - (int)graph.MeasureString(strText, fontResultNumber).Width / 2, curY));
                        curY += (int)graph.MeasureString(strText, fontResultNumber).Height + 5;
                        strText = Convert.ToInt16(_dtHistory.Rows[_currentHisrory][5]).ToString() + " уд/мин";
                        graph.DrawString(strText, fontResultNumber, brushResult, new Point(
                            _rctWatch.X + _rctWatch.Width / 2 - (int)graph.MeasureString(strText, fontResultNumber).Width / 2, curY));
                        curY += (int)graph.MeasureString(strText, fontResultNumber).Height + 5;
                        strText = Convert.ToInt16(_dtHistory.Rows[_currentHisrory][6]).ToString() + " стресс";
                        graph.DrawString(strText, fontResultNumber, brushResult, new Point(
                            _rctWatch.X + _rctWatch.Width / 2 - (int)graph.MeasureString(strText, fontResultNumber).Width / 2, curY));
                    }
                    else if (!_firstShowHistory)
                    {
                        Font fontResultNo = new Font("Microsoft Sans Serif", 26, FontStyle.Regular);
                        SolidBrush brushResultNo = new SolidBrush(Color.FromArgb(230, 247, 255));
                        string strText = "Замеров нет";
                        graph.DrawString(strText, fontResultNo, brushResultNo, new Point(
                            _rctWatch.X + _rctWatch.Width / 2 - (int)graph.MeasureString(strText, fontResultNo).Width / 2,
                            _rctWatch.Y + _rctWatch.Height / 2 - (int)graph.MeasureString(strText, fontResultNo).Height / 2));
                    }
                    break;
            }
        }

        // создавать объект GraphicsPath, который будет содержать прямоугольник со скругленными краями
        private static GraphicsPath CreateRoundedRectangles(int x, int y, int width, int height, int radius)
        {
            int xw = x + width;
            int yh = y + height;
            int xwr = xw - radius;
            int yhr = yh - radius;
            int xr = x + radius;
            int yr = y + radius;
            int r2 = radius * 2;
            int xwr2 = xw - r2;
            int yhr2 = yh - r2;
            GraphicsPath p = new GraphicsPath();
            p.StartFigure();
            p.AddArc(x, y, r2, r2, 180, 90);
            p.AddLine(xr, y, xwr, y);
            p.AddArc(xwr2, y, r2, r2, 270, 90);
            p.AddLine(xw, yr, xw, yhr);
            p.AddArc(xwr2, yhr2, r2, r2, 0, 90);
            p.AddLine(xwr, yh, xr, yh);
            p.AddArc(x, yhr2, r2, r2, 90, 90);
            p.AddLine(x, yhr, x, yr);
            p.CloseFigure();
            return p;
        }


        private bool _lightWatch = false; // флаг, что часы подсвечены

        // курсор мыши на часах
        private void picWatch_MouseMove(object sender, MouseEventArgs e)
        {
            // подсветка кнопок
            bool flagMouseOnButton = false;
            if ((_lightButton != -1) && !CursorInRectangle(e, RctButton[_lightButton]))
            {
                picWatch.Invalidate(RctButton[_lightButton]);
                _lightButton = -1;
                picWatch.Update();
                picWatch.Cursor = Cursors.Default;
            }
            for (int i = 0; i < 5; i++)
                if (CursorInRectangle(e, RctButton[i]))
                {
                    if (EnableButton[i])
                    {
                        if ((_lightButton != -1) && (_lightButton != i))
                        {
                            picWatch.Invalidate(RctButton[_lightButton]);
                            _lightButton = -1;
                            picWatch.Update();
                        }
                        _lightButton = i;
                        picWatch.Invalidate(RctButton[i]);
                        picWatch.Update();
                        picWatch.Cursor = Cursors.Hand;
                        flagMouseOnButton = true;
                    }
                    else
                        if (picWatch.Cursor != Cursors.Default)
                            picWatch.Cursor = Cursors.Default;
                }
            // сами часы
            if (!flagMouseOnButton)
            {
                if (((_watchState == CurStations.Watch) && CursorInRectangle(e, _rctWatch)) ||
                    (((_watchState == CurStations.Menu) || (_watchState == CurStations.Pulse) || (_watchState == CurStations.History)) &&
                    (_templates == CurTemplates.WatchOnly) && (e.Y < _rctWatch.Top)))
                {
                    picWatch.Cursor = Cursors.Hand;
                    if (!_lightWatch)
                    {
                        picWatch.Image = (Bitmap)Resources.ResourceManager.GetObject("watch_" + _curWatchNumber.ToString("D2") + "_light");
                        _lightWatch = true;
                    }
                }
                else if (((_watchState == CurStations.Watch) && !CursorInRectangle(e, _rctWatch)) ||
                    (((_watchState == CurStations.Menu) || (_watchState == CurStations.Pulse) || (_watchState == CurStations.History)) &&
                    !(e.Y < _rctWatch.Top)))
                {
                    picWatch.Cursor = Cursors.Default;
                    if (_lightWatch)
                    {
                        picWatch.Image = (Bitmap)Resources.ResourceManager.GetObject("watch_" + _curWatchNumber.ToString("D2"));
                        _lightWatch = false;
                    }
                }
            }
        }

        // курсор мыши на форме
        private void frmMain_MouseMove(object sender, MouseEventArgs e)
        {
            if (_lightWatch)
            {
                picWatch.Image = (Bitmap)Resources.ResourceManager.GetObject("watch_" + _curWatchNumber.ToString("D2"));
                _lightWatch = false;
            }
            if (_lightButton != -1)
            {
                picWatch.Invalidate(RctButton[_lightButton]);
                _lightButton = -1;
                picWatch.Update();
            }
        }

        // проверка, что курсор мыши в прямоугольнике
        private static bool CursorInRectangle(MouseEventArgs e, Rectangle rct)
        {
            if (e.X >= rct.Left && e.X <= rct.Right && e.Y >= rct.Top && e.Y <= rct.Bottom)
                return true;
            else
                return false;
        }

        // нажатие мышью на часы
        private void picWatch_MouseDown(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < 5; i++)
                if (EnableButton[i] && CursorInRectangle(e, RctButton[i]))
                {
                    _lightButton = -1;
                    _pressButton = i;
                    picWatch.Invalidate(RctButton[i]);
                    picWatch.Update();
                }
        }

        // мышь отпущена
        private void picWatch_MouseUp(object sender, MouseEventArgs e)
        {
            if (_pressButton != -1)
            {
                int intPresentedButton = _pressButton;
                _pressButton = -1;
                picWatch.Invalidate(RctButton[intPresentedButton]);
                picWatch.Update();
                switch (intPresentedButton)
                {
                    case 0: PressOk(); break;
                    case 1: PressLeft(); break;
                    case 2: PressTop(); break;
                    case 3: PressRight(); break;
                    case 4: PressBottom(); break;
                }
            }
            if (((_watchState == CurStations.Menu) || (_watchState == CurStations.Pulse) || (_watchState == CurStations.History)) &&
                (_templates == CurTemplates.WatchOnly) && (e.Y < _rctWatch.Top))
                CloseWatch();
            else if ((_watchState == CurStations.Watch) && CursorInRectangle(e, _rctWatch))
                OpenWatch();
            _lightWatch = false;
        }

        // открытие часов
        private void OpenWatch()
        {
            StopLightElement();
            SetEnableArrows(false);
            _watchState = CurStations.WatchUp;
            picWatch.Cursor = Cursors.Default;
            _curWatchNumber = 1;
            tmrWatch.Interval = 30;
            tmrWatch.Start();
            lblStatus.Text = "";
        }

        // закрытие часов
        private void CloseWatch()
        {
            StopDeviceAndSound();
            _watchState = CurStations.WatchClose;
            picWatch.Cursor = Cursors.Default;
            SetEnableArrows(false);
            tmrWatch.Interval = 30;
            tmrWatch.Start();
            prbRec.Visible = false;
            lblStatus.Text = "";
        }


        // подстветка элементов для привлечения внимания
        bool _flagLightElement = false;
        byte _lightElement = 1;

        private void tmrLight_Tick(object sender, EventArgs e)
        {
            if (_lightElement != 0)
            {
                _flagLightElement = !_flagLightElement;
                if (_lightElement == 1)
                    picWatch.Invalidate(_rctWatch);
                else
                    picWatch.Invalidate(RctButton[0]);
                picWatch.Update();
            }
        }

        private void StartLightElement(byte el)
        {
            _flagLightElement = false;
            _lightElement = el;
            tmrLight.Start();
        }

        private void StopLightElement()
        {
            tmrLight.Stop();
            _flagLightElement = false;
        }


        private static Rectangle _rctWatch = new Rectangle(86, 120, 247, 263),
                                 _rctSignal = new Rectangle(58, 120, 10, 263),
                                 _rctBattery = new Rectangle(_rctWatch.X + 5, _rctWatch.Y + HeaderHeight/2 - 22/2, 71, 22);

        private ushort[] _pulseBuffer;
        private ushort _pulseBufferSize = 0, _pulseFirstCounter = 0;
        private int _goodSignal = 5000;

        // параметры верхней строки состояния
        private const int HeaderHeight = 32;
        private static int _signalLevel,
                           _batteryLevel;

        // определение среднего значения сигнала за запись
        private int _signalLevelSum, _signalLevelSumCount;

        private void tmrWatch_Tick(object sender, EventArgs e)
        {
            switch (_watchState)
            {
                case CurStations.Watch:
                    picWatch.Invalidate(_rctWatch);
                    picWatch.Update();
                    break;
                case CurStations.WatchUp:
                    picWatch.Image = (Bitmap)Resources.ResourceManager.GetObject("watch_" + _curWatchNumber.ToString("D2"));
                    if (_curWatchNumber < 23)
                        _curWatchNumber++;
                    else
                    {
                        tmrWatch.Stop();
                        if (!_firstShowHistory)
                            ShowMainMenu();
                        else
                        {
                            SetModeHistory();
                            _firstShowHistory = false;
                        }
                    }
                    break;
                case CurStations.Pulse:
                case CurStations.Rec:
                    if ((_device != null) && _device.DeviceStarted && (_device.DeviceMode == DeviceModes.FtdiNew || _device.DeviceMode == DeviceModes.Bluetooth))
                    {
                        int newBatteryLevel = -1;
                        if (_device.DeviceMode == DeviceModes.FtdiNew)
                            newBatteryLevel = _devicesList.GetVoltLevelById(((FtdiNewDevice)_device).EndDeviceId);
                        else if (_device.DeviceMode == DeviceModes.Bluetooth)
                            newBatteryLevel = ((BluetoothDevice)_device).GetVoltLevel;
                        if ((newBatteryLevel != -1) && (newBatteryLevel != _batteryLevel))
                        {
                            _batteryLevel = newBatteryLevel;
                            picWatch.Invalidate(_rctBattery);
                            picWatch.Update();
                        }
                    }

                    if (_pulseBufferSize > 1)
                    {
                        if (_pulseFirstCounter > 660)
                        {
                            if ((!EnableButton[0]) && (_watchState != CurStations.Rec))
                            {
                                SetEnableArrows(0);
                                lblStatus.Text = "Получение сигнала с приемного устройства...";
                                StartLightElement(2);
                            }

                            if (_clockSettings.FlagLevel)
                            {
                                Graphics graphPulse = picWatch.CreateGraphics();
                                LinearGradientBrush gradientBrush = new LinearGradientBrush(
                                    new Point(0, _rctSignal.Bottom),
                                    new Point(0, _rctSignal.Top), Color.Red, Color.Yellow);
                                ColorBlend colorBlend = new ColorBlend();
                                colorBlend.Colors = new Color[] { Color.Red, Color.Yellow, Color.Green };
                                colorBlend.Positions = new float[] { 0.0f, 0.5f, 1.0f };
                                gradientBrush.InterpolationColors = colorBlend;
                                float percent = Math.Max(0, Math.Min(100f, (_maxPulseBuffer - _minPulseBuffer) * 100f / _goodSignal));
                                picWatch.Invalidate(_rctSignal);
                                picWatch.Update();
                                graphPulse.FillRectangle(gradientBrush, new RectangleF(_rctSignal.X,
                                    _rctSignal.Bottom - _rctSignal.Height * percent / 100,
                                    _rctSignal.Width,
                                    _rctSignal.Height * percent / 100));
                                graphPulse.DrawRectangle(new Pen(Color.Black), _rctSignal);
                                int signalLevelCurrent = (int)percent;
                                if (signalLevelCurrent != _signalLevel)
                                {
                                    _signalLevel = signalLevelCurrent;
                                    picWatch.Invalidate(new Rectangle(_rctWatch.X + _rctWatch.Width / 2 + 8, _rctWatch.Y, _rctWatch.Width / 2 - 8, HeaderHeight));
                                    picWatch.Update();
                                }
                                if (_watchState == CurStations.Rec)
                                {
                                    _signalLevelSum += signalLevelCurrent;
                                    _signalLevelSumCount++;
                                }
                            }

                        }
                    }
                    break;
                case CurStations.WatchClose:
                    picWatch.Image = (Bitmap)Resources.ResourceManager.GetObject("watch_" + _curWatchNumber.ToString("D2"));
                    if (_curWatchNumber > 0)
                        _curWatchNumber--;
                    else
                    {
                        _watchState = CurStations.Watch;
                        tmrWatch.Interval = 500;
                        lblStatus.Text = "Настройте параметры, и нажмите на крышку часов для начала работы.";
                        StartLightElement(1);
                    }
                    break;
                case CurStations.History:
                    switch (_templates)
                    {
                        case CurTemplates.MoveWatchRight:
                            if (picWatch.Location.X < _newPicWatchX)
                                picWatch.Location = new Point(picWatch.Location.X + 5,
                                    menuStripMain.Height + (this.ClientSize.Height - menuStripMain.Height - statusStripMain.Height - picWatch.Height) / 2);
                            else
                            {
                                picTemplates.Image = SetImgOpacity(_templatesImage, _templatesTransparent);
                                picTemplates.Location = new Point(_newPicTemplatesX, picWatch.Location.Y);
                                picTemplates.Visible = true;
                                _templates = CurTemplates.TransparentOn;
                            }
                            break;
                        case CurTemplates.TransparentOn:
                            if (_templatesTransparent < 1)
                            {
                                _templatesTransparent += 0.1f;
                                picTemplates.Image = SetImgOpacity(_templatesImage, _templatesTransparent);
                            }
                            else
                            {
                                _templates = CurTemplates.Templates;
                                pnlWave.Location = new Point(picTemplates.Location.X + picTemplates.Width,
                                    picTemplates.Top + 230);
                                pnlWave.Width = picWatch.Left - picTemplates.Right + 31;
                                pnlWave.Visible = true;
                                tmrWatch.Stop();
                                SetEnableArrows(0);
                                SetEnableArrows(1, (_currentHisrory < (_dtHistory.Rows.Count - 1)));
                                SetEnableArrows(3, (_currentHisrory > 0));
                                mnuTemplates.Enabled = true;
                            }
                            break;
                        case CurTemplates.MoveWatchCenter:
                            if (picWatch.Location.X > (this.ClientSize.Width - picWatch.Width) / 2)
                                picWatch.Location = new Point(picWatch.Location.X - 5,
                                    menuStripMain.Height + (this.ClientSize.Height - menuStripMain.Height - statusStripMain.Height - picWatch.Height) / 2);
                            else
                            {
                                _templates = CurTemplates.WatchOnly;
                                tmrWatch.Stop();
                                SetEnableArrows(0);
                                SetEnableArrows(1, (_currentHisrory < (_dtHistory.Rows.Count - 1)));
                                SetEnableArrows(3, (_currentHisrory > 0));
                                mnuTemplates.Enabled = true;
                            }
                            break;
                    }
                    break;
            }
        }

        // установка прозрачности картинки
        public static Image SetImgOpacity(Image imgPic, float imgOpac)
        {
            Bitmap bmpPic = new Bitmap(imgPic.Width, imgPic.Height);
            Graphics gfxPic = Graphics.FromImage(bmpPic);
            ColorMatrix cmxPic = new ColorMatrix {Matrix33 = imgOpac};
            ImageAttributes iaPic = new ImageAttributes();
            iaPic.SetColorMatrix(cmxPic, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            gfxPic.DrawImage(imgPic, new Rectangle(0, 0, bmpPic.Width, bmpPic.Height), 0, 0, imgPic.Width, imgPic.Height, GraphicsUnit.Pixel, iaPic);
            gfxPic.Dispose();
            return bmpPic;
        }


        //*****************************************************************
        //                  Прорисовка пульсовой волны                    *
        //*****************************************************************

        private readonly Rectangle _rctPulse = new Rectangle(_rctWatch.X, _rctWatch.Y + HeaderHeight, _rctWatch.Width,
                                                            _rctWatch.Height - HeaderHeight*2);
        private float _pulseTension = 1.0f;
        private Point[] _pulsePoints;
        private readonly Pen _penPulse = new Pen(Color.Yellow, 2);
        private const ushort PulseHeight = 125;
        private static readonly ushort PulseTopBottom = (ushort)((_rctWatch.Height - PulseHeight) / 2);
        private ushort _minPulseBuffer, _maxPulseBuffer;

        private void bkwPulseWave_DoWork(object sender, DoWorkEventArgs e)
        {
            if (bkwPulseWave.CancellationPending)
                return;
            if (_pulseBufferSize > 1)
            {
                if (_pulseFirstCounter > 660)
                {
                    ushort newMinPulseBuffer = _pulseBuffer.Min();
                    ushort newMaxPulseBuffer = _pulseBuffer.Max();
                    if (newMinPulseBuffer <= _minPulseBuffer)
                        _minPulseBuffer = newMinPulseBuffer;
                    else
                        _minPulseBuffer += (ushort)((newMinPulseBuffer - _minPulseBuffer) / 10);
                    if (newMaxPulseBuffer >= _maxPulseBuffer)
                        _maxPulseBuffer = newMaxPulseBuffer;
                    else
                        _maxPulseBuffer -= (ushort)((_maxPulseBuffer - newMaxPulseBuffer) / 10);
                }

                int difference = _maxPulseBuffer - _minPulseBuffer;
                if (difference > 0)
                {
                    _pulseTension = 1.0f;
                    _pulsePoints = new Point[_pulseBufferSize];
                    ushort h;
                    for (int i = _pulsePoints.Length - 1; i >= 0; i--)
                    {
                        h = (ushort)(_pulseBuffer[i] - _minPulseBuffer);
                        _pulsePoints[i] = new Point(_rctWatch.Right - 2 - (_pulseBufferSize - 1 - i),
                            (int)(_rctWatch.Bottom - PulseTopBottom - Math.Abs(h * PulseHeight / difference)));
                        if ((i > 0) && (Math.Abs(_pulseBuffer[i] - _pulseBuffer[i - 1]) > (difference / 4)))
                            _pulseTension = 0;
                    }

                }
            }
        }

        private void bkwPulseWave_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if ((_watchState == CurStations.Pulse) || (_watchState == CurStations.Rec))
                {
                    Graphics graphPulse = picWatch.CreateGraphics();
                    BufferedGraphicsContext currentContext = BufferedGraphicsManager.Current;
                    using (BufferedGraphics myBuffer = currentContext.Allocate(graphPulse, _rctPulse))
                    {
                        myBuffer.Graphics.Clear(Color.FromArgb(17, 17, 17));
                        if (_pulsePoints != null)
                        {
                            myBuffer.Graphics.SmoothingMode = SmoothingMode.HighQuality;
                            myBuffer.Graphics.DrawCurve(_penPulse, _pulsePoints, _pulseTension);
                        }
                        myBuffer.Render();
                    }
                }
            }
            catch { }
        }

        
        //*****************************************************************
        //                    Работа с оборудованием                      *
        //*****************************************************************

        // устройство
        private IDevicePulse _device;

        private bool _flagNeedToDisposeDevice = false;

        // событие новых данных с прибора
        private void NewPulseData(Object sender, NewPulseEventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new EventHandler<NewPulseEventArgs>(NewPulseData), new object[] { sender, e });
            }
            else
            {
                AddPulseBuffer(e.Value);
            }
        }

        // событие подключения или отключения КУ
        private void EndDeviceStatus(Object sender, FtdiNewDevice.EndDeviceStatusEventArgs e)
        {
            if (InvokeRequired)
            {
                BeginInvoke(new EventHandler<FtdiNewDevice.EndDeviceStatusEventArgs>(EndDeviceStatus), new object[] { sender, e });
            }
            else
            {
                // отключено
                if (!e.Connected)
                {
                    if (_flagFormIsClosing || _flagNeedToDisposeDevice)
                    {
                        _flagNeedToDisposeDevice = false;
                        if (_device != null && _device.DeviceMode == DeviceModes.FtdiNew)
                        {
                            ((FtdiNewDevice)_device).EndDeviceStatus -= EndDeviceStatus;
                            _device.Dispose();
                        }
                    }
                    if (_flagFormIsClosing)
                        this.Close();
                }
            }
        }

        // список КУ
        private readonly DevicesList _devicesList;

        // запуск оборудования и звука
        private void StartDeviceAndSound()
        {
            _pulseBuffer = new ushort[_rctWatch.Width - 4];
            _pulseBufferSize = 0;
            _minPulseBuffer = (ushort)((_clockSettings.DeviceMode == DeviceModes.Usb) ? 1400 : 9000);
            _maxPulseBuffer = (ushort)((_clockSettings.DeviceMode == DeviceModes.Usb) ? 1600 : 11000);
            _pulsePoints = null;
            _pulseFirstCounter = 0;
            _signalLevel = 0;
            _batteryLevel = -1;
            picWatch.Refresh();
            if (_clockSettings.FlagSoundPulse)
                StartSoundPulse();
            _goodSignal = ((_clockSettings.DeviceMode == DeviceModes.Usb) ? 5000 : 6500);
            _flagNeedToDisposeDevice = false;
            try
            {
                switch (_clockSettings.DeviceMode)
                {
                    case DeviceModes.Usb:
                        _device = new UsbHidDevice(_clockSettings.VendorId, _clockSettings.ProductId);
                        break;
                    case DeviceModes.UsbEc:
                        _device = new UsbEcDevice(_clockSettings.UsbEcCom, _clockSettings.Dac1, _clockSettings.Dac2);
                        break;
                    case DeviceModes.FtdiOld:
                        _device = new FtdiDevice();
                        break;
                    case DeviceModes.FtdiNew:
                        if (_device == null || _device.DeviceMode != DeviceModes.FtdiNew)
                        {
                            _device = new FtdiNewDevice(_devicesList, _clockSettings.SrcId);
                            ((FtdiNewDevice)_device).EndDeviceStatus += EndDeviceStatus;
                        }
                        break;
                    case DeviceModes.Bluetooth:
                        _device = new BluetoothDevice(_clockSettings.BtId, _clockSettings.BtPin);
                        break;
                }
                _device.NewPulse += NewPulseData;
                _device.StartDevice();
            }
            catch (SocketException exc)
            {
                MessageBox.Show(String.Format(
                    "Возникла ошибка при попытке установления связи с оборудованием:{0}{0}{1}{0}{0}{2}",
                    Environment.NewLine,
                    exc.Message, "Код ошибки сокета: " + exc.ErrorCode),
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                StopDeviceAndSound();
                ShowMainMenu();
            }
            catch (Exception exc)
            {
                MessageBox.Show(String.Format(
                    "Возникла ошибка при попытке установления связи с оборудованием:{0}{0}{1}",
                    Environment.NewLine,
                    exc.Message), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                StopDeviceAndSound();
                ShowMainMenu();
            }
        }

        // остановка оборудования и звука
        private void StopDeviceAndSound()
        {
            bkwPulseWave.CancelAsync();
            bkwSound.CancelAsync();
            tmrWatch.Stop();
            if (_device != null)
            {
                try
                {
                    _device.NewPulse -= NewPulseData;
                    _device.StopDevice();
                    if (_device.DeviceMode != DeviceModes.FtdiNew)
                        _device.Dispose();
                }
                catch (Exception exc)
                {
                    MessageBox.Show(String.Format(
                        "Возникла ошибка при попытке отключения устройства:{0}{0}{1}",
                        Environment.NewLine,
                        exc.Message), "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            StopSoundPulse();
            StopSoundResult();
        }

        // инициализация устройства для фонового опроса КУ
        private void InitFonDevice(bool showMessages = true)
        {
            try
            {
                if (_clockSettings.DeviceMode == DeviceModes.FtdiNew)
                {
                    if (_device == null || _device.DeviceMode != DeviceModes.FtdiNew)
                        _device = new FtdiNewDevice(_devicesList, _clockSettings.SrcId);
                    else if (!_device.DeviceStarted)
                    {
                        _device.Dispose();
                        _device = new FtdiNewDevice(_devicesList, _clockSettings.SrcId);
                    }
                }
                else if (_device != null && _device.DeviceMode == DeviceModes.FtdiNew)
                {
                    _flagNeedToDisposeDevice = true;
                    _device.StopDevice();
                }
            }
            catch (Exception exc)
            {
                if (showMessages)
                    MessageBox.Show(String.Format(
                        "Возникла ошибка при инициализации беспроводного устройства:{0}{0}{1}",
                        Environment.NewLine, exc.Message));
            }
        }

        //*****************************************************************
        //               Общие пункты меню и операции                     *
        //*****************************************************************

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_watchState == CurStations.Rec)
            {
                DialogResult result = MessageBox.Show("Вы хотите прервать запись фрагмента волны?",
                    "Внимание", MessageBoxButtons.OKCancel, MessageBoxIcon.Stop);
                if (result != System.Windows.Forms.DialogResult.OK)
                {
                    e.Cancel = true;
                    return;
                }
            }
            bool lastFlagFormIsClosing = _flagFormIsClosing;
            _flagFormIsClosing = true;
            SetEnableArrows(false);
            StopLightElement();
            // отключаем оборудование и звук
            StopDeviceAndSound();
            if (_device != null && _device.DeviceMode == DeviceModes.FtdiNew && ((FtdiNewDevice)_device).ConnectedEndDevice)
            {
                if (!lastFlagFormIsClosing)
                {
                    lblStatus.Text = "Отключение от беспроводного конечного устройства";
                    MessageBox.Show("Подождите, отключаемся от беспроводного конечного устройства...", "Подождите",
                                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Cancel = true;
                    picWatch.Refresh();
                    return;
                }
                if (MessageBox.Show("Не удалось отключиться от беспроводного конечного устройства.\n" +
                                    "Все равно продолжить закрытие?", "Закрытие",
                                    MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
                                    MessageBoxDefaultButton.Button2) != DialogResult.Yes)
                {
                    ((FtdiNewDevice)_device).EndDeviceStatus -= EndDeviceStatus;
                    _device.Dispose();
                }
                else
                {
                    e.Cancel = true;
                    return;
                }
            }
            // Сохранение параметров
            _clockSettings.Save();
            string strError = string.Empty;
            if (!_devicesList.WriteListToTextFile(ConstGlobals.DevicesFileFullName, ref strError))
                MessageBox.Show("Возникла ошибка при сохранении списка приемных устройств:\n" + strError,
                                "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            // Результат диалога
            this.DialogResult = _flagOkResult ? DialogResult.OK : DialogResult.Cancel;
        }


        //*****************************************************************
        //                      Установка процентов                       *
        //*****************************************************************

        private void SetPrc(byte val)
        {
            prbRec.Value = val;
        }


        //*****************************************************************
        //                          Параметры                             *
        //*****************************************************************

        /* ----- настройка параметров ----- */
        private void mnuProperties_Click(object sender, EventArgs e)
        {
            bool flagPulseMode = (_watchState == CurStations.Connecting || _watchState == CurStations.Pulse ||
                                  _watchState == CurStations.Rec);
            frmProperties myFormProperties = new frmProperties(_clockSettings, _devicesList, _device, InitFonDevice, flagPulseMode);
            myFormProperties.ShowDialog();
            if (myFormProperties.DialogResult == DialogResult.OK)
            {
                SetMenu();
                if (_watchState == CurStations.Pulse || _watchState == CurStations.Rec)
                    picWatch.Refresh();
                //InitFonDevice();
            }
        }


        //*****************************************************************
        //                       Запись фрагмента                         *
        //*****************************************************************

        private PulseWave _myPulseWave;
        private List<ushort> _lstPulseWaveRecord;
        private bool _flagGongSound;
        private WaveChannel32 _soundRec32;
        private WaveMixerStream32 _mixerRec;
        private int _tickCountBeginRecord;

        // добавление значения в буфер
        private void AddPulseBuffer(ushort val)
        {
            if (_pulseBuffer != null)
            {
                if (_watchState == CurStations.Connecting)
                {
                    _watchState = CurStations.Pulse;
                    lblStatus.Text = "Подождите, пожалуйста... идет настройка оборудования...";
                    picWatch.Invalidate(_rctWatch);
                    picWatch.Update();
                }
                if (_pulseFirstCounter <= 660)
                {
                    val = (ushort)((_clockSettings.DeviceMode == DeviceModes.Usb) ? 1500 : 10000);
                    _pulseFirstCounter++;
                }
                else if (Console.CapsLock)
                    val = (ushort)(ushort.MaxValue - val);


                if (_pulseBufferSize < _pulseBuffer.Length)
                    _pulseBufferSize++;
                else
                    for (int i = 0; i < _pulseBuffer.Length - 1; i++)
                        _pulseBuffer[i] = _pulseBuffer[i + 1];
                _pulseBuffer[_pulseBufferSize - 1] = val;
                if (!bkwPulseWave.IsBusy)
                    bkwPulseWave.RunWorkerAsync();

                if (_clockSettings.FlagSoundPulse && _waveOutPulse != null &&
                    _waveOutPulse.PlaybackState != PlaybackState.Playing &&
                    (_device != null && _device.DeviceStarted) &&
                    _pulseFirstCounter > 660 && !bkwSound.IsBusy)
                {
                    bkwSound.RunWorkerAsync();
                }

                // запись
                if (_watchState == CurStations.Rec)
                {
                    int msRecord = Math.Min(20000, Environment.TickCount - _tickCountBeginRecord);
                    if (msRecord < 20000)
                    {
                        _lstPulseWaveRecord.Add(val);
                        SetPrc((byte)(msRecord * prbRec.Maximum / 20000));
                    }
                    else
                        StopRecord();
                    //if (_myPulseWave.GetPulseWrited() < _myPulseWave.GetPulseLength())
                    //{
                    //    _myPulseWave.AddPulse(val);
                    //    SetPrc((byte)(_myPulseWave.GetPulseWrited() * prbRec.Maximum / _myPulseWave.GetPulseLength()));
                    //}
                    //else
                    //    StopRecord();
                }
            }
        }

        private void StartRecord()
        {
            SetEnableArrows(false);
            _flagOkResult = false;
            //_myPulseWave = new PulseWaveField(_clockSettings.DeviceMode == DeviceModes.ComNew ? 4445 : 4000);
            prbRec.Value = prbRec.Minimum;
            prbRec.Visible = true;
            lblStatus.Text = "Запись фрагмента 20 секунд...";
            _lstPulseWaveRecord = new List<ushort>();
            _tickCountBeginRecord = Environment.TickCount;
            _watchState = CurStations.Rec;
            picWatch.Refresh();
            _signalLevelSum = 0;
            _signalLevelSumCount = 0;
            if (_clockSettings.FlagSoundPulse)
            {
                bkwSound.CancelAsync();
                try
                {
                    // микшер
                    _mixerRec = new WaveMixerStream32 {AutoStop = true};

                    WaveFileReader sVoice;
                    if (_clockSettings.Voice == 1)
                        sVoice = new WaveFileReader(@"sound/voice_1/record/rec.wav");
                    else
                        sVoice = new WaveFileReader(@"sound/voice_0/record/rec.wav");
                    _mixerRec.AddInputStream(new WaveChannel32(sVoice) {Volume = _clockSettings.VoiceVolume});

                    var sRec = new WaveFileReader(@"sound/record/rec_" +
                                              (_clockSettings.Rec == 1 ? "1" : "0") + ".wav");
                    _soundRec32 = new WaveChannel32(sRec) {Volume = 0f};
                    _mixerRec.AddInputStream(_soundRec32);
                    if ((_waveOutRec = new WaveOut()) != null)
                    {
                        _waveOutRec.Init(_mixerRec);
                        _waveOutRec.Play();
                        _flagPlayingRec = true;
                    }
                }
                catch (Exception exc)
                {
                    StopSoundPulse();
                    MessageBox.Show("Возникла ошибка при работе со звуком:\n" +
                                    exc.Message,
                                    "Ошибка",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void StopRecord()
        {
            int srcIdCurrent = 0;
            if (_device != null && _device.DeviceMode == DeviceModes.FtdiNew)
                srcIdCurrent = ((FtdiNewDevice) _device).EndDeviceId;
            _watchState = CurStations.Pulse;
            // останавливаем оборудование
            StopDeviceAndSound();
            prbRec.Visible = false;
            // вывод диагноза на экран
            _watchState = CurStations.Saving;
            picWatch.Refresh();
            lblStatus.Text = "Данные сохраняются в базу...";

            _myPulseWave = new PulseWave(_lstPulseWaveRecord.ToArray());
            _myPulseWave.AnalyzePulseWave();

            //int msRecord = Environment.TickCount - _tickCountBeginRecord;
            //try
            //{
            //    using (StreamWriter myStreamWriter = new StreamWriter(ConstGlobals.FrequencyFileFullName, true))
            //    {
            //        myStreamWriter.WriteLine("Режим = " + _clockSettings.DeviceMode + ";\tдискрет = " + _myPulseWave.GetPulseLength() +
            //            ";\tмиллисекунд = " + msRecord + ";\tчастота = " + _myPulseWave.GetPulseLength() * 1000f / msRecord);
            //    }
            //}
            //catch (Exception exc)
            //{
            //    MessageBox.Show("Возникла ошибка при записи статистики замеров:\n" +
            //                    exc.Message,
            //                    "Ошибка",
            //                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}

            // глобал
            var calc = new GlobalCalc(_myPulseWave.PulseWaveField);
            calc.AutoRepers();
            calc.MakeCalc(_patientSex, _patientAge, _patientGrowth, _patientWeight, _globalSettings.J, _globalSettings.N,
                          _globalSettings.H,
                          _globalSettings.I, _globalSettings.R);
            _myPulseWave.Repers = calc.Repers;

            byte topPressureResult, lowerPressureResult;
            _myPulseWave.AnalyzePulseWave();
            _patientFss = _myPulseWave.PulseFSS;
            _patientA2A1 = _myPulseWave.PulseA2A1;
            int sosudResult = _myPulseWave.GetSosud(_patientA2A1, _patientS1, _patientS2),
                stressResult = _myPulseWave.GetStress();
            // если это 1-й замер у пациента - калибровка
            if (_idControlMes < 0)
            {
                topPressureResult = _topPressureControl;
                lowerPressureResult = _lowerPressureControl;
                double polynomTop = PulseWave.PolynomTop(_patientA2A1, _patientFss, _patientFssOpor, _patientK, _patientA, _patientB),
                        polynomLower = PulseWave.PolynomLower(_patientA2A1, _patientFss, _patientFssOpor, _patientK, _patientC, _patientD);
                _patientK1 = (float)(230f * polynomTop / _topPressureControl);
                _patientK2 = (float)(230f * polynomLower / _lowerPressureControl);
            }
            else
                _myPulseWave.CalculateAD(_patientA2A1, _patientFss, _patientFssOpor, _patientK1, _patientK2, _patientK, _patientA, _patientB, _patientC, _patientD,
                    out topPressureResult, out lowerPressureResult);
            string deviceSrcId = string.Empty;
            if (_clockSettings.DeviceMode == DeviceModes.FtdiNew)
                deviceSrcId = srcIdCurrent.ToString(_myDbCultInfo);
            else if (_clockSettings.DeviceMode == DeviceModes.Bluetooth)
                deviceSrcId = _clockSettings.BtId;
            
            // сохранение в базу
            try
            {
                using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    OleDbCommand myOleDbCommand = new OleDbCommand(
                        "INSERT INTO Measurements " +
                        "       (patient, mesDateTime, " +
                        "        sosud, topPressure, lowerPressure, pulse, stress, control, " +
                        "        a2a1, s1, s2, fssOpor, k1, k2, k, a, b, c, d, signal, deviceMode, deviceSrcId, " +
                        "        globalA1, globalA2, globalA3, globalA4, globalA5, " +
                        "        globalTop, globalLower, age, blood) " +
                        "VALUES (" + _idPatient + ", " + DateTime.Now.ToString("#yyyy-MM-dd HH:mm:ss#") + ", " +
                        sosudResult + ", " + topPressureResult + ", " + lowerPressureResult + ", " +
                        _patientFss + ", " + stressResult + ", " + ((_idControlMes < 0) ? "true" : "false") + ", " +
                        _patientA2A1.ToString(_myDbCultInfo) + ", " +
                        _patientS1.ToString(_myDbCultInfo) + ", " + _patientS2.ToString(_myDbCultInfo) + ", " + _patientFssOpor + ", " +
                        _patientK1.ToString(_myDbCultInfo) + ", " + _patientK2.ToString(_myDbCultInfo) + ", " + _patientK.ToString(_myDbCultInfo) + ", " +
                        _patientA.ToString(_myDbCultInfo) + ", " + _patientB.ToString(_myDbCultInfo) + ", " + _patientC.ToString(_myDbCultInfo) + ", " +
                        _patientD.ToString(_myDbCultInfo) + ", " + (_signalLevelSum / _signalLevelSumCount) + ", " +
                        (short)_clockSettings.DeviceMode + ", '" + deviceSrcId + "', " +
                        calc.Repers[0] + ", " + calc.Repers[1] + ", " + calc.Repers[2] + ", " +
                        calc.Repers[3] + ", " + calc.Repers[4] + ", " +
                        calc.ResultTopPressure + ", " + calc.ResultLowerPressure + ", " +
                        _myPulseWave.CalcAge() + ", " + _myPulseWave.CalcBlood(_patientK1) + ")",
                        myOleDbConnection);
                    if (myOleDbCommand.ExecuteNonQuery() <= 0)
                        throw new Exception("Не удалось вставить новые данные о замере.");
                    myOleDbCommand.CommandText = "SELECT @@IDENTITY";
                    _idControlMes = (int)myOleDbCommand.ExecuteScalar();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                    exc.Message + "\n\n" +
                    "Пожалуйста, обратитесь к администратору.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                CloseWatch();
                return;
            }
            // сохранение в файл
            DirectoryInfo dir = new DirectoryInfo(ConstGlobals.MeasurementsFolder);
            if (!dir.Exists)
                dir.Create();
            string strFileName = dir.FullName + _idControlMes.ToString() + ".dat",
                strError = "";
            if (_myPulseWave.SavePulseToBinFile(strFileName, ref strError))
            {
                _flagOkResult = true;
                lblStatus.Text = "Данные успешно сохранены. Нажмите на кнопку ОК для возврата в меню.";
            }
            else
                MessageBox.Show(this, "При сохранении файла '" + Path.GetFileName(strFileName) + "' произошла ошибка:\n" + strError,
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            // переход в режим истории
            _flagGongSound = true;
            SetModeHistory();
            //StartLightElement(2);
        }


        //*****************************************************************
        //                             Кнопки                             *
        //*****************************************************************

        // состояния кнопок
        private readonly bool[] EnableButton = new bool[5];
        private int _lightButton = -1,      // кнопка, которая сейчас подсвечена (-1 - никакая)
                    _pressButton = -1;      // кнопка, которая сейчас нажата (-1 - никакая)

        // установка доступности одной кнопки
        private void SetEnableArrows(int btnNum, bool flagEnable = true)
        {
            EnableButton[btnNum] = flagEnable;
            picWatch.Invalidate(RctButton[btnNum]);
            picWatch.Update();
        }

        // установка доступности всех кнопок
        private void SetEnableArrows(bool flagEnable = true)
        {
            for (int i = 0; i < 5; i++)
                EnableButton[i] = flagEnable;
            picWatch.Invalidate(_rctArrows);
            picWatch.Update();
        }

        // прямоугольники кнопок
        private static Rectangle _rctArrows = new Rectangle(78, 397, 264, 208);
        private static readonly Rectangle[] RctButton = new Rectangle[]{
            new Rectangle(_rctArrows.X + _rctArrows.Width / 2 - 80 / 2, _rctArrows.Y + _rctArrows.Height / 2 - 70 / 2, 80, 70),
            new Rectangle(_rctArrows.X, _rctArrows.Y + _rctArrows.Height / 2 - 64 / 2, 87, 64),
            new Rectangle(_rctArrows.X + _rctArrows.Width / 2 - 87 / 2, _rctArrows.Y, 87, 64),
            new Rectangle(_rctArrows.Right - 87, _rctArrows.Y + _rctArrows.Height / 2 - 64 / 2, 87, 64),
            new Rectangle(_rctArrows.X + _rctArrows.Width / 2 - 87 / 2, _rctArrows.Bottom - 64, 87, 64),
        };

        // нажатие на кнопку ОК
        private void PressOk()
        {
            StopLightElement();
            if (_watchState == CurStations.Menu)
            {
                switch (_mainMenuSelected)
                {
                    case 0:
                        SetModePulseWave();
                        break;
                    case 1:
                        SetModeKontrol();
                        break;
                    case 2:
                        SetModeHistory();
                        break;
                }
            }
            else if (_watchState == CurStations.Control)
            {
                if (NumbersToPressure())
                {
                    SetEnableArrows(false);
                    SavePressureToDb();
                    ShowMainMenu();
                }
                else
                    MessageBox.Show("Верхнее или нижнее давление не может быть больше 230.\n" +
                        "Пожалуйста, задайте корректное значение!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (_watchState == CurStations.Pulse)
                StartRecord();
            else if (_watchState == CurStations.History)
            {
                StopSoundResult();
                if (_templates == CurTemplates.Templates) // шаблоны
                {
                    _templatesMode = 0;
                    StartHideTemplates();
                }
                else
                {
                    mnuTemplates.Enabled = false;
                    mnuMolodimetr.Enabled = false;
                    ShowMainMenu();
                }
            }
        }

        // нажатие на кнопку Left
        private void PressLeft()
        {
            if (_watchState == CurStations.Control)
            {
                _selectedNumber--;
                picWatch.Invalidate(_rctWatch);
                picWatch.Update();
                SetEnableArrows(1, _selectedNumber > 0);
                SetEnableArrows(2, _numbersPressure[_selectedNumber] < 9);
                SetEnableArrows(3, _selectedNumber < 5);
                SetEnableArrows(4, _numbersPressure[_selectedNumber] > 0);
            }
            else if (_watchState == CurStations.History)
            {
                _currentHisrory++;
                LoadHistoryData();
                if (_clockSettings.FlagSoundResult)
                    StartSoundResult();
                SetEnableArrows(1, (_currentHisrory < (_dtHistory.Rows.Count - 1)));
                SetEnableArrows(3, (_currentHisrory > 0));
            }
        }

        // нажатие на кнопку Right
        private void PressRight()
        {
            if (_watchState == CurStations.Control)
            {
                _selectedNumber++;
                picWatch.Invalidate(_rctWatch);
                picWatch.Update();
                SetEnableArrows(1, _selectedNumber > 0);
                SetEnableArrows(2, _numbersPressure[_selectedNumber] < 9);
                SetEnableArrows(3, _selectedNumber < 5);
                SetEnableArrows(4, _numbersPressure[_selectedNumber] > 0);
            }
            else if (_watchState == CurStations.History)
            {
                _currentHisrory--;
                LoadHistoryData();
                if (_clockSettings.FlagSoundResult)
                    StartSoundResult();
                SetEnableArrows(1, (_currentHisrory < (_dtHistory.Rows.Count - 1)));
                SetEnableArrows(3, (_currentHisrory > 0));
            }
        }

        // нажатие на кнопку Top
        private void PressTop()
        {
            if (_watchState == CurStations.Menu)
            {
                _mainMenuSelected--;
                picWatch.Invalidate(_rctWatch);
                picWatch.Update();
                SetEnableArrows(2, _mainMenuSelected > 0);
                SetEnableArrows(4, _mainMenuSelected < 2);
            }
            else if (_watchState == CurStations.Control)
            {
                _numbersPressure[_selectedNumber]++;
                picWatch.Invalidate(_rctWatch);
                picWatch.Update();
                SetEnableArrows(2, _numbersPressure[_selectedNumber] < 9);
                SetEnableArrows(4, _numbersPressure[_selectedNumber] > 0);
            }
        }

        // нажатие на кнопку Bottom
        private void PressBottom()
        {
            if (_watchState == CurStations.Menu)
            {
                _mainMenuSelected++;
                picWatch.Invalidate(_rctWatch);
                picWatch.Update();
                SetEnableArrows(2, _mainMenuSelected > 0);
                SetEnableArrows(4, _mainMenuSelected < 2);
            }
            else if (_watchState == CurStations.Control)
            {
                _numbersPressure[_selectedNumber]--;
                picWatch.Invalidate(_rctWatch);
                picWatch.Update();
                SetEnableArrows(2, _numbersPressure[_selectedNumber] < 9);
                SetEnableArrows(4, _numbersPressure[_selectedNumber] > 0);
            }
        }

        // нажатие на клавиатуре

        // нажата клавиша клавиатуры
        private void frmMain_KeyDown(object sender, KeyEventArgs e)
        {
            int i = -1;
            switch (e.KeyValue)
            {
                case 13: i = 0; break;
                case 37: i = 1; break;
                case 38: i = 2; break;
                case 39: i = 3; break;
                case 40: i = 4; break;
                case 32:
                    if (!_lightWatch && ((_watchState == CurStations.Watch) ||
                    (_watchState == CurStations.Menu) || (_watchState == CurStations.Pulse) || (_watchState == CurStations.History)) &&
                    (_templates == CurTemplates.WatchOnly))
                    {
                        picWatch.Image = (Bitmap)Resources.ResourceManager.GetObject("watch_" + _curWatchNumber.ToString("D2") + "_light");
                        _lightWatch = true;
                    }
                    break;
                case 112:
                    frmAboutBox myFormAbout = new frmAboutBox();
                    myFormAbout.ShowDialog();
                    break;
            }
            if ((i >= 0) && EnableButton[i])
            {
                _lightButton = -1;
                _pressButton = i;
                picWatch.Invalidate(RctButton[i]);
                picWatch.Update();
            }
        }

        // отпущена клавиша клавиатуры
        private void frmMain_KeyUp(object sender, KeyEventArgs e)
        {
            int i = -1;
            switch (e.KeyValue)
            {
                case 13: i = 0; break;
                case 37: i = 1; break;
                case 38: i = 2; break;
                case 39: i = 3; break;
                case 40: i = 4; break;
                case 32:
                    if (((_watchState == CurStations.Menu) || (_watchState == CurStations.Pulse) || (_watchState == CurStations.History)) &&
                        (_templates == CurTemplates.WatchOnly))
                        CloseWatch();
                    else if (_watchState == CurStations.Watch)
                        OpenWatch();
                    _lightWatch = false;
                    break;
                case 48: case 49: case 50: case 51: case 52: case 53: case 54: case 55: case 56: case 57:
                    if (_watchState == CurStations.Control)
                    {
                        _numbersPressure[_selectedNumber] = (byte)(e.KeyValue - 48);
                        if (_selectedNumber < 5)
                            _selectedNumber++;
                        picWatch.Invalidate(_rctWatch);
                        picWatch.Update();
                        SetEnableArrows(1, _selectedNumber > 0);
                        SetEnableArrows(2, _numbersPressure[_selectedNumber] < 9);
                        SetEnableArrows(3, _selectedNumber < 5);
                        SetEnableArrows(4, _numbersPressure[_selectedNumber] > 0);
                    }
                    break;
            }
            if (_pressButton != -1)
            {
                picWatch.Invalidate(RctButton[_pressButton]);
                _pressButton = -1;
                picWatch.Update();
                switch (i)
                {
                    case 0: PressOk(); break;
                    case 1: PressLeft(); break;
                    case 2: PressTop(); break;
                    case 3: PressRight(); break;
                    case 4: PressBottom(); break;
                }
            }
        }


        //*****************************************************************
        //                 Режимы  +  контроль тонометром                 *
        //*****************************************************************

        // переход в режим ПВ
        private void SetModePulseWave()
        {
            _watchState = CurStations.Connecting;
            SetEnableArrows(false);
            //picWatch.Refresh();
            lblStatus.Text = "Идет подключение к устройству...";
            this.Refresh();
            StartDeviceAndSound();
            tmrWatch.Interval = 500;
            tmrWatch.Start();
        }

        // переход в режим контроля давления тонометром
        private void SetModeKontrol()
        {
            _numbersPressure = GetPressureNumbers();
            _watchState = CurStations.Control;
            _selectedNumber = 0;
            picWatch.Refresh();
            SetEnableArrows(false);
            SetEnableArrows(0);
            SetEnableArrows(2, _numbersPressure[_selectedNumber] < 9);
            SetEnableArrows(3);
            SetEnableArrows(4, _numbersPressure[_selectedNumber] > 0);
            lblStatus.Text = "Введите с помощью стрелок показания контрольного тонометра.";
        }

        // давление
        private byte _topPressureControl = 120,
                     _lowerPressureControl = 80,
                     _selectedNumber;

        private byte[] _numbersPressure;

        // значение давления -> в отдельные цифры
        private byte[] GetPressureNumbers()
        {
            byte[] result = new byte[6];
            result[0] = (byte)(_topPressureControl / 100);
            result[1] = (byte)((_topPressureControl - result[0] * 100) / 10);
            result[2] = (byte)(_topPressureControl - result[0] * 100 - result[1] * 10);
            result[3] = (byte)(_lowerPressureControl / 100);
            result[4] = (byte)((_lowerPressureControl - result[3] * 100) / 10);
            result[5] = (byte)(_lowerPressureControl - result[3] * 100 - result[4] * 10);
            return result;
        }

        // отдельные цифры -> в давление
        private bool NumbersToPressure()
        {
            int tempPressure = _numbersPressure[0] * 100 + _numbersPressure[1] * 10 + _numbersPressure[2];
            if (tempPressure > 230)
                return false;
            _topPressureControl = (byte)tempPressure;
            tempPressure = _numbersPressure[3] * 100 + _numbersPressure[4] * 10 + _numbersPressure[5];
            if (tempPressure > 230)
                return false;
            _lowerPressureControl = (byte)tempPressure;
            return true;
        }

        // сохранить давление в базу
        private void SavePressureToDb()
        {
            try
            {
                using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    OleDbCommand myOleDbCommand = new OleDbCommand(
                        "UPDATE Patients " +
                        "SET topPressureControl = " + _topPressureControl.ToString() + ", " +
                        "    lowerPressureControl = " + _lowerPressureControl.ToString() + " " +
                        "WHERE id = " + _idPatient.ToString(),
                       myOleDbConnection);
                    if (myOleDbCommand.ExecuteNonQuery() <= 0)
                        throw new Exception("Не обнаружен пациент с таким идентификатором.");
                    if (_idControlMes >= 0)
                    {
                        double polynomTop = PulseWave.PolynomTop(_patientA2A1, _patientFss, _patientFssOpor, _patientK, _patientA, _patientB),
                               polynomLower = PulseWave.PolynomLower(_patientA2A1, _patientFss, _patientFssOpor, _patientK, _patientC, _patientD);
                        _patientK1 = (float) (230f*polynomTop/_topPressureControl);
                        _patientK2 = (float) (230f*polynomLower/_lowerPressureControl);
                        myOleDbCommand.CommandText =
                            "UPDATE Measurements " +
                            "SET topPressure = " + _topPressureControl.ToString() + ", " +
                            "    lowerPressure = " + _lowerPressureControl.ToString() + ", " +
                            "    control = true, " +
                            "    k1 = " + _patientK1.ToString(CultureInfo.InvariantCulture) + ", " +
                            "    k2 = " + _patientK2.ToString(CultureInfo.InvariantCulture) + " " +
                            "WHERE id = " + _idControlMes.ToString();
                        if (myOleDbCommand.ExecuteNonQuery() <= 0)
                            throw new Exception("Не удалось выполнить калибровку.");
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                        exc.Message + "\n\n" +
                        "Пожалуйста, обратитесь к администратору.",
                        "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //*****************************************************************
        //                               Звук                             *
        //*****************************************************************

        private bool _flagPlayingRec;
        private WaveOut _waveOutPulse, _waveOutRec;
        private PortamentoSineWaveOscillator _oscillatorPulse;

        private void bkwSound_DoWork(object sender, DoWorkEventArgs e)
        {
            if (bkwSound.CancellationPending)
                return;
            int msSleep = 200;
            if (!_flagPlayingRec && _pulseBufferSize >= _pulseBuffer.Length)
            {
                PulseWave tempPulseWave = new PulseWave(_pulseBuffer);
                tempPulseWave.FindChina();
                int chinaIndex;
                if ((chinaIndex = tempPulseWave.ChinaInDiapazon(_pulseBufferSize / 2 + 22, _pulseBufferSize)) >= 0 &&
                    !bkwSound.CancellationPending)
                {
                    Thread.Sleep((chinaIndex - (_pulseBufferSize / 2 + 22)) * 1000 / 220);
                    if (!bkwSound.CancellationPending)
                        bkwSound.ReportProgress(0, true);
                    Thread.Sleep(200);
                    bkwSound.ReportProgress(0, false);
                }
            }
            else
            {
                //var msCur = (float) _soundRec32.CurrentTime.TotalMilliseconds;

                int msRecord = Math.Min(20000, Environment.TickCount - _tickCountBeginRecord);
                if (msRecord >= 15000)
                {
                    _soundRec32.Volume = Math.Max(_clockSettings.SoundRecVolume * (20000 - msRecord) / (20000 - 15000), 0f);
                    msSleep = 10;
                }
                else if (_soundRec32.Volume < _clockSettings.SoundRecVolume)
                {
                    _soundRec32.Volume = Math.Min(_clockSettings.SoundRecVolume * (msRecord / 10000f), 1f);
                    msSleep = 50;
                }
                //_soundRec32.Volume =
                    //    Math.Min(_clockSettings.SoundRecVolume * ((msCur * (msCur / MsRecUp)) / MsRecUp), 1f);
            }
            Thread.Sleep(msSleep);
        }

        private void bkwSound_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (_waveOutPulse != null)
            {
                try
                {
                    if ((bool)e.UserState)
                        _waveOutPulse.Play();
                    else
                        _waveOutPulse.Stop();
                }
                catch (Exception)
                { }
            }
        }

        private void StartSoundPulse()
        {
            _flagPlayingRec = false;
            try
            {
                if ((_waveOutPulse = new WaveOut()) != null)
                {
                    _oscillatorPulse = new PortamentoSineWaveOscillator(44100, _clockSettings.SoundPulsePitch);
                    _oscillatorPulse.Amplitude = 8192;
                    _waveOutPulse.Volume = _clockSettings.SoundPulseVolume;
                    _waveOutPulse.Init(_oscillatorPulse);
                }
            }
            catch (Exception exc)
            {
                StopSoundPulse();
                MessageBox.Show("Возникла ошибка при работе со звуком:\n" +
                                exc.Message,
                                "Ошибка",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void StopSoundPulse()
        {
            bkwSound.CancelAsync();
            if (_waveOutPulse != null)
            {
                _waveOutPulse.Stop();
                _waveOutPulse.Dispose();
                _waveOutPulse = null;
            }
            if (_waveOutRec != null)
            {
                _waveOutRec.Stop();
                _waveOutRec.Dispose();
                _waveOutRec = null;
            }
        }


        //*****************************************************************
        //                            Шаблоны                             *
        //*****************************************************************

        // режим экрана
        private enum CurTemplates
        {
            WatchOnly,          // только часы
            MoveWatchRight,     // смещение вправо
            MoveWatchCenter,    // смещение к центру
            TransparentOn,      // появление
            Templates,          // шаблоны
        };
        private CurTemplates _templates;

        private byte _templatesMode;
        private float _templatesTransparent; // прозрачность
        private Image _templatesImage;
        private int _newPicWatchX, _newPicTemplatesX;

        private void StartShowTemplates()
        {
            mnuTemplates.Enabled = false;
            StopLightElement();
            SetEnableArrows(false);
            SetTemplates();
            _templates = CurTemplates.MoveWatchRight;
            _templatesTransparent = 0;

            int pnlWaveWidth = this.ClientSize.Width - picWatch.Width - picTemplates.Width + 31;
            _newPicWatchX = this.ClientSize.Width - picWatch.Width;
            _newPicTemplatesX = 0;
            if (pnlWaveWidth < MinWaveWidth)
            {
                _newPicTemplatesX = -Math.Min((MinWaveWidth - pnlWaveWidth) / 2, 45);
                _newPicWatchX = _newPicWatchX + Math.Min((MinWaveWidth - pnlWaveWidth) / 2, 160);
            }

            tmrWatch.Interval = 10;
            tmrWatch.Start();
            lblStatus.Text = "";
            pnlWave.Refresh();
        }

        private void StartHideTemplates()
        {
            mnuTemplates.Enabled = false;
            StopLightElement();
            SetEnableArrows(false);
            SetTemplates();
            pnlWave.Visible = false;
            picTemplates.Visible = false;
            _templates = CurTemplates.MoveWatchCenter;
            tmrWatch.Interval = 10;
            tmrWatch.Start();
        }

        private void SetTemplates()
        {
            switch (_templatesMode)
            {
                case 0:
                    mnuTemplatesHide.Enabled = false;
                    mnuTemplatesWaves.Enabled = true;
                    mnuTemplatesStress.Enabled = true;
                    break;
                case 1:
                    mnuTemplatesWaves.Enabled = false;
                    mnuTemplatesHide.Enabled = true;
                    mnuTemplatesStress.Enabled = true;
                    _templatesImage = (Bitmap)Resources.ResourceManager.GetObject("waves");
                    break;
                case 2:
                    mnuTemplatesStress.Enabled = false;
                    mnuTemplatesHide.Enabled = true;
                    mnuTemplatesWaves.Enabled = true;
                    _templatesImage = (Bitmap)Resources.ResourceManager.GetObject("stress");
                    break;
            }
        }


        private void mnuTemplatesWaves_Click(object sender, EventArgs e)
        {
            _templatesMode = 1;
            StartShowTemplates();
        }

        private void mnuTemplatesStress_Click(object sender, EventArgs e)
        {
            _templatesMode = 2;
            StartShowTemplates();
        }

        private void mnuTemplatesHide_Click(object sender, EventArgs e)
        {
            _templatesMode = 0;
            StartHideTemplates();
        }

        private void pnlWave_Paint(object sender, PaintEventArgs e)
        {
            Graphics graph = e.Graphics;
            int scroll;
            switch (_templatesMode)
            {
                case 1:
                    if (_myPulseWave != null)
                        _myPulseWave.DrawPulseWave(graph, new Rectangle(pnlWave.HorizontalScroll.Value,
                                                                       pnlWave.VerticalScroll.Value,
                                                                       pnlWave.ClientSize.Width,
                                                                       pnlWave.ClientSize.Height - 10),
                                                  new Point(0, 5),
                                                  out scroll, false, true);
                    break;
                case 2:
                    if (_myPulseWave != null)
                        _myPulseWave.DrawPulseStress(graph, new Rectangle(pnlWave.HorizontalScroll.Value,
                                                                         pnlWave.VerticalScroll.Value,
                                                                         pnlWave.ClientSize.Width,
                                                                         pnlWave.ClientSize.Height),
                                                    new Point(0, 0),
                                                    out scroll, true);
                    break;
            }
        }


        //*****************************************************************
        //                  История замеров (память)                      *
        //*****************************************************************

        private DataTable _dtHistory;
        private int _currentHisrory;
        private bool _firstShowHistory;
        private WaveOut _waveOutResult;

        private void SetModeHistory(bool toLast = true, bool soundResult = true)
        {
            _watchState = CurStations.History;
            SetEnableArrows(false);
            try
            {
                using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    using (OleDbCommand myOleDbCommand = new OleDbCommand(
                        "SELECT id, mesDateTime, sosud, topPressure, lowerPressure, pulse, stress, control " +
                        "FROM Measurements " +
                        "WHERE patient = " + _idPatient.ToString() + " " +
                        "ORDER BY mesDateTime DESC",
                        myOleDbConnection))
                    {
                        using (OleDbDataReader myReader = myOleDbCommand.ExecuteReader())
                        {
                            _dtHistory = new DataTable();
                            _dtHistory.Columns.Add("id");
                            _dtHistory.Columns.Add("mesDateTime");
                            _dtHistory.Columns.Add("sosud");
                            _dtHistory.Columns.Add("topPressure");
                            _dtHistory.Columns.Add("lowerPressure");
                            _dtHistory.Columns.Add("pulse");
                            _dtHistory.Columns.Add("stress");
                            _dtHistory.Columns.Add("control");
                            while (myReader.Read())
                            {
                                _dtHistory.Rows.Add(new object[]
                                                        {
                                                            myReader[0], myReader[1], myReader[2], myReader[3],
                                                            myReader[4],
                                                            myReader[5], myReader[6], myReader[7]
                                                        });
                            }
                        }
                    }
                    if (toLast)
                        _currentHisrory = 0;
                    else
                    {
                        if (_currentHisrory < 0)
                            _currentHisrory = 0;
                        else if (_currentHisrory > _dtHistory.Rows.Count - 1)
                            _currentHisrory = _dtHistory.Rows.Count - 1;
                    }
                    SetEnableArrows(0);
                    SetEnableArrows(1, (_currentHisrory < (_dtHistory.Rows.Count - 1)));
                    SetEnableArrows(3, (_currentHisrory > 0));
                    //if (_dtHistory.Rows.Count > 1)
                    //    SetEnableArrows(1);
                    LoadHistoryData();
                    if (_clockSettings.FlagSoundResult && soundResult)
                        StartSoundResult();
                    // шаблоны и меню
                    _templatesMode = 0;
                    SetTemplates();
                    mnuTemplates.Enabled = true;
                    mnuMolodimetr.Enabled = true;
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Внимание! При попытке открытия базы данных произошла ошибка:\n" +
                    exc.Message + "\n\n" +
                    "Пожалуйста, обратитесь к администратору.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // загрузка данных истории
        private void LoadHistoryData()
        {
            picWatch.Invalidate(_rctWatch);
            picWatch.Update();
            if ((_dtHistory != null) && (_currentHisrory >= 0) && (_currentHisrory < _dtHistory.Rows.Count))
                OpenMeasurementFile(Convert.ToInt32(_dtHistory.Rows[_currentHisrory][0]));
            else
                _myPulseWave = null;
            if (_myPulseWave != null)
                _myPulseWave.AnalyzePulseWave();
            pnlWave.Refresh();
        }

        // открываем файл с замером
        private void OpenMeasurementFile(int idMeasurement)
        {
            try
            {
                string strFileName = ConstGlobals.MeasurementsFolder + idMeasurement.ToString() + ".dat",
                    strError = "";
                _myPulseWave = new PulseWave();
                if (!_myPulseWave.LoadPulseFromBinFile(strFileName, ref strError))
                    throw new Exception("При попытке открытия файла '" + Path.GetFileName(strFileName) + "' произошла ошибка:\n" + strError);
            }
            catch (Exception exc)
            {
                MessageBox.Show("Внимание! При попытке открытия файла с замером произошла ошибка:\n" +
                    exc.Message,
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                _myPulseWave = null;
            }
        }

        // озвучка результатов замера
        private void StartSoundResult()
        {
            StopSoundResult();
            if ((_dtHistory != null) && (_currentHisrory >= 0) && (_currentHisrory < _dtHistory.Rows.Count))
            {
                try
                {
                    // микшер
                    var mixer = new WaveMixerStream32();
                    mixer.AutoStop = true;

                    // гонг
                    TimeSpan tsStart = TimeSpan.Zero;
                    if (_flagGongSound)
                    {
                        WaveFileReader sGong = new WaveFileReader(@"sound/gong.wav");
                        tsStart = sGong.TotalTime + TimeSpan.FromMilliseconds(250);
                        mixer.AddInputStream(new WaveChannel32(sGong));
                        _flagGongSound = false;
                    }

                    // фон
                    WaveFileReader sBackground;
                    switch (Convert.ToInt16(_dtHistory.Rows[_currentHisrory][2]))
                    {
                        case 1:
                            sBackground = new WaveFileReader(@"sound/background/Сосуды расширены.wav");
                            break;
                        case 3:
                            sBackground = new WaveFileReader(@"sound/background/Сосуды сжаты.wav");
                            break;
                        default:
                            sBackground = new WaveFileReader(@"sound/background/Сосуды в среднем состоянии.wav");
                            break;
                    }
                    var sBackgroundOffsetted = new WaveOffsetStream(sBackground, tsStart,
                                                                    TimeSpan.Zero, sBackground.TotalTime);
                    var sBackgroundOffsetted_32 = new WaveChannel32(sBackgroundOffsetted);
                    sBackgroundOffsetted_32.Volume = _clockSettings.SoundResultVolume;
                    mixer.AddInputStream(sBackgroundOffsetted_32);
                    tsStart = sBackgroundOffsetted.StartTime + TimeSpan.FromSeconds(3);

                    // голос
                    if (_clockSettings.Voice == 1)
                        MixerVoiceMan(@"sound/voice_1/", tsStart, mixer, _clockSettings.VoiceVolume);
                    else
                        MixerVoiceWoman(@"sound/voice_0/", tsStart, mixer, _clockSettings.VoiceVolume);
                    
                    if ((_waveOutResult = new WaveOut()) != null)
                    {
                        _waveOutResult.Init(mixer);
                        _waveOutResult.Play();
                    }
                    else
                        throw new Exception("Не удалось запустить воспроизведение звука.");
                }
                catch (FileNotFoundException exc)
                {
                    MessageBox.Show("При попытке открытия файла со звуком произошла ошибка:\n" +
                        exc.Message,
                        "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                catch (Exception exc)
                {
                    MessageBox.Show("При попытке озвучки результатов замера произошла ошибка:\n" +
                        exc.Message,
                        "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void MixerVoiceWoman(string strBase, TimeSpan tsStart, WaveMixerStream32 mixer, float volume)
        {
            // интервалы промежутков
            TimeSpan tsSmall = TimeSpan.FromSeconds(0.03),  // между словами
                     tsMedium = TimeSpan.FromSeconds(0.2),  // в начале раздела после заголовка
                     tsBig = TimeSpan.FromSeconds(1);       // перед заголовком
            // звуковые файлы
            WaveFileReader sSosud;
            switch (Convert.ToInt16(_dtHistory.Rows[_currentHisrory][2]))
            {
                case 1:
                    sSosud = new WaveFileReader(strBase + @"Сосуды/Сосуды расширены.wav");
                    break;
                case 3:
                    sSosud = new WaveFileReader(strBase + @"Сосуды/Сосуды сжаты.wav");
                    break;
                default:
                    sSosud = new WaveFileReader(strBase + @"Сосуды/Сосуды в среднем состоянии.wav");
                    break;
            }
            // смещения
            var sSosudOffsetted = new WaveOffsetStream(sSosud, tsStart,
                TimeSpan.Zero, sSosud.TotalTime);
            mixer.AddInputStream(new WaveChannel32(sSosudOffsetted) {Volume = volume});

            short topPressure = Convert.ToInt16(_dtHistory.Rows[_currentHisrory][3]),
                lowerPressure = Convert.ToInt16(_dtHistory.Rows[_currentHisrory][4]),
                pulse = Convert.ToInt16(_dtHistory.Rows[_currentHisrory][5]),
                stress = Convert.ToInt16(_dtHistory.Rows[_currentHisrory][6]);

            var sPressure = new WaveFileReader(strBase + @"Давление/Давление.wav");
            var sPressureOffsetted = new WaveOffsetStream(sPressure, sSosudOffsetted.TotalTime + tsBig,
                TimeSpan.Zero, sPressure.TotalTime);
            mixer.AddInputStream(new WaveChannel32(sPressureOffsetted) { Volume = volume });

            TimeSpan timeSpanCurrent = sPressureOffsetted.TotalTime + tsMedium;
            timeSpanCurrent = NumberToVoice(strBase, topPressure, mixer, timeSpanCurrent, tsSmall, volume);

            var sPressure2 = new WaveFileReader(strBase + @"Давление/На.wav");
            var sPressure2Offsetted = new WaveOffsetStream(sPressure2, timeSpanCurrent + tsMedium,
                TimeSpan.Zero, sPressure2.TotalTime);
            mixer.AddInputStream(new WaveChannel32(sPressure2Offsetted) { Volume = volume });
            timeSpanCurrent = sPressure2Offsetted.TotalTime + tsMedium;
            timeSpanCurrent = NumberToVoice(strBase, lowerPressure, mixer, timeSpanCurrent, tsSmall, volume);

            WaveFileReader sPulse = new WaveFileReader(strBase + @"Пульс/Пульс.wav");
            var sPulseOffsetted = new WaveOffsetStream(sPulse, timeSpanCurrent + tsBig,
                TimeSpan.Zero, sPulse.TotalTime);
            mixer.AddInputStream(new WaveChannel32(sPulseOffsetted) { Volume = volume });
            timeSpanCurrent = sPulseOffsetted.TotalTime + tsMedium;
            timeSpanCurrent = NumberToVoice(strBase, pulse, mixer, timeSpanCurrent, tsSmall, volume);

            WaveFileReader sStress = new WaveFileReader(strBase + @"Стресс/Стресс.wav");
            var sStressOffsetted = new WaveOffsetStream(sStress, timeSpanCurrent + tsBig,
                TimeSpan.Zero, sStress.TotalTime);
            mixer.AddInputStream(new WaveChannel32(sStressOffsetted) { Volume = volume });
            timeSpanCurrent = sStressOffsetted.TotalTime + tsMedium;
            timeSpanCurrent = NumberToVoice(strBase, stress, mixer, timeSpanCurrent, tsSmall, volume);

            WaveFileReader sStress2;
            if (stress == 1)
                sStress2 = new WaveFileReader(strBase + @"Стресс/Балл.wav");
            else if (stress == 2 || stress == 3 || stress == 4)
                sStress2 = new WaveFileReader(strBase + @"Стресс/Балла.wav");
            else
                sStress2 = new WaveFileReader(strBase + @"Стресс/Баллов.wav");
            var sStress2Offsetted = new WaveOffsetStream(sStress2, timeSpanCurrent + tsMedium,
                TimeSpan.Zero, sStress2.TotalTime);
            mixer.AddInputStream(new WaveChannel32(sStress2Offsetted) { Volume = volume });
        }

        // число --> в звук
        private TimeSpan NumberToVoice(string strBase, int num, WaveMixerStream32 mixer, TimeSpan start, TimeSpan sepTime, float volume)
        {
            TimeSpan result = start;
            string strDigits = strBase + @"digits/";
            WaveFileReader sNum;
            WaveOffsetStream sNumOffsetted;
            if ((num / 100) != 0)
            {
                sNum = new WaveFileReader(strDigits + (num - num % 100).ToString() + @".wav");
                sNumOffsetted = new WaveOffsetStream(sNum, result,
                                                     TimeSpan.Zero, sNum.TotalTime);
                mixer.AddInputStream(new WaveChannel32(sNumOffsetted) { Volume = volume });
                result = sNumOffsetted.TotalTime + sepTime;
                num = num % 100;
            }
            if ((num / 10) != 0)
            {
                if ((num - num % 10) == 10)
                {
                    sNum = new WaveFileReader(strDigits + num.ToString() + @".wav");
                    sNumOffsetted = new WaveOffsetStream(sNum, result,
                                                         TimeSpan.Zero, sNum.TotalTime);
                    mixer.AddInputStream(new WaveChannel32(sNumOffsetted) { Volume = volume });
                    return sNumOffsetted.TotalTime + sepTime;
                }
                else
                {
                    sNum = new WaveFileReader(strDigits + (num - num % 10).ToString() + @".wav");
                    sNumOffsetted = new WaveOffsetStream(sNum, result,
                                                         TimeSpan.Zero, sNum.TotalTime);
                    mixer.AddInputStream(new WaveChannel32(sNumOffsetted) { Volume = volume });
                    result = sNumOffsetted.TotalTime + sepTime;
                    num = num % 10;
                }
            }
            if (num != 0 || result == start)
            {
                sNum = new WaveFileReader(strDigits + num.ToString() + @".wav");
                sNumOffsetted = new WaveOffsetStream(sNum, result,
                                                     TimeSpan.Zero, sNum.TotalTime);
                mixer.AddInputStream(new WaveChannel32(sNumOffsetted) { Volume = volume });
                result = sNumOffsetted.TotalTime + sepTime;
            }
            return result;
        }

        private void MixerVoiceMan(string strBase, TimeSpan tsStart, WaveMixerStream32 mixer, float volume)
        {
            // звуковые файлы
            WaveFileReader sSosud;
            switch (Convert.ToInt16(_dtHistory.Rows[_currentHisrory][2]))
            {
                case 1:
                    sSosud = new WaveFileReader(strBase + @"Сосуды/Сосуды расширены.wav");
                    break;
                case 3:
                    sSosud = new WaveFileReader(strBase + @"Сосуды/Сосуды сжаты.wav");
                    break;
                default:
                    sSosud = new WaveFileReader(strBase + @"Сосуды/Сосуды в среднем состоянии.wav");
                    break;
            }
            short topPressure = Convert.ToInt16(_dtHistory.Rows[_currentHisrory][3]),
                lowerPressure = Convert.ToInt16(_dtHistory.Rows[_currentHisrory][4]),
                pulse = Convert.ToInt16(_dtHistory.Rows[_currentHisrory][5]),
                stress = Convert.ToInt16(_dtHistory.Rows[_currentHisrory][6]);
            WaveFileReader sPressure = new WaveFileReader(strBase + @"ДавлениеВ/Давление.wav");
            WaveFileReader sTopPressure = new WaveFileReader(strBase + @"ДавлениеВ/" +
                (topPressure - topPressure % 5 + (topPressure % 5 >= 3 ? 5 : 0)).ToString() +
                ".wav");
            WaveFileReader sLowerPressure = new WaveFileReader(strBase + @"ДавлениеН/на" +
                (lowerPressure - lowerPressure % 5 + (lowerPressure % 5 >= 3 ? 5 : 0)).ToString() +
                ".wav");
            WaveFileReader sPulse = new WaveFileReader(strBase + @"Пульс/П" +
                (pulse - pulse % 5 + (pulse % 5 >= 3 ? 5 : 0)).ToString() +
                ".wav");
            WaveFileReader sStress = new WaveFileReader(strBase + @"Стресс/С" +
                stress.ToString() + "Б" +
                ".wav");

            // смещения
            var sSosudOffsetted = new WaveOffsetStream(sSosud, tsStart,
                TimeSpan.Zero, sSosud.TotalTime);
            var sPressureOffsetted = new WaveOffsetStream(sPressure, sSosudOffsetted.TotalTime + TimeSpan.FromSeconds(1),
                TimeSpan.Zero, sPressure.TotalTime);
            var sTopPressureOffsetted = new WaveOffsetStream(sTopPressure, sPressureOffsetted.TotalTime,
                TimeSpan.Zero, sTopPressure.TotalTime);
            var sLowerPressureOffsetted = new WaveOffsetStream(sLowerPressure, sTopPressureOffsetted.TotalTime,
                TimeSpan.Zero, sLowerPressure.TotalTime);
            var sPulseOffsetted = new WaveOffsetStream(sPulse, sLowerPressureOffsetted.TotalTime + TimeSpan.FromSeconds(1),
                TimeSpan.Zero, sPulse.TotalTime);
            var sStressOffsetted = new WaveOffsetStream(sStress, sPulseOffsetted.TotalTime + TimeSpan.FromSeconds(1),
                TimeSpan.Zero, sStress.TotalTime);

            mixer.AddInputStream(new WaveChannel32(sSosudOffsetted) { Volume = volume });
            mixer.AddInputStream(new WaveChannel32(sPressureOffsetted) { Volume = volume });
            mixer.AddInputStream(new WaveChannel32(sTopPressureOffsetted) { Volume = volume });
            mixer.AddInputStream(new WaveChannel32(sLowerPressureOffsetted) { Volume = volume });
            mixer.AddInputStream(new WaveChannel32(sPulseOffsetted) { Volume = volume });
            mixer.AddInputStream(new WaveChannel32(sStressOffsetted) { Volume = volume });
        }

        private void StopSoundResult()
        {
            if (_waveOutResult != null)
            {
                _waveOutResult.Stop();
                _waveOutResult.Dispose();
                _waveOutResult = null;
            }
        }


    }
}
