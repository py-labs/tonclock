﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace TonClock
{
    public partial class frmDoctorAdd : Form
    {
        bool flagAdd, flagOK;
        int idRecord;

        public frmDoctorAdd(int id = 0, bool add = true)
        {
            InitializeComponent();
            idRecord = id;
            flagAdd = add;
            if (flagAdd)
            {
                this.Text = "Добавление консультанта";
                lblTitle.Text = "Добавление записи о новом консультанте:";
                picDoctor.Image = global::TonClock.Properties.Resources.user_add;
            }
            else
            {
                this.Text = "Редактирование консультанта";
                lblTitle.Text = "Редактирование записи о консультанте:";
                picDoctor.Image = global::TonClock.Properties.Resources.user_edit;
            }
            flagOK = false;
        }

        private void frmDoctorAdd_Shown(object sender, EventArgs e)
        {
            try
            {
                using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    OleDbCommand myOleDbCommand = new OleDbCommand(
                        "SELECT cName " +
                        "FROM Cities",
                        myOleDbConnection);
                    OleDbDataReader myReader = myOleDbCommand.ExecuteReader();
                    cmbCity.Items.Clear();
                    while (myReader.Read())
                        cmbCity.Items.Add(myReader[0].ToString());
                    myReader.Close();
                    myOleDbCommand.CommandText = "SELECT oName " +
                        "FROM Organizations";
                    myReader = myOleDbCommand.ExecuteReader();
                    cmbOrganization.Items.Clear();
                    while (myReader.Read())
                        cmbOrganization.Items.Add(myReader[0].ToString());
                    myReader.Close();
                    if (!flagAdd)
                    {
                        myOleDbCommand.CommandText = "SELECT Doctors.lastname, Doctors.firstname, Doctors.patronymic, " +
                            "       Cities.cName, Organizations.oName " +
                            "FROM Doctors, Cities, Organizations " +
                            "WHERE Doctors.city = Cities.id " +
                            "  AND Doctors.organization = Organizations.id " +
                            "  AND Doctors.id = " + idRecord.ToString();
                        myReader = myOleDbCommand.ExecuteReader();
                        if (myReader.Read())
                        {
                            txtLastname.Text = myReader[0].ToString();
                            txtFirstname.Text = myReader[1].ToString();
                            txtPatronymic.Text = myReader[2].ToString();
                            cmbCity.Text = myReader[3].ToString();
                            cmbOrganization.Text = myReader[4].ToString();
                            myReader.Close();
                        }
                        else
                        {
                            myReader.Close();
                            throw new Exception();
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                    exc.Message + "\n\n" +
                    "Пожалуйста, обратитесь к администратору.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (!this.ValidateChildren())
                MessageBox.Show("Ошибка во введенных данных!\n\n" +
                                "Для получения дополнительной информации\n" +
                                "наведите курсор мыши на восклицательные знаки.",
                                "Ошибка",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                try
                {
                    using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                    {
                        myOleDbConnection.Open();
                        int idCity, idOrg;
                        string strCity = cmbCity.Text.Trim(),
                            strOrg = cmbOrganization.Text.Trim();
                        // город
                        OleDbCommand myOleDbCommand = new OleDbCommand(
                            "SELECT id " +
                            "FROM Cities " +
                            "WHERE cName LIKE '" + strCity + "'",
                            myOleDbConnection);
                        OleDbDataReader myReader = myOleDbCommand.ExecuteReader();
                        if (myReader.Read())
                        {
                            idCity = (int)myReader[0];
                            myReader.Close();
                        }
                        else
                        {
                            myReader.Close();
                            myOleDbCommand.CommandText = "INSERT INTO Cities " +
                                "(cName) " +
                                "VALUES ('" + strCity + "')";
                            myOleDbCommand.ExecuteNonQuery();
                            myOleDbCommand.CommandText = "SELECT @@IDENTITY";
                            idCity = (int)myOleDbCommand.ExecuteScalar();
                        }
                        // организация
                        myOleDbCommand.CommandText = "SELECT id " +
                            "FROM Organizations " +
                            "WHERE oName LIKE '" + strOrg + "'";
                        myReader = myOleDbCommand.ExecuteReader();
                        if (myReader.Read())
                        {
                            idOrg = (int)myReader[0];
                            myReader.Close();
                        }
                        else
                        {
                            myReader.Close();
                            myOleDbCommand.CommandText = "INSERT INTO Organizations " +
                                "(oName) " +
                                "VALUES ('" + strOrg + "')";
                            myOleDbCommand.ExecuteNonQuery();
                            myOleDbCommand.CommandText = "SELECT @@IDENTITY";
                            idOrg = (int)myOleDbCommand.ExecuteScalar();
                        }
                        // основная часть работы
                        if (flagAdd)
                            myOleDbCommand.CommandText = "INSERT INTO Doctors " +
                                "(lastname, firstname, patronymic, city, organization) " +
                                "VALUES ('" + txtLastname.Text.Trim() + "', '" +
                                txtFirstname.Text.Trim() + "', '" + txtPatronymic.Text.Trim() + "', " +
                                idCity.ToString() + ", " + idOrg.ToString() + ")";
                        else
                            myOleDbCommand.CommandText = "UPDATE Doctors " +
                                "SET lastname = '" + txtLastname.Text.Trim() +
                                "', firstname = '" + txtFirstname.Text.Trim() + "', patronymic = '" + txtPatronymic.Text.Trim() +
                                "', city = " + idCity.ToString() + ", organization = " + idOrg.ToString() + " " +
                                "WHERE id = " + idRecord.ToString();
                        flagOK = (myOleDbCommand.ExecuteNonQuery() > 0);
                        if (flagAdd)
                        {
                            myOleDbCommand.CommandText = "SELECT @@IDENTITY";
                            idRecord = (int)myOleDbCommand.ExecuteScalar();
                        }
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                        exc.Message + "\n\n" +
                        "Пожалуйста, обратитесь к администратору.",
                        "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    flagOK = false;
                }
                this.DialogResult = DialogResult.OK;
            }
        }

        public bool returnFlagOK() { return flagOK; }
        public int returnIdRecord() { return idRecord; }


        // ---------------- проверка введенных данных ----------------

        private void txtLastname_Validating(object sender, CancelEventArgs e)
        {
            if (txtLastname.Text.Trim().Length > 0 && txtLastname.Text.IndexOf("'") == -1)
                errorProvider1.SetError(txtLastname, String.Empty);
            else
            {
                if (txtLastname.Text.IndexOf("'") != -1)
                    errorProvider1.SetError(txtLastname, "Символ одинарной кавычки \"'\" является недопустимым.");
                else
                    errorProvider1.SetError(txtLastname, "Поле \"Фамилия\" должно быть заполнено.");
                e.Cancel = true;
            }
        }

        private void txtFirstname_Validating(object sender, CancelEventArgs e)
        {
            if (txtFirstname.Text.Trim().Length > 0 && txtFirstname.Text.IndexOf("'") == -1)
                errorProvider1.SetError(txtFirstname, String.Empty);
            else
            {
                if (txtFirstname.Text.IndexOf("'") != -1)
                    errorProvider1.SetError(txtFirstname, "Символ одинарной кавычки \"'\" является недопустимым.");
                else
                    errorProvider1.SetError(txtFirstname, "Поле \"Имя\" должно быть заполнено.");
                e.Cancel = true;
            }
        }

        private void txtPatronymic_Validating(object sender, CancelEventArgs e)
        {
            if (txtPatronymic.Text.Trim().Length > 0 && txtPatronymic.Text.IndexOf("'") == -1)
                errorProvider1.SetError(txtPatronymic, String.Empty);
            else
            {
                if (txtPatronymic.Text.IndexOf("'") != -1)
                    errorProvider1.SetError(txtPatronymic, "Символ одинарной кавычки \"'\" является недопустимым.");
                else
                    errorProvider1.SetError(txtPatronymic, "Поле \"Отчество\" должно быть заполнено.");
                e.Cancel = true;
            }
        }

        private void cmbCity_Validating(object sender, CancelEventArgs e)
        {
            if (cmbCity.Text.Trim().Length > 0 && cmbCity.Text.IndexOf("'") == -1)
                errorProvider1.SetError(cmbCity, String.Empty);
            else
            {
                if (cmbCity.Text.IndexOf("'") != -1)
                    errorProvider1.SetError(cmbCity, "Символ одинарной кавычки \"'\" является недопустимым.");
                else
                    errorProvider1.SetError(cmbCity, "Поле \"Город\" должно быть заполнено.");
                e.Cancel = true;
            }
        }

        private void cmbOrganization_Validating(object sender, CancelEventArgs e)
        {
            if (cmbOrganization.Text.Trim().Length > 0 && cmbOrganization.Text.IndexOf("'") == -1)
                errorProvider1.SetError(cmbOrganization, String.Empty);
            else
            {
                if (cmbOrganization.Text.IndexOf("'") != -1)
                    errorProvider1.SetError(cmbOrganization, "Символ одинарной кавычки \"'\" является недопустимым.");
                else
                    errorProvider1.SetError(cmbOrganization, "Поле \"Организация\" должно быть заполнено.");
                e.Cancel = true;
            }
        }

        private void txtLastname_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(txtLastname, String.Empty);
        }

        private void txtFirstname_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(txtFirstname, String.Empty);
        }

        private void txtPatronymic_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(txtPatronymic, String.Empty);
        }

        private void cmbCity_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(cmbCity, String.Empty);
        }

        private void cmbOrganization_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(cmbOrganization, String.Empty);
        }



    }
}
