﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Globalization;

namespace TonClock
{
    public partial class frmMeasurement : Form
    {
        int idMeasurement;
        PulseWave myPulseWave;

        public frmMeasurement(int id)
        {
            InitializeComponent();
            idMeasurement = id;
        }

        private void frmMeasurement_Shown(object sender, EventArgs e)
        {
            try
            {
                using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    OleDbCommand myOleDbCommand = new OleDbCommand(
                        "SELECT Patients.lastname, Patients.firstname, Patients.patronymic, Patients.age, " +
                        "       Measurements.mesDateTime, " +
                        "       Measurements.sosud, Measurements.topPressure, Measurements.lowerPressure, " +
                        "       Measurements.pulse, Measurements.stress, Measurements.control " +
                        "FROM Measurements, Patients " +
                        "WHERE Measurements.id = " + idMeasurement.ToString() + " " +
                        "  AND Patients.id = Measurements.patient",
                        myOleDbConnection);
                    OleDbDataReader myReader = myOleDbCommand.ExecuteReader();
                    if (myReader.Read())
                    {
                        lblFio.Text = myReader[0].ToString() + " " +
                            myReader[1].ToString()[0] + "." + myReader[2].ToString()[0] + ".";
                        lblAge.Text = DbPulseOperations.PrintAge(myReader.GetInt16(3));
                        lblDate.Text = myReader.GetDateTime(4).ToShortDateString();
                        lblTime.Text = myReader.GetDateTime(4).ToShortTimeString();
                        int sosudVal = myReader.GetInt16(5);
                        switch (sosudVal)
                        {
                            case 1: lblSosud.Text = "расширены"; lblSosud.ForeColor = Color.Green; break;
                            case 3: lblSosud.Text = "сжаты"; lblSosud.ForeColor = Color.Red; break;
                            default: lblSosud.Text = "в среднем состоянии"; lblSosud.ForeColor = Color.Blue; break;
                        }
                        lblPressure.Text = myReader.GetInt16(6).ToString() + " / " + myReader.GetInt16(7).ToString();
                        lblPulse.Text = myReader.GetInt16(8).ToString() + "  уд/мин";
                        lblStressIndex.Text = DbPulseOperations.PrintStress(myReader.GetInt16(9));
                        if (myReader.GetBoolean(10))
                        {
                            lblControl.Text = "Да";
                            lblControl.ForeColor = Color.Red;
                            lblControl.Font = new Font(lblControl.Font, FontStyle.Bold);
                        }
                        else
                            lblControl.Text = "Нет";
                        myReader.Close();
                    }
                    else
                    {
                        myReader.Close();
                        throw new Exception("В базе данных не обнаружен замер с таким идентификатором.");
                    }
                    // открываем файл с замером
                    string strFileName = ConstGlobals.MeasurementsFolder + idMeasurement.ToString() + ".dat",
                        strError = "";
                    myPulseWave = new PulseWave();
                    if (!myPulseWave.LoadPulseFromBinFile(strFileName, ref strError))
                        throw new Exception("При попытке открытия файла '" + strFileName + "' произошла ошибка:\n" + strError);
                    else
                        pnlPulseData.Refresh();
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                    exc.Message + "\n\n" +
                    "Пожалуйста, обратитесь к администратору.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pnlPulseData_Paint(object sender, PaintEventArgs e)
        {
            if (myPulseWave != null)
            {
                Rectangle rctPulse = new Rectangle(pnlPulseData.HorizontalScroll.Value, pnlPulseData.VerticalScroll.Value,
                    pnlPulseData.ClientSize.Width, pnlPulseData.ClientSize.Height);
                int scroll;
                myPulseWave.DrawPulseWaveDataSimple(e.Graphics, rctPulse, out scroll);
                pnlPulseData.AutoScrollMinSize = new Size(scroll, 0);
            }
        }

        private void pnlPulseData_Scroll(object sender, ScrollEventArgs e)
        {
            pnlPulseData.Refresh();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }



    }
}
