﻿//
//   Класс GlobalCalc для расчета давления без калибровки.
// (полином "глобал")
//

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;

namespace TonClock
{
    class GlobalCalc
    {
        private ushort[] _pulseWave;    // средняя волна

        private int[] _repers,          // реперы
                      _derivative;      // производная

        public int[] Repers
        {
            set { _repers = value; }
            get { return _repers; }
        }

        public GlobalCalc(ushort[] data)
        {
            _pulseWave = data;
        }

        // авто-расстановка реперов
        public void AutoRepers()
        {
            if (_pulseWave == null) return;
            _derivative = new int[_pulseWave.Length];
            for (int i = 0; i < _pulseWave.Length - 1; i++)
                _derivative[i + 1] = _pulseWave[i + 1] - _pulseWave[i];
            _derivative[0] = _derivative[1];
            _repers = new int[5];
            ushort max = _pulseWave.Max();
            for (int i = 0; i < _pulseWave.Length; i++)
            {
                if (_pulseWave[i] == max)
                    _repers[0] = i;
            }
            _repers[1] = Math.Min(_repers[0] + 22, _pulseWave.Length - 1);

            // ищем максимальную производную на 2/3 оставшегося участка
            int maxDer = _repers[1] + 1;
            for (int i = _repers[1] + 2; i < _repers[1] + (_pulseWave.Length - _repers[1]) / 3 * 2; i++)
            {
                if (_derivative[i] > _derivative[maxDer])
                    maxDer = i;
            }
            //A3 - минимальное значение от этой максимальной производной до А2
            _repers[2] = Math.Min(_repers[1] + 1, _pulseWave.Length - 1);
            float minDerA3 = (_pulseWave[_repers[2] - 1] + _pulseWave[_repers[2]] + _pulseWave[_repers[2] + 1])/3f;
            for (int i = _repers[1] + 2; i < maxDer; i++)
            {
                float curDerA3 = (_pulseWave[i - 1] + _pulseWave[i] + _pulseWave[i + 1]) / 3f;
                if (curDerA3 < minDerA3)
                {
                    _repers[2] = i;
                    minDerA3 = curDerA3;
                }
            }
            //A4 - максимальное значение от этой максимальной производной до конца
            _repers[3] = Math.Min(maxDer, _pulseWave.Length - 1);
            float maxDerA3 = (_pulseWave[_repers[3] - 1] + _pulseWave[_repers[3]] + _pulseWave[_repers[3] + 1]) / 3f;
            for (int i = maxDer + 1; i < _pulseWave.Length - 1; i++)
            {
                float curDerA4 = (_pulseWave[i - 1] + _pulseWave[i] + _pulseWave[i + 1]) / 3f;
                if (curDerA4 > maxDerA3)
                {
                    _repers[3] = i;
                    maxDerA3 = curDerA4;
                }
            }

            //int[] repers = new int[(_pulseWave.Length - _repers[1])/2];
            //for (int i = 0; i < repers.Length; i++)
            //    repers[i] = _repers[1] + 1 + i;
            //for (int i = 0; i < repers.Length; i++)
            //    for (int j = i + 1; j < repers.Length; j++)
            //        if (Math.Abs(_derivative[repers[j]]) < Math.Abs(_derivative[repers[i]]))
            //        {
            //            int temp = repers[i];
            //            repers[i] = repers[j];
            //            repers[j] = temp;
            //        }
            //repers = (int[])repers.Take(20).ToArray();
            //_repers[2] = 0;
            //_repers[3] = 0;
            //int count2 = 0, count3 = 0, srednee = 0;
            //for (int i = 0; i < 20 && i < repers.Length; i++)
            //    srednee += repers[i];
            //if (repers.Length > 0)
            //    srednee /= Math.Min(20, repers.Length);
            //for (int i = 0; i < 20 && i < repers.Length; i++)
            //{
            //    if (repers[i] < srednee)
            //    {
            //        _repers[2] += repers[i];
            //        count2++;
            //    }
            //    else
            //    {
            //        _repers[3] += repers[i];
            //        count3++;
            //    }
            //}
            //if (count2 != 0)
            //    _repers[2] /= count2;
            //if (count3 != 0)
            //    _repers[3] /= count3;

            //for (int i = _repers[1] + 1; i < _repers[3]; i++)
            //    if (_pulseWave[i] < _pulseWave[_repers[2]])
            //        _repers[2] = i;
            //for (int i = _repers[2] + 1; i < _pulseWave.Length; i++)
            //    if (_pulseWave[i] > _pulseWave[_repers[3]])
            //        _repers[3] = i;

            if (_repers[3] - _repers[2] < 2)
            {
                _repers[2] = _repers[1] + 11;
                _repers[3] = _repers[2] + 11;
            }

            _repers[4] = Math.Max(_repers[3] + 2, _derivative.Length / 4 * 3 + 1);
            float minDer = float.MaxValue;
            for (int i = _repers[4]; i < _derivative.Length - 2 - 10; i++)
            {
                float sredDerivative = 0;
                for (int j = -1; j <= 1; j++)
                    sredDerivative += Math.Abs(_derivative[i + j]);
                sredDerivative /= 3;
                if (sredDerivative <= minDer)
                {
                    _repers[4] = i;
                    minDer = sredDerivative;
                }
            }
            if ((_pulseWave.Length - 1) - _repers[4] < 5)
                _repers[4] = (_pulseWave.Length - 1) - 11;
        }

        public void AutoRepers_OLD2()
        {
            if (_pulseWave == null) return;
            _derivative = new int[_pulseWave.Length];
            for (int i = 0; i < _pulseWave.Length - 1; i++)
                _derivative[i + 1] = _pulseWave[i + 1] - _pulseWave[i];
            _derivative[0] = _derivative[1];
            _repers = new int[5];
            ushort max = _pulseWave.Max();
            for (int i = 0; i < _pulseWave.Length; i++)
            {
                if (_pulseWave[i] == max)
                    _repers[0] = i;
            }
            _repers[1] = Math.Min(_repers[0] + 22, _pulseWave.Length - 1);

            int[] repers = new int[(_pulseWave.Length - _repers[1]) / 2];
            for (int i = 0; i < repers.Length; i++)
                repers[i] = _repers[1] + 1 + i;
            for (int i = 0; i < repers.Length; i++)
                for (int j = i + 1; j < repers.Length; j++)
                    if (Math.Abs(_derivative[repers[j]]) < Math.Abs(_derivative[repers[i]]))
                    {
                        int temp = repers[i];
                        repers[i] = repers[j];
                        repers[j] = temp;
                    }
            repers = (int[])repers.Take(20).ToArray();
            _repers[2] = 0;
            _repers[3] = 0;
            int count2 = 0, count3 = 0, srednee = 0;
            for (int i = 0; i < 20 && i < repers.Length; i++)
                srednee += repers[i];
            if (repers.Length > 0)
                srednee /= Math.Min(20, repers.Length);
            for (int i = 0; i < 20 && i < repers.Length; i++)
            {
                if (repers[i] < srednee)
                {
                    _repers[2] += repers[i];
                    count2++;
                }
                else
                {
                    _repers[3] += repers[i];
                    count3++;
                }
            }
            if (count2 != 0)
                _repers[2] /= count2;
            if (count3 != 0)
                _repers[3] /= count3;

            for (int i = _repers[1] + 1; i < _repers[3]; i++)
                if (_pulseWave[i] < _pulseWave[_repers[2]])
                    _repers[2] = i;
            for (int i = _repers[2] + 1; i < _pulseWave.Length; i++)
                if (_pulseWave[i] > _pulseWave[_repers[3]])
                    _repers[3] = i;

            if (_repers[3] - _repers[2] < 5)
            {
                _repers[2] = _repers[1] + 11;
                _repers[3] = _repers[2] + 11;
            }

            _repers[4] = Math.Max(_repers[3] + 2, _derivative.Length / 4 * 3 + 1);
            float minDer = float.MaxValue;
            for (int i = _repers[4]; i < _derivative.Length - 2 - 10; i++)
            {
                float sredDerivative = 0;
                for (int j = -1; j <= 1; j++)
                    sredDerivative += Math.Abs(_derivative[i + j]);
                sredDerivative /= 3;
                if (sredDerivative <= minDer)
                {
                    _repers[4] = i;
                    minDer = sredDerivative;
                }
            }
            if ((_pulseWave.Length - 1) - _repers[4] < 5)
                _repers[4] = (_pulseWave.Length - 1) - 11;
        }

        public void AutoRepers_OLD()
        {
            if (_pulseWave == null) return;
            _derivative = new int[_pulseWave.Length];
            for (int i = 0; i < _pulseWave.Length - 1; i++)
                _derivative[i + 1] = _pulseWave[i + 1] - _pulseWave[i];
            _derivative[0] = _derivative[1];
            _repers = new int[6];
            _repers[0] = 0;
            _repers[5] = _pulseWave.Length - 1;
            ushort max = _pulseWave.Max();
            for (int i = 0; i < _pulseWave.Length; i++)
            {
                if (_pulseWave[i] == max)
                    _repers[1] = i;
            }
            int begin = (_repers[5] - _repers[1])/15,
                end = (_repers[5] - _repers[1])/10,
                analyzePeriod = (_repers[5] - _repers[1] - begin - end) / 3;
            for (int i = 0; i < 3; i++)
            {
                _repers[i + 2] = _repers[1] + begin + analyzePeriod * i;
                int maxDerivative = _derivative[_repers[i + 2]];
                for (int j = _repers[1] + begin + analyzePeriod * i + 1; j < _repers[1] + begin + analyzePeriod * (i + 1); j++)
                    if (_derivative[j] > maxDerivative)
                    {
                        _repers[i + 2] = j;
                        maxDerivative = _derivative[j];
                    }
            }
        }

        private double _scale = 1.0;
        private int _padding, _reperWidth;

        public void DrawPulseWave(Graphics gr, Rectangle rct, out int scroll, bool widthScale = true, double scale = 1.0)
        {
            gr.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            gr.Clear(Color.White);
            if (_pulseWave != null)
            {
                Font fontText = new Font("Microsoft Sans Serif", 8);
                _padding = (int) gr.MeasureString("A1", fontText).Height + 10;
                _reperWidth = (int) gr.MeasureString("A1", fontText).Width;
                _scale = widthScale ? (double)(rct.Width - _padding * 2) / _pulseWave.Length : scale;
                Point[] pulsePoints = new Point[(int)Math.Min(Math.Round((rct.Width - Math.Max(_padding - rct.Left, 0)) / _scale),
                        Math.Max(Math.Floor(_pulseWave.Length - Math.Max((rct.Left - _padding) / _scale, 0)), 0))];
                Pen penPulse = new Pen(Color.Black, 2);
                Color colorTags = Color.Gray;
                gr.DrawRectangle(new Pen(colorTags), _padding - rct.Left, _padding, _pulseWave.Length * (float)_scale,
                                 rct.Height - _padding * 2);
                uint h, H = (uint)(rct.Height - _padding * 2 - 2);
                uint min = _pulseWave.Min(), max = _pulseWave.Max();
                int iPoint = 0;
                double curX = Math.Max(_padding - rct.Left, 0);
                while ((curX < rct.Width) && (iPoint + Math.Max((int)(Math.Round((rct.Left - _padding) / _scale)), 0) < _pulseWave.Length) &&
                    (iPoint >= 0) && (iPoint < pulsePoints.Length))
                {
                    int iIndex = iPoint + Math.Max((int)(Math.Round((rct.Left - _padding) / _scale)), 0);
                    h = _pulseWave[iIndex] - min;
                    Point curPoint = new Point((int) curX,
                                               (int)(rct.Height - _padding - 1 - h * H / (max - min)));
                    pulsePoints[iPoint] = curPoint;
                    // разметка
                    for (int i = 0; i < _repers.Length; i++)
                    {
                        if (_repers[i] == iIndex)
                        {
                            Color curColor = colorTags;
                            int curWidth = 1;
                            Font curFont = fontText;
                            if (_idMouseMove == i)
                            {
                                curColor = Color.Red;
                                curWidth = 2;
                                if (MoveReperMode)
                                {
                                    //curWidth = 3;
                                    curFont = new Font(curFont, FontStyle.Bold);
                                }
                            }
                            Pen curPen = new Pen(curColor, curWidth);
                            SolidBrush curBrush = new SolidBrush(curColor);
                            gr.DrawLine(curPen, new Point(curPoint.X, curPoint.Y),
                                        new Point(curPoint.X, rct.Height - _padding));
                            string strReper = "A" + (i + 1);
                            gr.DrawString(strReper, curFont, curBrush,
                                          new PointF(curPoint.X - gr.MeasureString(strReper, curFont).Width / 2,
                                                     rct.Height - _padding + 5));
                        }
                    }
                    curX += _scale;
                    iPoint++;
                }
                gr.DrawCurve(penPulse, pulsePoints, 1.0F);
                scroll = (int) (Math.Ceiling(_pulseWave.Length*_scale + _padding*2));
            }
            else
                scroll = 0;
        }


        //*****************************************************************
        //                  Перемещение реперов мышью                     *
        //*****************************************************************

        private int _idMouseMove = -1;

        public bool MouseMove(Point loc, Rectangle rct, out bool refresh)
        {
            refresh = false;
            for (int i = 0; i < _repers.Length; i++)
            {
                if (PointInReper(i, loc, rct))
                {
                    int _idMouseMoveNew = i;
                    if (_idMouseMove != _idMouseMoveNew)
                    {
                        refresh = true;
                        _idMouseMove = _idMouseMoveNew;
                    }
                    return true;
                }
            }
            if (_idMouseMove != -1)
                refresh = true;
            _idMouseMove = -1;
            return false;
        }

        // проверка, что курсор над репером
        private bool PointInReper(int i, Point loc, Rectangle rct)
        {
            int curX = (int)(Math.Max(_padding - rct.Left, 0) - rct.Left + _repers[i]*_scale);
            if (loc.X >= curX - _reperWidth/2 && loc.X <= curX + _reperWidth/2 &&
                loc.Y >= _padding)
                return true;
            return false;
        }

        public bool MoveReperMode = false;

        public bool MouseDown(Point loc)
        {
            if (_idMouseMove >= 0 && _idMouseMove < _repers.Length)
            {
                MoveReperMode = true;
                return true;
            }
            return false;
        }

        public bool MouseUp()
        {
            if (MoveReperMode)
            {
                MoveReperMode = false;
                return true;
            }
            return false;
        }

        public bool ReperMove(Point loc, Rectangle rct)
        {
            if (_idMouseMove >= 0 && _idMouseMove < _repers.Length)
            {
                int newX = (int)Math.Round((loc.X - Math.Max(_padding - rct.Left, 0) + rct.Left) / _scale);
                if (newX < 0)
                    newX = 0;
                if (newX > _pulseWave.Length - 1)
                    newX = _pulseWave.Length - 1;
                _repers[_idMouseMove] = newX;
                return true;
            }
            return false;
        }


        //*****************************************************************
        //                            Расчеты                             *
        //*****************************************************************

        public string ResultStrP5P2 { get; private set; }
        public string ResultStrKp { get; private set; }
        public string ResultStrKn { get; private set; }
        public int ResultTopPressure { get; private set; }
        public int ResultLowerPressure { get; private set; }

        public void MakeCalc(bool sex, int age, float growth, int weight,
            float j, float n, float h, float i, float r)
        {
            if (_repers == null || _repers.Length < 5)
                return;
            float s = (sex ? 1 : 0.96f),
                  b = DiscretesToMillimeters(Math.Abs(_repers[3] - _repers[0]));
            ResultStrP5P2 = "A4 - A1 = " + Math.Abs(_repers[3] - _repers[0]) + " дискрет(ы) = " + PrintStr(b) +
                            " в \"Федянинских\" мм.";
            ResultStrKp = "Kp = A3/A4 = " + _pulseWave[_repers[2]] + "/" + _pulseWave[_repers[3]] + " = ";
            ResultStrKn = "Kn = (" + age + "/54,5)^" + PrintStr(j) + "*(" + weight + "/(" + growth.ToString(CultureInfo.InvariantCulture) + "-100)^" + PrintStr(n) +
                "*" + PrintStr(s) + "^" + PrintStr(h) + "*(14/" + PrintStr(b) + ")^" + PrintStr(i);
            double polynom = Math.Pow(age/54.5, j),
                   kN = polynom;
            polynom = Math.Pow((double)weight / (growth - 100), n);
            kN *= polynom;
            polynom = Math.Pow(s, h);
            kN *= polynom;
            polynom = Math.Pow(14.0/b, i);
            kN *= polynom;
            polynom = (double) _pulseWave[_repers[2]]/_pulseWave[_repers[3]];
            ResultStrKp += PrintStr(polynom) + ".";
            ResultStrKn += "*(1,43*" + PrintStr(polynom) + ")^" + PrintStr(r) + " = ";
            polynom = Math.Pow(1.43 * polynom, r);
            kN *= polynom;
            ResultStrKn += PrintStr(kN) + ".";
            ResultTopPressure = (int)Math.Round(120 * kN);
            if (ResultTopPressure > 120)
                ResultLowerPressure = (int) Math.Round(150*(1 - Math.Exp(-(ResultTopPressure - 41.3)/(310.0/3))));
            //ResultLowerPressure = (int)Math.Round(105*(1 - Math.Exp(- ResultTopPressure/(200.0/3.0))));
            else
                ResultLowerPressure = (int)Math.Round(ResultTopPressure * 2.0 / 3.0);
        }

        // переводим дискреты в "Федянинские" миллиметры
        private static float DiscretesToMillimeters(int dicretes)
        {
            return dicretes * 5.8f / 22f;
            //return dicretes * 6.14f / 22f;
        }

        private static string PrintStr(float val)
        {
            return val.ToString("0.##", CultureInfo.CreateSpecificCulture("ru-RU"));
        }

        private static string PrintStr(double val)
        {
            return val.ToString("0.##", CultureInfo.CreateSpecificCulture("ru-RU"));
        }


    }
}