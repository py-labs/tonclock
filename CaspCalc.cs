﻿//
//   Класс CaspCalc для расчета центральной ПВ.
//

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace TonClock
{
    class CaspCalc
    {
        private readonly int[] _repers;  // реперы

        public ushort[] PulseWave { get; private set; } // пв

        public CaspCalc(ushort[] pw, int[] repers)
        {
            PulseWave = pw;
            _repers = repers;
        }


        //*****************************************************************
        //                            Расчеты                             *
        //*****************************************************************

        public float ResultSrpvp { get; private set; }
        public float ResultDelta { get; private set; }
        //public float ResultSrpvc { get; private set; }
        public ushort[] ResultPulseWave { get; private set; }
        public int[] ResultRepers { get; private set; }

        private static float CalcSrpvp(float l1, float l2, float l5, float timeA1A4)
        {
            return 2f * (l2 + l1 + l5) / timeA1A4;
        }

        private static float CalcDelta(float l2, float l5, float srpvp)
        {
            return (l2 - l5) / srpvp;
        }

        private static float CalcSrpvc(float l1, float l2, float l5, float timeA1A4, float delta)
        {
            return 2f*l5/((timeA1A4/2) - (delta*(l1 + l2 - l5)/(l2 - l5)));
        }

        public void MakeCalc(float l1, float l2, float l5, float timeA1A4, bool testPrint = false)
        {
            if (_repers == null || _repers.Length < 5 || PulseWave == null || PulseWave.Length <= 0)
                return;

            l1 = SmToM(l1);
            l2 = SmToM(l2);
            l5 = SmToM(l5);

            ResultSrpvp = CalcSrpvp(l1, l2, l5, timeA1A4);
            ResultDelta = CalcDelta(l2, l5, ResultSrpvp);
            //ResultSrpvc = CalcSrpvc(l1, l2, l5, timeA1A4, ResultDelta);

            CaspEtalov etalon = new CaspEtalov(ResultSrpvp);
            ResultPulseWave = new ushort[PulseWave.Length];

            if (testPrint)
            {
                File.AppendAllText(ConstGlobals.TestPrintFileFullName,
                                   string.Format("=== Начало обработки CASP, {0}, в {1} ==={2}{2}{2}",
                                                 DateTime.Now.ToShortDateString(), DateTime.Now.ToLongTimeString(), Environment.NewLine));

                File.AppendAllText(ConstGlobals.TestPrintFileFullName,
                                   string.Format("{1}{2}{2}|{0}i{0}|{0}Периферийная (лучевая){0}|{0}Центральная (сонная){0}|{2}{2}",
                                                 "\t", "--- ЭТАЛОН: ---", Environment.NewLine));
                for (int i = 0; i < etalon.PulseWavePerif.Length && i < etalon.PulseWaveCentral.Length; i++)
                    File.AppendAllText(ConstGlobals.TestPrintFileFullName,
                                   string.Format("|{0}{1}{0}|{0}{2}{0}{0}{0}|{0}{3}{0}{0}{0}|{4}",
                                                 "\t", i + 1, PrintStr(etalon.PulseWavePerif[i]), PrintStr(etalon.PulseWaveCentral[i]), Environment.NewLine));
                File.AppendAllText(ConstGlobals.TestPrintFileFullName,
                                   string.Format("{2}{2}{1}{2}{2}|{0}i{0}|{0}Периферийная (расчет){0}|{0}Периферийная (эталон){0}|{0}Центральная (эталон){0}|{0}Отношение{0}|{0}Центральная (расчет){0}|{2}{2}",
                                                 "\t", "--- РАСЧЕТ: ---", Environment.NewLine));
            }
            
            for (int i = 0; i < PulseWave.Length; i++)
            {
                int curI = etalon.GetIndex(PulseWave.Length, _repers[0], _repers[3], i);
                double proportion = etalon.CalcProportion(curI);
                //ResultPulseWave[i] = etalon.CalcProportion(PulseWave[i], curI);
                ResultPulseWave[i] = (ushort)Math.Round(PulseWave[i] * proportion);

                if (testPrint)
                {
                    var flagReper = string.Empty;
                    if (i == _repers[0]) flagReper = "A1";
                    else if (i == _repers[3]) flagReper = "A4";

                    var flagReperEtalon = etalon.GetFlagReper(curI);

                    File.AppendAllText(ConstGlobals.TestPrintFileFullName,
                                       string.Format(
                                           "|{0}{1}{0}|{0}{2}{0}{3}{0}{0}|{0}{4}{0}{5}{0}{0}|{0}{6}{0}{0}{0}|{0}{7}{0}{0}|{0}{8}{0}{0}{0}|{9}",
                                           "\t", i + 1, PrintStr(PulseWave[i]), flagReper,
                                           PrintStr(etalon.PulseWavePerif[curI]), flagReperEtalon,
                                           PrintStr(etalon.PulseWaveCentral[curI]), PrintStr(proportion),
                                           PrintStr(ResultPulseWave[i]), Environment.NewLine));
                }
            }

            if (testPrint)
                File.AppendAllText(ConstGlobals.TestPrintFileFullName,
                                   string.Format("{0}{0}{0}", Environment.NewLine));

            // расстановка реперов
            ResultRepers = CalcRepers(ResultPulseWave);
        }

        private int[] CalcRepers(ushort[] pulseWave)
        {
            if (pulseWave == null) return null;
            
            var derivative = new int[pulseWave.Length];
            for (int i = 0; i < pulseWave.Length - 1; i++)
                derivative[i + 1] = pulseWave[i + 1] - pulseWave[i];
            derivative[0] = derivative[1];
            var repers = new int[2];
            ushort max = pulseWave.Max();
            for (int i = 0; i < pulseWave.Length; i++)
            {
                if (pulseWave[i] == max)
                    repers[1] = i;
            }

            //repers[1] = 48;
            //repers[0] = 0;
            //float minDer = float.MaxValue;
            //for (int i = repers[0] + 1; i < repers[1] - repers[1]/10; i++)
            //{ 
            //    float sredDerivative = 0;
            //    for (int j = -1; j <= 1; j++)
            //        sredDerivative += Math.Abs(derivative[i + j]);
            //    sredDerivative /= 3;
            //    if (sredDerivative <= minDer)
            //    {
            //        repers[0] = i;
            //        minDer = sredDerivative;
            //    }
            //}

            repers[0] = repers[1]*2/3;

            return repers;
        }

        private static float SmToM(float sm)
        {
            return sm/100f;
        }

        public static string PrintStr(float val)
        {
            return val.ToString("0.##", CultureInfo.CreateSpecificCulture("ru-RU"));
        }

        public static string PrintStr(double val)
        {
            return val.ToString("0.##", CultureInfo.CreateSpecificCulture("ru-RU"));
        }

        public static string PrintStrShort(float val)
        {
            return val.ToString("0.#", CultureInfo.CreateSpecificCulture("ru-RU"));
        }

        public static string PrintStrShort(double val)
        {
            return val.ToString("0.#", CultureInfo.CreateSpecificCulture("ru-RU"));
        }


    }

    class CaspEtalov
    {
        public double[] PulseWavePerif { get; private set; }    // пв периферийная (лучевая)

        public double[] PulseWaveCentral { get; private set; }  // пв центральная (сонная)

        public int ReperA1 { get; private set; }                // репер А1 (от 0)
        public int ReperA4 { get; private set; }                // репер А4 (от 0)

        public CaspEtalov(float srpvp)
        {
            //if (srpvp > 12)
            //{
            //    PulseWavePerif = new[]
            //                         {
            //                             90.20960288, 91.27392422, 95.01474926, 102.1067155, 111.4787055, 120.922507,
            //                             126.10097, 128.1085736, 126.7431856, 124.0163641, 121.5832366, 118.9246134,
            //                             117.1110563, 115.8078031, 114.4908233, 112.9877741, 110.2861061, 107.1969423,
            //                             105.3510713, 104.867517, 105.547284, 106.8498087, 108.0541242, 108.2339588,
            //                             107.8158529, 106.7567318, 105.3091466, 103.6954365, 102.0751077, 100.8919035,
            //                             99.34154335, 98.35012084, 97.50720901, 96.88494945, 96.10969339, 95.61496061,
            //                             95.28578031, 94.91586225, 94.78281562, 94.59848825, 94.59848825, 94.59721045,
            //                             94.47784927, 94.65781335, 94.197902, 93.69932815, 92.98243526, 92.35726205,
            //                             91.47492625, 90.7688135
            //                         };
            //    PulseWaveCentral = new[]
            //                           {
            //                               92, 95.17184073, 102.4807462, 109.3957893, 113.7525024, 116.1040737,
            //                               117.1318043,
            //                               117.0839532, 116.9173256, 117.1765654, 117.7003889, 118, 117.9471758,
            //                               117.6139207,
            //                               116.8296418, 114.0125246, 109.5595583, 106.2951167, 106.344379, 106.8442616,
            //                               106.8434828, 106.5702719, 105.734692, 104.7997936, 103.5084301, 102.324063,
            //                               101.1443826, 100.2166358, 99.04849145, 98.29866749, 97.55398898, 96.98011141,
            //                               96.14805605, 95.36158966, 94.6534226, 94.08314681, 93.77734126, 93.55708149,
            //                               93.29442218, 93.04448087, 92.95575883, 93.17200202, 93.11494183, 92.45720151,
            //                               91.81300101, 91.47974592, 91.34900601, 91.30777945, 91.48948966, 91.70850781
            //                           };
            //    ReperA1 = 7;
            //    ReperA4 = 23;
            //}
            PulseWavePerif = new[]
                                 {
                                     171d, 299d, 622d, 937d, 1490d, 2058d, 2813d, 3653d, 4585d, 5634d, 6702d, 7890d,
                                     9016d, 10230d, 11274d, 12367d, 13284d, 14193d, 14920d, 15552d, 16097d, 16476d,
                                     16824d, 16975d, 17149d, 17142d, 17188d, 17098d, 17042d, 16906d, 16758d, 16627d,
                                     16417d, 16289d, 16023d, 15869d, 15580d, 15393d, 15109d, 14891d, 14630d, 14354d,
                                     14106d, 13793d, 13555d, 13186d, 12920d, 12523d, 12206d, 11768d, 11387d, 10934d,
                                     10475d, 10017d, 9510d, 9106d, 8604d, 8230d, 7785d, 7455d, 7068d, 6769d, 6464d,
                                     6216d, 6026d, 5791d, 5668d, 5473d, 5402d, 5235d, 5199d, 5061d, 5018d, 4910d, 4836d,
                                     4779d, 4649d, 4608d, 4465d, 4436d, 4283d, 4250d, 4091d, 4014d, 3896d, 3784d, 3697d,
                                     3546d, 3469d, 3302d, 3243d, 3069d, 3008d, 2834d, 2754d, 2644d, 2532d, 2460d, 2327d,
                                     2271d, 2120d, 2091d, 1938d, 1912d, 1792d, 1725d, 1615d, 1531d, 1485d, 1357d, 1326d,
                                     1201d, 1193d, 1062d, 1044d, 937d, 901d, 842d, 776d, 765d, 683d, 686d, 581d, 617d,
                                     527d, 555d, 494d, 502d, 471d, 453d, 458d, 404d, 445d, 397d, 450d, 381d, 433d, 381d,
                                     381d, 363d, 343d, 353d, 302d, 325d, 248d, 294d, 194d, 243d, 174d, 189d, 159d, 146d,
                                     143d, 140d, 135d, 129d, 126d, 125d, 124d, 123d, 122d, 121d, 120d, 120d, 120d, 120d
                                 };
            PulseWaveCentral = new[]
                                   {
                                       116d, 45d, 65d, 16d, 48d, 40d, 102d, 247d, 592d, 1204d, 1977d, 2950d, 3897d,
                                       4877d, 5672d, 6388d, 6942d, 7452d, 7885d, 8308d, 8680d, 8939d, 9155d, 9209d,
                                       9251d, 9168d, 9094d, 8956d, 8855d, 8749d, 8631d, 8562d, 8481d, 8483d, 8441d,
                                       8493d, 8503d, 8567d, 8592d, 8641d, 8695d, 8732d, 8788d, 8784d, 8808d, 8781d,
                                       8788d, 8705d, 8675d, 8567d, 8473d, 8340d, 8171d, 8011d, 7764d, 7545d, 7230d,
                                       6957d, 6608d, 6246d, 5876d, 5574d, 5350d, 5222d, 5172d, 5101d, 5091d, 5020d,
                                       5012d, 4948d, 4953d, 4914d, 4916d, 4914d, 4889d, 4872d, 4806d, 4781d, 4670d,
                                       4624d, 4493d, 4407d, 4274d, 4168d, 4043d, 3905d, 3794d, 3656d, 3575d, 3412d,
                                       3344d, 3198d, 3142d, 3028d, 2952d, 2873d, 2780d, 2723d, 2615d, 2578d, 2455d,
                                       2411d, 2312d, 2248d, 2152d, 2093d, 2024d, 1936d, 1881d, 1773d, 1739d, 1640d,
                                       1603d, 1505d, 1470d, 1382d, 1342d, 1291d, 1239d, 1204d, 1153d, 1148d, 1067d,
                                       1074d, 1020d, 1017d, 966d, 953d, 921d, 870d, 867d, 803d, 801d, 734d, 747d, 680d,
                                       668d, 624d, 596d, 564d, 530d, 523d, 468d, 473d, 404d, 436d, 400d, 402d, 375d,
                                       380d, 380d, 363d, 358d, 301d, 323d, 264d, 218d, 200d, 185d, 173d, 160d, 150d,
                                       140d, 132d, 125d, 120d
                                   };
            ReperA1 = 26;
            ReperA4 = 74;
        }

        public int GetIndex(int length, int a1, int a4, int curI)
        {
            if (PulseWavePerif == null || PulseWaveCentral == null) return 0;

            int minLength = Math.Min(PulseWavePerif.Length, PulseWaveCentral.Length);

            if (curI <= a1)
            {
                if (a1 <= 0) return 0;
                return ReperA1 * curI / a1;
            }
            if (a1 < curI && curI < a4)
            {
                if ((a4 - a1) <= 0) return ReperA1;
                return ReperA1 + (ReperA4 - ReperA1) * (curI - a1) / (a4 - a1);
            }
            if ((length - a4) <= 0) return minLength;
            return ReperA4 + (minLength - ReperA4) * (curI - a4) / (length - a4);
        }

        public double CalcProportion(int i)
        {
            if (PulseWavePerif == null || PulseWaveCentral == null || i >= PulseWavePerif.Length || i >= PulseWaveCentral.Length) return 0;

            //return (ushort) Math.Round(value*PulseWaveCentral[curI]/PulseWavePerif[curI]);
            return PulseWaveCentral[i] / PulseWavePerif[i];
        }

        public string GetFlagReper(int i)
        {
            var flagReper = string.Empty;
            if (i == ReperA1) flagReper = "A1";
            else if (i == ReperA4) flagReper = "A4";
            
            return flagReper;
        }
    }
}