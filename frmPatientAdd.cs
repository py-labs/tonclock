﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Globalization;

namespace TonClock
{
    public partial class frmPatientAdd : Form
    {
        bool flagAdd, flagOK;
        int idDoctor, idRecord;

        public frmPatientAdd(int idD, int id = 0, bool add = true)
        {
            InitializeComponent();
            idDoctor = idD;
            idRecord = id;
            flagAdd = add;
            if (flagAdd)
            {
                this.Text = "Добавление пациента";
                lblTitle.Text = "Добавление записи о новом пациенте:";
                picDoctor.Image = global::TonClock.Properties.Resources.user_add;
            }
            else
            {
                this.Text = "Редактирование пациента";
                lblTitle.Text = "Редактирование записи о пациенте:";
                picDoctor.Image = global::TonClock.Properties.Resources.user_edit;
            }
            flagOK = false;
        }

        private void frmPatientAdd_Shown(object sender, EventArgs e)
        {
            if (!flagAdd)
            {
                try
                {
                    using (var myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                    {
                        myOleDbConnection.Open();
                        var myOleDbCommand = new OleDbCommand(
                            "SELECT lastname, firstname, patronymic, " +
                            "       sex, age, growth, weight, " +
                            "       topPressureControl, lowerPressureControl, wrist, " +
                            "       l1, l2, l3, l4, l5, " +
                            "       history, complaints, destination " +
                            "FROM Patients " +
                            "WHERE id = " + idRecord,
                            myOleDbConnection);
                        using (var myReader = myOleDbCommand.ExecuteReader())
                        {
                            if (myReader != null && myReader.Read())
                            {
                                txtLastname.Text = myReader[0].ToString();
                                txtFirstname.Text = myReader[1].ToString();
                                txtPatronymic.Text = myReader[2].ToString();
                                SetSexRdb(myReader.GetBoolean(3));
                                numAge.Value = myReader.GetInt16(4);
                                numGrowth.Value = myReader.IsDBNull(5) ? 0 : (decimal) myReader.GetFloat(5);
                                numWeight.Value = myReader.GetInt16(6);
                                numTopPressure.Value = myReader.GetInt16(7);
                                numLowerPressure.Value = myReader.GetInt16(8);
                                numWrist.Value = myReader.IsDBNull(9) ? 0 : (decimal) myReader.GetFloat(9);
                                numL1.Value = myReader.IsDBNull(10) ? 0 : (decimal) myReader.GetFloat(10);
                                numL2.Value = myReader.IsDBNull(11) ? 0 : (decimal) myReader.GetFloat(11);
                                numL3.Value = myReader.IsDBNull(12) ? 0 : (decimal) myReader.GetFloat(12);
                                numL4.Value = myReader.IsDBNull(13) ? 0 : (decimal) myReader.GetFloat(13);
                                numL5.Value = myReader.IsDBNull(14) ? 0 : (decimal) myReader.GetFloat(14);
                                txtHistory.Text = myReader[15].ToString();
                                txtComplaints.Text = myReader[16].ToString();
                                txtDestination.Text = myReader[17].ToString();
                            }
                            else
                            {
                                throw new Exception("Нет пациента с таким идентификатором.");
                            }
                        }
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                                    exc.Message + "\n\n" +
                                    "Пожалуйста, обратитесь к администратору.",
                                    "Ошибка",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
        
        private void SetSexRdb(bool sex)
        {
            rdbSexMale.Checked = sex;
            rdbSexFemale.Checked = !sex;
        }


        private void btnOK_Click(object sender, EventArgs e)
        {
            if (!this.ValidateChildren())
                MessageBox.Show("Ошибка во введенных данных!\n\n" +
                                "Для получения дополнительной информации\n" +
                                "наведите курсор мыши на восклицательные знаки.",
                                "Ошибка",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                try
                {
                    using (var myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                    {
                        myOleDbConnection.Open();
                        var myOleDbCommand = new OleDbCommand {Connection = myOleDbConnection};
                        if (flagAdd)
                            myOleDbCommand.CommandText =
                                "INSERT INTO Patients " +
                                "   (lastname, firstname, patronymic, " +
                                "    sex, age, growth, weight, topPressureControl, lowerPressureControl, wrist, l1, l2, l3, l4, l5, " +
                                "    doctor, history, complaints, destination) " +
                                "VALUES ('" + txtLastname.Text.Trim() + "', '" +
                                txtFirstname.Text.Trim() + "', '" + txtPatronymic.Text.Trim() + "', " +
                                (rdbSexMale.Checked ? "true" : "false") + ", " + numAge.Value + ", " +
                                numGrowth.Value.ToString(CultureInfo.InvariantCulture) + ", " + numWeight.Value + ", " +
                                numTopPressure.Value + ", " + numLowerPressure.Value + ", " +
                                numWrist.Value.ToString(CultureInfo.InvariantCulture) + ", " +
                                numL1.Value.ToString(CultureInfo.InvariantCulture) + ", " +
                                numL2.Value.ToString(CultureInfo.InvariantCulture) + ", " +
                                numL3.Value.ToString(CultureInfo.InvariantCulture) + ", " +
                                numL4.Value.ToString(CultureInfo.InvariantCulture) + ", " +
                                numL5.Value.ToString(CultureInfo.InvariantCulture) + ", " +
                                idDoctor + ", " + "'" + txtHistory.Text + "', '" + txtComplaints.Text + "', '" + txtDestination.Text + "')";
                        else
                        {
                            myOleDbCommand.CommandText =
                                "UPDATE Patients " +
                                "SET lastname = '" + txtLastname.Text.Trim() + "', " +
                                "    firstname = '" + txtFirstname.Text.Trim() + "', patronymic = '" + txtPatronymic.Text.Trim() + "', " +
                                "    sex = " + (rdbSexMale.Checked ? "true" : "false") + ", " +
                                "    age = " + numAge.Value + ", growth = " + numGrowth.Value.ToString(CultureInfo.InvariantCulture) + ", weight = " + numWeight.Value + ", " +
                                "    topPressureControl = " + numTopPressure.Value + ", lowerPressureControl = " + numLowerPressure.Value + ", " +
                                "    wrist = " + numWrist.Value.ToString(CultureInfo.InvariantCulture) + ", " +
                                "    l1 = " + numL1.Value.ToString(CultureInfo.InvariantCulture) + ", " +
                                "    l2 = " + numL2.Value.ToString(CultureInfo.InvariantCulture) + ", " +
                                "    l3 = " + numL3.Value.ToString(CultureInfo.InvariantCulture) + ", " +
                                "    l4 = " + numL4.Value.ToString(CultureInfo.InvariantCulture) + ", " +
                                "    l5 = " + numL5.Value.ToString(CultureInfo.InvariantCulture) + ", " +
                                "    history = '" + txtHistory.Text + "', complaints = '" + txtComplaints.Text + "', destination = '" + txtDestination.Text + "' " +
                                "WHERE id = " + idRecord;
                        }
                        flagOK = (myOleDbCommand.ExecuteNonQuery() > 0);
                        if (flagAdd)
                        {
                            myOleDbCommand.CommandText = "SELECT @@IDENTITY";
                            idRecord = (int)myOleDbCommand.ExecuteScalar();
                        }
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                        exc.Message + "\n\n" +
                        "Пожалуйста, обратитесь к администратору.",
                        "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    flagOK = false;
                }
                this.DialogResult = DialogResult.OK;
            }
        }

        public bool returnFlagOK() { return flagOK; }
        public int returnIdRecord() { return idRecord; }


        // ---------------- проверка введенных данных ----------------

        private void txtLastname_Validating(object sender, CancelEventArgs e)
        {
            if (txtLastname.Text.Trim().Length > 0 && txtLastname.Text.IndexOf("'") == -1)
                errorProvider1.SetError(txtLastname, String.Empty);
            else
            {
                if (txtLastname.Text.IndexOf("'") != -1)
                    errorProvider1.SetError(txtLastname, "Символ одинарной кавычки \"'\" является недопустимым.");
                else
                    errorProvider1.SetError(txtLastname, "Поле \"Фамилия\" должно быть заполнено.");
                e.Cancel = true;
            }
        }

        private void txtFirstname_Validating(object sender, CancelEventArgs e)
        {
            if (txtFirstname.Text.Trim().Length > 0 && txtFirstname.Text.IndexOf("'") == -1)
                errorProvider1.SetError(txtFirstname, String.Empty);
            else
            {
                if (txtFirstname.Text.IndexOf("'") != -1)
                    errorProvider1.SetError(txtFirstname, "Символ одинарной кавычки \"'\" является недопустимым.");
                else
                    errorProvider1.SetError(txtFirstname, "Поле \"Имя\" должно быть заполнено.");
                e.Cancel = true;
            }
        }

        private void txtPatronymic_Validating(object sender, CancelEventArgs e)
        {
            if (txtPatronymic.Text.Trim().Length > 0 && txtPatronymic.Text.IndexOf("'") == -1)
                errorProvider1.SetError(txtPatronymic, String.Empty);
            else
            {
                if (txtPatronymic.Text.IndexOf("'") != -1)
                    errorProvider1.SetError(txtPatronymic, "Символ одинарной кавычки \"'\" является недопустимым.");
                else
                    errorProvider1.SetError(txtPatronymic, "Поле \"Имя\" должно быть заполнено.");
                e.Cancel = true;
            }
        }

        private void txtLastname_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(txtLastname, String.Empty);
        }

        private void txtFirstname_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(txtFirstname, String.Empty);
        }

        private void txtPatronymic_TextChanged(object sender, EventArgs e)
        {
            errorProvider1.SetError(txtPatronymic, String.Empty);
        }


    }
}
