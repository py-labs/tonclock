﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using TonClock;

namespace TonClock
{
    public partial class frmPatients : Form
    {
        
        int idDoctor;   // id врача (консультанта) 
        frmDoctor myFrmDoctor;
        private string _strTitle;

        public frmPatients(int id, frmDoctor frm)
        {
            InitializeComponent();
            this.Icon = Properties.Resources.yagodka;
            idDoctor = id;
            try
            {
                using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    OleDbCommand myOleDbCommand = new OleDbCommand(
                        "SELECT Doctors.lastname, Doctors.firstname, Doctors.patronymic, " +
                        "       Cities.cName, Organizations.oName " +
                        "FROM Doctors, Cities, Organizations " +
                        "WHERE Doctors.id = " + idDoctor.ToString() + " " +
                        "  AND Doctors.city = Cities.id " +
                        "  AND Doctors.organization = Organizations.id",
                       myOleDbConnection);
                    using (OleDbDataReader myReader = myOleDbCommand.ExecuteReader())
                    {
                        if (myReader.Read())
                        {
                            _strTitle = ConstGlobals.AssemblyTitle + " - " + myReader[0].ToString() + " " +
                                        myReader[1].ToString()[0] + "." + myReader[2].ToString()[0] + "., " +
                                        myReader[3].ToString() + ", " + myReader[4].ToString();
                        }
                        else
                            throw new Exception("В базе не обнаружен консультант с таким идентификатором.");
                    }
                }
                UpdateMainTable();
                UpdatePatientInfo();
                myFrmDoctor = frm;
            }
            catch (Exception exc)
            {
                MessageBox.Show("Внимание! При попытке открытия базы данных произошла ошибка:\n" +
                    exc.Message + "\n\n" +
                    "Дальнейшая работа программы не возможна.\n\n" +
                    "Пожалуйста, обратитесь к администратору.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
            }
            // стили таблиц
            //var grdHeaderStyle = new DataGridViewCellStyle(grdMeasurements.ColumnHeadersDefaultCellStyle)
            //                         {Font = new Font(grdMeasurements.ColumnHeadersDefaultCellStyle.Font.FontFamily, 8)};
            //grdMeasurements.ColumnHeadersDefaultCellStyle = grdHeaderStyle;
            //grdMeasurements.Columns[0].Visible = false;
        }


        private void frmPatients_Load(object sender, EventArgs e)
        {
            this.Text = _strTitle;
        }

        private void frmPatients_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (!myFrmDoctor.Visible)
                Application.Exit();
        }

        private void UpdateMainTable(int id = 0)
        {
            using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
            {
                myOleDbConnection.Open();
                OleDbCommand myOleDbCommand = new OleDbCommand(
                    "SELECT id, lastname, firstname, patronymic " +
                    "FROM Patients " +
                    "WHERE doctor = " + idDoctor.ToString() + " " +
                    "ORDER BY id ASC",
                    myOleDbConnection);
                grdPatients.Rows.Clear();
                using (OleDbDataReader myReader = myOleDbCommand.ExecuteReader())
                {
                    while (myReader.Read())
                    {
                        grdPatients.Rows.Add();
                        for (int i = 0; i < myReader.FieldCount; i++)
                            grdPatients.Rows[grdPatients.Rows.Count - 1].Cells[i].Value = myReader[i].ToString();
                        if ((id != 0) && ((int) myReader[0] == id))
                        {
                            grdPatients.Rows[grdPatients.Rows.Count - 1].Selected = true;
                            grdPatients.CurrentCell = grdPatients.Rows[grdPatients.Rows.Count - 1].Cells[1];
                        }
                    }
                    btnAdd.Enabled = true;
                    btnEdit.Enabled = myReader.HasRows;
                    btnDelete.Enabled = myReader.HasRows;
                    btnBuffer.Enabled = myReader.HasRows;
                }
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            StopTimerButton();
            frmPatientAdd myFrmPatientAdd = new frmPatientAdd(idDoctor);
            myFrmPatientAdd.ShowDialog();
            if ((myFrmPatientAdd.DialogResult == DialogResult.OK) && (myFrmPatientAdd.returnFlagOK()))
            {
                try
                {
                    UpdateMainTable(myFrmPatientAdd.returnIdRecord());
                    UpdatePatientInfo();
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                        exc.Message + "\n\n" +
                        "Пожалуйста, обратитесь к администратору.",
                        "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            StartTimerButton();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            StopTimerButton();
            if ((grdPatients.SelectedRows.Count > 0) &&
                (grdPatients.SelectedRows[0].Cells[0].Value != null))
            {
                int id = int.Parse(grdPatients.SelectedRows[0].Cells[0].Value.ToString());
                frmPatientAdd myFrmPatientAdd = new frmPatientAdd(idDoctor, id, false);
                myFrmPatientAdd.ShowDialog();
                if ((myFrmPatientAdd.DialogResult == DialogResult.OK) && (myFrmPatientAdd.returnFlagOK()))
                {
                    id = myFrmPatientAdd.returnIdRecord();
                    try
                    {
                        UpdateMainTable(id);
                        UpdatePatientInfo();
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                            exc.Message + "\n\n" +
                            "Пожалуйста, обратитесь к администратору.",
                            "Ошибка",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
                MessageBox.Show("В таблице нет выделенной строки.\n" +
                    "Выполнение операции не возможно.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            StartTimerButton();
        }

        private void grdPatients_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                StopTimerButton();
                int id = int.Parse(grdPatients.Rows[e.RowIndex].Cells[0].Value.ToString());
                frmPatientAdd myFrmPatientAdd = new frmPatientAdd(idDoctor, id, false);
                myFrmPatientAdd.ShowDialog();
                if ((myFrmPatientAdd.DialogResult == DialogResult.OK) && (myFrmPatientAdd.returnFlagOK()))
                {
                    id = myFrmPatientAdd.returnIdRecord();
                    try
                    {
                        UpdateMainTable(id);
                        UpdatePatientInfo();
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                            exc.Message + "\n\n" +
                            "Пожалуйста, обратитесь к администратору.",
                            "Ошибка",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                StartTimerButton();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            StopTimerButton();
            if ((grdPatients.SelectedRows.Count > 0) &&
                (grdPatients.SelectedRows[0].Cells[0].Value != null))
            {
                DialogResult result = MessageBox.Show("Удалить выделенную запись пациента " +
                    grdPatients.SelectedRows[0].Cells[1].Value.ToString() + " " +
                    grdPatients.SelectedRows[0].Cells[2].Value.ToString()[0] + "." +
                    grdPatients.SelectedRows[0].Cells[3].Value.ToString()[0] + ". ?\n\n" +
                    "ВНИМАНИЕ: будут так же удалены все замеры данного пациента.",
                    "Удаление",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    int id = int.Parse(grdPatients.SelectedRows[0].Cells[0].Value.ToString());
                    string strError;
                    if (!DbPulseOperations.DeletePatient(id, out strError))
                        MessageBox.Show("Возникла ошибка при удалении пациента:\n" + strError,
                                        "Ошибка",
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    try
                    {
                        UpdateMainTable();
                        UpdatePatientInfo();
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                            exc.Message + "\n\n" +
                            "Пожалуйста, обратитесь к администратору.",
                            "Ошибка",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
                MessageBox.Show("В таблице нет выделенной строки.\n" +
                    "Выполнение операции не возможно.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            StartTimerButton();
        }

        // выбранный пациент
        int idPatient;

        // обновление выбранного пациента
        private void UpdatePatientInfo(int id = 0)
        {
            lblFio.Text = "";
            lblDate.Text = "";
            lblPressure.Text = "";
            grdMeasurements.Rows.Clear();
            btnAdd2.Enabled = false;
            btnDelete2.Enabled = false;
            btnHistory.Enabled = false;
            btnBuffer.Enabled = false;
            if ((grdPatients.SelectedRows.Count > 0) &&
                (grdPatients.SelectedRows[0].Cells[0].Value != null))
            {
                idPatient = int.Parse(grdPatients.SelectedRows[0].Cells[0].Value.ToString());
                using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    OleDbCommand myOleDbCommand = new OleDbCommand(
                        "SELECT id, mesDateTime, topPressure, lowerPressure, control " +
                        "FROM Measurements " +
                        "WHERE patient = " + idPatient.ToString() + " " +
                        "ORDER BY mesDateTime DESC",
                        myOleDbConnection);
                    OleDbDataReader myReader = myOleDbCommand.ExecuteReader();
                    while (myReader.Read())
                    {
                        var newRow = new DataGridViewRow()
                                         {
                                             DefaultCellStyle = new DataGridViewCellStyle(grdMeasurements.DefaultCellStyle)
                                         };
                        if (myReader.GetBoolean(4))
                        {
                            //newRow.DefaultCellStyle.ForeColor = Color.Red;
                            //newRow.DefaultCellStyle.Font = new Font(newRow.DefaultCellStyle.Font, FontStyle.Bold);
                            newRow.DefaultCellStyle.BackColor = Color.FromArgb(255, 220, 220);
                            newRow.DefaultCellStyle.ForeColor = Color.Black;
                            newRow.DefaultCellStyle.SelectionBackColor = ControlPaint.Dark(grdMeasurements.DefaultCellStyle.SelectionBackColor, 20);
                            newRow.DefaultCellStyle.SelectionForeColor = Color.White;
                        }
                        grdMeasurements.Rows.Add(newRow);
                        grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells[0].Value = myReader[0].ToString();
                        grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells[2].Value = myReader.GetDateTime(1).ToShortDateString();
                        grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells[3].Value = myReader.GetDateTime(1).ToShortTimeString();
                        grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells[4].Value = myReader.GetInt16(2).ToString();
                        grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells[5].Value = myReader.GetInt16(3).ToString();
                        if ((id != 0) && ((int)myReader[0] == id))
                        {
                            grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Selected = true;
                            grdMeasurements.CurrentCell = grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells[1];
                        }
                    }
                    for (int i = 0; i < grdMeasurements.Rows.Count; i++)
                        grdMeasurements.Rows[i].Cells[1].Value = (grdMeasurements.Rows.Count - i).ToString();
                    btnAdd2.Enabled = true;
                    btnDelete2.Enabled = myReader.HasRows;
                    btnHistory.Enabled = myReader.HasRows;
                    btnBuffer.Enabled = myReader.HasRows;
                    myReader.Close();
                    myOleDbCommand.CommandText =
                        "SELECT lastname, firstname, patronymic, age, topPressureControl, lowerPressureControl " +
                        "FROM Patients " +
                        "WHERE id = " + idPatient.ToString();
                    myReader = myOleDbCommand.ExecuteReader();
                    if (myReader.Read())
                    {
                        lblFio.Text = myReader.GetString(0) + " " + myReader.GetString(1)[0] + "." + myReader.GetString(2)[0] + ".";
                        lblDate.Text = DbPulseOperations.PrintAge(myReader.GetInt16(3));
                        lblPressure.Text = myReader.GetInt16(4).ToString() + " / " + myReader.GetInt16(5).ToString();
                    }
                    myReader.Close();
                }
            }
        }

        private void grdPatients_SelectionChanged(object sender, EventArgs e)
        {
            UpdatePatientInfo();
        }

        private void btnAdd2_Click(object sender, EventArgs e)
        {
            StopTimerButton();
            int curMesDb = (grdMeasurements.SelectedRows.Count > 0) &&
                           (grdMeasurements.SelectedRows[0].Cells[0].Value != null)
                               ? int.Parse(grdMeasurements.SelectedRows[0].Cells[0].Value.ToString())
                               : 0;
            using (var myFrmClock = new frmClock(idPatient))
            {
                myFrmClock.ShowDialog();
                UpdatePatientInfo(myFrmClock.DialogResult == DialogResult.OK ? myFrmClock.GetMesId() : curMesDb);
            }
            StartTimerButton();
        }

        private void btnBuffer_Click(object sender, EventArgs e)
        {
            StopTimerButton();
            using (frmPas myFormPas = new frmPas("буфер"))
            {
                myFormPas.ShowDialog();
                if (myFormPas.DialogResult == DialogResult.OK)
                {
                    int curMesDb = (grdMeasurements.SelectedRows.Count > 0) &&
                                   (grdMeasurements.SelectedRows[0].Cells[0].Value != null)
                                       ? int.Parse(grdMeasurements.SelectedRows[0].Cells[0].Value.ToString())
                                       : 0;
                    using (frmBuffer myFrmBuffer = new frmBuffer(idPatient, curMesDb))
                    {
                        myFrmBuffer.ShowDialog();
                        UpdatePatientInfo(curMesDb);
                    }
                }
            }
            StartTimerButton();
        }

        private void btnDelete2_Click(object sender, EventArgs e)
        {
            StopTimerButton();
            if ((grdMeasurements.CurrentRow != null) &&
                (grdMeasurements.CurrentRow.Cells[0].Value != null))
            {
                int number = int.Parse(grdMeasurements.CurrentRow.Cells[1].Value.ToString());
                DialogResult result = MessageBox.Show("Удалить замер № " + number + " из базы?\n" +
                    "(замер был проведен " +
                    grdMeasurements.CurrentRow.Cells[2].Value.ToString() + " в " +
                    grdMeasurements.CurrentRow.Cells[3].Value.ToString() + ")",
                    "Удаление",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    int id = int.Parse(grdMeasurements.CurrentRow.Cells[0].Value.ToString());
                    string strError;
                    if (!DbPulseOperations.DeleteMeasurement(id, out strError))
                        MessageBox.Show("Возникла ошибка при удалении замера:\n" + strError,
                                        "Ошибка",
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //try
                    //{
                    //    UpdatePatientInfo();
                    //}
                    //catch (Exception exc)
                    //{
                    //    MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                    //        exc.Message + "\n\n" +
                    //        "Пожалуйста, обратитесь к администратору.",
                    //        "Ошибка",
                    //        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //}
                    grdMeasurements.Rows.RemoveAt(grdMeasurements.CurrentRow.Index);
                }
            }
            else
                MessageBox.Show("В таблице нет выделенной строки.\n" +
                    "Выполнение операции не возможно.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            StartTimerButton();
        }

        private void grdMeasurements_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                StopTimerButton();
                int id = int.Parse(grdMeasurements.Rows[e.RowIndex].Cells[0].Value.ToString());
                using (var myFrmMeasurement = new frmMeasurement(id))
                {
                    myFrmMeasurement.ShowDialog();
                }
                StartTimerButton();
            }
        }


        private void btnUndo_Click(object sender, EventArgs e)
        {
            myFrmDoctor.Show();
            myFrmDoctor.StartTimerButton();
            this.Close();
        }

        private void btnHistory_Click(object sender, EventArgs e)
        {
            StopTimerButton();
            int curMesDb = (grdMeasurements.SelectedRows.Count > 0) &&
                           (grdMeasurements.SelectedRows[0].Cells[0].Value != null)
                               ? int.Parse(grdMeasurements.SelectedRows[0].Cells[0].Value.ToString())
                               : 0;
            using (var myFrmClock = new frmClock(idPatient, true))
            {
                myFrmClock.ShowDialog();
                UpdatePatientInfo(myFrmClock.DialogResult == DialogResult.OK ? myFrmClock.GetMesId() : curMesDb);
            }
            StartTimerButton();
        }


        private void tmrButton_Tick(object sender, EventArgs e)
        {
            if (btnAdd2.Enabled)
                btnAdd2.BackColor = (btnAdd2.BackColor == DefaultBackColor ? Color.LawnGreen : DefaultBackColor);
            else if (btnAdd.Enabled)
                btnAdd.BackColor = (btnAdd.BackColor == DefaultBackColor ? Color.LawnGreen : DefaultBackColor);
        }

        public void StartTimerButton()
        {
            tmrButton.Start();
        }

        private void StopTimerButton()
        {
            tmrButton.Stop();
            btnAdd.BackColor = DefaultBackColor;
            btnAdd2.BackColor = DefaultBackColor;
        }

        // О программе (F1)
        private void frmPatients_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 112)
            {
                frmAboutBox myFormAbout = new frmAboutBox();
                myFormAbout.ShowDialog();
            }
        }


    }
}
