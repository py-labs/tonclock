﻿//
// Класс DbPulseOperations содержит операции для работы
// с базой данных Pulse.mdb

using System;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Windows.Forms;

namespace TonClock
{
    internal static class DbPulseOperations
    {
        // удаление замера
        internal static bool DeleteMeasurement(int id, out string strError)
        {
            strError = string.Empty;
            try
            {
                // удаление записи в БД
                using (var myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    var myOleDbCommand = new OleDbCommand(
                        "DELETE FROM Measurements " +
                        "WHERE id = " + id,
                        myOleDbConnection);
                    myOleDbCommand.ExecuteNonQuery();
                }
                // удаление файла
                File.Delete(ConstGlobals.MeasurementsFolder + id + ".dat");
            }
            catch (Exception exc)
            {
                strError = exc.Message;
                return false;
            }
            return true;
        }

        // удаление пациента
        internal static bool DeletePatient(int id, out string strError)
        {
            strError = string.Empty;
            try
            {
                // удаление записи в БД
                using (var myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    var myOleDbCommand = new OleDbCommand(
                        "DELETE FROM Patients " +
                        "WHERE id = " + id,
                        myOleDbConnection);
                    myOleDbCommand.ExecuteNonQuery();
                    // удаление замеров
                    myOleDbCommand.CommandText =
                        "SELECT id " +
                        "FROM Measurements " +
                        "WHERE patient = " + id;
                    var myReader = myOleDbCommand.ExecuteReader();
                    if (myReader != null)
                    {
                        while (myReader.Read())
                        {
                            var idMes = (int) myReader[0];
                            File.Delete(ConstGlobals.MeasurementsFolder + idMes + ".dat");
                        }
                        myReader.Close();
                    }
                    myOleDbCommand.CommandText =
                        "DELETE FROM Measurements " +
                        "WHERE patient = " + id;
                    myOleDbCommand.ExecuteNonQuery();
                }
            }
            catch (Exception exc)
            {
                strError = exc.Message;
                return false;
            }
            return true;
        }

        // удаление врача
        internal static bool DeleteDoctor(int id, out string strError)
        {
            strError = string.Empty;
            try
            {
                // удаление записи в БД
                using (var myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    var myOleDbCommand = new OleDbCommand(
                        "DELETE FROM Doctors " +
                        "WHERE id = " + id,
                        myOleDbConnection);
                    myOleDbCommand.ExecuteNonQuery();
                    // удаление пациентов
                    myOleDbCommand.CommandText =
                        "SELECT id " +
                        "FROM Patients " +
                        "WHERE doctor = " + id;
                    var myReader = myOleDbCommand.ExecuteReader();
                    if (myReader != null)
                    {
                        while (myReader.Read())
                        {
                            var idPatient = (int)myReader[0];
                            if (!DeletePatient(idPatient, out strError))
                                throw new Exception(strError);
                        }
                        myReader.Close();
                    }
                }
            }
            catch (Exception exc)
            {
                strError = exc.Message;
                return false;
            }
            return true;
        }

        // печать возраста
        internal static string PrintAge(int age)
        {
            var result = age.ToString();
            switch (result[result.Length - 1])
            {
                case '0':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    result += " лет";
                    break;
                case '1':
                    result += " год";
                    break;
                case '2':
                case '3':
                case '4':
                    result += " года";
                    break;
            }
            return result;
        }

        // печать стрессового индекса
        internal static string PrintStress(int index)
        {
            var result = index.ToString();
            switch (result[result.Length - 1])
            {
                case '0':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                    result += " баллов";
                    break;
                case '1':
                    result += " балл";
                    break;
                case '2':
                case '3':
                case '4':
                    result += " балла";
                    break;
            }
            return result;
        }


        // обновление данных после перестановки реперов
        // results: resultSosud, _resultTopPressure, _resultLowerPressure, _resultAge, _resultBlood;
        internal static bool UpdateRepers(PulseWave pulseWave, int[] repers, int idDb, int idPatient, out string strError,
            out int[] results, out Double bloodVal)
        {
            try
            {
                // открываем файл с замером, если он не передан в функцию
                if (pulseWave == null)
                {
                    pulseWave = new PulseWave();
                    string strPulseWaveError = String.Empty,
                           strFileName = ConstGlobals.MeasurementsFolder + idDb.ToString() + ".dat";
                    if (!pulseWave.LoadPulseFromBinFile(strFileName, ref strPulseWaveError))
                        throw new Exception(strPulseWaveError);
                }
                pulseWave.Repers = repers;
                pulseWave.AnalyzePulseWave();
                using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    using (OleDbCommand myOleDbCommand = new OleDbCommand(
                                "SELECT s1, s2, fssOpor, k1, k2, k, a, b, c, d " +
                                "FROM Measurements " +
                                "WHERE id = " + idDb.ToString(),
                            myOleDbConnection))
                    {
                        int fssOpor;
                        float s1, s2, k1, k2, k, a, b, c, d;
                        using (OleDbDataReader myReader = myOleDbCommand.ExecuteReader())
                        {
                            if (myReader != null && myReader.Read())
                            {
                                s1 = myReader.GetFloat(0);
                                s2 = myReader.GetFloat(1);
                                fssOpor = myReader.GetInt16(2);
                                k1 = myReader.GetFloat(3);
                                k2 = myReader.GetFloat(4);
                                k = myReader.GetFloat(5);
                                a = myReader.GetFloat(6);
                                b = myReader.GetFloat(7);
                                c = myReader.GetFloat(8);
                                d = myReader.GetFloat(9);
                            }
                            else
                            {
                                throw new Exception("В базе данных не обнаружен замер с таким идентификатором.");
                            }
                        }
                        // рассчет новых значений
                        // results: resultSosud, _resultTopPressure, _resultLowerPressure, _resultAge, _resultBlood;
                        results = new int[5];
                        byte topPressureResult, lowerPressureResult;
                        results[0] = pulseWave.GetSosud(s1, s2);
                        pulseWave.CalculateAD(fssOpor, k1, k2, k, a, b, c, d,
                                              out topPressureResult, out lowerPressureResult);
                        results[1] = topPressureResult;
                        results[2] = lowerPressureResult;
                        results[3] = pulseWave.CalcAge();
                        results[4] = pulseWave.CalcBlood(k1, out bloodVal);
                        // глобал
                        GlobalCalc globalCalc = new GlobalCalc(pulseWave.PulseWaveField);
                        GlobalSettings globalSettings = new GlobalSettings();
                        bool globalSex;
                        int globalAge, globalWeight;
                        float globalGrowth;
                        myOleDbCommand.CommandText =
                            "SELECT sex, age, growth, weight " +
                            "FROM Patients " +
                            "WHERE id = " + idPatient;
                        using (OleDbDataReader myReader = myOleDbCommand.ExecuteReader())
                        {
                            if (myReader != null && myReader.Read())
                            {
                                globalSex = myReader.GetBoolean(0);
                                globalAge = myReader.GetInt16(1);
                                globalGrowth = myReader.IsDBNull(2) ? 0 : myReader.GetFloat(2);
                                globalWeight = myReader.GetInt16(3);
                            }
                            else
                            {
                                throw new Exception("В базе данных не обнаружен пациент с таким идентификатором.");
                            }
                        }
                        globalCalc.Repers = repers;
                        globalCalc.MakeCalc(globalSex, globalAge, globalGrowth, globalWeight, globalSettings.J,
                                            globalSettings.N, globalSettings.H,
                                            globalSettings.I, globalSettings.R);
                        // изменим данные в базе
                        CultureInfo _myDbCultInfo = CultureInfo.InvariantCulture;
                        myOleDbCommand.CommandText =
                            "UPDATE Measurements " +
                            "SET sosud = " + results[0].ToString(_myDbCultInfo) + ", " +
                            "    topPressure = " + results[1].ToString(_myDbCultInfo) + ", " +
                            "    lowerPressure = " + results[2].ToString(_myDbCultInfo) + ", " +
                            "    pulse = " + pulseWave.PulseFSS.ToString(_myDbCultInfo) + ", " +
                            "    a2a1 = " + pulseWave.PulseA2A1.ToString(_myDbCultInfo) + ", " +
                            "    age = " + results[3].ToString(_myDbCultInfo) + ", " +
                            "    blood = " + results[4].ToString(_myDbCultInfo) + ", " +
                            "    globalA1 = " + repers[0].ToString(_myDbCultInfo) + ", " +
                            "    globalA2 = " + repers[1].ToString(_myDbCultInfo) + ", " +
                            "    globalA3 = " + repers[2].ToString(_myDbCultInfo) + ", " +
                            "    globalA4 = " + repers[3].ToString(_myDbCultInfo) + ", " +
                            "    globalA5 = " + repers[4].ToString(_myDbCultInfo) + ", " +
                            "    globalTop = " + globalCalc.ResultTopPressure.ToString(_myDbCultInfo) + ", " +
                            "    globalLower = " + globalCalc.ResultLowerPressure.ToString(_myDbCultInfo) + " " +
                            "WHERE id = " + idDb.ToString();
                        if (myOleDbCommand.ExecuteNonQuery() <= 0)
                            throw new Exception("Не удалось изменить данные в базе.");
                    }
                }
                strError = String.Empty;
                return true;
            }
            catch (Exception exc)
            {
                strError = exc.Message;
                results = null;
                bloodVal = 0;
                return false;
            }
        }

        internal static bool UpdateRepers(int[] repers, int idDb, int idPatient, out string strError, out Double bloodVal)
        {
            int[] tempResult;
            return UpdateRepers(null, repers, idDb, idPatient, out strError, out tempResult, out bloodVal);
        }

        internal static bool UpdateRepers(int[] repers, int idDb, int idPatient, out string strError)
        {
            int[] tempResult;
            Double tempBlood;
            return UpdateRepers(null, repers, idDb, idPatient, out strError, out tempResult, out tempBlood);
        }

    }
}
