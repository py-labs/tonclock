﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using Microsoft.VisualBasic.ApplicationServices;

namespace TonClock
{
    // делегаты
    internal delegate void DelegateInitFonDevice(bool showMessages = true);

    // класс для хранения глобальных констант
    internal static class ConstGlobals
    {
        //string userAppData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        //string commonAppData = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
        private const string DbFolder = @"db";
        private const string DbFileName = "Pulse.mdb";
        private const string DevicesFileName = "devices.txt";
        internal static string AppDataFolder        // папка данных программы
        {
            get { return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"TonClock\"); }
        }
        internal static string DbFileFullName       // файл БД
        {
            get { return Path.Combine(AppDataFolder, DbFolder, DbFileName); }
        }
        internal static string СonnectionString     // подключение к БД
        {
            get
            {
                return "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" +
                       DbFileFullName + ";User Id=admin;Password=;";
            }
        }
        internal static string MeasurementsFolder   // папка с измерениями
        {
            get { return Path.Combine(AppDataFolder, @"measurements\"); }
        }
        internal static string DevicesFileFullName  // файл с информацией об оборудовании
        {
            get { return Path.Combine(AppDataFolder, DevicesFileName); }
        }
        internal static string FrequencyFileFullName  // файл с частотой оборудования
        {
            get { return Path.Combine(AppDataFolder, "frequency.txt"); }
        }
        internal static string TestPrintFileFullName  // файл для тестовой печати
        {
            get { return Path.Combine(AppDataFolder, "test.txt"); }
        }
        internal static string AssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (titleAttribute.Title != "")
                    {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }
    }

    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            // удаление программы, если задан параметр командной строки /u
            for (int i = 0; i != args.Length; ++i)
            {
                if (args[i].Split('=')[0].ToLower() == "/u")
                {
                    string guid = args[i].Split('=')[1];
                    string path = Environment.GetFolderPath(Environment.SpecialFolder.System);
                    var uninstallProcess = new ProcessStartInfo(path + "\\msiexec.exe", "/x " + guid);
                    Process.Start(uninstallProcess);
                    return;
                }
            }
            // проверка существования необходимых файлов
            try
            {
                if (!Directory.Exists(ConstGlobals.AppDataFolder))
                    throw new Exception("Отсутвует директория пользовательских данных\n(" + ConstGlobals.AppDataFolder + ").");
                if (!File.Exists(ConstGlobals.DbFileFullName))
                    throw new Exception("Отсутвует файл базы данных данных\n(" + ConstGlobals.DbFileFullName + ").");
                
            }
            catch (Exception exc)
            {
                MessageBox.Show("Внимание! При попытке запуска программы произошла ошибка:\n" +
                    exc.Message + "\n\n" +
                    "Необходима переустановка программы.\n" +
                    "Пожалуйста, обратитесь к администратору.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.Exit();
                return;
            }
            // запуск программы
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            new MyApp().Run(args); 
        }
    }

    class MyApp : WindowsFormsApplicationBase
    {
        private const int SplashScreenMinTime = 1500;
        private int _msStart;

        protected override void OnCreateSplashScreen()
        {
            this.SplashScreen = new frmSplashScreen(SplashScreenMinTime);
            _msStart = Environment.TickCount;
        }
        protected override void OnCreateMainForm()
        {
            var myFrmDoctor = new frmDoctor();
            string strError;
            if (myFrmDoctor.UpdateMainTable(out strError))
            {
                int msLoad = Environment.TickCount - _msStart;
                //MessageBox.Show("Время загрузки: " + msLoad.ToString() + " мс");
                if (msLoad < SplashScreenMinTime)
                    Thread.Sleep(SplashScreenMinTime - msLoad);
            }
            else
                throw new Exception(strError);
            // Then create the main form, the splash screen will close automatically 
            this.MainForm = myFrmDoctor;
        }
    }

}
