﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TonClock.Properties;

namespace TonClock
{
    public partial class frmMolodimetr : Form
    {
        //*****************************************************************
        //                      Поля и параметры                          *
        //*****************************************************************

        private int _idDb,      // замер в базе
                    _idOrder;   // порядковый номер замера пациента
        private readonly int _idPatient;    // пациент
        private PulseWave _myPulseWave;

        private int _resultSosud,
                    _resultTopPressure,
                    _resultLowerPressure,
                    _resultPulse,
                    _resultStress,
                    _resultAge,
                    _resultBlood;

        public frmMolodimetr(int idDb, int idOrder, DataTable dt, int idP)
        {
            InitializeComponent();
            this.Icon = Properties.Resources.yagodka;
            _idDb = idDb;
            _idOrder = idOrder;
            _dtHistory = dt;
            _idPatient = idP;
            ShowHistory();
        }

        private void frmMolodimetr_Shown(object sender, EventArgs e)
        {
            UpdateData();
        }

        private void UpdateData()
        {
            tlpButtons.Enabled = false;
            // Чтение файла замеров
            string strFileName = ConstGlobals.MeasurementsFolder + _idDb.ToString() + ".dat";
            try
            {
                // открываем файл с замером
                string strError = String.Empty;
                _myPulseWave = new PulseWave(false);
                if (!_myPulseWave.LoadPulseFromBinFile(strFileName, ref strError))
                    throw new Exception(strError);
            }
            catch (Exception exc)
            {
                MessageBox.Show("При попытке открытия файла '" + Path.GetFileName(strFileName) +
                                "' произошла ошибка:\n" + exc.Message + "\n\n" +
                                "Пожалуйста, обратитесь к администратору.",
                                "Ошибка",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.DialogResult = DialogResult.Cancel;
            }
            // Чтение данных из БД
            try
            {
                using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    using (OleDbCommand myOleDbCommand = new OleDbCommand(
                        "SELECT Patients.lastname, Patients.firstname, Patients.patronymic, Patients.age, " +
                        "       Measurements.sosud, Measurements.topPressure, Measurements.lowerPressure, " +
                        "       Measurements.pulse, Measurements.stress, Measurements.Age, Measurements.Blood, " +
                        "       Measurements.globalA1, Measurements.globalA2, Measurements.globalA3, " +
                        "       Measurements.globalA4, Measurements.globalA5 " +
                        "FROM Measurements, Patients " +
                        "WHERE Measurements.id = " + _idDb.ToString() + " " +
                        "  AND Patients.id = Measurements.patient",
                        myOleDbConnection))
                    {
                        using (OleDbDataReader myReader = myOleDbCommand.ExecuteReader())
                        {
                            if (myReader != null && myReader.Read())
                            {
                                this.Text = ConstGlobals.AssemblyTitle + " - Молодиметр - " + myReader[0].ToString() +
                                            " " +
                                            myReader[1].ToString()[0] + "." + myReader[2].ToString()[0] + "., " +
                                            DbPulseOperations.PrintAge(myReader.GetInt16(3)) +
                                            " (замер № " + _idOrder + ")";
                                _resultSosud = myReader.GetInt16(4);
                                _resultTopPressure = myReader.GetInt16(5);
                                _resultLowerPressure = myReader.GetInt16(6);
                                _resultPulse = myReader.GetInt16(7);
                                _resultStress = myReader.GetInt16(8);
                                _resultAge = (!myReader.IsDBNull(9) ? myReader.GetInt16(9) : 50);
                                _resultBlood = (!myReader.IsDBNull(10) ? myReader.GetInt16(10) : 75);
                                toolTip1.SetToolTip(pnlBlood, string.Empty);
                                if (myReader.IsDBNull(11) || myReader.IsDBNull(12) || myReader.IsDBNull(13) ||
                                    myReader.IsDBNull(14) || myReader.IsDBNull(15))
                                {
                                    _myPulseWave.AnalyzePulseWave();
                                    GlobalCalc calc = new GlobalCalc(_myPulseWave.PulseWaveField);
                                    calc.AutoRepers();
                                    _myPulseWave.Repers = calc.Repers;
                                }
                                else
                                {
                                    _myPulseWave.Repers = new int[]
                                                               {
                                                                   myReader.GetInt32(11), myReader.GetInt32(12),
                                                                   myReader.GetInt32(13),
                                                                   myReader.GetInt32(14), myReader.GetInt32(15)
                                                               };
                                }
                            }
                            else
                            {
                                throw new Exception("В базе данных не обнаружен замер с таким идентификатором.");
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                    exc.Message + "\n\n" +
                    "Пожалуйста, обратитесь к администратору.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.DialogResult = DialogResult.Cancel;
            }
            _myPulseWave.AnalyzePulseWave();
            this.Refresh();
        }

        private void frmMolodimetr_Resize(object sender, EventArgs e)
        {
            this.Refresh();
        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            this.Refresh();
        }

        
        //*****************************************************************
        //                         Прорисовка                             *
        //*****************************************************************

        private void pnlSosud_Paint(object sender, PaintEventArgs e)
        {
            Molodimetr.DrawSosud(e.Graphics, pnlSosud.ClientRectangle, _resultSosud);
        }

        private void pnlStress_Paint(object sender, PaintEventArgs e)
        {
            Molodimetr.DrawStress(e.Graphics, pnlSosud.ClientRectangle, _resultStress);
        }

        private void pnlAge_Paint(object sender, PaintEventArgs e)
        {
            Molodimetr.DrawAge(e.Graphics, pnlAge.ClientRectangle, _resultAge);
        }

        private void pnlBlood_Paint(object sender, PaintEventArgs e)
        {
            Molodimetr.DrawBlood(e.Graphics, pnlBlood.ClientRectangle, _resultBlood);
        }

        private void pnlPulse_Paint(object sender, PaintEventArgs e)
        {
            Molodimetr.DrawPulse(e.Graphics, pnlPulse.ClientRectangle, _resultPulse);
        }

        private void pnlPressure_Paint(object sender, PaintEventArgs e)
        {
            Molodimetr.DrawPressure(e.Graphics, pnlPressure.ClientRectangle,
                _resultTopPressure, _resultLowerPressure, pnlSosud.ClientRectangle.Width, _topFontSize);
        }

        private float _topFontSize = 10;

        private void pnlTopTitle_Paint(object sender, PaintEventArgs e)
        {
            _topFontSize = Molodimetr.DrawTopTitle(e.Graphics, pnlTopTitle.ClientRectangle);
            pnlPressure.Invalidate();
        }

        private void pnlLegend_Paint(object sender, PaintEventArgs e)
        {
            Molodimetr.DrawLegend(e.Graphics, pnlLegend.ClientRectangle);
        }

        // History

        private DataTable _dtHistory;

        private void ShowHistory()
        {
            cmbHistory.Items.Clear();
            if ((_dtHistory != null) && (_dtHistory.Rows.Count > 0))
            {
                for (int i = 0; i < _dtHistory.Rows.Count; i++ )
                {
                    string strItem = string.Format("{0,5}). ", _dtHistory.Rows.Count - i);
                    strItem += Convert.ToDateTime(_dtHistory.Rows[i]["mesDateTime"]).ToShortDateString() +
                        ", в " + Convert.ToDateTime(_dtHistory.Rows[i]["mesDateTime"]).ToShortTimeString();
                    cmbHistory.Items.Add(strItem);
                    if (_idOrder == _dtHistory.Rows.Count - i)
                        cmbHistory.SelectedIndex = i;
                }
                cmbHistory.Enabled = true;
                UpdateHistoryButtons();
            }
            else
            {
                cmbHistory.Enabled = btnHistoryLast.Enabled = btnHistoryNext.Enabled = false;
            }
        }

        private void btnHistoryLast_Click(object sender, EventArgs e)
        {
            if (cmbHistory.SelectedIndex + 1 < cmbHistory.Items.Count)
                cmbHistory.SelectedIndex++;
        }

        private void btnHistoryNext_Click(object sender, EventArgs e)
        {
            if (cmbHistory.SelectedIndex > 0)
                cmbHistory.SelectedIndex--;
        }

        private void cmbHistory_SelectedIndexChanged(object sender, EventArgs e)
        {
            tlpHistory.Enabled = false;
            _idDb = Convert.ToInt32(_dtHistory.Rows[cmbHistory.SelectedIndex]["id"]);
            _idOrder = _dtHistory.Rows.Count - cmbHistory.SelectedIndex;
            UpdateData();
            this.Refresh();
            UpdateHistoryButtons();
            tlpHistory.Enabled = true;
        }

        private void UpdateHistoryButtons()
        {
            btnHistoryLast.Enabled = (cmbHistory.Items.Count > 0) && (cmbHistory.SelectedIndex < cmbHistory.Items.Count - 1);
            btnHistoryNext.Enabled = (cmbHistory.Items.Count > 0) && (cmbHistory.SelectedIndex > 0);
        }

        private void ShowRepers()
        {
            //string strFileName = ConstGlobals.MeasurementsFolder + _idDb.ToString() + ".dat";
            //try
            //{
                //// открываем файл с замером
                //string strError = String.Empty;
                //PulseWaveField _myPulseWave = new PulseWaveField();
                //if (!_myPulseWave.LoadPulseFromBinFile(strFileName, ref strError))
                //    throw new Exception(strError);
                //_myPulseWave.AnalyzePulseWave();
                using (var myFrmRepers = new frmRepers(_idPatient, _idDb, _myPulseWave.PulseWaveField, _idOrder))
                {
                    myFrmRepers.ShowDialog();
                    if (myFrmRepers.DialogResult == DialogResult.OK)
                    {
                        UpdateData();
                        this.Refresh();
                        toolTip1.SetToolTip(pnlBlood, myFrmRepers.BloodVal.ToString("0.##"));
                        //return true;
                    }
                }
            //}
            //catch (Exception exc)
            //{
            //    MessageBox.Show("При попытке открытия файла '" + Path.GetFileName(strFileName) +
            //                    "' произошла ошибка:\n" + exc.Message + "\n\n" +
            //                    "Пожалуйста, обратитесь к администратору.",
            //                    "Ошибка",
            //                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            //return false;
        }

        private void mnuPulseWave_Click(object sender, EventArgs e)
        {
            using (frmPas myFormPas = new frmPas("окн"))
            {
                myFormPas.ShowDialog();
                if (myFormPas.DialogResult == DialogResult.OK)
                {
                    ShowRepers();
                }
            }
        }

        private void mnuExit_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        
        //*****************************************************************
        //             Прорисовка средней ПВ и расстановка реперов        *
        //*****************************************************************

        private void pnlRepers_Paint(object sender, PaintEventArgs e)
        {
            if (_myPulseWave != null)
            {
                Rectangle rctPulse = new Rectangle(pnlRepers.HorizontalScroll.Value,
                    pnlRepers.VerticalScroll.Value,
                    pnlRepers.ClientSize.Width, pnlRepers.ClientSize.Height);
                int scroll;
                _myPulseWave.DrawPulseWave(e.Graphics, rctPulse, new Point(0, 0), out scroll, true);
                pnlRepers.AutoScrollMinSize = new Size(scroll, 0);
            }
        }

        private void pnlRepers_Scroll(object sender, ScrollEventArgs e)
        {
            pnlRepers.Refresh();
        }

        private void pnlRepers_MouseMove(object sender, MouseEventArgs e)
        {
            if (_myPulseWave == null)
                return;

            Rectangle rctPulse = new Rectangle(pnlRepers.HorizontalScroll.Value,
                                               pnlRepers.VerticalScroll.Value,
                                               pnlRepers.ClientSize.Width,
                                               pnlRepers.ClientSize.Height);
            if (_myPulseWave.MoveReperMode)
            {
                if (_myPulseWave.ReperMove(e.Location, rctPulse))
                    pnlRepers.Refresh();
            }
            else
            {
                bool flagRefresh;
                pnlRepers.Cursor = _myPulseWave.MouseMove(e.Location, rctPulse,
                                                  out flagRefresh)
                                      ? Cursors.Hand
                                      : Cursors.Default;
                if (flagRefresh)
                    pnlRepers.Refresh();
            }
        }

        private void pnlRepers_MouseDown(object sender, MouseEventArgs e)
        {
            if (_myPulseWave != null && _myPulseWave.MouseDown(e.Location))
                pnlRepers.Refresh();
        }

        private void pnlRepers_MouseUp(object sender, MouseEventArgs e)
        {
            if (_myPulseWave != null && _myPulseWave.MouseUp())
            {
                pnlRepers.Refresh();
                tlpButtons.Enabled = true;
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            string strError;
            Double tempBloodVal;
            if (!DbPulseOperations.UpdateRepers(_myPulseWave.Repers, _idDb, _idPatient, out strError, out tempBloodVal))
            {
                MessageBox.Show(
                    String.Format("При попытке обновления данных после перестановки реперов произошла ошибка:{0}" +
                                  strError + "{0}{0}" +
                                  "Пожалуйста, обратитесь к администратору.", Environment.NewLine),
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                UpdateData();
                toolTip1.SetToolTip(pnlBlood, tempBloodVal.ToString("0.##"));
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            UpdateData();
        }

        private void pnlPulseWave_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.DrawImage(Resources.waves, pnlPulseWave.ClientRectangle);
        }

        private void pnlPulseWave_Resize(object sender, EventArgs e)
        {
            pnlRepers.Height = pnlPulseWave.Height / 3 + (pnlRepers.Height - pnlRepers.ClientSize.Height) + 20;
        }


    }
}
