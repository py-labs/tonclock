﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TonClock
{
    public partial class frmRepers : Form
    {
        //*****************************************************************
        //                         Базовые поля                           *
        //*****************************************************************

        private readonly int _idPatient, _idMeasurement, _idNumber;
        private readonly GlobalCalc _calc;


        //*****************************************************************
        //                     Конструкторы и загрузка                    *
        //*****************************************************************

        public frmRepers(int id, int idM, ushort[] data, int n)
        {
            InitializeComponent();
            this.Icon = Icon.FromHandle(Properties.Resources.CalculatorHS.GetHicon());
            _idPatient = id;
            _idMeasurement = idM;
            _calc = new GlobalCalc(data);
            _idNumber = n;
        }

        private void frmRepers_Shown(object sender, EventArgs e)
        {
            try
            {
                using (var myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    OleDbCommand myOleDbCommand = new OleDbCommand(
                        "SELECT lastname, firstname, patronymic " +
                        "FROM Patients " +
                        "WHERE id = " + _idPatient,
                        myOleDbConnection);
                    using (var myReader = myOleDbCommand.ExecuteReader())
                    {
                        if (myReader != null && myReader.Read())
                        {
                            this.Text += " - " + myReader[0].ToString() + " " +
                                         myReader[1].ToString()[0] + "." + myReader[2].ToString()[0] + "." +
                                         " - замер " + _idNumber;
                        }
                        else
                        {
                            throw new Exception("В базе не обнаружен пациент с таким идентификатором.");
                        }
                    }
                    myOleDbCommand.CommandText =
                        "SELECT globalA1, globalA2, globalA3, globalA4, globalA5 " +
                        "FROM Measurements " +
                        "WHERE id = " + _idMeasurement;
                    using (var myReader = myOleDbCommand.ExecuteReader())
                    {
                        if (myReader != null && myReader.Read())
                        {
                            if (myReader.IsDBNull(0) || myReader.IsDBNull(1) || myReader.IsDBNull(2) ||
                                myReader.IsDBNull(3) || myReader.IsDBNull(4))
                            {
                                _calc.AutoRepers();
                            }
                            else
                            {
                                _calc.Repers = new int[]
                                                   {
                                                       myReader.GetInt32(0), myReader.GetInt32(1),
                                                       myReader.GetInt32(2),
                                                       myReader.GetInt32(3), myReader.GetInt32(4)
                                                   };
                            }
                            //try
                            //{
                            //    var repers = new int[]
                            //                        {
                            //                            myReader.GetInt32(0), myReader.GetInt32(1),
                            //                            myReader.GetInt32(2),
                            //                            myReader.GetInt32(3), myReader.GetInt32(4)
                            //                        };
                            //    _calc.Repers = repers;
                            //}
                            //catch (InvalidCastException)
                            //{
                            //    _calc.AutoRepers();
                            //}
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Внимание! При попытке открытия базы данных произошла ошибка:\n" +
                    exc.Message + "\n\n" +
                    "Дальнейшая работа модуля не возможна.\n\n" +
                    "Пожалуйста, обратитесь к администратору.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.DialogResult = DialogResult.Abort;
            }
            if (_calc.Repers == null)
                _calc.AutoRepers();
        }

        private void frmRepers_Resize(object sender, EventArgs e)
        {
            this.Refresh();
        }


        //*****************************************************************
        //                 Прорисовка пульсовой волны                     *
        //*****************************************************************

        private void pnlPulse_Paint(object sender, PaintEventArgs e)
        {
            Rectangle rctPulse = new Rectangle(pnlPulse.HorizontalScroll.Value,
                pnlPulse.VerticalScroll.Value,
                pnlPulse.ClientSize.Width, pnlPulse.ClientSize.Height);
            int scroll;
            _calc.DrawPulseWave(e.Graphics, rctPulse, out scroll);
            pnlPulse.AutoScrollMinSize = new Size(scroll, 0);
        }

        private void pnlPulse_Scroll(object sender, ScrollEventArgs e)
        {
            pnlPulse.Refresh();
        }


        //*****************************************************************
        //                       Расстановка реперов                      *
        //*****************************************************************

        private void pnlPulse_MouseMove(object sender, MouseEventArgs e)
        {
            Rectangle rctPulse = new Rectangle(pnlPulse.HorizontalScroll.Value,
                                                    pnlPulse.VerticalScroll.Value,
                                                    pnlPulse.ClientSize.Width,
                                                    pnlPulse.ClientSize.Height);
            if (_calc.MoveReperMode)
            {
                if (_calc.ReperMove(e.Location, rctPulse))
                    pnlPulse.Refresh();
            }
            else
            {
                bool flagRefresh;
                pnlPulse.Cursor = _calc.MouseMove(e.Location, rctPulse,
                                                  out flagRefresh)
                                      ? Cursors.Hand
                                      : Cursors.Default;
                if (flagRefresh)
                    pnlPulse.Refresh();
            }
        }

        private void pnlPulse_MouseDown(object sender, MouseEventArgs e)
        {
            if (_calc.MouseDown(e.Location))
                pnlPulse.Refresh();
        }

        private void pnlPulse_MouseUp(object sender, MouseEventArgs e)
        {
            if (_calc.MouseUp())
                pnlPulse.Refresh();
        }


        //*****************************************************************
        //                             Выход                              *
        //*****************************************************************

        public int[] ResultRepers
        {
            get { return _calc.Repers; }
        }

        public Double BloodVal { get; private set; }

        private void Accept()
        {
            string strError;
            Double tempBloodVal;
            if (!DbPulseOperations.UpdateRepers(_calc.Repers, _idMeasurement, _idPatient, out strError, out tempBloodVal))
            {
                MessageBox.Show(
                    String.Format("При попытке обновления данных после перестановки реперов произошла ошибка:{0}" +
                                  strError + "{0}{0}" +
                                  "Пожалуйста, обратитесь к администратору.", Environment.NewLine),
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            BloodVal = tempBloodVal;
            this.DialogResult = DialogResult.OK;
        }

        private void Cancel()
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Accept();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Cancel();
        }

        private void frmRepers_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    Accept();
                    break;
                case Keys.Escape:
                    Cancel();
                    break;
            }
        }


    }
}
