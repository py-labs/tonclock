﻿//
// В классе DevicesList хранится список конечных устройств (КУ)
//

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TonClock
{
    public class EndDevice
    {
        private ushort _id;
        private string _name;
        private bool _flagOn;
        private ushort _volt;
        private int _lastTick;

        public ushort Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public bool FlagOn
        {
            get { return _flagOn; }
            set { _flagOn = value; }
        }
        public ushort Volt
        {
            get { return _volt; }
            set { _volt = value; }
        }
        public int LastTick
        {
            get { return _lastTick; }
            set { _lastTick = value; }
        }

        public void NewVoltDataReceived(ushort volt)
        {
            if (!_flagOn)
                _flagOn = true;
            _volt = volt;
            _lastTick = Environment.TickCount;
        }
    }

    public class DevicesList
    {
        private List<EndDevice> _deviceList;
        private const int MaxOnTick = 30000;

        public DevicesList()
        {
            _deviceList = new List<EndDevice>();
        }

        public int Count
        {
            get { return (_deviceList != null) ? _deviceList.Count : 0; }
        }

        public EndDevice GetDeviceStructByOrder(int n)
        {
            if ((_deviceList == null) || (n >= _deviceList.Count))
                return null;
            return _deviceList[n];
        }

        public EndDevice GetDeviceStructByDeviceId(int id)
        {
            return _deviceList.FirstOrDefault(t => t.Id == id);
        }

        public int GetVoltLevelById(int id)
        {
            foreach (EndDevice t in _deviceList.Where(t => t.Id == id))
            {
                if (t.Volt < 320)
                    return 0;
                if ((t.Volt >= 320) && (t.Volt < 345))
                    return 1;
                if ((t.Volt >= 345) && (t.Volt < 370))
                    return 2;
                if ((t.Volt >= 370) && (t.Volt < 395))
                    return 3;
                if ((t.Volt >= 395) && (t.Volt < 420))
                    return 4;
                if (t.Volt >= 420)
                    return 5;
            }
            return -1;
        }

        public void ReceivedPackageFromDevice(ushort id, ushort volt)
        {
            foreach (EndDevice t in _deviceList.Where(t => t.Id == id))
            {
                t.NewVoltDataReceived(volt);
                return;
            }
            AddNewDevice(id, volt);
        }

        private void AddNewDevice(ushort id, ushort volt, string name = "")
        {
            EndDevice t = new EndDevice {Id = id};
            t.NewVoltDataReceived(volt);
            if (!string.IsNullOrEmpty(name))
                t.Name = name;
            _deviceList.Add(t);
        }

        public void UpdateTimer()
        {
            int curTick = Environment.TickCount;
            foreach (EndDevice t in _deviceList)
                if ((curTick - t.LastTick) > MaxOnTick)
                    t.FlagOn = false;
        }

        public int GetFirstConnectedId()
        {
            foreach (EndDevice t in _deviceList)
                if (t.FlagOn)
                    return t.Id;
            return -1;
        }


        //*****************************************************************
        //                    Чтение и запись в файл                      *
        //*****************************************************************

        public bool ReadListFromTextFile(string strFileName, ref string strError)
        {
            _deviceList = new List<EndDevice>();
            try
            {
                using (StreamReader myStreamReader = File.OpenText(strFileName))
                {
                    string strLine;
                    while ((strLine = myStreamReader.ReadLine()) != null)
                    {
                        if (String.IsNullOrEmpty(strLine))
                            throw new Exception("Ошибка в структуре файла.");
                        ushort id, volt;
                        if (!ushort.TryParse(strLine, out id))
                            throw new Exception("Строка '" + strLine + "' не является числом.");
                        strLine = myStreamReader.ReadLine();
                        if (String.IsNullOrEmpty(strLine))
                            throw new Exception("Ошибка в структуре файла.");
                        if (!ushort.TryParse(strLine, out volt))
                            throw new Exception("Строка '" + strLine + "' не является числом.");
                        strLine = myStreamReader.ReadLine();
                        EndDevice t = new EndDevice { Id = id, Name = strLine, Volt = volt, FlagOn = false, LastTick = 0 };
                        _deviceList.Add(t);
                        myStreamReader.ReadLine();
                    }
                }
                
            }
            catch (Exception e)
            {
                _deviceList = new List<EndDevice>();
                strError = e.Message;
                return false;
            }
            return true;
        }

        public void ReadListFromTextFile(string strFileName)
        {
            _deviceList = new List<EndDevice>();
            try
            {
                using (StreamReader myStreamReader = File.OpenText(strFileName))
                {
                    string strLine;
                    while ((strLine = myStreamReader.ReadLine()) != null)
                    {
                        if (String.IsNullOrEmpty(strLine))
                            throw new Exception("Ошибка в структуре файла.");
                        ushort id, volt;
                        if (!ushort.TryParse(strLine, out id))
                            throw new Exception("Строка '" + strLine + "' не является числом.");
                        strLine = myStreamReader.ReadLine();
                        if (String.IsNullOrEmpty(strLine))
                            throw new Exception("Ошибка в структуре файла.");
                        if (!ushort.TryParse(strLine, out volt))
                            throw new Exception("Строка '" + strLine + "' не является числом.");
                        strLine = myStreamReader.ReadLine();
                        EndDevice t = new EndDevice { Id = id, Name = strLine, Volt = volt, FlagOn = false, LastTick = 0 };
                        _deviceList.Add(t);
                        myStreamReader.ReadLine();
                    }
                }

            }
            catch (Exception)
            {
                _deviceList = new List<EndDevice>();
            }
        }

        public bool WriteListToTextFile(string strFileName, ref string strError)
        {
            try
            {
                using (StreamWriter myStreamWriter = new StreamWriter(strFileName, false))
                {
                    foreach (var dStruct in _deviceList)
                    {
                        myStreamWriter.WriteLine(dStruct.Id);
                        myStreamWriter.WriteLine(dStruct.Volt);
                        myStreamWriter.WriteLine(dStruct.Name);
                        myStreamWriter.WriteLine();
                    }
                }
            }
            catch (IOException ioex)
            {
                strError = ioex.Message;
                return false;
            }
            return true;
        }

    }
}
