﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Printing;
using System.Threading;
using System.Windows.Forms;

namespace TonClock
{
    public partial class frmVario : Form
    {
        //*****************************************************************
        //                      Поля и параметры                          *
        //*****************************************************************

        private readonly PulseWave _myPulseWave;
        private readonly string _strPatient;
        private readonly string[] _strsParams;


        //*****************************************************************
        //                        Конструкторы                            *
        //*****************************************************************

        public frmVario(PulseWave pw, string strP = null, String[] strsParams = null)
        {
            InitializeComponent();
            this.Icon = Icon.FromHandle(Properties.Resources.graphhs.GetHicon());
            _myPulseWave = pw;
            if (_myPulseWave != null)
            {
                _myPulseWave.CalculateIntegrals();
            }
            _strPatient = strP;
            _strsParams = strsParams;
            if (_strsParams != null && _strsParams.Length >= 9)
            {
                this.Text += " - замер " + _strsParams[0];
            }
        }


        //*****************************************************************
        //                          Загрузка                              *
        //*****************************************************************

        private void frmVario_Load(object sender, EventArgs e)
        {
            if (_myPulseWave != null)
            {
                mnuPageSetup.Enabled = true;
                mnuPrintPreview.Enabled = true;
                mnuPrint.Enabled = true;
            }
            // параметры печати
            printDocument1.DefaultPageSettings = new PageSettings
                                                     {Landscape = false, Margins = new Margins(50, 50, 50, 50)};
            printDocument1.DocumentName = "TonClockVario";
        }

        //*****************************************************************
        //                         Прорисовка                             *
        //*****************************************************************

        private void pnlStress_Paint(object sender, PaintEventArgs e)
        {
            if (_myPulseWave != null && !bkwStress.IsBusy)
            {
                Rectangle rctStress = new Rectangle(pnlStress.HorizontalScroll.Value,
                    pnlStress.VerticalScroll.Value,
                    pnlStress.ClientSize.Width, pnlStress.ClientSize.Height);
                int scroll;
                _myPulseWave.DrawPulseStress(e.Graphics, rctStress, new Point(0, 0), out scroll,
                    false, Math.Max((double)rctStress.Width / _myPulseWave.GetStressMaxLength(), 0.01));
                //pnlStress.AutoScrollMinSize = new Size(scroll, 0);
            }
        }

        private void pnlStress_Scroll(object sender, ScrollEventArgs e)
        {
            pnlStress.Refresh();
        }

        private void pnlPulseWave_Paint(object sender, PaintEventArgs e)
        {
            if (_myPulseWave != null)
            {
                Rectangle rctPulse = new Rectangle(pnlPulseWave.HorizontalScroll.Value,
                    pnlPulseWave.VerticalScroll.Value,
                    pnlPulseWave.ClientSize.Width, pnlPulseWave.ClientSize.Height);
                int scroll;
                //myPulseWave.DrawPulseWave(e.Graphics, rctPulse, new Point(0, 0), out scroll, false,
                //    false, Math.Max((double)rctPulse.Width / myPulseWave.PulseWaveField.Length, 0.01));
                int min = _myPulseWave.GetStressMinLength(), max = _myPulseWave.GetStressMaxLength();
                _myPulseWave.DrawPulseWave(e.Graphics, rctPulse, new Point(0, 0), out scroll, false,
                    false, Math.Max((double)rctPulse.Width / ((min + max) / 2d), 0.01));
                pnlPulseWave.AutoScrollMinSize = new Size(scroll, 0);
            }
        }

        private void pnlPulseWave_Scroll(object sender, ScrollEventArgs e)
        {
            pnlPulseWave.Refresh();
        }

        private void pnlIntegrAmplit_Paint(object sender, PaintEventArgs e)
        {
            if (_myPulseWave != null)
            {
                Rectangle rctIntegr = new Rectangle(pnlIntegrAmplit.HorizontalScroll.Value,
                    pnlIntegrAmplit.VerticalScroll.Value,
                    pnlIntegrAmplit.ClientSize.Width, pnlIntegrAmplit.ClientSize.Height);
                int scroll;
                _myPulseWave.DrawPulseIntegrals(e.Graphics, rctIntegr, new Point(0, 0), out scroll);
                pnlIntegrAmplit.AutoScrollMinSize = new Size(scroll, 0);
            }
        }

        private void pnlIntegrAmplit_Scroll(object sender, ScrollEventArgs e)
        {
            pnlIntegrAmplit.Refresh();
        }

        private void pnlIntegrAmplitPhas_Paint(object sender, PaintEventArgs e)
        {
            if (_myPulseWave != null)
            {
                Rectangle rctIntegr = new Rectangle(pnlIntegrAmplitPhas.HorizontalScroll.Value,
                    pnlIntegrAmplitPhas.VerticalScroll.Value,
                    pnlIntegrAmplitPhas.ClientSize.Width, pnlIntegrAmplitPhas.ClientSize.Height);
                int scroll;
                _myPulseWave.DrawPulseIntegralsPhas(e.Graphics, rctIntegr, new Point(0, 0), out scroll);
                pnlIntegrAmplitPhas.AutoScrollMinSize = new Size(scroll, 0);
            }
        }

        private void pnlIntegrAmplitPhas_Scroll(object sender, ScrollEventArgs e)
        {
            pnlIntegrAmplitPhas.Refresh();
        }

        private void frmVario_Resize(object sender, EventArgs e)
        {
            this.Refresh();
        }


        //*****************************************************************
        //                         Кинограмма                             *
        //*****************************************************************

        private Pen penStress = new Pen(Color.Black);
        private Point[][] pointsStress;

        private void pnlStress_Click(object sender, EventArgs e)
        {
            if (_myPulseWave != null && !bkwStress.IsBusy)
            {
                pnlStress.AutoScrollPosition = new Point(0, 0);
                Rectangle rctPulse = new Rectangle(pnlStress.HorizontalScroll.Value,
                    pnlStress.VerticalScroll.Value,
                    pnlStress.ClientSize.Width, pnlStress.ClientSize.Height);
                int scroll;
                pointsStress = _myPulseWave.GetPulseStressPoints(rctPulse, new Point(0, 0), out scroll,
                    false, Math.Max((double)rctPulse.Width / _myPulseWave.GetStressMaxLength(), 0.01));
                pnlStress.AutoScrollMinSize = new Size(scroll, 0);
                if (pointsStress != null)
                {
                    prbStress.Value = 0;
                    prbStress.Visible = true;
                    pnlStress.Enabled = false;
                    pnlStress.Cursor = Cursors.WaitCursor;
                    pnlStress.CreateGraphics().Clear(Color.White);
                    bkwStress.RunWorkerAsync();
                }
            }
        }

        private void bkwStress_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                Graphics graph = pnlStress.CreateGraphics();
                graph.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                Pen penPulse = new Pen(Color.Black);
                int intSleep = 1000 / pointsStress.Length;
                for (int i = 0; i < pointsStress.Length; i++)
                {
                    for (int j = 0; j < pointsStress[i].Length - 1; j++)
                        graph.DrawLine(penStress, pointsStress[i][j],
                                       pointsStress[i][j + 1]);
                    bkwStress.ReportProgress(i * 100 / (pointsStress.Length - 1));
                    if (bkwStress.CancellationPending)
                        return;
                    Thread.Sleep(intSleep);
                }
                Thread.Sleep(500);
            }
            catch (Exception)
            {
                bkwStress.CancelAsync();
            }
            
        }

        private void bkwStress_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            prbStress.Value = e.ProgressPercentage;
        }

        private void bkwStress_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            prbStress.Visible = false;
            pnlStress.Refresh();
            pnlStress.Cursor = Cursors.Hand;
            pnlStress.Enabled = true;
        }

        private void frmVario_FormClosing(object sender, FormClosingEventArgs e)
        {
            bkwStress.CancelAsync();
        }

        //*****************************************************************
        //                            Меню                                *
        //*****************************************************************

        private void mnuExit_Click(object sender, EventArgs e)
        {
            //this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void frmVario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }


        //*****************************************************************
        //                           Печать                               *
        //*****************************************************************

        private void mnuPageSetup_Click(object sender, EventArgs e)
        {
            PageSetupDialog diag = new PageSetupDialog();
            diag.Document = printDocument1;
            diag.EnableMetric = true;
            diag.ShowDialog();
        }

        private void mnuPrintPreview_Click(object sender, EventArgs e)
        {
            PrintPreviewDialog diag = new PrintPreviewDialog();
            diag.Document = printDocument1;
            diag.Icon = Properties.Resources.yagodka;
            diag.ShowDialog();
        }

        private void mnuPrint_Click(object sender, EventArgs e)
        {
            PrintDialog diag = new PrintDialog();
            diag.Document = printDocument1;
            diag.AllowSelection = false;
            diag.AllowSomePages = false;
            if (diag.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    printDocument1.Print();
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Возникла ошибка при попытке печати:\n" +
                                    exc.Message,
                                    "Ошибка",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            // Проверяем наличие данных
            if (_myPulseWave == null)
            {
                e.Cancel = true;
                return;
            }

            // Cоздаем экземпляр graph класса Graphics:
            Graphics graph = e.Graphics;
            graph.PageUnit = GraphicsUnit.Display;
            graph.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;

            // Определяем печатную область страницы:
            float leftMargin = e.MarginBounds.Left,
                  rightMargin = e.MarginBounds.Right,
                  topMargin = e.MarginBounds.Top,
                  bottomMargin = e.MarginBounds.Bottom,
                  printableWidth = e.MarginBounds.Width,
                  printableHeight = e.MarginBounds.Height;

            // Определим используемые шрифты и кисти
            Font fontHeader1 = new Font("Microsoft Sans Serif", 16, FontStyle.Bold),
                 fontTextTitle = new Font("Microsoft Sans Serif", 10, FontStyle.Bold),
                 fontText = new Font("Microsoft Sans Serif", 10);
            SolidBrush brushText = new SolidBrush(Color.Black);

            // Координаты курсора
            float curX = leftMargin,
                  curY = topMargin;

            // Выводим заголовок
            float separator = graph.MeasureString(_strPatient, fontHeader1).Height / 2;
            curY += PrintString(graph, _strPatient, fontHeader1, brushText,
                                new RectangleF(curX, curY, printableWidth,
                                               fontHeader1.GetHeight(graph)),
                                true).Height + separator;

            // Печатаем информацию о замере
            if (_strsParams != null && _strsParams.Length >= 9)
            {
                curX += PrintStringsColumn(graph, fontTextTitle, brushText,
                                           new string[] {"Номер замера: ", "Дата и время: "},
                                           new RectangleF(curX, curY, rightMargin - curX, printableHeight)).Width + 5;
                PrintStringsColumn(graph, fontText, brushText,
                                   new string[] { _strsParams[0] + _strsParams[3], _strsParams[1] + ", " + _strsParams[2] },
                                   new RectangleF(curX, curY, rightMargin - curX, printableHeight));
                curX = leftMargin + printableWidth/2 + separator/2;
                curX += PrintStringsColumn(graph, fontTextTitle, brushText,
                                           new string[] { "Сосуды: ", "Давление: " },
                                           new RectangleF(curX, curY, rightMargin - curX, printableHeight)).Width + 5;
                PrintStringsColumn(graph, fontText, brushText,
                                   new string[] {_strsParams[4], _strsParams[5]},
                                   new RectangleF(curX, curY, rightMargin - curX, printableHeight));
                curX = leftMargin + printableWidth*3/4 + separator*2;
                curX += PrintStringsColumn(graph, fontTextTitle, brushText,
                                           new string[] { "Пульс: ", "Стресс: " },
                                           new RectangleF(curX, curY, rightMargin - curX, printableHeight)).Width + 5;
                curY += PrintStringsColumn(graph, fontText, brushText,
                                           new string[] { _strsParams[6], _strsParams[7] },
                                           new RectangleF(curX, curY, rightMargin - curX, printableHeight)).Height;
                if (_strsParams[8].Length > 0)
                {
                    curX = leftMargin;
                    curX += PrintString(graph, "Комментарий: ", fontTextTitle, brushText,
                                        new RectangleF(curX, curY, rightMargin - curX, printableHeight)).Width + 10;
                    curY += PrintString(graph, _strsParams[8], fontText, brushText,
                                        new RectangleF(curX, curY, rightMargin - curX, printableHeight)).Height;
                }
                curX = leftMargin;
                curY += separator;
            }

            // Выводим секции
            Pen penSections = new Pen(brushText);
            int sectionHeight = (int)(bottomMargin - curY - separator * 2) / 3;
            float startCurY = curY;

            // Стресс
            int sectionBottom = (int)curY + sectionHeight;
            curY += PrintString(graph, "Стресс:", fontTextTitle, brushText,
                                new RectangleF(curX, curY, printableWidth, sectionHeight)).Height;
            int scroll;
            Rectangle rctPic = new Rectangle(new Point(0, 0),
                                             new Size((int)(printableWidth - separator) / 2,
                                                      (int)sectionBottom - (int) curY - 2));
            _myPulseWave.DrawPulseStress(graph, rctPic,
                                        new Point((int) curX, (int) curY + 1), out scroll,
                                        false, Math.Max((double) rctPic.Width/_myPulseWave.GetStressMaxLength(), 0.01));
            graph.DrawRectangle(penSections, curX, curY, rctPic.Width, rctPic.Height + 2);

            // Средняя
            curX += (printableWidth - separator)/2 + separator;
            curY = startCurY;
            sectionBottom = (int)curY + sectionHeight;
            curY += PrintString(graph, "Средняя ПВ:", fontTextTitle, brushText,
                                new RectangleF(curX, curY, printableWidth, sectionHeight)).Height;
            rctPic = new Rectangle(new Point(0, 0),
                                   new Size((int) (printableWidth - separator)/2,
                                            sectionBottom - (int) curY));
            int min = _myPulseWave.GetStressMinLength(), max = _myPulseWave.GetStressMaxLength();
            _myPulseWave.DrawPulseWave(graph, rctPic, new Point((int)curX, (int)curY), out scroll, false,
                false, Math.Max((double)rctPic.Width / ((min + max) / 2.0), 0.01));
            graph.DrawRectangle(penSections, curX, curY, rctPic.Width, rctPic.Height);

            // Амплитудная интегралогистограмма
            curX = leftMargin;
            curY = startCurY + sectionHeight + separator;
            startCurY = curY;
            sectionBottom = (int)curY + sectionHeight;
            curY += PrintString(graph, "Амплитудная интегралогистограмма:", fontTextTitle, brushText,
                                new RectangleF(curX, curY, printableWidth, sectionHeight)).Height;
            rctPic = new Rectangle(new Point(0, 0),
                                   new Size((int) printableWidth, sectionBottom - (int) curY));
            _myPulseWave.DrawPulseIntegrals(graph, rctPic, new Point((int)curX, (int)curY), out scroll);
            graph.DrawRectangle(penSections, curX, curY, rctPic.Width, rctPic.Height);

            // Амплитудо-фазовая интегралогистограмма
            curY = startCurY + sectionHeight + separator;
            sectionBottom = (int)curY + sectionHeight;
            curY += PrintString(graph, "Амплитудо-фазовая интегралогистограмма:", fontTextTitle, brushText,
                                new RectangleF(curX, curY, printableWidth, sectionHeight)).Height;
            rctPic = new Rectangle(new Point(0, 0),
                                   new Size((int)printableWidth, sectionBottom - (int)curY));
            _myPulseWave.DrawPulseIntegralsPhas(graph, rctPic, new Point((int)curX, (int)curY), out scroll);
            graph.DrawRectangle(penSections, curX, curY, rctPic.Width, rctPic.Height);
        }

        private static SizeF PrintString(Graphics graph, string str, Font font, Brush brush, RectangleF rct,
            bool center = false)
        {
            SizeF sizeStr = graph.MeasureString(str, font);
            while (sizeStr.Width > rct.Width && str.LastIndexOf(' ') > -1)
            {
                str = str.Remove(str.LastIndexOf(' ')) + "...";
                sizeStr = graph.MeasureString(str, font);
            }
            graph.DrawString(str, font, brush,
                             (center ? rct.X + rct.Width / 2 - sizeStr.Width / 2 : rct.X),
                             (center ? rct.Y + rct.Height / 2 - sizeStr.Height / 2 : rct.Y));
            return sizeStr;
        }

        private static SizeF PrintStringsColumn(Graphics graph, Font font, Brush brush, string[] strs, RectangleF rct)
        {
            SizeF result = new SizeF(0, 0);
            foreach (var str in strs)
            {
                SizeF size = PrintString(graph, str, font, brush, rct);
                rct.Y += size.Height;
                result.Height += size.Height;
                if (size.Width > result.Width)
                    result.Width = size.Width;
            }
            return result;
        }

    }
}
