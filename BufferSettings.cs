﻿//
// В классе BufferSettings хранятся настройки формы буфера
//

using System.Configuration;

namespace TonClock
{
    sealed class BufferSettings : ApplicationSettingsBase
    {

        // ----------------------------- Свойства отображения графиков -----------------------------

        // Масштаб по горизонтальной оси
        [UserScopedSetting()]
        [DefaultSettingValue("1")]
        public double Scale
        {
            get { return (double)this["Scale"]; }
            set { this["Scale"] = value; }
        }

        // Флаг, что надо показать точки, по которым строятся графики
        [UserScopedSetting()]
        [DefaultSettingValue("false")]
        public bool FlagPoints
        {
            get { return (bool)this["FlagPoints"]; }
            set { this["FlagPoints"] = value; }
        }

        // Флаг, что надо печатать реальные значения данных
        [UserScopedSetting()]
        [DefaultSettingValue("false")]
        public bool FlagPrintValues
        {
            get { return (bool)this["FlagPrintValues"]; }
            set { this["FlagPrintValues"] = value; }
        }

        // Флаг, что надо рисовать график производной
        [UserScopedSetting()]
        [DefaultSettingValue("false")]
        public bool FlagDerivative
        {
            get { return (bool)this["FlagDerivative"]; }
            set { this["FlagDerivative"] = value; }
        }

    }
}
