﻿using System;
using System.Drawing;
using System.Windows.Forms;
using NAudio.Wave;

namespace TonClock
{
    internal partial class frmProperties : Form
    {
        private ClockSettings _clockSettings, _clockSettingsBefore;
        private DevicesList _devicesList;
        private IDevicePulse _device;
        private DelegateInitFonDevice _delegateInitFonDevice;
        private bool _pulseMode;

        private const string BtIdP1 = "PWAVE6",
                             BtPinP1 = "0000",
                             BtIdP2 = "P6W0002",
                             BtPinP2 = "220002";

        /* ----- констуктор ----- */
        public frmProperties(ClockSettings settings, DevicesList dList, IDevicePulse device, DelegateInitFonDevice delegateInitFonDevice, bool pulseMode = false)
        {
            InitializeComponent();
            _clockSettings = settings;
            _clockSettingsBefore = new ClockSettings(settings);
            rdbUsb.Checked = (_clockSettings.DeviceMode == DeviceModes.Usb);
            rdbUsbEc.Checked = (_clockSettings.DeviceMode == DeviceModes.UsbEc);
            rdbFtdiOld.Checked = (_clockSettings.DeviceMode == DeviceModes.FtdiOld);
            rdbFtdiNew.Checked = (_clockSettings.DeviceMode == DeviceModes.FtdiNew);
            rdbBluetooth.Checked = (_clockSettings.DeviceMode == DeviceModes.Bluetooth);
            txtVendorId.Text = _clockSettings.VendorId;
            txtProductId.Text = _clockSettings.ProductId;
            rdbComUsbEcAuto.Checked = String.IsNullOrEmpty(_clockSettings.UsbEcCom);
            rdbComUsbEcManual.Checked = !rdbComUsbEcAuto.Checked;
            if (_clockSettings.Dac1 >= nudDac1.Minimum && _clockSettings.Dac1 <= nudDac1.Maximum)
                nudDac1.Value = _clockSettings.Dac1;
            if (_clockSettings.Dac2 >= nudDac2.Minimum && _clockSettings.Dac2 <= nudDac2.Maximum)
                nudDac2.Value = _clockSettings.Dac2;
            string[] comPorts = System.IO.Ports.SerialPort.GetPortNames();
            foreach (var comPort in comPorts)
            {
                cmbUsbEcCom.Items.Add(comPort);
            }
            cmbUsbEcCom.Text = _clockSettings.UsbEcCom;
            if (_clockSettings.BtId.Equals(BtIdP1))
                rdbBtP1.Checked = true;
            else if (_clockSettings.BtId.Equals(BtIdP2))
                rdbBtP2.Checked = true;
            else
            {
                rdbBtManual.Checked = true;
                txtBtId.Text = _clockSettings.BtId;
                txtBtPin.Text = _clockSettings.BtPin;
            }
            chkSoundPulse.Checked = _clockSettings.FlagSoundPulse;
            SetTrackBarValue(trbSoundPulseVolume, (int)Math.Round(_clockSettings.SoundPulseVolume * 100));
            SetTrackBarValue(trbSoundPulsePitch, (int)Math.Round(_clockSettings.SoundPulsePitch));
            cmbRec.SelectedIndex = _clockSettings.Rec;
            SetTrackBarValue(trbSoundRecVolume, (int)Math.Round(_clockSettings.SoundRecVolume * 100));
            chkSoundResult.Checked = _clockSettings.FlagSoundResult;
            SetTrackBarValue(trbSoundResultVolume, (int)Math.Round(_clockSettings.SoundResultVolume * 100));
            cmbVoice.SelectedIndex = _clockSettings.Voice;
            SetTrackBarValue(trbVoiceVolume, (int)Math.Round(_clockSettings.VoiceVolume * 100));
            chkLevel.Checked = _clockSettings.FlagLevel;

            _devicesList = dList;
            dgvAutoDevices.Rows.Clear();
            UpdateDeviceTable(true);
            tmrDevices.Enabled = (_clockSettings.DeviceMode == DeviceModes.FtdiNew);

            _device = device;
            _delegateInitFonDevice = delegateInitFonDevice;

            _pulseMode = pulseMode;
            if (_pulseMode)
            {
                lblMode.Enabled =
                    rdbUsb.Enabled =
                    rdbUsbEc.Enabled = rdbFtdiOld.Enabled = rdbFtdiNew.Enabled = rdbBluetooth.Enabled = false;
                if (_clockSettings.DeviceMode == DeviceModes.UsbEc)
                    grbVirtualCom.Enabled = false;
                else
                    tbcDeviceOptions.Visible = false;
            }
        }

        private void frmProperties_Load(object sender, EventArgs e)
        {
            tbcDeviceOptions.DrawMode = TabDrawMode.OwnerDrawFixed;
            tbcDeviceOptions.Appearance = TabAppearance.Buttons;
            tbcDeviceOptions.ItemSize = new System.Drawing.Size(0, 1);
            tbcDeviceOptions.SizeMode = TabSizeMode.Fixed;
            tbcDeviceOptions.TabStop = false;
        }

        private void UpdateDeviceTable(bool flagSelectRow = false)
        {
            for (int i = 0; i < dgvAutoDevices.Rows.Count; i++)
            {
                int idDevice;
                if (int.TryParse(dgvAutoDevices["clmId", i].Value.ToString(), out idDevice))
                {
                    EndDevice dev =
                        _devicesList.GetDeviceStructByDeviceId(idDevice);
                    if (dev != null)
                    {
                        UpdateDeviceTableString(i, dev, false);
                    }
                }
            }
            for (int i = dgvAutoDevices.Rows.Count; i < _devicesList.Count; i++)
            {
                EndDevice dev = _devicesList.GetDeviceStructByOrder(i);
                if (dev != null)
                {
                    dgvAutoDevices.Rows.Add();
                    UpdateDeviceTableString(dgvAutoDevices.Rows.Count - 1, dev);
                    if (flagSelectRow && (dev.Id == _clockSettings.SrcId))
                    {
                        dgvAutoDevices.CurrentCell = dgvAutoDevices["clmId", dgvAutoDevices.Rows.Count - 1];
                    }
                }
            }
        }

        private void UpdateDeviceTableString(int iRow, EndDevice dev, bool flagUpdateName = true)
        {
            if (dev == null)
                return;
            dgvAutoDevices["clmId", iRow].Value = dev.Id;
            if (flagUpdateName)
                dgvAutoDevices["clmName", iRow].Value = dev.Name;
            dgvAutoDevices["clmOn", iRow].Value = (dev.FlagOn ? "Вкл" : "Выкл");
            DataGridViewCellStyle cellStyle = new DataGridViewCellStyle(dgvAutoDevices.DefaultCellStyle);
            if (!dev.FlagOn)
                cellStyle.ForeColor = cellStyle.SelectionForeColor = Color.LightGray;
            for (int i = 0; i < dgvAutoDevices.ColumnCount; i++)
            {
                dgvAutoDevices[i, iRow].Style = cellStyle;
            }
            float volt = dev.Volt / 100f;
            dgvAutoDevices["clmVolt", iRow].Value = volt.ToString("N2");
        }

        private void tmrDevices_Tick(object sender, EventArgs e)
        {
            UpdateDeviceTable();
        }

        private void SetTrackBarValue(TrackBar trb, int val)
        {
            if (val >= trb.Minimum && val <= trb.Maximum)
                trb.Value = val;
            else
                trb.Value = trb.Minimum;
        }

        /* ----- возврат значений в главную форму ----- */

        private void btnOK_Click(object sender, EventArgs e)
        {
            AcceptSettings();
            this.DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            _clockSettings.DeepCopy(_clockSettingsBefore);
            SetDacDevice();
            this.DialogResult = DialogResult.Cancel;
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            AcceptSettings();
            tmrDevices.Enabled = (_clockSettings.DeviceMode == DeviceModes.FtdiNew);
        }

        private void AcceptSettings()
        {
            for (int i = 0; i < dgvAutoDevices.Rows.Count; i++)
            {
                int idDevice;
                if (int.TryParse(dgvAutoDevices[0, i].Value.ToString(), out idDevice))
                {
                    EndDevice dev =
                        _devicesList.GetDeviceStructByDeviceId(idDevice);
                    if ((dev != null) && (dgvAutoDevices[1, i].Value != null))
                    {
                        dev.Name = dgvAutoDevices[1, i].Value.ToString();
                    }
                }
            }
            if (rdbUsb.Checked)
                _clockSettings.DeviceMode = DeviceModes.Usb;
            else if (rdbUsbEc.Checked)
                _clockSettings.DeviceMode = DeviceModes.UsbEc;
            else if (rdbFtdiOld.Checked)
                _clockSettings.DeviceMode = DeviceModes.FtdiOld;
            else if (rdbFtdiNew.Checked)
                _clockSettings.DeviceMode = DeviceModes.FtdiNew;
            else if (rdbBluetooth.Checked)
                _clockSettings.DeviceMode = DeviceModes.Bluetooth;
            _clockSettings.VendorId = txtVendorId.Text;
            _clockSettings.ProductId = txtProductId.Text;
            _clockSettings.UsbEcCom = rdbComUsbEcManual.Checked ? cmbUsbEcCom.Text : String.Empty;
            _clockSettings.Dac1 = (ushort)nudDac1.Value;
            _clockSettings.Dac2 = (ushort)nudDac2.Value;
            if (dgvAutoDevices.SelectedRows.Count <= 0)
                _clockSettings.SrcId = -1;
            else
            {
                int idDevice;
                if (int.TryParse(dgvAutoDevices.SelectedRows[0].Cells[0].Value.ToString(), out idDevice))
                    _clockSettings.SrcId = idDevice;
            }
            if (rdbBtP1.Checked)
            {
                _clockSettings.BtId = BtIdP1;
                _clockSettings.BtPin = BtPinP1;
            }
            else if (rdbBtP2.Checked)
            {
                _clockSettings.BtId = BtIdP2;
                _clockSettings.BtPin = BtPinP2;
            }
            else
            {
                _clockSettings.BtId = txtBtId.Text;
                _clockSettings.BtPin = txtBtPin.Text;
            }

            _clockSettings.FlagSoundPulse = chkSoundPulse.Checked;
            _clockSettings.SoundPulseVolume = trbSoundPulseVolume.Value / 100f;
            _clockSettings.SoundPulsePitch = trbSoundPulsePitch.Value;
            _clockSettings.Rec = cmbRec.SelectedIndex;
            _clockSettings.SoundRecVolume = trbSoundRecVolume.Value / 100f;
            _clockSettings.FlagSoundResult = chkSoundResult.Checked;
            _clockSettings.SoundResultVolume = trbSoundResultVolume.Value / 100f;
            _clockSettings.Voice = cmbVoice.SelectedIndex;
            _clockSettings.VoiceVolume = trbVoiceVolume.Value / 100f;
            _clockSettings.FlagLevel = chkLevel.Checked;

            SetDacDevice();
            _delegateInitFonDevice();
        }

        private void SetDacDevice()
        {
            try
            {
                if (_device != null && _device.DeviceMode == DeviceModes.UsbEc)
                    ((UsbEcDevice) _device).SetDac(_clockSettings.Dac1, _clockSettings.Dac2);
            }
            catch (Exception exc)
            {
                MessageBox.Show(
                    String.Format("Возникла ошибка при установке параметров DAC:{0}{1}", exc.Message,
                                  Environment.NewLine),
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /* ----- вид и поведение формы ----- */

        private void tbcProperties_DrawItem(object sender, DrawItemEventArgs e)
        {
            InterfaceManager iManager = new InterfaceManager(); //экземпляр нашего класса
            iManager.DrawHorizontalTabControl(sender as TabControl, e); //вызываем метод и передаем туда параметры
        }


        private void rdbUsb_CheckedChanged(object sender, EventArgs e)
        {
            tbcDeviceOptions.SelectedTab = tbcDeviceOptions.TabPages["tbp_" + ((RadioButton) sender).Name];
        }

        private void rdbComUsbEcManual_CheckedChanged(object sender, EventArgs e)
        {
            cmbUsbEcCom.Enabled = rdbComUsbEcManual.Checked;
        }


        private void chkSoundPulse_CheckedChanged(object sender, EventArgs e)
        {
            pnlSoundPulse.Enabled = chkSoundPulse.Checked;
        }

        private void chkSoundResult_CheckedChanged(object sender, EventArgs e)
        {
            pnlSoundResult.Enabled = chkSoundResult.Checked;
        }

        private void trbSoundPulseVolume_ValueChanged(object sender, EventArgs e)
        {
            lblSoundPulseVolume.Text = trbSoundPulseVolume.Value.ToString("0") + " %";
        }

        private void trbSoundRecVolume_ValueChanged(object sender, EventArgs e)
        {
            lblSoundRecVolume.Text = trbSoundRecVolume.Value.ToString("0") + " %";
        }

        private void trbSoundPulsePitch_ValueChanged(object sender, EventArgs e)
        {
            lblSoundPulsePitch.Text = trbSoundPulsePitch.Value.ToString("0");
        }

        private void trbSoundResultVolume_ValueChanged(object sender, EventArgs e)
        {
            lblSoundResultVolume.Text = trbSoundResultVolume.Value.ToString("0") + " %";
        }

        private void trbVoiceVolume_ValueChanged(object sender, EventArgs e)
        {
            lblVoiceVolume.Text = trbVoiceVolume.Value.ToString("0") + " %";
        }


        private WaveOut waveOutPulse;
        private PortamentoSineWaveOscillator oscillator;

        private void trbSoundPulsePitch_Scroll(object sender, EventArgs e)
        {
            StartSoundPulse();
            System.Threading.Thread.Sleep(200);
            StopSoundPulse();
        }

        private void StartSoundPulse()
        {
            try
            {
                if ((waveOutPulse = new WaveOut()) != null)
                {
                    oscillator = new PortamentoSineWaveOscillator(44100, trbSoundPulsePitch.Value);
                    oscillator.Amplitude = 8192;
                    waveOutPulse.Init(oscillator);
                    waveOutPulse.Volume = trbSoundPulseVolume.Value / 100F;
                    waveOutPulse.Play();
                }
            }
            catch (Exception)
            {
                StopSoundPulse();
            }
        }

        private void StopSoundPulse()
        {
            if (waveOutPulse != null)
            {
                waveOutPulse.Stop();
                waveOutPulse.Dispose();
                waveOutPulse = null;
            }
        }

        private void frmProperties_FormClosing(object sender, FormClosingEventArgs e)
        {
            StopSoundPulse();
        }

        private void tbcProperties_SelectedIndexChanged(object sender, EventArgs e)
        {
            StopSoundPulse();
        }

        private void trbDac1_Scroll(object sender, EventArgs e)
        {
            if (trbDac1.Value >= nudDac1.Minimum && trbDac1.Value <= nudDac1.Maximum)
                nudDac1.Value = trbDac1.Value;
        }

        private void nudDac1_ValueChanged(object sender, EventArgs e)
        {
            int val = (int)nudDac1.Value;
            if (val >= trbDac1.Minimum && val <= trbDac1.Maximum)
                trbDac1.Value = val;
        }

        private void trbDac2_Scroll(object sender, EventArgs e)
        {
            if (trbDac2.Value >= nudDac2.Minimum && trbDac2.Value <= nudDac2.Maximum)
                nudDac2.Value = trbDac2.Value;
        }

        private void nudDac2_ValueChanged(object sender, EventArgs e)
        {
            int val = (int)nudDac2.Value;
            if (val >= trbDac2.Minimum && val <= trbDac2.Maximum)
                trbDac2.Value = val;
        }

        private void rdbBtManual_CheckedChanged(object sender, EventArgs e)
        {
            lblBtId.Enabled = lblBtPin.Enabled = txtBtId.Enabled = txtBtPin.Enabled = rdbBtManual.Checked;
        }

    }


    /// <summary>
    /// Метод для изменения TabControl на горизонтальный
    /// </summary>
    /// <param name="tabControl">Ваш TabControl</param>
    /// <param name="e">Аргументы события 
    /// </param>
    class InterfaceManager
    {
        public void DrawHorizontalTabControl(TabControl tabControl, DrawItemEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush brush;
            Font _TabFont = new Font("Microsoft San Sherif", 12, FontStyle.Bold, GraphicsUnit.Pixel);

            TabPage tabPage = tabControl.TabPages[e.Index]; //текущая вкладка
            Rectangle tabBounds = tabControl.GetTabRect(e.Index); //прямоугольник текущей вкладки
            tabPage.BackColor = SystemColors.Control; //установка фона текущей вкладки
            if (e.State == DrawItemState.Selected) //если это Выбранная вкладка то...
            {
                brush = new SolidBrush(Color.Black); //красный сплошной Браш для выделения надписи Выбранной вкладки
                g.FillRectangle(new SolidBrush(tabPage.BackColor), e.Bounds); //зарисовываем Выбранную вкладку одним цветом
            }
            else
            {
                brush = new SolidBrush(Color.DarkSlateGray); //цвет НЕ выбранной вкладки остается старым
                g.FillRectangle(new SolidBrush(Color.FromArgb(223, 226, 232)), e.Bounds); //зарисовываем НЕ выбранную вкладку другим цветом
            }

            StringFormat strFormat = new StringFormat(); // надпись по центру
            strFormat.Alignment = StringAlignment.Center;
            strFormat.LineAlignment = StringAlignment.Center;
            g.DrawString(tabPage.Text, _TabFont, brush, tabBounds, strFormat);
                //рисуем саму надпись с уже установленным ранее Браш
        }
    }

}
