﻿namespace TonClock
{
    partial class frmStatist
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuStripBuffer = new System.Windows.Forms.MenuStrip();
            this.mnuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPageSetup = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPrintPreview = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPrint = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.grdStatist = new System.Windows.Forms.DataGridView();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.top1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lower1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.top2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lower2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.topResult = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lowerResult = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuStripBuffer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdStatist)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStripBuffer
            // 
            this.menuStripBuffer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFile});
            this.menuStripBuffer.Location = new System.Drawing.Point(0, 0);
            this.menuStripBuffer.Name = "menuStripBuffer";
            this.menuStripBuffer.Size = new System.Drawing.Size(696, 24);
            this.menuStripBuffer.TabIndex = 2;
            this.menuStripBuffer.Text = "menuStrip1";
            // 
            // mnuFile
            // 
            this.mnuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuPageSetup,
            this.mnuPrintPreview,
            this.mnuPrint,
            this.toolStripSeparator2,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Size = new System.Drawing.Size(48, 20);
            this.mnuFile.Text = "&Файл";
            // 
            // mnuPageSetup
            // 
            this.mnuPageSetup.Image = global::TonClock.Properties.Resources.PrintSetupHS;
            this.mnuPageSetup.Name = "mnuPageSetup";
            this.mnuPageSetup.Size = new System.Drawing.Size(233, 22);
            this.mnuPageSetup.Text = "Пара&метры страницы";
            this.mnuPageSetup.Click += new System.EventHandler(this.mnuPageSetup_Click);
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Image = global::TonClock.Properties.Resources.PrintPreviewHS;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Size = new System.Drawing.Size(233, 22);
            this.mnuPrintPreview.Text = "Пред&варительный просмотр";
            this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // mnuPrint
            // 
            this.mnuPrint.Image = global::TonClock.Properties.Resources.PrintHS;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.mnuPrint.Size = new System.Drawing.Size(233, 22);
            this.mnuPrint.Text = "&Печать...";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(230, 6);
            // 
            // mnuExit
            // 
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.mnuExit.Size = new System.Drawing.Size(233, 22);
            this.mnuExit.Text = "&Закрыть окно";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // grdStatist
            // 
            this.grdStatist.AllowUserToAddRows = false;
            this.grdStatist.AllowUserToDeleteRows = false;
            this.grdStatist.AllowUserToResizeRows = false;
            this.grdStatist.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdStatist.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdStatist.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdStatist.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.date,
            this.time,
            this.top1,
            this.lower1,
            this.top2,
            this.lower2,
            this.topResult,
            this.lowerResult});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdStatist.DefaultCellStyle = dataGridViewCellStyle4;
            this.grdStatist.EnableHeadersVisualStyles = false;
            this.grdStatist.Location = new System.Drawing.Point(12, 27);
            this.grdStatist.Name = "grdStatist";
            this.grdStatist.ReadOnly = true;
            this.grdStatist.RowHeadersWidth = 25;
            this.grdStatist.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.grdStatist.Size = new System.Drawing.Size(672, 508);
            this.grdStatist.TabIndex = 8;
            this.grdStatist.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.grdStatist_CellPainting);
            this.grdStatist.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.grdStatist_ColumnWidthChanged);
            this.grdStatist.Scroll += new System.Windows.Forms.ScrollEventHandler(this.grdStatist_Scroll);
            this.grdStatist.Paint += new System.Windows.Forms.PaintEventHandler(this.grdStatist_Paint);
            // 
            // printDocument1
            // 
            this.printDocument1.BeginPrint += new System.Drawing.Printing.PrintEventHandler(this.printDocument1_BeginPrint);
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // id
            // 
            this.id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.id.HeaderText = "№";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.id.Width = 24;
            // 
            // date
            // 
            this.date.HeaderText = "Дата";
            this.date.Name = "date";
            this.date.ReadOnly = true;
            this.date.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // time
            // 
            this.time.HeaderText = "Время";
            this.time.Name = "time";
            this.time.ReadOnly = true;
            this.time.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // top1
            // 
            this.top1.HeaderText = "АД в.";
            this.top1.Name = "top1";
            this.top1.ReadOnly = true;
            this.top1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // lower1
            // 
            this.lower1.HeaderText = "АД н.";
            this.lower1.Name = "lower1";
            this.lower1.ReadOnly = true;
            this.lower1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // top2
            // 
            this.top2.HeaderText = "АД в.";
            this.top2.Name = "top2";
            this.top2.ReadOnly = true;
            this.top2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // lower2
            // 
            this.lower2.HeaderText = "АД н.";
            this.lower2.Name = "lower2";
            this.lower2.ReadOnly = true;
            this.lower2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // topResult
            // 
            this.topResult.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.NullValue = null;
            this.topResult.DefaultCellStyle = dataGridViewCellStyle2;
            this.topResult.HeaderText = "Разница по АД в.";
            this.topResult.Name = "topResult";
            this.topResult.ReadOnly = true;
            this.topResult.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.topResult.Width = 102;
            // 
            // lowerResult
            // 
            this.lowerResult.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.lowerResult.DefaultCellStyle = dataGridViewCellStyle3;
            this.lowerResult.HeaderText = "Разница по АД н.";
            this.lowerResult.Name = "lowerResult";
            this.lowerResult.ReadOnly = true;
            this.lowerResult.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.lowerResult.Width = 102;
            // 
            // frmStatist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(696, 547);
            this.Controls.Add(this.grdStatist);
            this.Controls.Add(this.menuStripBuffer);
            this.KeyPreview = true;
            this.MinimizeBox = false;
            this.Name = "frmStatist";
            this.Text = "Статистика";
            this.Load += new System.EventHandler(this.frmStatist_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmStatist_KeyDown);
            this.menuStripBuffer.ResumeLayout(false);
            this.menuStripBuffer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdStatist)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStripBuffer;
        private System.Windows.Forms.ToolStripMenuItem mnuFile;
        private System.Windows.Forms.ToolStripMenuItem mnuPageSetup;
        private System.Windows.Forms.ToolStripMenuItem mnuPrintPreview;
        private System.Windows.Forms.ToolStripMenuItem mnuPrint;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem mnuExit;
        private System.Windows.Forms.DataGridView grdStatist;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn date;
        private System.Windows.Forms.DataGridViewTextBoxColumn time;
        private System.Windows.Forms.DataGridViewTextBoxColumn top1;
        private System.Windows.Forms.DataGridViewTextBoxColumn lower1;
        private System.Windows.Forms.DataGridViewTextBoxColumn top2;
        private System.Windows.Forms.DataGridViewTextBoxColumn lower2;
        private System.Windows.Forms.DataGridViewTextBoxColumn topResult;
        private System.Windows.Forms.DataGridViewTextBoxColumn lowerResult;
    }
}