﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TonClock
{
    public partial class frmCasp : Form
    {
        //*****************************************************************
        //                         Базовые поля                           *
        //*****************************************************************

        private int _idPatient, _idMeasurement, _idNumber, _top, _lower;
        private float _l1, _l2, _l5;
        private PulseWave _myPulseWave, _myPulseWaveResult;
        private CaspCalc _calc;
        private double  _scale;         // масштаб по оси х


        //*****************************************************************
        //                     Конструкторы и загрузка                    *
        //*****************************************************************

        public frmCasp(int id, int idM, PulseWave pw, int n, double scale = 1)
        {
            InitializeComponent();
            this.Icon = Icon.FromHandle(Properties.Resources.CaspIcon.GetHicon());

            if (pw == null) return;

            ResultChanged = false;
            _idPatient = id;
            _idMeasurement = idM;
            _myPulseWave = PulseWave.DeepClone(pw);
            _myPulseWave.SetRepersVisible(new bool[] { true, false, false, true, false });
            _idNumber = n;
            _scale = scale;
        }

        private void frmCasp_Shown(object sender, EventArgs e)
        {
            try
            {
                using (var myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    using (var myOleDbCommand = new OleDbCommand(
                        "SELECT lastname, firstname, patronymic, l1, l2, l5 " +
                        "FROM Patients " +
                        "WHERE id = " + _idPatient,
                        myOleDbConnection))
                    {
                        using (var myReader = myOleDbCommand.ExecuteReader())
                        {
                            if (myReader != null)
                            {
                                if (myReader.Read())
                                {
                                    this.Text += " - " + myReader[0].ToString() + " " +
                                                 myReader[1].ToString()[0] + "." + myReader[2].ToString()[0] + "." +
                                                 " - замер " + _idNumber;
                                    _l1 = myReader.IsDBNull(3) ? 0 : myReader.GetFloat(3);
                                    _l2 = myReader.IsDBNull(4) ? 0 : myReader.GetFloat(4);
                                    _l5 = myReader.IsDBNull(5) ? 0 : myReader.GetFloat(5);
                                }
                                else
                                {
                                    throw new Exception("В базе не обнаружен пациент с таким идентификатором.");
                                }
                            }
                            else
                                throw new Exception("Не удалось прочитать информацию о пациенте.");
                        }
                    }
                    // читаем давление
                    using (var myOleDbCommand = new OleDbCommand(
                        "SELECT topPressure, lowerPressure " +
                        "FROM Measurements " +
                        "WHERE id = " + _idMeasurement,
                        myOleDbConnection))
                    {
                        using (var myReader = myOleDbCommand.ExecuteReader())
                        {
                            if (myReader != null)
                            {
                                if (myReader.Read())
                                {
                                    _top = myReader.IsDBNull(0) ? 0 : myReader.GetInt16(0);
                                    _lower = myReader.IsDBNull(1) ? 0 : myReader.GetInt16(1);
                                    _myPulseWave.CalcScalePress(_top, _lower);
                                }
                                else
                                {
                                    throw new Exception("В базе не обнаружен выбранный замер.");
                                }
                            }
                            else
                                throw new Exception("Не удалось прочитать информацию о выбранном замере.");
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Внимание! При попытке открытия базы данных произошла ошибка:\n" +
                    exc.Message + "\n\n" +
                    "Дальнейшая работа модуля не возможна.\n\n" +
                    "Пожалуйста, обратитесь к администратору.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.DialogResult = DialogResult.Abort;
            }
        }

        private void frmCasp_Resize(object sender, EventArgs e)
        {
            this.Refresh();
        }

        private void frmCasp_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.DialogResult = DialogResult.OK;
            }
        }


        //*****************************************************************
        //                 Прорисовка пульсовой волны                     *
        //*****************************************************************

        private void pnlPulse_Paint(object sender, PaintEventArgs e)
        {
            if (_myPulseWave != null)
            {
                Rectangle rctPulse = new Rectangle(pnlPulse.HorizontalScroll.Value,
                    pnlPulse.VerticalScroll.Value,
                    pnlPulse.ClientSize.Width, pnlPulse.ClientSize.Height);
                int scroll;
                _myPulseWave.DrawPulseWave(e.Graphics, rctPulse, new Point(0, 0), out scroll,
                                          true, false, _scale, false, false, false, true, true);
                pnlPulse.AutoScrollMinSize = new Size(scroll, 0);
            }
        }

        private void pnlPulse_Scroll(object sender, ScrollEventArgs e)
        {
            pnlPulse.Refresh();
        }


        private void pnlCentralPulse_Paint(object sender, PaintEventArgs e)
        {
            if (_myPulseWaveResult != null)
            {
                Rectangle rctPulse = new Rectangle(pnlCentralPulse.HorizontalScroll.Value,
                    pnlCentralPulse.VerticalScroll.Value,
                    pnlCentralPulse.ClientSize.Width, pnlCentralPulse.ClientSize.Height);
                int scroll;
                _myPulseWaveResult.DrawPulseWave(e.Graphics, rctPulse, new Point(0, 0), out scroll,
                                          true, false, _scale, false, false, false, true, true);
                pnlCentralPulse.AutoScrollMinSize = new Size(scroll, 0);
            }
        }


        private void pnlCentralPulse_Scroll(object sender, ScrollEventArgs e)
        {
            pnlCentralPulse.Refresh();
        }


        //*****************************************************************
        //                       Расстановка реперов                      *
        //*****************************************************************

        private void pnlPulse_MouseDown(object sender, MouseEventArgs e)
        {
            if (_myPulseWave != null && _myPulseWave.MouseDown(e.Location))
                pnlPulse.Refresh();
        }

        private void pnlPulse_MouseMove(object sender, MouseEventArgs e)
        {
            if (_myPulseWave == null)
                return;

            Rectangle rctPulse = new Rectangle(pnlPulse.HorizontalScroll.Value,
                                               pnlPulse.VerticalScroll.Value,
                                               pnlPulse.ClientSize.Width,
                                               pnlPulse.ClientSize.Height);
            if (_myPulseWave.MoveReperMode)
            {
                if (_myPulseWave.ReperMove(e.Location, rctPulse))
                    pnlPulse.Refresh();
            }
            else
            {
                bool flagRefresh;
                pnlPulse.Cursor = _myPulseWave.MouseMove(e.Location, rctPulse,
                                                  out flagRefresh)
                                      ? Cursors.Hand
                                      : Cursors.Default;
                if (flagRefresh)
                    pnlPulse.Refresh();
            }
        }

        private void pnlPulse_MouseUp(object sender, MouseEventArgs e)
        {
            if (_myPulseWave != null && _myPulseWave.MouseUp())
            {
                pnlPulse.Refresh();
            }
        }

        private void pnlCentralPulse_MouseDown(object sender, MouseEventArgs e)
        {
            if (_myPulseWaveResult != null && _myPulseWaveResult.MouseDown(e.Location))
                pnlCentralPulse.Refresh();
        }

        private void pnlCentralPulse_MouseMove(object sender, MouseEventArgs e)
        {
            if (_myPulseWaveResult == null)
                return;

            Rectangle rctPulse = new Rectangle(pnlCentralPulse.HorizontalScroll.Value,
                                               pnlCentralPulse.VerticalScroll.Value,
                                               pnlCentralPulse.ClientSize.Width,
                                               pnlCentralPulse.ClientSize.Height);
            if (_myPulseWaveResult.MoveReperMode)
            {
                if (_myPulseWaveResult.ReperMove(e.Location, rctPulse))
                    pnlCentralPulse.Refresh();
            }
            else
            {
                bool flagRefresh;
                pnlCentralPulse.Cursor = _myPulseWaveResult.MouseMove(e.Location, rctPulse,
                                                  out flagRefresh)
                                      ? Cursors.Hand
                                      : Cursors.Default;
                if (flagRefresh)
                    pnlCentralPulse.Refresh();
            }
        }

        private void pnlCentralPulse_MouseUp(object sender, MouseEventArgs e)
        {
            if (_myPulseWaveResult != null && _myPulseWaveResult.MouseUp())
            {
                pnlCentralPulse.Refresh();
            }
        }


        //*****************************************************************
        //                            Расчеты                             *
        //*****************************************************************

        public bool ResultChanged { get; private set; }
        public int[] ResultRepers { get { return _myPulseWave == null ? null : _myPulseWave.Repers; } }

        private void btnCalc_Click(object sender, EventArgs e)
        {
            // проверка необходимых данных
            if (AlmostEqual(_l1, 0) || AlmostEqual(_l2, 0) || AlmostEqual(_l5, 0) )
            {
                MessageBox.Show("Необходимо задать параметры L1, L2 и L5 в карточке пациента.",
                                "Ошибка",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            StopTimerButton();

            // расчет
            float timeA1A4 = _myPulseWave.GetRepersSeconds(1, 4);

            _calc = new CaspCalc(_myPulseWave.PulseWaveField, _myPulseWave.Repers);
            _calc.MakeCalc(_l1, _l2, _l5, timeA1A4, checkBox1.Checked);

            txtTimeA1A4.Text = CaspCalc.PrintStr(timeA1A4);
            txtSrpvp.Text = CaspCalc.PrintStr(_calc.ResultSrpvp);
            //txtSrpvc.Text = CaspCalc.PrintStr(_calc.ResultSrpvc);

            _myPulseWave.SetStartDelay(_calc.ResultDelta);

            _myPulseWaveResult = PulseWave.DeepClone(_myPulseWave);
            _myPulseWaveResult.PulseWaveField = _calc.ResultPulseWave;
            _myPulseWaveResult.SetStartDelay(0);
            _myPulseWaveResult.RepersLabel = "P";
            _myPulseWaveResult.Repers = _calc.ResultRepers;
            _myPulseWaveResult.SetRepersVisible(new bool[] { true, true, false, false, false });
            int maxAllPulse = Math.Max(_myPulseWave.PulseWaveField.Max(), _myPulseWaveResult.PulseWaveField.Max());
            _myPulseWaveResult.CalcScalePress(_top, _lower, maxAllPulse);

            var pressure = _myPulseWaveResult.ConvertToPress();
            //txtCasp.Text = String.Format("{0} / {1}", CaspCalc.PrintStrShort(pressure.Max()), CaspCalc.PrintStrShort(pressure.Min()));
            // коэффициент - костыль для испытаний
            double coef = 1.1d;
            if (_top > 120)
                coef = 1.15d;
            txtCasp.Text = CaspCalc.PrintStrShort(pressure.Max() * coef);

            pnlPulse.AutoScrollPosition = new Point(0, 0);
            pnlPulse.Refresh();
            pnlCentralPulse.AutoScrollPosition = new Point(0, 0);
            pnlCentralPulse.Refresh();

            string strError;
            if (!DbPulseOperations.UpdateRepers(_myPulseWave.Repers, _idMeasurement, _idPatient, out strError))
            {
                MessageBox.Show(
                    String.Format("При попытке обновления данных после перестановки реперов произошла ошибка:{0}" +
                                  strError + "{0}{0}" +
                                  "Пожалуйста, обратитесь к администратору.", Environment.NewLine),
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            ResultChanged = true;

            // Покажем кнопку "Вычислить ИА"
            btnCalcIa.Enabled = true;
            _flagCalcIa = true;
            StartTimerButton();
        }

        private bool _flagCalcIa;

        private void tmrButton_Tick(object sender, EventArgs e)
        {
            if (_flagCalcIa)
                btnCalcIa.BackColor = (btnCalcIa.BackColor == DefaultBackColor ? Color.LawnGreen : DefaultBackColor);
            else
                btnCalc.BackColor = (btnCalc.BackColor == DefaultBackColor ? Color.LawnGreen : DefaultBackColor);
        }

        public void StartTimerButton()
        {
            tmrButton.Start();
        }

        private void StopTimerButton()
        {
            tmrButton.Stop();
            btnCalcIa.BackColor = DefaultBackColor;
            btnCalc.BackColor = DefaultBackColor;
        }

        private void btnCalcIa_Click(object sender, EventArgs e)
        {
            StopTimerButton();
            _flagCalcIa = false;

            if (_myPulseWaveResult == null || _myPulseWaveResult.Repers == null) return;

            int p1 = _myPulseWaveResult.GetReper(1),
                p2 = _myPulseWaveResult.GetReper(2);

            if (_myPulseWaveResult.PulseWaveField == null || _myPulseWaveResult.PulseWaveField.Length <= p1 || _myPulseWaveResult.PulseWaveField.Length <= p2)
                return;

            double ia1 = (double) _myPulseWaveResult.PulseWaveField[p2]/_myPulseWaveResult.PulseWaveField[p1];
            double ia2 = (double) (_myPulseWaveResult.PulseWaveField[p2] - _myPulseWaveResult.PulseWaveField[p1])/
                         _myPulseWaveResult.PulseWaveField[p2];

            txtIa1.Text = CaspCalc.PrintStr(ia1);
            txtIa2.Text = CaspCalc.PrintStr(ia2);
        }

        public static bool AlmostEqual(float a, float b)
        {
            if (a == b)
            {
                return true;
            }
            return Math.Abs(a - b) < Math.Abs(a) / 281474976710656.0;
        }

        private void frmCasp_Load(object sender, EventArgs e)
        {
            _flagCalcIa = false;
            StartTimerButton();
        }

    }
}