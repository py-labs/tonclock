﻿namespace TonClock
{
    partial class frmPatients
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPatients));
            this.grdPatients = new System.Windows.Forms.DataGridView();
            this.id_p = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.firstname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.patronymic = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblHello = new System.Windows.Forms.Label();
            this.grpInfo = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.btnAdd2 = new System.Windows.Forms.Button();
            this.btnDelete2 = new System.Windows.Forms.Button();
            this.btnBuffer = new System.Windows.Forms.Button();
            this.btnHistory = new System.Windows.Forms.Button();
            this.grdMeasurements = new System.Windows.Forms.DataGridView();
            this.idDB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.id_m = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.top = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lower = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblPressure = new System.Windows.Forms.Label();
            this.lblMeasurements = new System.Windows.Forms.Label();
            this.lblPressureTitle = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblDateTitle = new System.Windows.Forms.Label();
            this.lblFio = new System.Windows.Forms.Label();
            this.lblFioTitle = new System.Windows.Forms.Label();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.tmrButton = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnDelete = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnUndo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grdPatients)).BeginInit();
            this.grpInfo.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMeasurements)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grdPatients
            // 
            this.grdPatients.AllowUserToAddRows = false;
            this.grdPatients.AllowUserToDeleteRows = false;
            this.grdPatients.AllowUserToResizeRows = false;
            this.grdPatients.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdPatients.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdPatients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdPatients.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id_p,
            this.lastname,
            this.firstname,
            this.patronymic});
            this.grdPatients.Location = new System.Drawing.Point(8, 51);
            this.grdPatients.Margin = new System.Windows.Forms.Padding(4);
            this.grdPatients.MultiSelect = false;
            this.grdPatients.Name = "grdPatients";
            this.grdPatients.ReadOnly = true;
            this.grdPatients.RowHeadersWidth = 25;
            this.grdPatients.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdPatients.Size = new System.Drawing.Size(439, 385);
            this.grdPatients.TabIndex = 6;
            this.grdPatients.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdPatients_CellDoubleClick);
            this.grdPatients.SelectionChanged += new System.EventHandler(this.grdPatients_SelectionChanged);
            // 
            // id_p
            // 
            this.id_p.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.id_p.HeaderText = "№";
            this.id_p.Name = "id_p";
            this.id_p.ReadOnly = true;
            this.id_p.Visible = false;
            // 
            // lastname
            // 
            this.lastname.HeaderText = "Фамилия";
            this.lastname.Name = "lastname";
            this.lastname.ReadOnly = true;
            // 
            // firstname
            // 
            this.firstname.HeaderText = "Имя";
            this.firstname.Name = "firstname";
            this.firstname.ReadOnly = true;
            // 
            // patronymic
            // 
            this.patronymic.HeaderText = "Отчество";
            this.patronymic.Name = "patronymic";
            this.patronymic.ReadOnly = true;
            // 
            // lblHello
            // 
            this.lblHello.AutoSize = true;
            this.lblHello.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblHello.Location = new System.Drawing.Point(8, 23);
            this.lblHello.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHello.Name = "lblHello";
            this.lblHello.Size = new System.Drawing.Size(90, 20);
            this.lblHello.TabIndex = 5;
            this.lblHello.Text = "Пациенты:";
            // 
            // grpInfo
            // 
            this.grpInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpInfo.Controls.Add(this.tableLayoutPanel2);
            this.grpInfo.Controls.Add(this.grdMeasurements);
            this.grpInfo.Controls.Add(this.lblPressure);
            this.grpInfo.Controls.Add(this.lblMeasurements);
            this.grpInfo.Controls.Add(this.lblPressureTitle);
            this.grpInfo.Controls.Add(this.lblDate);
            this.grpInfo.Controls.Add(this.lblDateTitle);
            this.grpInfo.Controls.Add(this.lblFio);
            this.grpInfo.Controls.Add(this.lblFioTitle);
            this.grpInfo.Location = new System.Drawing.Point(7, 4);
            this.grpInfo.Margin = new System.Windows.Forms.Padding(4);
            this.grpInfo.Name = "grpInfo";
            this.grpInfo.Padding = new System.Windows.Forms.Padding(4);
            this.grpInfo.Size = new System.Drawing.Size(356, 499);
            this.grpInfo.TabIndex = 11;
            this.grpInfo.TabStop = false;
            this.grpInfo.Text = "Информация о пациенте";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.btnAdd2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnDelete2, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnBuffer, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnHistory, 0, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(8, 390);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(342, 100);
            this.tableLayoutPanel2.TabIndex = 15;
            // 
            // btnAdd2
            // 
            this.btnAdd2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd2.Enabled = false;
            this.btnAdd2.ImageIndex = 0;
            this.btnAdd2.Location = new System.Drawing.Point(0, 4);
            this.btnAdd2.Margin = new System.Windows.Forms.Padding(0, 4, 4, 4);
            this.btnAdd2.Name = "btnAdd2";
            this.btnAdd2.Size = new System.Drawing.Size(167, 42);
            this.btnAdd2.TabIndex = 12;
            this.btnAdd2.Text = "Сделать замер...";
            this.btnAdd2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAdd2.UseVisualStyleBackColor = false;
            this.btnAdd2.Click += new System.EventHandler(this.btnAdd2_Click);
            // 
            // btnDelete2
            // 
            this.btnDelete2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete2.Enabled = false;
            this.btnDelete2.Image = global::TonClock.Properties.Resources.DeleteHS;
            this.btnDelete2.Location = new System.Drawing.Point(175, 4);
            this.btnDelete2.Margin = new System.Windows.Forms.Padding(4, 4, 0, 4);
            this.btnDelete2.Name = "btnDelete2";
            this.btnDelete2.Size = new System.Drawing.Size(167, 42);
            this.btnDelete2.TabIndex = 11;
            this.btnDelete2.Text = "Удалить замер";
            this.btnDelete2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete2.UseVisualStyleBackColor = false;
            this.btnDelete2.Click += new System.EventHandler(this.btnDelete2_Click);
            // 
            // btnBuffer
            // 
            this.btnBuffer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnBuffer.Enabled = false;
            this.btnBuffer.Image = ((System.Drawing.Image)(resources.GetObject("btnBuffer.Image")));
            this.btnBuffer.Location = new System.Drawing.Point(175, 54);
            this.btnBuffer.Margin = new System.Windows.Forms.Padding(4, 4, 0, 4);
            this.btnBuffer.Name = "btnBuffer";
            this.btnBuffer.Size = new System.Drawing.Size(167, 42);
            this.btnBuffer.TabIndex = 14;
            this.btnBuffer.Text = "Буфер";
            this.btnBuffer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuffer.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnBuffer.UseVisualStyleBackColor = false;
            this.btnBuffer.Click += new System.EventHandler(this.btnBuffer_Click);
            // 
            // btnHistory
            // 
            this.btnHistory.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnHistory.Enabled = false;
            this.btnHistory.Image = global::TonClock.Properties.Resources.History;
            this.btnHistory.Location = new System.Drawing.Point(0, 54);
            this.btnHistory.Margin = new System.Windows.Forms.Padding(0, 4, 4, 4);
            this.btnHistory.Name = "btnHistory";
            this.btnHistory.Size = new System.Drawing.Size(167, 42);
            this.btnHistory.TabIndex = 14;
            this.btnHistory.Text = "Память";
            this.btnHistory.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnHistory.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnHistory.UseVisualStyleBackColor = false;
            this.btnHistory.Click += new System.EventHandler(this.btnHistory_Click);
            // 
            // grdMeasurements
            // 
            this.grdMeasurements.AllowUserToAddRows = false;
            this.grdMeasurements.AllowUserToDeleteRows = false;
            this.grdMeasurements.AllowUserToResizeRows = false;
            this.grdMeasurements.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grdMeasurements.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.grdMeasurements.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.grdMeasurements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdMeasurements.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDB,
            this.id_m,
            this.date,
            this.time,
            this.top,
            this.lower});
            this.grdMeasurements.Location = new System.Drawing.Point(8, 93);
            this.grdMeasurements.Margin = new System.Windows.Forms.Padding(4);
            this.grdMeasurements.MultiSelect = false;
            this.grdMeasurements.Name = "grdMeasurements";
            this.grdMeasurements.ReadOnly = true;
            this.grdMeasurements.RowHeadersWidth = 25;
            this.grdMeasurements.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.grdMeasurements.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdMeasurements.Size = new System.Drawing.Size(342, 289);
            this.grdMeasurements.TabIndex = 14;
            this.grdMeasurements.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdMeasurements_CellDoubleClick);
            // 
            // idDB
            // 
            this.idDB.HeaderText = "id";
            this.idDB.Name = "idDB";
            this.idDB.ReadOnly = true;
            this.idDB.Visible = false;
            // 
            // id_m
            // 
            this.id_m.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.id_m.HeaderText = "№";
            this.id_m.Name = "id_m";
            this.id_m.ReadOnly = true;
            this.id_m.Width = 47;
            // 
            // date
            // 
            this.date.HeaderText = "Дата";
            this.date.Name = "date";
            this.date.ReadOnly = true;
            // 
            // time
            // 
            this.time.HeaderText = "Время";
            this.time.Name = "time";
            this.time.ReadOnly = true;
            // 
            // top
            // 
            this.top.HeaderText = "АД верхнее";
            this.top.Name = "top";
            this.top.ReadOnly = true;
            // 
            // lower
            // 
            this.lower.HeaderText = "АД нижнее";
            this.lower.Name = "lower";
            this.lower.ReadOnly = true;
            // 
            // lblPressure
            // 
            this.lblPressure.AutoSize = true;
            this.lblPressure.Location = new System.Drawing.Point(177, 57);
            this.lblPressure.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPressure.Name = "lblPressure";
            this.lblPressure.Size = new System.Drawing.Size(38, 16);
            this.lblPressure.TabIndex = 7;
            this.lblPressure.Text = "label";
            // 
            // lblMeasurements
            // 
            this.lblMeasurements.AutoSize = true;
            this.lblMeasurements.Location = new System.Drawing.Point(9, 73);
            this.lblMeasurements.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMeasurements.Name = "lblMeasurements";
            this.lblMeasurements.Size = new System.Drawing.Size(128, 16);
            this.lblMeasurements.TabIndex = 7;
            this.lblMeasurements.Text = "Замеры пациента:";
            // 
            // lblPressureTitle
            // 
            this.lblPressureTitle.AutoSize = true;
            this.lblPressureTitle.Location = new System.Drawing.Point(9, 57);
            this.lblPressureTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblPressureTitle.Name = "lblPressureTitle";
            this.lblPressureTitle.Size = new System.Drawing.Size(160, 16);
            this.lblPressureTitle.TabIndex = 7;
            this.lblPressureTitle.Text = "Давление с тонометра:";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(177, 41);
            this.lblDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(38, 16);
            this.lblDate.TabIndex = 7;
            this.lblDate.Text = "label";
            // 
            // lblDateTitle
            // 
            this.lblDateTitle.AutoSize = true;
            this.lblDateTitle.Location = new System.Drawing.Point(8, 41);
            this.lblDateTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDateTitle.Name = "lblDateTitle";
            this.lblDateTitle.Size = new System.Drawing.Size(119, 16);
            this.lblDateTitle.TabIndex = 7;
            this.lblDateTitle.Text = "Полный возраст:";
            // 
            // lblFio
            // 
            this.lblFio.AutoSize = true;
            this.lblFio.Location = new System.Drawing.Point(177, 25);
            this.lblFio.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFio.Name = "lblFio";
            this.lblFio.Size = new System.Drawing.Size(38, 16);
            this.lblFio.TabIndex = 7;
            this.lblFio.Text = "label";
            // 
            // lblFioTitle
            // 
            this.lblFioTitle.AutoSize = true;
            this.lblFioTitle.Location = new System.Drawing.Point(9, 25);
            this.lblFioTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblFioTitle.Name = "lblFioTitle";
            this.lblFioTitle.Size = new System.Drawing.Size(42, 16);
            this.lblFioTitle.TabIndex = 7;
            this.lblFioTitle.Text = "ФИО:";
            // 
            // btnEdit
            // 
            this.btnEdit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEdit.Enabled = false;
            this.btnEdit.Location = new System.Drawing.Point(150, 4);
            this.btnEdit.Margin = new System.Windows.Forms.Padding(4);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(138, 42);
            this.btnEdit.TabIndex = 8;
            this.btnEdit.Text = "Вызвать карту...";
            this.btnEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEdit.UseVisualStyleBackColor = false;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAdd.Enabled = false;
            this.btnAdd.Location = new System.Drawing.Point(0, 4);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(0, 4, 4, 4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(142, 42);
            this.btnAdd.TabIndex = 10;
            this.btnAdd.Text = "Новый пациент...";
            this.btnAdd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // tmrButton
            // 
            this.tmrButton.Enabled = true;
            this.tmrButton.Interval = 400;
            this.tmrButton.Tick += new System.EventHandler(this.tmrButton_Tick);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.btnAdd, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnEdit, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnDelete, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(8, 444);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(439, 50);
            this.tableLayoutPanel1.TabIndex = 12;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Enabled = false;
            this.btnDelete.Image = global::TonClock.Properties.Resources.DeleteHS;
            this.btnDelete.Location = new System.Drawing.Point(296, 4);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4, 4, 0, 4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(143, 42);
            this.btnDelete.TabIndex = 9;
            this.btnDelete.Text = "Удалить";
            this.btnDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDelete.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.btnUndo);
            this.splitContainer1.Panel1.Controls.Add(this.lblHello);
            this.splitContainer1.Panel1.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Panel1.Controls.Add(this.grdPatients);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.grpInfo);
            this.splitContainer1.Size = new System.Drawing.Size(837, 507);
            this.splitContainer1.SplitterDistance = 451;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 13;
            // 
            // btnUndo
            // 
            this.btnUndo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnUndo.Image = global::TonClock.Properties.Resources.Back;
            this.btnUndo.Location = new System.Drawing.Point(164, 10);
            this.btnUndo.Margin = new System.Windows.Forms.Padding(4);
            this.btnUndo.Name = "btnUndo";
            this.btnUndo.Size = new System.Drawing.Size(283, 33);
            this.btnUndo.TabIndex = 13;
            this.btnUndo.Text = "Вернуться в список консультантов";
            this.btnUndo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnUndo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnUndo.UseVisualStyleBackColor = false;
            this.btnUndo.Click += new System.EventHandler(this.btnUndo_Click);
            // 
            // frmPatients
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 507);
            this.Controls.Add(this.splitContainer1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmPatients";
            this.Text = "ТонКлок";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmPatients_FormClosed);
            this.Load += new System.EventHandler(this.frmPatients_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmPatients_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.grdPatients)).EndInit();
            this.grpInfo.ResumeLayout(false);
            this.grpInfo.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdMeasurements)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.DataGridView grdPatients;
        private System.Windows.Forms.Label lblHello;
        private System.Windows.Forms.GroupBox grpInfo;
        private System.Windows.Forms.Label lblPressure;
        private System.Windows.Forms.Label lblPressureTitle;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblDateTitle;
        private System.Windows.Forms.Label lblFio;
        private System.Windows.Forms.Label lblFioTitle;
        private System.Windows.Forms.Label lblMeasurements;
        private System.Windows.Forms.Button btnDelete2;
        private System.Windows.Forms.Button btnAdd2;
        private System.Windows.Forms.DataGridView grdMeasurements;
        private System.Windows.Forms.Timer tmrButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnUndo;
        private System.Windows.Forms.Button btnBuffer;
        private System.Windows.Forms.Button btnHistory;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_p;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastname;
        private System.Windows.Forms.DataGridViewTextBoxColumn firstname;
        private System.Windows.Forms.DataGridViewTextBoxColumn patronymic;
        private System.Windows.Forms.DataGridViewTextBoxColumn idDB;
        private System.Windows.Forms.DataGridViewTextBoxColumn id_m;
        private System.Windows.Forms.DataGridViewTextBoxColumn date;
        private System.Windows.Forms.DataGridViewTextBoxColumn time;
        private System.Windows.Forms.DataGridViewTextBoxColumn top;
        private System.Windows.Forms.DataGridViewTextBoxColumn lower;
    }
}