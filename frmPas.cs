﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TonClock
{
    public partial class frmPas : Form
    {
        public frmPas(string target)
        {
            InitializeComponent();
            lblPas.Text = "Введите пароль для доступа к " + target + "у:";
        }

        private const string password = "123";

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (mskPas.Text.Equals(password))
                this.DialogResult = DialogResult.OK;
            else
            {
                MessageBox.Show(this, "Указан не верный пароль!",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                mskPas.Focus();
                mskPas.SelectAll();
                return;
            }
        }

        private void frmPas_Shown(object sender, EventArgs e)
        {
            mskPas.Focus();
        }
    }
}
