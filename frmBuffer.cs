﻿using System;
using System.Collections;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Globalization;
using System.Data.OleDb;

namespace TonClock
{
    public partial class frmBuffer : Form
    {
        //*****************************************************************
        //                      Поля и параметры                          *
        //*****************************************************************

        private PulseWave _myPulseWave;
        private readonly CultureInfo _myDbCultInfo = CultureInfo.InvariantCulture;

        private readonly int _idPatient; // текущий пациент

        private int _idMeasurement,      // выбранный замер
                    _idRowTable;         // номер выбранной строки в таблице

        private string _strPatient;      // строка с фио пациента


        //*****************************************************************
        //                        Конструкторы                            *
        //*****************************************************************

        public frmBuffer(int idP, int idM = 0, PulseWave pw = null)
        {
            InitializeComponent();
            this.Icon = Properties.Resources.yagodka;
            _idPatient = idP;
            _idMeasurement = idM;
            _myPulseWave = pw;
        }

        private void frmBuffer_Load(object sender, EventArgs e)
        {
            try
            {
                using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    OleDbCommand myOleDbCommand = new OleDbCommand(
                        "SELECT lastname, firstname, patronymic, age " +
                        "FROM Patients " +
                        "WHERE id = " + _idPatient.ToString(),
                        myOleDbConnection);
                    OleDbDataReader myReader = myOleDbCommand.ExecuteReader();
                    if (myReader != null)
                    {
                        if (myReader.Read())
                        {
                            this.Text = ConstGlobals.AssemblyTitle + " - Буфер - " + myReader[0].ToString() + " " +
                                        myReader[1].ToString()[0] + "." + myReader[2].ToString()[0] + "., " +
                                        DbPulseOperations.PrintAge(myReader.GetInt16(3));
                            lblTitle.Text = "Замеры пациента " + myReader[0].ToString() + " " +
                                            myReader[1].ToString()[0] + "." + myReader[2].ToString()[0] + ". :";
                            _strPatient = myReader[0].ToString() + " " +
                                         myReader[1].ToString() + " " + myReader[2].ToString() + ", " +
                                         DbPulseOperations.PrintAge(myReader.GetInt16(3));
                            myReader.Close();
                        }
                        else
                        {
                            myReader.Close();
                            throw new Exception("В базе не обнаружен пациент с таким идентификатором.");
                        }
                    }
                    else
                        throw new Exception("Не удалось прочитать информацию о пациенте.");
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Внимание! При попытке открытия базы данных произошла ошибка:\n" +
                    exc.Message + "\n\n" +
                    "Дальнейшая работа модуля не возможна.\n\n" +
                    "Пожалуйста, обратитесь к администратору.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.DialogResult = DialogResult.Abort;
            }
            grdMeasurements.ColumnHeadersHeight = grdMeasurements.ColumnHeadersHeight * 2;
            UpdateMainTable(_idMeasurement);
            UpdateMeasurementInfo();
            grdMeasurements.Select();
            // параметры печати
            printDocument1.DefaultPageSettings = new PageSettings
                                                     {Landscape = false, Margins = new Margins(50, 50, 50, 50)};
            printDocument1.DocumentName = "TonClockBuffer";
        }

        private void UpdateMainTable(int id = 0)
        {
            try
            {
                using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    OleDbCommand myOleDbCommand = new OleDbCommand(
                        "SELECT id, mesDateTime, sosud, topPressure, lowerPressure, " +
                        "       pulse, stress, control, signal, deviceMode, deviceSrcId, comment, " +
                        "       globalTop, globalLower, korotkovTop, korotkovLower, xgdTop, xgdLower, mtpTop, mtpLower " +
                        "FROM Measurements " +
                        "WHERE patient = " + _idPatient + " " +
                        "ORDER BY mesDateTime DESC",
                        myOleDbConnection);
                    grdMeasurements.Rows.Clear();
                    using (OleDbDataReader myReader = myOleDbCommand.ExecuteReader())
                    {
                        while (myReader.Read())
                        {
                            var newRow = new DataGridViewRow()
                                             {
                                                 DefaultCellStyle =
                                                     new DataGridViewCellStyle(grdMeasurements.DefaultCellStyle)
                                             };
                            if (myReader.GetBoolean(7))
                            {
                                newRow.DefaultCellStyle.BackColor = Color.FromArgb(255, 220, 220);
                                newRow.DefaultCellStyle.ForeColor = Color.Black;
                                newRow.DefaultCellStyle.SelectionBackColor = ControlPaint.Dark(grdMeasurements.DefaultCellStyle.SelectionBackColor, 20);
                                newRow.DefaultCellStyle.SelectionForeColor = Color.White;
                            }
                            grdMeasurements.Rows.Add(newRow);
                            grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmIdDb"].Value = myReader[0].ToString();
                            grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmControl"].Value = myReader.GetBoolean(7);
                            grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmDate"].Value =
                                myReader.GetDateTime(1).ToShortDateString();
                            grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmTime"].Value =
                                myReader.GetDateTime(1).ToShortTimeString();
                            int sosudVal = myReader.GetInt16(2);
                            string sosudText;
                            switch (sosudVal)
                            {
                                case 1:
                                    sosudText = "расшир.";
                                    break;
                                case 3:
                                    sosudText = "сжаты";
                                    break;
                                default:
                                    sosudText = "средние";
                                    break;
                            }
                            grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmSosud"].Value = sosudText;
                            grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmTop"].Value = myReader.GetInt16(3);
                            grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmLower"].Value = myReader.GetInt16(4);
                            grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmPulse"].Value = myReader.GetInt16(5);
                            grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmStress"].Value = myReader.GetInt16(6);
                            grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmSignal"].Value = myReader.GetByte(8);
                            string strMode = "?";
                            switch ((DeviceModes)myReader.GetByte(9))
                            {
                                case DeviceModes.Usb:
                                    strMode = "Провод.(USB)";
                                    break;
                                case DeviceModes.UsbEc:
                                    strMode = "Провод.(ЭК)";
                                    break;
                                case DeviceModes.FtdiOld:
                                    strMode = "Беспрвод.(1)";
                                    break;
                                case DeviceModes.FtdiNew:
                                    strMode = "Сеть";
                                    if (!myReader.IsDBNull(10))
                                        strMode += "(" + myReader[10].ToString() + ")";
                                    break;
                                case DeviceModes.Bluetooth:
                                    strMode = "Bluetooth";
                                    if (!myReader.IsDBNull(10))
                                        strMode += "(" + myReader[10].ToString() + ")";
                                    break;
                            }
                            grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmMode"].Value = strMode;
                            grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmComment"].Value = myReader[11].ToString();

                            if (!myReader.IsDBNull(12))
                                grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmGlobalTop"].Value = (short)myReader[12];
                            if (!myReader.IsDBNull(13))
                                grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmGlobalLower"].Value = (short)myReader[13];
                            if (!myReader.IsDBNull(14))
                                grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmKorotkovTop"].Value = (short)myReader[14];
                            if (!myReader.IsDBNull(15))
                                grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmKorotkovLower"].Value = (short)myReader[15];
                            if (!myReader.IsDBNull(16))
                                grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmXgdTop"].Value = (short)myReader[16];
                            if (!myReader.IsDBNull(17))
                                grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmXgdLower"].Value = (short)myReader[17];
                            if (!myReader.IsDBNull(18))
                                grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmMtpTop"].Value = (short)myReader[18];
                            if (!myReader.IsDBNull(19))
                                grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmMtpLower"].Value = (short)myReader[19];

                            grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmComparison"].Value = false;
                            
                            if ((id != 0) && ((int) myReader[0] == id))
                            {
                                grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Selected = true;
                                grdMeasurements.CurrentCell = grdMeasurements.Rows[grdMeasurements.Rows.Count - 1].Cells["clmNumber"];
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                                        exc.Message + "\n\n" +
                                        "Пожалуйста, обратитесь к администратору.",
                                        "Ошибка",
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                for (int i = 0; i < grdMeasurements.Rows.Count; i++)
                    grdMeasurements.Rows[i].Cells["clmNumber"].Value = grdMeasurements.Rows.Count - i;
                grdMeasurements.Columns["clmNumber"].HeaderCell.SortGlyphDirection = SortOrder.Descending;
            }
        }

        // обновление выбранного замера
        private void UpdateMeasurementInfo()
        {
            ClearPulseData();
            if ((grdMeasurements.SelectedRows.Count > 0) &&
                (grdMeasurements.SelectedRows[0].Cells[0].Value != null))
            {
                _idRowTable = grdMeasurements.SelectedRows[0].Index;
                int[] globalA = null;
                short top = 0, lower = 0;
                try
                {
                    _idMeasurement = int.Parse(grdMeasurements.SelectedRows[0].Cells[0].Value.ToString());
                    using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                    {
                        myOleDbConnection.Open();
                        OleDbCommand myOleDbCommand = new OleDbCommand(
                            "SELECT pulse, a2a1, s1, s2, fssOpor, k1, k2, k, a, b, c, d, " +
                            "       globalA1, globalA2, globalA3, globalA4, globalA5, " +
                            "       topPressure, lowerPressure " +
                            "FROM Measurements " +
                            "WHERE id = " + _idMeasurement.ToString(),
                            myOleDbConnection);
                        using (OleDbDataReader myReader = myOleDbCommand.ExecuteReader())
                        {
                            if (myReader != null && myReader.Read())
                            {
                                CultureInfo ciRu = CultureInfo.CreateSpecificCulture("ru-RU");
                                txtFSS.Text = myReader.GetInt16(0).ToString();
                                txtA2A1.Text = myReader.GetFloat(1).ToString("0.####", ciRu);
                                txtS1.Text = myReader.GetFloat(2).ToString("0.####", ciRu);
                                txtS2.Text = myReader.GetFloat(3).ToString("0.####", ciRu);
                                txtFSSOpor.Text = myReader.GetInt16(4).ToString("0.####", ciRu);
                                txtK1.Text = myReader.GetFloat(5).ToString("0.####", ciRu);
                                txtK2.Text = myReader.GetFloat(6).ToString("0.####", ciRu);
                                txtK.Text = myReader.GetFloat(7).ToString("0.####", ciRu);
                                txtA.Text = myReader.GetFloat(8).ToString("0.####", ciRu);
                                txtB.Text = myReader.GetFloat(9).ToString("0.####", ciRu);
                                txtC.Text = myReader.GetFloat(10).ToString("0.####", ciRu);
                                txtD.Text = myReader.GetFloat(11).ToString("0.####", ciRu);
                                try
                                {
                                    if (!myReader.IsDBNull(12) && !myReader.IsDBNull(13) && !myReader.IsDBNull(14) &&
                                        !myReader.IsDBNull(15) && !myReader.IsDBNull(16))
                                    {
                                        globalA = new int[]
                                                      {
                                                          myReader.GetInt32(12), myReader.GetInt32(13),
                                                          myReader.GetInt32(14),
                                                          myReader.GetInt32(15), myReader.GetInt32(16)
                                                      };
                                        nudA1_A2.Value = globalA[1] - globalA[0];
                                    }
                                    else
                                        throw new Exception("Отсутствуют данные о расстановке реперов.");
                                }
                                catch(Exception)
                                {
                                    globalA = null;
                                    nudA1_A2.Value = nudA1_A2.Minimum;
                                }
                                top = myReader.GetInt16(17);
                                lower = myReader.GetInt16(18);
                            }
                            else
                            {
                                throw new Exception("В базе данных не обнаружен замер с таким идентификатором.");
                            }
                        }
                        btnParamsDB.Enabled = true;
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                        exc.Message + "\n\n" +
                        "Пожалуйста, обратитесь к администратору.",
                        "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                // открываем файл с замером
                string strFileName = ConstGlobals.MeasurementsFolder + _idMeasurement.ToString() + ".dat";
                try
                {
                    string strError = "";
                    _myPulseWave = new PulseWave();
                    if (!_myPulseWave.LoadPulseFromBinFile(strFileName, ref strError))
                        throw new Exception(strError);
                    _myPulseWave.Repers = globalA;
                    ShowAnalyzeData(false);
                    _myPulseWave.CalcScalePress(top, lower, _myPulseWave.PulseWaveField.Max());
                    pnlPulse.Refresh();
                }
                catch (Exception exc)
                {
                    MessageBox.Show("При попытке открытия файла '" + Path.GetFileName(strFileName) +
                                            "' произошла ошибка:\n" + exc.Message + "\n\n" +
                                            "Пожалуйста, обратитесь к администратору.",
                                            "Ошибка",
                                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                _flagUserCanChangeText = true;
                UpdateRepersNumericUpDown();
            }
        }

        private void grdMeasurements_SelectionChanged(object sender, EventArgs e)
        {
            UpdateMeasurementInfo();
        }

        private void UpdateRepersNumericUpDown()
        {
            if ((_myPulseWave != null) && (_myPulseWave.Repers != null))
            {
                int a2_a1 = _myPulseWave.Repers[1] - _myPulseWave.Repers[0];
                if ((a2_a1 >= nudA1_A2.Minimum) && (a2_a1 <= nudA1_A2.Maximum))
                    nudA1_A2.Value = a2_a1;
                else
                    nudA1_A2.Value = nudA1_A2.Minimum;
            }
            else
                nudA1_A2.Value = nudA1_A2.Minimum;
        }

        //*****************************************************************
        //                      Открытие файла                            *
        //*****************************************************************

        private void mnuOpen_Click(object sender, EventArgs e)
        {
            // создание диалога
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Title = "Открытие файла с фрагментом пульсовой волны";
            openFileDialog1.Filter =  "Специальный двоичный файл (*.dat)|*.dat" + "|" +
                "Текстовый файл (*.txt)|*.txt";
            // отображение диалога
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                ClearPulseData();
                grdMeasurements.ClearSelection();
                string strFileName = openFileDialog1.FileName;
                string strError = "";
                _myPulseWave = new PulseWave();
                bool flagOK = false;
                switch (openFileDialog1.FilterIndex)
                {
                    case 1:
                        flagOK = _myPulseWave.LoadPulseFromBinFile(strFileName, ref strError);
                        break;
                    case 2:
                        flagOK = _myPulseWave.LoadPulseFromTextFile(strFileName, ref strError);
                        break;
                }
                if (flagOK)
                    ShowAnalyzeData();
                else
                    MessageBox.Show(this, "При попытке открытия файла '" + Path.GetFileName(strFileName) + "' произошла ошибка:\n" + strError,
                        "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        // очистка данных
        private void ClearPulseData()
        {
            _flagUserCanChangeText = false;
            _flagUserChangedText = false;
            _flagNewRepers = false;
            StopTimerButton();
            mnuDelete.Enabled = false;
            mnuPageSetup.Enabled = false;
            mnuPrintPreview.Enabled = false;
            mnuPrint.Enabled = false;
            mnuVario.Enabled = false;
            mnuGlobal.Enabled = false;
            btnParamsPulse.Enabled = false;
            btnParamsDB.Enabled = false;
            _myPulseWave = null;
            RefreshPanels();
            txtFSS.Text = String.Empty;
            txtA2A1.Text = String.Empty;
            nudA1_A2.Value = nudA1_A2.Minimum;
            txtS1.Text = String.Empty;
            txtS2.Text = String.Empty;
            txtFSSOpor.Text = String.Empty;
            txtK1.Text = String.Empty;
            txtK2.Text = String.Empty;
            txtK.Text = String.Empty;
            txtA.Text = String.Empty;
            txtB.Text = String.Empty;
            txtC.Text = String.Empty;
            txtD.Text = String.Empty;
            lblPulseWaveData.Text = "Записанный фрагмент пульсовой волны:";
        }

        // отображение данных анализа
        private void ShowAnalyzeData(bool showParams = true)
        {
            if (_myPulseWave != null)
            {
                _myPulseWave.AnalyzePulseWave();
                RefreshPanels();
                if (showParams)
                {
                    txtFSS.Text = _myPulseWave.PulseFSS.ToString();
                    txtA2A1.Text = _myPulseWave.PulseA2A1.ToString("0.####", CultureInfo.CreateSpecificCulture("ru-RU"));
                }
                btnParamsPulse.Enabled = true;
                lblPulseWaveData.Text = "Записанный фрагмент пульсовой волны (файл \"" + _myPulseWave.GetFileNameShort() +
                                        "\"):";
                mnuDelete.Enabled = true;
                mnuPageSetup.Enabled = !_flagStatistMode;
                mnuPrintPreview.Enabled = !_flagStatistMode;
                mnuPrint.Enabled = !_flagStatistMode;
                mnuVario.Enabled = true;
                mnuGlobal.Enabled = true;
            }
        }

        // обновление картинок
        private void RefreshPanels()
        {
            pnlPulseData.AutoScrollPosition = new Point(0, 0);
            pnlPulseData.Refresh();
            pnlPulse.AutoScrollPosition = new Point(0, 0);
            pnlPulse.Refresh();
            pnlStress.AutoScrollPosition = new Point(0, 0);
            pnlStress.Refresh();
            RefreshComparisonPanels();
        }

        private void btnParamsPulse_Click(object sender, EventArgs e)
        {
            StopTimerButton();
            _flagNewRepers = false;
            btnParamsPulse.Enabled = false;
            ShowAnalyzeData();
        }

        // удаление замера
        private void mnuDelete_Click(object sender, EventArgs e)
        {
            if ((grdMeasurements.CurrentRow != null) &&
                (grdMeasurements.CurrentRow.Cells["clmIdDb"].Value != null))
            {
                int number = int.Parse(grdMeasurements.CurrentRow.Cells["clmNumber"].Value.ToString());
                DialogResult result = MessageBox.Show("Удалить замер № " + number + " из базы?\n" +
                    "(замер был проведен " +
                    grdMeasurements.CurrentRow.Cells["clmDate"].Value.ToString() + " в " +
                    grdMeasurements.CurrentRow.Cells["clmTime"].Value.ToString() + ")",
                    "Удаление",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    int id = int.Parse(grdMeasurements.CurrentRow.Cells["clmIdDb"].Value.ToString());
                    string strError;
                    if (!DbPulseOperations.DeleteMeasurement(id, out strError))
                        MessageBox.Show("Возникла ошибка при удалении замера:\n" + strError,
                                        "Ошибка",
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //UpdateMainTable();
                    grdMeasurements.Rows.RemoveAt(grdMeasurements.CurrentRow.Index);
                }
            }
            else
                MessageBox.Show("В таблице нет выделенной строки.\n" +
                    "Выполнение операции не возможно.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        //*****************************************************************
        //                 Прорисовка пульсовой волны                     *
        //*****************************************************************

        private void pnlPulseData_Paint(object sender, PaintEventArgs e)
        {
            if (_myPulseWave != null)
            {
                Rectangle rctPulse = new Rectangle(pnlPulseData.HorizontalScroll.Value,
                    pnlPulseData.VerticalScroll.Value,
                    pnlPulseData.ClientSize.Width, pnlPulseData.ClientSize.Height);
                int scroll;
                _myPulseWave.DrawPulseWaveDataAndDerivative(e.Graphics, rctPulse, out scroll, true, _bufferSettings.Scale,
                                                           _bufferSettings.FlagPoints, _bufferSettings.FlagPrintValues,
                                                           _bufferSettings.FlagDerivative);
                pnlPulseData.AutoScrollMinSize = new Size(scroll, 0);
            }
        }

        private void pnlPulseData_Scroll(object sender, ScrollEventArgs e)
        {
            pnlPulseData.Refresh();
        }

        // средняя ПВ
        private void pnlPulse_Paint(object sender, PaintEventArgs e)
        {
            if (_myPulseWave != null)
            {
                Rectangle rctPulse = new Rectangle(pnlPulse.HorizontalScroll.Value,
                    pnlPulse.VerticalScroll.Value,
                    pnlPulse.ClientSize.Width, pnlPulse.ClientSize.Height);
                int scroll;
                _myPulseWave.DrawPulseWave(e.Graphics, rctPulse, new Point(0, 0), out scroll,
                                          true, false, _bufferSettings.Scale, _bufferSettings.FlagPoints,
                                          _bufferSettings.FlagPrintValues, false, true, true);
                pnlPulse.AutoScrollMinSize = new Size(scroll, 0);
            }
        }


        private void pnlPulse_Scroll(object sender, ScrollEventArgs e)
        {
            pnlPulse.Refresh();
        }

        // стресс
        private void pnlStress_Paint(object sender, PaintEventArgs e)
        {
            if (_myPulseWave != null)
            {
                Rectangle rctPulse = new Rectangle(pnlStress.HorizontalScroll.Value,
                    pnlStress.VerticalScroll.Value,
                    pnlStress.ClientSize.Width, pnlStress.ClientSize.Height);
                int scroll;
                _myPulseWave.DrawPulseStress(e.Graphics, rctPulse, new Point(0, 0),  out scroll, false, _bufferSettings.Scale);
                pnlStress.AutoScrollMinSize = new Size(scroll, 0);
            }
        }

        private void pnlStress_Scroll(object sender, ScrollEventArgs e)
        {
            pnlStress.Refresh();
        }

        private void frmBuffer_Resize(object sender, EventArgs e)
        {
            this.Refresh();
        }


        //*****************************************************************
        //                      Общие пункты меню                         *
        //*****************************************************************

        /* ----- о программе ----- */
        private void mnuAbout_Click(object sender, EventArgs e)
        {
            frmAboutBox myFormAbout = new frmAboutBox();
            myFormAbout.ShowDialog();
        }

        // О программе (F1)
        private void frmBuffer_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 112)
            {
                frmAboutBox myFormAbout = new frmAboutBox();
                myFormAbout.ShowDialog();
            }
        }

        /* ----- выход из программы ----- */
        private void mnuExit_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }


        //*****************************************************************
        //                   Сохранение в базу данных                     *
        //*****************************************************************

        int _fss, _fssOpor;
        float _a2a1, _s1, _s2, _a, _b, _c, _d, _k1, _k2, _k;

        private void btnParamsDB_Click(object sender, EventArgs e)
        {
            if (!this.ValidateChildren())
                MessageBox.Show("Ошибка во введенных параметрах!\n\n" +
                                "Для получения дополнительной информации\n" +
                                "наведите курсор мыши на восклицательный(-е) знак(-и).",
                                "Ошибка",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                StopTimerButton();
                _flagUserChangedText = false;
                grdMeasurements.Enabled = false;
                grpParams.Enabled = false;
                if (_myPulseWave != null)
                {
                    byte topPressureResult, lowerPressureResult;
                    int sosudResult = _myPulseWave.GetSosud(_a2a1, _s1, _s2);
                    _myPulseWave.CalculateAD(_a2a1, _fss, _fssOpor, _k1, _k2, _k, _a, _b, _c, _d,
                                            out topPressureResult, out lowerPressureResult);
                    try
                    {
                        using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                        {
                            myOleDbConnection.Open();
                            OleDbCommand myOleDbCommand = new OleDbCommand(
                                "UPDATE Measurements " +
                                "SET sosud = " + sosudResult.ToString() + ", " +
                                "    topPressure = " + topPressureResult.ToString() + ", " +
                                "    lowerPressure = " + lowerPressureResult.ToString() + ", " +
                                "    pulse = " + _fss.ToString() + ", " +
                                "    a2a1 = " + _a2a1.ToString(_myDbCultInfo) + ", " +
                                "    s1 = " + _s1.ToString(_myDbCultInfo) + ", " +
                                "    s2 = " + _s2.ToString(_myDbCultInfo) + ", " +
                                "    fssOpor = " + _fssOpor.ToString() + ", " +
                                "    k1 = " + _k1.ToString(_myDbCultInfo) + ", " +
                                "    k2 = " + _k2.ToString(_myDbCultInfo) + ", " +
                                "    k = " + _k.ToString(_myDbCultInfo) + ", " +
                                "    a = " + _a.ToString(_myDbCultInfo) + ", " +
                                "    b = " + _b.ToString(_myDbCultInfo) + ", " +
                                "    c = " + _c.ToString(_myDbCultInfo) + ", " +
                                "    d = " + _d.ToString(_myDbCultInfo) + ", " +
                                "    globalA1 = " + _myPulseWave.GetReper(1).ToString(_myDbCultInfo) + ", " +
                                "    globalA2 = " + _myPulseWave.GetReper(2).ToString(_myDbCultInfo) + " " +
                                "WHERE id = " + _idMeasurement.ToString(),
                                myOleDbConnection);
                            if (myOleDbCommand.ExecuteNonQuery() <= 0)
                                throw new Exception("Не удалось изменить данные в базе.");
                            //UpdateMainTable(idMeasurement);
                            string sosudText;
                            switch (sosudResult)
                            {
                                case 1:
                                    sosudText = "расшир.";
                                    break;
                                case 3:
                                    sosudText = "сжаты";
                                    break;
                                default:
                                    sosudText = "средние";
                                    break;
                            }
                            grdMeasurements.Rows[_idRowTable].Cells["clmSosud"].Value = sosudText;
                            grdMeasurements.Rows[_idRowTable].Cells["clmTop"].Value = topPressureResult.ToString();
                            grdMeasurements.Rows[_idRowTable].Cells["clmLower"].Value = lowerPressureResult.ToString();
                            grdMeasurements.Rows[_idRowTable].Cells["clmPulse"].Value = _fss.ToString();
                        }
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                                        exc.Message + "\n\n" +
                                        "Пожалуйста, обратитесь к администратору.",
                                        "Ошибка",
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Отсутствуют данные замера.\n" +
                                    "Операция не возможна.",
                                    "Ошибка",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                grdMeasurements.Enabled = true;
                grpParams.Enabled = true;
            }
        }

        private void txtFSS_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!int.TryParse(txtFSS.Text, out _fss))
            {
                errorProvider1.SetError(txtFSS, "Число введено не верно.");
                e.Cancel = true;
            }
            else
                errorProvider1.SetError(txtFSS, String.Empty);
        }

        private void txtA2A1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!float.TryParse(txtA2A1.Text, out _a2a1))
            {
                errorProvider1.SetError(txtA2A1, "Число введено не верно.");
                e.Cancel = true;
            }
            else
                errorProvider1.SetError(txtA2A1, String.Empty);
        }

        private void txtS1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!float.TryParse(txtS1.Text, out _s1))
            {
                errorProvider1.SetError(txtS1, "Число введено не верно.");
                e.Cancel = true;
            }
            else
                errorProvider1.SetError(txtS1, String.Empty);
        }

        private void txtS2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!float.TryParse(txtS2.Text, out _s2))
            {
                errorProvider1.SetError(txtS2, "Число введено не верно.");
                e.Cancel = true;
            }
            else
                errorProvider1.SetError(txtS2, String.Empty);
        }

        private void txtFSSOpor_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!int.TryParse(txtFSSOpor.Text, out _fssOpor))
            {
                errorProvider1.SetError(txtFSSOpor, "Число введено не верно.");
                e.Cancel = true;
            }
            else
                errorProvider1.SetError(txtFSSOpor, String.Empty);
        }

        private void txtK1_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!float.TryParse(txtK1.Text, out _k1))
            {
                errorProvider1.SetError(txtK1, "Число введено не верно.");
                e.Cancel = true;
            }
            else
                errorProvider1.SetError(txtK1, String.Empty);
        }

        private void txtK2_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!float.TryParse(txtK2.Text, out _k2))
            {
                errorProvider1.SetError(txtK2, "Число введено не верно.");
                e.Cancel = true;
            }
            else
                errorProvider1.SetError(txtK2, String.Empty);
        }

        private void txtK_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!float.TryParse(txtK.Text, out _k))
            {
                errorProvider1.SetError(txtK, "Число введено не верно.");
                e.Cancel = true;
            }
            else
                errorProvider1.SetError(txtK, String.Empty);
        }

        private void txtA_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!float.TryParse(txtA.Text, out _a))
            {
                errorProvider1.SetError(txtA, "Число введено не верно.");
                e.Cancel = true;
            }
            else
                errorProvider1.SetError(txtA, String.Empty);
        }

        private void txtB_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!float.TryParse(txtB.Text, out _b))
            {
                errorProvider1.SetError(txtB, "Число введено не верно.");
                e.Cancel = true;
            }
            else
                errorProvider1.SetError(txtB, String.Empty);
        }

        private void txtC_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!float.TryParse(txtC.Text, out _c))
            {
                errorProvider1.SetError(txtC, "Число введено не верно.");
                e.Cancel = true;
            }
            else
                errorProvider1.SetError(txtC, String.Empty);
        }

        private void txtD_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!float.TryParse(txtD.Text, out _d))
            {
                errorProvider1.SetError(txtD, "Число введено не верно.");
                e.Cancel = true;
            }
            else
                errorProvider1.SetError(txtD, String.Empty);
        }


        //*****************************************************************
        //               Редактирование комментария в таблице             *
        //*****************************************************************

        private int _gridEditResult;
        private bool _flagGridEditResultNotNull;

        private readonly ArrayList _lstGridIntValues = new ArrayList()
                                                           {
                                                               "clmKorotkovTop",
                                                               "clmKorotkovLower",
                                                               "clmXgdTop",
                                                               "clmXgdLower",
                                                               "clmMtpTop",
                                                               "clmMtpLower"
                                                           };

        private void grdMeasurements_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (grdMeasurements.IsCurrentCellDirty)
                grdMeasurements.CommitEdit(DataGridViewDataErrorContexts.Commit);

            if ((grdMeasurements.Columns[e.ColumnIndex].Name == "clmComment") &&
                (grdMeasurements["clmComment", e.RowIndex].Value != null))
            {
                if (!grdMeasurements["clmComment", e.RowIndex].Value.ToString().Contains("'"))
                {
                    grdMeasurements["clmComment", e.RowIndex].ErrorText = string.Empty;
                    return;
                }
                grdMeasurements.EndEdit();
                grdMeasurements["clmComment", e.RowIndex].ErrorText = "В комментарии запрещено использовать символ кавычки '.";
                e.Cancel = true;
                MessageBox.Show("Ошибка ввода данных в таблицу!\n\n" +
                                "Для получения дополнительной информации\n" +
                                "наведите курсор мыши на восклицательный(-е) знак(-и).",
                                "Ошибка",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (_lstGridIntValues.Contains(grdMeasurements.Columns[e.ColumnIndex].Name))
            {
                string strText = (grdMeasurements[e.ColumnIndex, e.RowIndex].Value != null
                                      ? grdMeasurements[e.ColumnIndex, e.RowIndex].Value.ToString().Trim()
                                      : String.Empty);
                if (strText.Length > 0)
                {
                    if (int.TryParse(strText, out _gridEditResult))
                    {
                        _flagGridEditResultNotNull = true;
                        grdMeasurements[e.ColumnIndex, e.RowIndex].ErrorText = string.Empty;
                        return;
                    }
                    grdMeasurements.EndEdit();
                    grdMeasurements[e.ColumnIndex, e.RowIndex].ErrorText = "Не верно введено число.";
                    e.Cancel = true;
                    MessageBox.Show("Ошибка ввода данных в таблицу!\n\n" +
                                    "Для получения дополнительной информации\n" +
                                    "наведите курсор мыши на восклицательный(-е) знак(-и).",
                                    "Ошибка",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    _flagGridEditResultNotNull = false;
                    grdMeasurements[e.ColumnIndex, e.RowIndex].ErrorText = string.Empty;
                    return;
                }
            }
        }

        private void grdMeasurements_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            if (grdMeasurements.Columns[e.ColumnIndex].Name == "clmComment" ||
                _lstGridIntValues.Contains(grdMeasurements.Columns[e.ColumnIndex].Name))
            {
                try
                {
                    int idRecord = int.Parse(grdMeasurements.Rows[e.RowIndex].Cells[0].Value.ToString());
                    
                    using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                    {
                        myOleDbConnection.Open();
                        OleDbCommand myOleDbCommand = new OleDbCommand();
                        myOleDbCommand.Connection = myOleDbConnection;
                        myOleDbCommand.CommandText = "UPDATE Measurements ";
                        if (grdMeasurements.Columns[e.ColumnIndex].Name == "clmComment")
                        {
                            string valRecord = ((grdMeasurements[e.ColumnIndex, e.RowIndex].Value != null)
                                            ? grdMeasurements[e.ColumnIndex, e.RowIndex].Value.ToString().Trim()
                                            : string.Empty);
                            myOleDbCommand.CommandText += "SET comment = '" + valRecord + "' ";
                        }
                        else
                        {
                            string strDbColumnName = grdMeasurements.Columns[e.ColumnIndex].Name.ToLower()[3] +
                                                     grdMeasurements.Columns[e.ColumnIndex].Name.Substring(4);
                            myOleDbCommand.CommandText += "SET " + strDbColumnName + " = " +
                                                          (_flagGridEditResultNotNull ? _gridEditResult.ToString() : "null") + " ";
                        }
                        myOleDbCommand.CommandText += "WHERE id = " + idRecord;
                        if (myOleDbCommand.ExecuteNonQuery() <= 0)
                            throw new Exception("Не удалось изменить данные в базе.");
                    }
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                                    exc.Message + "\n\n" +
                                    "Пожалуйста, обратитесь к администратору.",
                                    "Ошибка",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void grdMeasurements_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (grdMeasurements.Rows.Count > 0 &&
                grdMeasurements.Rows[e.RowIndex].Cells["clmNumber"].Value != null &&
                grdMeasurements.Columns[e.ColumnIndex].Name == "clmComparison")
            {
                try
                {
                    int idRecord = int.Parse(grdMeasurements.Rows[e.RowIndex].Cells["clmNumber"].Value.ToString());
                    if ((bool)grdMeasurements[e.ColumnIndex, e.RowIndex].Value)
                    {
                        if (_pulseWaveComparator.GetCount() < 3)
                        {
                            Color newColor = _pulseWaveComparator.AddWave(_myPulseWave.PulseWaveField, idRecord);
                            grdMeasurements.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = newColor;
                            grdMeasurements.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.SelectionBackColor = newColor;
                            RefreshComparisonPanels();
                        }
                        else
                        {
                            MessageBox.Show("Подвергать реконструкции можно не более 3-х пульсовых волн!",
                                            "Внимание",
                                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            grdMeasurements[e.ColumnIndex, e.RowIndex].Value = false;
                        }
                    }
                    else
                    {
                        _pulseWaveComparator.DeleteWave(idRecord);
                        grdMeasurements.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = grdMeasurements.Rows[e.RowIndex].DefaultCellStyle.BackColor;
                        grdMeasurements.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.SelectionBackColor = grdMeasurements.Rows[e.RowIndex].DefaultCellStyle.SelectionBackColor;
                        RefreshComparisonPanels();
                    }
                    btnClearComparator.Enabled = (_pulseWaveComparator.GetCount() > 0);
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Возникла ошибка при попытке реконструкции пульсовых волн:\n" +
                                    exc.Message,
                                    "Ошибка",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void frmBuffer_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (grdMeasurements.IsCurrentCellDirty)
            {
                grdMeasurements.EndEdit();
                this.Validate();
            }
            // Сохранение параметров
            _bufferSettings.Save();
        }


        //*****************************************************************
        //                            Параметры                           *
        //*****************************************************************

        private BufferSettings _bufferSettings = new BufferSettings();

        private void mnuProperties_Click(object sender, EventArgs e)
        {
            frmPropBuffer MyFormProperties = new frmPropBuffer(_bufferSettings);
            MyFormProperties.ShowDialog();
            if (MyFormProperties.DialogResult == DialogResult.OK)
            {
                _bufferSettings.Scale = MyFormProperties.returnScale();
                _bufferSettings.FlagPoints = MyFormProperties.returnPoints();
                _bufferSettings.FlagPrintValues = MyFormProperties.returnPrintValues();
                _bufferSettings.FlagDerivative = MyFormProperties.returnDerivative();
                RefreshPanels();
            }
        }


        //*****************************************************************
        //                            Сравнение                           *
        //*****************************************************************

        PulseWaveComparator _pulseWaveComparator = new PulseWaveComparator();

        private void grdMeasurements_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (grdMeasurements.Columns[e.ColumnIndex].Name == "clmComparison")
            {
                grdMeasurements.EndEdit();
            }
        }

        private void RefreshComparisonPanels()
        {
            pnlComparison.AutoScrollPosition = new Point(0, 0);
            pnlComparison.Refresh();
            pnlComparisonOneHeight.AutoScrollPosition = new Point(0, 0);
            pnlComparisonOneHeight.Refresh();
        }

        private void pnlComparison_Paint(object sender, PaintEventArgs e)
        {
            if (_pulseWaveComparator != null && _pulseWaveComparator.GetCount() > 0)
            {
                Rectangle rctComparison = new Rectangle(pnlComparison.HorizontalScroll.Value,
                    pnlComparison.VerticalScroll.Value,
                    pnlComparison.ClientSize.Width, pnlComparison.ClientSize.Height);
                int scroll,
                    curId = (grdMeasurements.SelectedRows.Count > 0
                                 ? int.Parse(grdMeasurements.SelectedRows[0].Cells["clmNumber"].Value.ToString())
                                 : -1);
                _pulseWaveComparator.DrawPulseWaves(e.Graphics, rctComparison, new Point(0, 0), out scroll, curId,
                                                    _bufferSettings.Scale, _bufferSettings.FlagPoints,
                                                    _bufferSettings.FlagPrintValues);
                pnlComparison.AutoScrollMinSize = new Size(scroll, 0);
            }
        }

        private void pnlComparison_Scroll(object sender, ScrollEventArgs e)
        {
            pnlComparison.Refresh();
        }

        private void pnlComparisonOneHeight_Paint(object sender, PaintEventArgs e)
        {
            if (_pulseWaveComparator != null && _pulseWaveComparator.GetCount() > 0)
            {
                Rectangle rctComparison = new Rectangle(pnlComparisonOneHeight.HorizontalScroll.Value,
                    pnlComparisonOneHeight.VerticalScroll.Value,
                    pnlComparisonOneHeight.ClientSize.Width, pnlComparisonOneHeight.ClientSize.Height);
                int scroll,
                    curId = (grdMeasurements.SelectedRows.Count > 0
                                 ? int.Parse(grdMeasurements.SelectedRows[0].Cells["clmNumber"].Value.ToString())
                                 : -1);
                _pulseWaveComparator.DrawPulseWaves(e.Graphics, rctComparison, new Point(0, 0), out scroll, curId,
                                                    _bufferSettings.Scale, _bufferSettings.FlagPoints,
                                                    _bufferSettings.FlagPrintValues, true);
                pnlComparisonOneHeight.AutoScrollMinSize = new Size(scroll, 0);
            }
        }

        private void pnlComparisonOneHeight_Scroll(object sender, ScrollEventArgs e)
        {
            pnlComparisonOneHeight.Refresh();
        }

        private void btnClearComparator_Click(object sender, EventArgs e)
        {
            if (!_flagStatistMode)
            {
                btnClearComparator.Enabled = false;
                _pulseWaveComparator.ClearList();
                for (int i = 0; i < grdMeasurements.Rows.Count; i++)
                {
                    grdMeasurements.Rows[i].Cells["clmComparison"].Value = false;
                    grdMeasurements.Rows[i].Cells["clmComparison"].Style.BackColor =
                        grdMeasurements.Rows[i].DefaultCellStyle.BackColor;
                    grdMeasurements.Rows[i].Cells["clmComparison"].Style.SelectionBackColor =
                        grdMeasurements.Rows[i].DefaultCellStyle.SelectionBackColor;
                }
                grdMeasurements.RefreshEdit();
                RefreshComparisonPanels();
                pnlComparison.AutoScrollMinSize = new Size(0, 0);
                pnlComparisonOneHeight.AutoScrollMinSize = new Size(0, 0);
            }
            else
            {
                StopTimerButton();
                int[] rowsDb = new int[grdMeasurements.SelectedRows.Count],
                      rowsTable = new int[grdMeasurements.SelectedRows.Count],
                      statist = new int[] { lstStatist1.SelectedIndex, lstStatist2.SelectedIndex };
                int rowCount = 0;
                for (int i = 0; i < grdMeasurements.Rows.Count; i++)
                    if (grdMeasurements.Rows[i].Selected)
                    {
                        rowsDb[rowCount] = Convert.ToInt32(grdMeasurements["clmIdDb", i].Value);
                        rowsTable[rowCount] = Convert.ToInt32(grdMeasurements["clmNumber", i].Value);
                        rowCount++;
                    }
                frmStatist MyFrmStatist = new frmStatist(rowsDb, rowsTable, statist, _idPatient);
                MyFrmStatist.ShowDialog();
            }
        }


        private void frmBuffer_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.KeyCode == Keys.Delete)
            //{
            //    if (_pulseWaveComparator != null && _pulseWaveComparator.GetCount() > 0)
            //    {
            //        DialogResult result = MessageBox.Show("Очистить список сравниваемых волн?",
            //                                              "Очистка",
            //                                              MessageBoxButtons.OKCancel, MessageBoxIcon.Warning);
            //        if (result == DialogResult.OK)
            //            CLearComparator();
            //    }
            //    else
            //    {
            //        MessageBox.Show("Не возможно очистить список сравниваемых волн,\n" +
            //                        "т.к. этот список пуст.",
            //                        "Ошибка",
            //                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }
            //}
        }


        //*****************************************************************
        //                      Печать на принтер                         *
        //*****************************************************************

        private struct PrintColumn
        {
            internal float Width;
            internal int IdColumn;
            internal string NameColumn;

            internal PrintColumn(float w, int id, string n)
            {
                Width = w;
                IdColumn = id;
                NameColumn = n;
            }
        };

        private void printDocument1_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            // Проверяем наличие данных
            if (_myPulseWave == null)
            {
                e.Cancel = true;
                return;
            }

            // Cоздаем экземпляр graph класса Graphics:
            Graphics graph = e.Graphics;
            graph.PageUnit = GraphicsUnit.Display;
            graph.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;

            // Определяем печатную область страницы:
            float leftMargin = e.MarginBounds.Left,
                  rightMargin = e.MarginBounds.Right,
                  topMargin = e.MarginBounds.Top,
                  bottomMargin = e.MarginBounds.Bottom,
                  printableWidth = e.MarginBounds.Width,
                  printableHeight = e.MarginBounds.Height;

            // Определим используемые шрифты и кисти
            Font fontHeader1 = new Font("Microsoft Sans Serif", 16, FontStyle.Bold),
                 fontTableHeader = new Font("Microsoft Sans Serif", 8, FontStyle.Bold),
                 fontTable = new Font("Microsoft Sans Serif", 8),
                 fontHeader2 = new Font("Microsoft Sans Serif", 10, FontStyle.Bold),
                 fontHeader3 = new Font("Microsoft Sans Serif", 10, FontStyle.Italic),
                 fontText = new Font("Microsoft Sans Serif", 10);
            SolidBrush brushText = new SolidBrush(Color.Black);

            // Координаты курсора
            float curX = leftMargin,
                  curY = topMargin;

            // Выводим заголовок
            float separator = graph.MeasureString(_strPatient, fontHeader1).Height / 2;
            curY += PrintString(graph, _strPatient, fontHeader1, brushText,
                                new RectangleF(curX, curY, printableWidth,
                                               fontHeader1.GetHeight(graph)),
                                true).Height + separator;

            // Общие параметры
            float sectionWidth = (_pulseWaveComparator != null && _pulseWaveComparator.GetCount() > 0
                                      ? (printableWidth - separator*2)/3
                                      : (printableWidth - separator)/2);

            // Таблица замеров
            const float tablePadding = 5;
            float tableWidth = printableWidth,
                  tableHeight = Math.Max(bottomMargin - curY - sectionWidth * 2 + separator * 2 + fontHeader2.GetHeight(graph) * 2, (bottomMargin - curY)/2),
                  headerHeight = fontTableHeader.GetHeight(graph) + tablePadding * 2,
                  rowHeight = fontTable.GetHeight(graph) + tablePadding * 2;
            bool[] flagsRowPrint = new bool[grdMeasurements.Rows.Count];
            int countRows = 0,
                countRowsMax = (int)((tableHeight - headerHeight) / rowHeight);
            for (int i = 0; i < grdMeasurements.Rows.Count; i++)
            {
                if (i == 0 || i == grdMeasurements.Rows.Count - 1 || grdMeasurements.Rows[i].Selected ||
                    (bool)grdMeasurements["clmComparison", i].Value)
                {
                    flagsRowPrint[i] = true;
                    countRows++;
                }
                else
                    flagsRowPrint[i] = false;
            }
            bool flagAddDir = true;
            while (countRows + GetTableProbels(flagsRowPrint) < countRowsMax &&
                countRows < grdMeasurements.Rows.Count)
            {
                int i = (grdMeasurements.SelectedRows.Count > 0
                             ? grdMeasurements.SelectedRows[0].Index
                             : grdMeasurements.Rows.Count/2);
                while (i >= 0 && i < grdMeasurements.Rows.Count)
                {
                    if (!flagsRowPrint[i])
                    {
                        flagsRowPrint[i] = true;
                        countRows++;
                        break;
                    }
                    i = (flagAddDir ? i - 1 : i + 1);
                }
                flagAddDir = !flagAddDir;
            }
            // Ширина столбцов
            ArrayList printColumns = new ArrayList();
            float widthAllColumns = 0;
            for (int i = 0; i < grdMeasurements.Columns.Count; i++)
                if (grdMeasurements.Columns[i].Visible)
                {
                    if (grdMeasurements.Columns[i].Name != "clmComparison" ||
                        (_pulseWaveComparator != null && _pulseWaveComparator.GetCount() > 0))
                    {
                        PrintColumn newColumn =
                            new PrintColumn(
                                graph.MeasureString(grdMeasurements.Columns[i].HeaderText, fontTableHeader).Width +
                                tablePadding * 2, i, grdMeasurements.Columns[i].Name);
                        if (newColumn.NameColumn != "clmComparison" && newColumn.NameColumn != "clmComment")
                        {
                            for (int j = 0; j < grdMeasurements.Rows.Count; j++)
                            {
                                float newColumnWidth =
                                    graph.MeasureString(grdMeasurements[i, j].Value.ToString(), fontTable).Width +
                                    tablePadding * 2;
                                if (flagsRowPrint[j] && newColumnWidth > newColumn.Width)
                                    newColumn.Width = newColumnWidth;
                            }
                        }
                        if (grdMeasurements.Columns[i].Name != "clmComment")
                            widthAllColumns += newColumn.Width;
                        printColumns.Add(newColumn);
                    }
                }
            for (int i = 0; i < printColumns.Count; i++ )
            {
                PrintColumn curPrintColumn = (PrintColumn)printColumns[i];
                if (curPrintColumn.NameColumn == "clmComment")
                {
                    curPrintColumn.Width = tableWidth - widthAllColumns;
                    printColumns[i] = curPrintColumn;
                    break;
                }
            }
            // Печать заголовка
            SolidBrush brushFillHeader = new SolidBrush(Color.LightGray);
            StringFormat formatHeader = new StringFormat { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center };
            foreach (var printColumn in printColumns)
            {
                PrintColumn curPrintColumn = (PrintColumn) printColumn;
                graph.FillRectangle(brushFillHeader, curX, curY, curPrintColumn.Width, headerHeight);
                graph.DrawRectangle(new Pen(brushText), curX, curY, curPrintColumn.Width, headerHeight);
                graph.DrawString(grdMeasurements.Columns[curPrintColumn.IdColumn].HeaderText, fontTableHeader, brushText,
                    new RectangleF(curX, curY, curPrintColumn.Width, headerHeight), formatHeader);
                //PrintString(graph, grdMeasurements.Columns[curPrintColumn.IdColumn].HeaderText, fontTableHeader, brushText,
                //                    new RectangleF(curX + tablePadding, curY + tablePadding, curPrintColumn.Width, rowHeight));
                curX += curPrintColumn.Width;
            }
            curY += headerHeight;
            // Печать выбранных строк таблицы
            PointF selectedRowPoint = new PointF(0, 0);
            curX = leftMargin;
            int iRow = 0, selectedId = -1;
            while (iRow < flagsRowPrint.Length)
            {
                // пробел
                if (iRow > 0 && flagsRowPrint[iRow - 1] && !flagsRowPrint[iRow])
                {
                    graph.DrawRectangle(new Pen(brushText), curX, curY, tableWidth, rowHeight);
                    PrintString(graph, ". . .", fontTable, brushText,
                                new RectangleF(curX, curY, tableWidth, rowHeight),
                                true);
                    while (iRow < flagsRowPrint.Length && !flagsRowPrint[iRow])
                        iRow++;
                }
                else
                {
                    Font fontTableRow = fontTable;
                    if (grdMeasurements.Rows[iRow].Selected)
                    {
                        selectedRowPoint = new PointF(curX, curY);
                        selectedId = int.Parse(grdMeasurements.Rows[iRow].Cells["clmNumber"].Value.ToString());
                        fontTableRow = new Font(fontTableRow, FontStyle.Bold);
                    }
                    for (int i = 0; i < printColumns.Count; i++)
                    {
                        PrintColumn curPrintColumn = (PrintColumn)printColumns[i];
                        SolidBrush brushFill, brushTextCell;
                        if (grdMeasurements.Rows[iRow].Selected)
                        {
                            brushFill = new SolidBrush(grdMeasurements.Rows[iRow].DefaultCellStyle.SelectionBackColor);
                            brushTextCell = new SolidBrush(grdMeasurements.Rows[iRow].DefaultCellStyle.SelectionForeColor);
                        }
                        else
                        {
                            brushFill = new SolidBrush(grdMeasurements.Rows[iRow].DefaultCellStyle.BackColor);
                            brushTextCell = new SolidBrush(grdMeasurements.Rows[iRow].DefaultCellStyle.ForeColor);
                        }
                        string strVal = grdMeasurements[curPrintColumn.IdColumn, iRow].Value != null
                                            ? grdMeasurements[curPrintColumn.IdColumn, iRow].Value.ToString()
                                            : String.Empty;
                        graph.FillRectangle(brushFill, curX, curY, curPrintColumn.Width, rowHeight);
                        if (curPrintColumn.NameColumn == "clmComparison")
                        {
                            strVal = string.Empty;
                            if ((bool)grdMeasurements["clmComparison", iRow].Value)
                            {
                                brushFill = new SolidBrush(grdMeasurements.Rows[iRow].Cells[curPrintColumn.IdColumn].Style.BackColor);
                                graph.FillRectangle(brushFill, curX, curY, curPrintColumn.Width, rowHeight);
                                Rectangle rctChecked = new Rectangle((int) (curX + 3),
                                                                     (int) (curY + 3),
                                                                     (int) (rowHeight - 4),
                                                                     (int) (rowHeight - 4));
                                graph.FillRectangle(new SolidBrush(Color.White), rctChecked);
                                graph.DrawRectangle(new Pen(brushText), rctChecked);
                                graph.DrawLines(new Pen(brushText, 2),
                                                new PointF[]
                                                    {
                                                        new Point(rctChecked.X + 2, rctChecked.Y + rctChecked.Height/2),
                                                        new Point(rctChecked.X + rctChecked.Width/2,
                                                                  rctChecked.Bottom - 3),
                                                        new Point(rctChecked.Right - 1, rctChecked.Y + 1)
                                                    });
                            }
                        }
                        graph.DrawRectangle(new Pen(brushText), curX, curY, curPrintColumn.Width, rowHeight);
                        PrintString(graph, strVal, fontTableRow, brushTextCell,
                                    new RectangleF(curX + tablePadding, curY + tablePadding, curPrintColumn.Width, rowHeight));
                        curX += curPrintColumn.Width;
                    }
                    curX = leftMargin;
                    iRow++;
                }
                curY += rowHeight;
            }
            curY += separator;
            if (selectedRowPoint.Y > 0)
                graph.DrawRectangle(new Pen(brushText, 3), selectedRowPoint.X, selectedRowPoint.Y, tableWidth, rowHeight);
            
            // Начинаем печатать область под таблицей
            curY = Math.Max(curY, bottomMargin - sectionWidth * 2 + separator + fontHeader2.GetHeight(graph) * 2);
            float sectionBegin = curY;

            // Картинки слева
            curY += PrintString(graph, "Средняя ПВ:", fontHeader2, brushText,
                                new RectangleF(curX, curY, printableWidth, printableHeight)).Height;
            Rectangle rctPic = new Rectangle(0, 0, (int) sectionWidth,
                                             (int) ((bottomMargin - curY - separator - fontHeader2.GetHeight(graph))/2));
            int scroll;
            _myPulseWave.DrawPulseWave(graph, rctPic, new Point((int)curX, (int)curY), out scroll,
                                      true, false, _bufferSettings.Scale, _bufferSettings.FlagPoints,
                                      _bufferSettings.FlagPrintValues, true);
            graph.DrawRectangle(new Pen(brushText), new Rectangle((int) curX, (int) curY, rctPic.Width, rctPic.Height));
            curY += rctPic.Height + separator;
            curY += PrintString(graph, "Стресс:", fontHeader2, brushText,
                                new RectangleF(curX, curY, printableWidth, printableHeight)).Height;
            rctPic.Height -= 2;
            _myPulseWave.DrawPulseStress(graph, rctPic, new Point((int)curX, (int)curY + 1), out scroll, false, _bufferSettings.Scale);
            rctPic.Height += 2;
            graph.DrawRectangle(new Pen(brushText), new Rectangle((int)curX, (int)curY, rctPic.Width, rctPic.Height));

            // Параметры
            curX += sectionWidth + separator;
            curY = sectionBegin;
            curY += PrintString(graph, "Параметры замера:", fontHeader2, brushText,
                                new RectangleF(curX, curY, printableWidth, printableHeight)).Height;
            float textHeaderHeight = fontHeader3.GetHeight(graph) * 1.5f,
                  textHeaderSeparator = textHeaderHeight/3,
                  textHeight = (bottomMargin - curY - textHeaderHeight * 3 - textHeaderSeparator * 2) / 16,
                  otstup = sectionWidth / 10, otstupVal = sectionWidth / 2;
            //curY += textHeaderSeparator;
            PrintString(graph, "Основные:", fontHeader3, brushText,
                        new RectangleF(curX, curY, printableWidth, printableHeight));
            curY += textHeaderHeight;
            PrintString(graph, "ЧСС:", fontText, brushText,
                        new RectangleF(curX + otstup, curY, printableWidth, printableHeight));
            PrintString(graph, txtFSS.Text + " уд/мин", fontText, brushText,
                        new RectangleF(curX + otstupVal, curY, printableWidth, printableHeight));
            curY += textHeight;
            PrintString(graph, "А2 / А1:", fontText, brushText,
                        new RectangleF(curX + otstup, curY, printableWidth, printableHeight));
            PrintString(graph, txtA2A1.Text, fontText, brushText,
                        new RectangleF(curX + otstupVal, curY, printableWidth, printableHeight));
            curY += textHeight;
            PrintString(graph, "А2 - А1:", fontText, brushText,
                        new RectangleF(curX + otstup, curY, printableWidth, printableHeight));
            PrintString(graph, nudA1_A2.Value.ToString(), fontText, brushText,
                        new RectangleF(curX + otstupVal, curY, printableWidth, printableHeight));
            curY += textHeight + textHeaderSeparator;
            PrintString(graph, "Сосуды:", fontHeader3, brushText,
                        new RectangleF(curX, curY, printableWidth, printableHeight));
            curY += textHeaderHeight;
            PrintString(graph, "расшир.", fontText, brushText,
                        new RectangleF(curX + otstup, curY, printableWidth, printableHeight));
            curY += textHeight;
            PrintString(graph, txtS1.Text, fontText, brushText,
                        new RectangleF(curX + otstupVal, curY, printableWidth, printableHeight));
            curY += textHeight;
            PrintString(graph, "средние", fontText, brushText,
                        new RectangleF(curX + otstup, curY, printableWidth, printableHeight));
            curY += textHeight;
            PrintString(graph, txtS2.Text, fontText, brushText,
                        new RectangleF(curX + otstupVal, curY, printableWidth, printableHeight));
            curY += textHeight;
            PrintString(graph, "сжаты", fontText, brushText,
                        new RectangleF(curX + otstup, curY, printableWidth, printableHeight));
            curY += textHeight + textHeaderSeparator;
            PrintString(graph, "Другие:", fontHeader3, brushText,
                        new RectangleF(curX, curY, printableWidth, printableHeight));
            curY += textHeaderHeight;
            PrintString(graph, "ЧСС опор.:", fontText, brushText,
                        new RectangleF(curX + otstup, curY, printableWidth, printableHeight));
            PrintString(graph, txtFSSOpor.Text + " уд/мин", fontText, brushText,
                        new RectangleF(curX + otstupVal, curY, printableWidth, printableHeight));
            curY += textHeight;
            PrintString(graph, "k1:", fontText, brushText,
                        new RectangleF(curX + otstup, curY, printableWidth, printableHeight));
            PrintString(graph, txtK1.Text, fontText, brushText,
                        new RectangleF(curX + otstupVal, curY, printableWidth, printableHeight));
            curY += textHeight;
            PrintString(graph, "k2:", fontText, brushText,
                        new RectangleF(curX + otstup, curY, printableWidth, printableHeight));
            PrintString(graph, txtK2.Text, fontText, brushText,
                        new RectangleF(curX + otstupVal, curY, printableWidth, printableHeight));
            curY += textHeight;
            PrintString(graph, "k:", fontText, brushText,
                        new RectangleF(curX + otstup, curY, printableWidth, printableHeight));
            PrintString(graph, txtK.Text, fontText, brushText,
                        new RectangleF(curX + otstupVal, curY, printableWidth, printableHeight));
            curY += textHeight;
            PrintString(graph, "a:", fontText, brushText,
                        new RectangleF(curX + otstup, curY, printableWidth, printableHeight));
            PrintString(graph, txtA.Text, fontText, brushText,
                        new RectangleF(curX + otstupVal, curY, printableWidth, printableHeight));
            curY += textHeight;
            PrintString(graph, "b:", fontText, brushText,
                        new RectangleF(curX + otstup, curY, printableWidth, printableHeight));
            PrintString(graph, txtB.Text, fontText, brushText,
                        new RectangleF(curX + otstupVal, curY, printableWidth, printableHeight));
            curY += textHeight;
            PrintString(graph, "c:", fontText, brushText,
                        new RectangleF(curX + otstup, curY, printableWidth, printableHeight));
            PrintString(graph, txtC.Text, fontText, brushText,
                        new RectangleF(curX + otstupVal, curY, printableWidth, printableHeight));
            curY += textHeight;
            PrintString(graph, "d:", fontText, brushText,
                        new RectangleF(curX + otstup, curY, printableWidth, printableHeight));
            PrintString(graph, txtD.Text, fontText, brushText,
                        new RectangleF(curX + otstupVal, curY, printableWidth, printableHeight));

            // Картинка справа
            if (_pulseWaveComparator != null && _pulseWaveComparator.GetCount() > 0)
            {
                curX += sectionWidth + separator;
                curY = sectionBegin;
                curY += PrintString(graph, "Реконструкция ПВ:", fontHeader2, brushText,
                                    new RectangleF(curX, curY, printableWidth, printableHeight)).Height;
                rctPic = new Rectangle(0, 0, (int)sectionWidth,
                                             (int)(bottomMargin - curY));
                _pulseWaveComparator.DrawPulseWaves(graph, rctPic, new Point((int) curX, (int) curY), out scroll,
                                                    selectedId,
                                                    _bufferSettings.Scale, _bufferSettings.FlagPoints,
                                                    _bufferSettings.FlagPrintValues, true);
                graph.DrawRectangle(new Pen(brushText), new Rectangle((int)curX, (int)curY, rctPic.Width, rctPic.Height));
            }
        }

        private int GetTableProbels(bool[] flags)
        {
            int result = 0;
            for (int i = 1; i < flags.Length; i++)
                if (flags[i - 1] && !flags[i])
                    result++;
            if (flags.Length > 0 && !flags[flags.Length - 1] && result > 0)
                result--;
            return result;
        }

        private SizeF PrintString(Graphics graph, string str, Font font, Brush brush, RectangleF rct,
            bool center = false)
        {
            SizeF sizeStr = graph.MeasureString(str, font);
            while (sizeStr.Width > rct.Width && str.LastIndexOf(' ') > -1)
            {
                str = str.Remove(str.LastIndexOf(' ')) + "...";
                sizeStr = graph.MeasureString(str, font);
            }
            graph.DrawString(str, font, brush,
                             (center ? rct.X + rct.Width/2 - sizeStr.Width/2 : rct.X),
                             (center ? rct.Y + rct.Height / 2 - sizeStr.Height / 2 : rct.Y));
            return sizeStr;
        }

        private void mnuPageSetup_Click(object sender, EventArgs e)
        {
            PageSetupDialog diag = new PageSetupDialog();
            diag.Document = printDocument1;
            diag.EnableMetric = true;
            diag.ShowDialog();
        }

        private void mnuPrintPreview_Click(object sender, EventArgs e)
        {
            PrintPreviewDialog diag = new PrintPreviewDialog();
            diag.Document = printDocument1;
            diag.Icon = Properties.Resources.yagodka;
            diag.ShowDialog();
        }

        private void mnuPrint_Click(object sender, EventArgs e)
        {
            PrintDialog diag = new PrintDialog();
            diag.Document = printDocument1;
            diag.AllowSelection = false;
            diag.AllowSomePages = false;
            if (diag.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    printDocument1.Print();
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Возникла ошибка при попытке печати:\n" +
                                    exc.Message,
                                    "Ошибка",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }


        //*****************************************************************
        //                         Вариабельность                         *
        //*****************************************************************

        private void mnuVario_Click(object sender, EventArgs e)
        {
            string[] stringsParams = null;
            if ((grdMeasurements.SelectedRows.Count > 0) &&
                (grdMeasurements.SelectedRows[0].Cells[0].Value != null))
            {
                stringsParams = new string[]
                                    {
                                        grdMeasurements.SelectedRows[0].Cells["clmNumber"].Value.ToString(),
                                        grdMeasurements.SelectedRows[0].Cells["clmDate"].Value.ToString(),
                                        grdMeasurements.SelectedRows[0].Cells["clmTime"].Value.ToString(),
                                        ((bool)grdMeasurements.SelectedRows[0].Cells["clmControl"].Value ? " (контроль тонометром)" : String.Empty),
                                        grdMeasurements.SelectedRows[0].Cells["clmSosud"].Value.ToString(),
                                        grdMeasurements.SelectedRows[0].Cells["clmTop"].Value.ToString() + " / " + grdMeasurements.SelectedRows[0].Cells["clmLower"].Value.ToString(),
                                        grdMeasurements.SelectedRows[0].Cells["clmPulse"].Value.ToString() + " уд/мин",
                                        DbPulseOperations.PrintStress(int.Parse(grdMeasurements.SelectedRows[0].Cells["clmStress"].Value.ToString())),
                                        grdMeasurements.SelectedRows[0].Cells["clmComment"].Value.ToString()
                                    };
            }
            var myFrmVario = new frmVario(_myPulseWave, _strPatient, stringsParams);
            myFrmVario.Show();
        }


        //*****************************************************************
        //                        Глобал и CASP                           *
        //*****************************************************************

        private void mnuGlobal_Click(object sender, EventArgs e)
        {
            int number = 0;
            if ((grdMeasurements.SelectedRows.Count > 0) &&
                (grdMeasurements.SelectedRows[0].Cells["clmNumber"].Value != null))
                number = Convert.ToInt32(grdMeasurements.SelectedRows[0].Cells["clmNumber"].Value);
            using (var myFrmGlobal = new frmGlobal(_idPatient, _idMeasurement, _myPulseWave.PulseWaveField, number, _bufferSettings.Scale))
            {
                myFrmGlobal.ShowDialog();
                if (myFrmGlobal.ResultChanged)
                {
                    grdMeasurements.Rows[_idRowTable].Cells["clmGlobalTop"].Value =
                        myFrmGlobal.ResultTopPressure;
                    grdMeasurements.Rows[_idRowTable].Cells["clmGlobalLower"].Value =
                        myFrmGlobal.ResultLowerPressure;
                    int oldA1 = _myPulseWave.GetReper(1),
                        oldA2 = _myPulseWave.GetReper(2);
                    _myPulseWave.Repers = myFrmGlobal.ResultRepers;
                    if ((oldA1 != myFrmGlobal.ResultRepers[0]) ||
                        (oldA2 != myFrmGlobal.ResultRepers[1]))
                    {
                        pnlPulse.AutoScrollPosition = new Point(0, 0);
                        pnlPulse.Refresh();
                        UpdateRepersNumericUpDown();
                        _flagNewRepers = true;
                        StartTimerButton();
                    }
                }
            }
        }


        private void mnuCasp_Click(object sender, EventArgs e)
        {
            int number = 0;
            if ((grdMeasurements.SelectedRows.Count > 0) &&
                (grdMeasurements.SelectedRows[0].Cells["clmNumber"].Value != null))
                number = Convert.ToInt32(grdMeasurements.SelectedRows[0].Cells["clmNumber"].Value);
            using (var myFrmCasp = new frmCasp(_idPatient, _idMeasurement, _myPulseWave, number, _bufferSettings.Scale))
            {
                myFrmCasp.ShowDialog();
                if (myFrmCasp.ResultChanged)
                {
                    int oldA1 = _myPulseWave.GetReper(1),
                        oldA4 = _myPulseWave.GetReper(4);
                    _myPulseWave.Repers = myFrmCasp.ResultRepers;
                    if ((oldA1 != myFrmCasp.ResultRepers[0]) ||
                        (oldA4 != myFrmCasp.ResultRepers[3]))
                    {
                        pnlPulse.AutoScrollPosition = new Point(0, 0);
                        pnlPulse.Refresh();
                        UpdateRepersNumericUpDown();
                        _flagNewRepers = true;
                        StartTimerButton();
                    }
                }
            }
        }


        //*****************************************************************
        //           Перемещение цифр на реконструкции волн               *
        //*****************************************************************

        private void pnlComparisonOneHeight_MouseMove(object sender, MouseEventArgs e)
        {
            Rectangle rctComparison = new Rectangle(pnlComparisonOneHeight.HorizontalScroll.Value,
                                                    pnlComparisonOneHeight.VerticalScroll.Value,
                                                    pnlComparisonOneHeight.ClientSize.Width,
                                                    pnlComparisonOneHeight.ClientSize.Height);
            if (_pulseWaveComparator.MoveNumberMode)
            {
                if (_pulseWaveComparator.NumberMove(e.Location, rctComparison, _bufferSettings.Scale))
                    pnlComparisonOneHeight.Refresh();
            }
            else
            {
                bool flagRefresh;
                pnlComparisonOneHeight.Cursor = _pulseWaveComparator.MouseMove(e.Location, rctComparison,
                                                                               out flagRefresh, _bufferSettings.Scale)
                                                    ? Cursors.Hand
                                                    : Cursors.Default;
                if (flagRefresh)
                    pnlComparisonOneHeight.Refresh();
            }
        }

        private void pnlComparisonOneHeight_MouseDown(object sender, MouseEventArgs e)
        {
            Rectangle rctComparison = new Rectangle(pnlComparisonOneHeight.HorizontalScroll.Value,
                                                    pnlComparisonOneHeight.VerticalScroll.Value,
                                                    pnlComparisonOneHeight.ClientSize.Width,
                                                    pnlComparisonOneHeight.ClientSize.Height);
            if (_pulseWaveComparator.MouseDown(e.Location, rctComparison, _bufferSettings.Scale))
                pnlComparisonOneHeight.Refresh();
        }

        private void pnlComparisonOneHeight_MouseUp(object sender, MouseEventArgs e)
        {
            if (_pulseWaveComparator.MouseUp())
                pnlComparisonOneHeight.Refresh();
        }


        //*****************************************************************
        //                            Статист                             *
        //*****************************************************************

        private bool _flagStatistMode = false;

        private void mnuStatist_Click(object sender, EventArgs e)
        {
            _flagStatistMode = !_flagStatistMode;
            grdMeasurements.Columns["clmSosud"].Visible = !_flagStatistMode;
            grdMeasurements.Columns["clmPulse"].Visible = !_flagStatistMode;
            grdMeasurements.Columns["clmStress"].Visible = !_flagStatistMode;
            grdMeasurements.Columns["clmSignal"].Visible = !_flagStatistMode;
            grdMeasurements.Columns["clmMode"].Visible = !_flagStatistMode;
            grdMeasurements.Columns["clmComparison"].Visible = !_flagStatistMode;

            grdMeasurements.Columns["clmGlobalTop"].Visible = _flagStatistMode;
            grdMeasurements.Columns["clmGlobalLower"].Visible = _flagStatistMode;
            grdMeasurements.Columns["clmKorotkovTop"].Visible = _flagStatistMode;
            grdMeasurements.Columns["clmKorotkovLower"].Visible = _flagStatistMode;
            grdMeasurements.Columns["clmXgdTop"].Visible = _flagStatistMode;
            grdMeasurements.Columns["clmXgdLower"].Visible = _flagStatistMode;
            grdMeasurements.Columns["clmMtpTop"].Visible = _flagStatistMode;
            grdMeasurements.Columns["clmMtpLower"].Visible = _flagStatistMode;

            grdMeasurements.MultiSelect = _flagStatistMode;
            grdMeasurements.Rows[_idRowTable].Selected = true;

            lblComparisonStatist.Text = _flagStatistMode ? "Выберите сравниваемые показатели:" : "Реконструкция ПВ:";
            if (lstStatist1.SelectedIndex < 0)
                lstStatist1.SelectedIndex = 0;
            if (lstStatist2.SelectedIndex < 0)
                lstStatist2.SelectedIndex = 1;
            tableLayoutPanel2.Visible = !_flagStatistMode;
            tableLayoutPanel3.Visible = _flagStatistMode;
            btnClearComparator.Text = _flagStatistMode ? "Построить отчет \"Статистика\"" : "Очистить список волн";
            btnClearComparator.Enabled = _flagStatistMode || (_pulseWaveComparator.GetCount() > 0);

            mnuStatist.Checked = _flagStatistMode;

            if (_flagStatistMode)
                StartTimerButton();
        }

        private void grdMeasurements_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (_flagStatistMode && e.ColumnIndex > -1)
            {
                string colName = grdMeasurements.Columns[e.ColumnIndex].Name;
                ArrayList colNamesTop = new ArrayList
                                            {
                                                "clmTop",
                                                "clmGlobalTop",
                                                "clmKorotkovTop",
                                                "clmXgdTop",
                                                "clmMtpTop",
                                            },
                          colNamesLower = new ArrayList
                                              {
                                                  "clmLower",
                                                  "clmGlobalLower",
                                                  "clmKorotkovLower",
                                                  "clmXgdLower",
                                                  "clmMtpLower"
                                              };
                if (colNamesTop.Contains(colName))
                {
                    e.Paint(e.CellBounds, DataGridViewPaintParts.All); // рисуем полностью ячейку
                    // рисуем линию
                    using (Pen p = new Pen(Color.Black, 1))
                    {
                        e.Graphics.DrawLine(p,
                          new Point(e.CellBounds.Left - 1, e.CellBounds.Top - 1),
                          new Point(e.CellBounds.Left - 1, e.CellBounds.Bottom - 1));
                    }
                    e.Handled = true;
                }
                else if (colNamesLower.Contains(colName))
                {
                    e.Paint(e.CellBounds, DataGridViewPaintParts.All);
                    using (Pen p = new Pen(Color.Black, 1))
                    {
                        e.Graphics.DrawLine(p,
                          new Point(e.CellBounds.Right - 1, e.CellBounds.Top - 1),
                          new Point(e.CellBounds.Right - 1, e.CellBounds.Bottom - 1));
                    }
                    e.Handled = true;
                }
            }
            //if (e.RowIndex == -1 && e.ColumnIndex > -1)
            //{
            //    Rectangle r2 = e.CellBounds;
            //    r2.Y += e.CellBounds.Height / 2;
            //    r2.Height = e.CellBounds.Height / 2;
            //    e.PaintBackground(r2, true);
            //    e.PaintContent(r2);
            //    e.Handled = true;
            //}
        }

        private void grdMeasurements_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            Rectangle rtHeader = grdMeasurements.DisplayRectangle;
            rtHeader.Height = grdMeasurements.ColumnHeadersHeight / 2;
            grdMeasurements.Invalidate(rtHeader);
        }

        private void grdMeasurements_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.ScrollOrientation == ScrollOrientation.HorizontalScroll)
            {
                Rectangle rtHeader = grdMeasurements.DisplayRectangle;
                rtHeader.Height = grdMeasurements.ColumnHeadersHeight/2;
                grdMeasurements.Invalidate(rtHeader);
            }
        }

        private void grdMeasurements_Paint(object sender, PaintEventArgs e)
        {
            Graphics graph = e.Graphics;
            Font fontHeader = new Font(grdMeasurements.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
            StringFormat formatHeader = new StringFormat { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center };
            SolidBrush brushHeaderBack = new SolidBrush(grdMeasurements.ColumnHeadersDefaultCellStyle.BackColor),
                       brushHeaderText = new SolidBrush(grdMeasurements.ColumnHeadersDefaultCellStyle.ForeColor);
            Rectangle r1 = grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmNumber"].Index, -1, true);
            r1 = new Rectangle(r1.X + 1, r1.Y + 1, r1.Width +
                               grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmDate"].Index, -1, true).Width +
                               grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmTime"].Index, -1, true).Width - 2,
                               r1.Height / 2 - 2);
            graph.FillRectangle(brushHeaderBack, r1);
            graph.DrawString("Замер", fontHeader, brushHeaderText, r1, formatHeader);
            if (!_flagStatistMode)
            {
                r1 = grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmSosud"].Index, -1, true);
                r1 = new Rectangle(r1.X + 1, r1.Y + 1, r1.Width +
                                   grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmTop"].Index, -1, true).Width +
                                   grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmLower"].Index, -1, true).Width +
                                   grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmPulse"].Index, -1, true).Width +
                                   grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmStress"].Index, -1, true).Width - 2,
                                   r1.Height / 2 - 2);
                graph.FillRectangle(brushHeaderBack, r1);
                graph.DrawString("Результаты замера", fontHeader, brushHeaderText, r1, formatHeader);
                r1 = grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmMode"].Index, -1, true);
                r1 = new Rectangle(r1.X + 1, r1.Y + 1, r1.Width +
                                   grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmSignal"].Index, -1, true).Width - 2,
                                   r1.Height / 2 - 2);
                graph.FillRectangle(brushHeaderBack, r1);
                graph.DrawString("Оборудование", fontHeader, brushHeaderText, r1, formatHeader);
            }
            else
            {
                r1 = grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmTop"].Index, -1, true);
                r1 = new Rectangle(r1.X + 1, r1.Y + 1, r1.Width +
                                   grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmLower"].Index, -1, true).Width - 2,
                                   r1.Height / 2 - 2);
                graph.FillRectangle(brushHeaderBack, r1);
                graph.DrawString("ТонКлок", fontHeader, brushHeaderText, r1, formatHeader);
                r1 = grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmGlobalTop"].Index, -1, true);
                r1 = new Rectangle(r1.X + 1, r1.Y + 1, r1.Width +
                                   grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmGlobalLower"].Index, -1, true).Width - 2,
                                   r1.Height / 2 - 2);
                graph.FillRectangle(brushHeaderBack, r1);
                graph.DrawString("Глобал", fontHeader, brushHeaderText, r1, formatHeader);
                r1 = grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmKorotkovTop"].Index, -1, true);
                r1 = new Rectangle(r1.X + 1, r1.Y + 1, r1.Width +
                                   grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmKorotkovLower"].Index, -1, true).Width - 2,
                                   r1.Height / 2 - 2);
                graph.FillRectangle(brushHeaderBack, r1);
                graph.DrawString("Коротков", fontHeader, brushHeaderText, r1, formatHeader);
                r1 = grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmXgdTop"].Index, -1, true);
                r1 = new Rectangle(r1.X + 1, r1.Y + 1, r1.Width +
                                   grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmXgdLower"].Index, -1, true).Width - 2,
                                   r1.Height / 2 - 2);
                graph.FillRectangle(brushHeaderBack, r1);
                graph.DrawString("ХГД", fontHeader, brushHeaderText, r1, formatHeader);
                r1 = grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmMtpTop"].Index, -1, true);
                r1 = new Rectangle(r1.X + 1, r1.Y + 1, r1.Width +
                                   grdMeasurements.GetCellDisplayRectangle(grdMeasurements.Columns["clmMtpLower"].Index, -1, true).Width - 2,
                                   r1.Height / 2 - 2);
                graph.FillRectangle(brushHeaderBack, r1);
                graph.DrawString("МТП", fontHeader, brushHeaderText, r1, formatHeader);
            }
        }

        private void tmrButton_Tick(object sender, EventArgs e)
        {
            if (_flagNewRepers)
                btnParamsPulse.BackColor = (btnParamsPulse.BackColor == DefaultBackColor ? Color.LawnGreen : DefaultBackColor);
            if (_flagUserChangedText)
                btnParamsDB.BackColor = (btnParamsDB.BackColor == DefaultBackColor ? Color.LawnGreen : DefaultBackColor);
            if (_flagStatistMode)
                btnClearComparator.BackColor = (btnClearComparator.BackColor == DefaultBackColor ? Color.LawnGreen : DefaultBackColor);
        }

        public void StartTimerButton()
        {
            tmrButton.Start();
        }

        private void StopTimerButton()
        {
            tmrButton.Stop();
            btnParamsPulse.BackColor = DefaultBackColor;
            btnParamsDB.BackColor = DefaultBackColor;
            btnClearComparator.BackColor = DefaultBackColor;
        }

        private bool _flagUserCanChangeText, _flagUserChangedText, _flagNewRepers;

        private void txtFSS_TextChanged(object sender, EventArgs e)
        {
            if (_flagUserCanChangeText)
            {
                _flagUserChangedText = true;
                StartTimerButton();
            }
        }


        //*****************************************************************
        //                       Расстановка реперов                      *
        //*****************************************************************

        private void pnlPulse_MouseMove(object sender, MouseEventArgs e)
        {
            if (_myPulseWave == null)
                return;

            Rectangle rctPulse = new Rectangle(pnlPulse.HorizontalScroll.Value,
                                               pnlPulse.VerticalScroll.Value,
                                               pnlPulse.ClientSize.Width,
                                               pnlPulse.ClientSize.Height);
            if (_myPulseWave.MoveReperMode)
            {
                if (_myPulseWave.ReperMove(e.Location, rctPulse))
                    pnlPulse.Refresh();
            }
            else
            {
                bool flagRefresh;
                pnlPulse.Cursor = _myPulseWave.MouseMove(e.Location, rctPulse,
                                                  out flagRefresh)
                                      ? Cursors.Hand
                                      : Cursors.Default;
                if (flagRefresh)
                    pnlPulse.Refresh();
            }
        }

        private void pnlPulse_MouseDown(object sender, MouseEventArgs e)
        {
            if (_myPulseWave != null && _myPulseWave.MouseDown(e.Location))
                pnlPulse.Refresh();
        }

        private void pnlPulse_MouseUp(object sender, MouseEventArgs e)
        {
            if (_myPulseWave != null && _myPulseWave.MouseUp())
            {
                pnlPulse.Refresh();
                UpdateRepersNumericUpDown();
                _flagNewRepers = true;
                StartTimerButton();
            }
        }

        private void mnuPulseWaveAuto_Click(object sender, EventArgs e)
        {
            if (_myPulseWave == null)
                return;
            _myPulseWave.ClearRepers();
            _myPulseWave.AnalyzePulseWave();
            pnlPulse.AutoScrollPosition = new Point(0, 0);
            pnlPulse.Refresh();
            _flagNewRepers = true;
            StartTimerButton();
        }

        private void nudA1_A2_ValueChanged(object sender, EventArgs e)
        {
            if (_myPulseWave != null)
            {
                int[] repers = _myPulseWave.Repers;
                if (repers != null && repers.Length >= 2)
                {
                    repers[1] = repers[0] + (int) nudA1_A2.Value;
                    _myPulseWave.Repers = repers;
                    pnlPulse.Invalidate();
                    if (_flagUserCanChangeText)
                    {
                        _flagNewRepers = true;
                        StartTimerButton();
                    }
                }
            }
        }


    }
}
