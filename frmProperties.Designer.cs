﻿namespace TonClock
{
    internal partial class frmProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.tbcProperties = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tbcDeviceOptions = new System.Windows.Forms.TabControl();
            this.tbp_rdbUsb = new System.Windows.Forms.TabPage();
            this.grpUsb = new System.Windows.Forms.GroupBox();
            this.txtVendorId = new System.Windows.Forms.TextBox();
            this.txtProductId = new System.Windows.Forms.TextBox();
            this.lblVendorID = new System.Windows.Forms.Label();
            this.lblProductID = new System.Windows.Forms.Label();
            this.tbp_rdbUsbEc = new System.Windows.Forms.TabPage();
            this.grbEcSet = new System.Windows.Forms.GroupBox();
            this.nudDac2 = new System.Windows.Forms.NumericUpDown();
            this.nudDac1 = new System.Windows.Forms.NumericUpDown();
            this.trbDac2 = new System.Windows.Forms.TrackBar();
            this.lblDac2 = new System.Windows.Forms.Label();
            this.trbDac1 = new System.Windows.Forms.TrackBar();
            this.lblDac1 = new System.Windows.Forms.Label();
            this.grbVirtualCom = new System.Windows.Forms.GroupBox();
            this.cmbUsbEcCom = new System.Windows.Forms.ComboBox();
            this.rdbComUsbEcManual = new System.Windows.Forms.RadioButton();
            this.rdbComUsbEcAuto = new System.Windows.Forms.RadioButton();
            this.tbp_rdbFtdiOld = new System.Windows.Forms.TabPage();
            this.tbp_rdbFtdiNew = new System.Windows.Forms.TabPage();
            this.dgvAutoDevices = new System.Windows.Forms.DataGridView();
            this.tbp_rdbBluetooth = new System.Windows.Forms.TabPage();
            this.grpBluetooth = new System.Windows.Forms.GroupBox();
            this.txtBtPin = new System.Windows.Forms.TextBox();
            this.lblBtPin = new System.Windows.Forms.Label();
            this.txtBtId = new System.Windows.Forms.TextBox();
            this.lblBtId = new System.Windows.Forms.Label();
            this.rdbBtManual = new System.Windows.Forms.RadioButton();
            this.rdbBtP2 = new System.Windows.Forms.RadioButton();
            this.rdbBtP1 = new System.Windows.Forms.RadioButton();
            this.rdbBluetooth = new System.Windows.Forms.RadioButton();
            this.rdbFtdiNew = new System.Windows.Forms.RadioButton();
            this.rdbFtdiOld = new System.Windows.Forms.RadioButton();
            this.rdbUsbEc = new System.Windows.Forms.RadioButton();
            this.rdbUsb = new System.Windows.Forms.RadioButton();
            this.lblMode = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.grpVoiceProp = new System.Windows.Forms.GroupBox();
            this.pcbMicrophone = new System.Windows.Forms.PictureBox();
            this.lblVoiceVolume = new System.Windows.Forms.Label();
            this.lblVoiceVolumeTitle = new System.Windows.Forms.Label();
            this.trbVoiceVolume = new System.Windows.Forms.TrackBar();
            this.cmbVoice = new System.Windows.Forms.ComboBox();
            this.lblVoice = new System.Windows.Forms.Label();
            this.pnlSoundResult = new System.Windows.Forms.Panel();
            this.grpSoundResult = new System.Windows.Forms.GroupBox();
            this.trbSoundResultVolume = new System.Windows.Forms.TrackBar();
            this.lblSoundResultVolume = new System.Windows.Forms.Label();
            this.lblSoundResultVolumeTitle = new System.Windows.Forms.Label();
            this.pnlSoundPulse = new System.Windows.Forms.Panel();
            this.grpRecord = new System.Windows.Forms.GroupBox();
            this.lblSoundRecVolume = new System.Windows.Forms.Label();
            this.lblSoundRecVolumeTitle = new System.Windows.Forms.Label();
            this.cmbRec = new System.Windows.Forms.ComboBox();
            this.lblRec = new System.Windows.Forms.Label();
            this.trbSoundRecVolume = new System.Windows.Forms.TrackBar();
            this.grpPulse = new System.Windows.Forms.GroupBox();
            this.trbSoundPulsePitch = new System.Windows.Forms.TrackBar();
            this.lblSoundPulsePitchTitle = new System.Windows.Forms.Label();
            this.lblSoundPulsePitch = new System.Windows.Forms.Label();
            this.lblSoundPulseVolume = new System.Windows.Forms.Label();
            this.lblSoundPulseVolumeTitle = new System.Windows.Forms.Label();
            this.trbSoundPulseVolume = new System.Windows.Forms.TrackBar();
            this.chkSoundResult = new System.Windows.Forms.CheckBox();
            this.chkSoundPulse = new System.Windows.Forms.CheckBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.chkLevel = new System.Windows.Forms.CheckBox();
            this.tmrDevices = new System.Windows.Forms.Timer(this.components);
            this.btnApply = new System.Windows.Forms.Button();
            this.clmId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmOn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmVolt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblComNew = new System.Windows.Forms.Label();
            this.tbcProperties.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tbcDeviceOptions.SuspendLayout();
            this.tbp_rdbUsb.SuspendLayout();
            this.grpUsb.SuspendLayout();
            this.tbp_rdbUsbEc.SuspendLayout();
            this.grbEcSet.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDac2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDac1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDac2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDac1)).BeginInit();
            this.grbVirtualCom.SuspendLayout();
            this.tbp_rdbFtdiNew.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAutoDevices)).BeginInit();
            this.tbp_rdbBluetooth.SuspendLayout();
            this.grpBluetooth.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.grpVoiceProp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbMicrophone)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbVoiceVolume)).BeginInit();
            this.pnlSoundResult.SuspendLayout();
            this.grpSoundResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbSoundResultVolume)).BeginInit();
            this.pnlSoundPulse.SuspendLayout();
            this.grpRecord.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbSoundRecVolume)).BeginInit();
            this.grpPulse.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbSoundPulsePitch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbSoundPulseVolume)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(358, 406);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(117, 30);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Отмена";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(226, 406);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(117, 30);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "ОК";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // tbcProperties
            // 
            this.tbcProperties.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tbcProperties.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbcProperties.Controls.Add(this.tabPage1);
            this.tbcProperties.Controls.Add(this.tabPage3);
            this.tbcProperties.Controls.Add(this.tabPage4);
            this.tbcProperties.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tbcProperties.ItemSize = new System.Drawing.Size(30, 120);
            this.tbcProperties.Location = new System.Drawing.Point(12, 12);
            this.tbcProperties.Multiline = true;
            this.tbcProperties.Name = "tbcProperties";
            this.tbcProperties.SelectedIndex = 0;
            this.tbcProperties.Size = new System.Drawing.Size(463, 381);
            this.tbcProperties.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tbcProperties.TabIndex = 6;
            this.tbcProperties.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tbcProperties_DrawItem);
            this.tbcProperties.SelectedIndexChanged += new System.EventHandler(this.tbcProperties_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tbcDeviceOptions);
            this.tabPage1.Controls.Add(this.rdbBluetooth);
            this.tabPage1.Controls.Add(this.rdbFtdiNew);
            this.tabPage1.Controls.Add(this.rdbFtdiOld);
            this.tabPage1.Controls.Add(this.rdbUsbEc);
            this.tabPage1.Controls.Add(this.rdbUsb);
            this.tabPage1.Controls.Add(this.lblMode);
            this.tabPage1.Location = new System.Drawing.Point(124, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(335, 373);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Режим работы";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tbcDeviceOptions
            // 
            this.tbcDeviceOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbcDeviceOptions.Controls.Add(this.tbp_rdbUsb);
            this.tbcDeviceOptions.Controls.Add(this.tbp_rdbUsbEc);
            this.tbcDeviceOptions.Controls.Add(this.tbp_rdbFtdiOld);
            this.tbcDeviceOptions.Controls.Add(this.tbp_rdbFtdiNew);
            this.tbcDeviceOptions.Controls.Add(this.tbp_rdbBluetooth);
            this.tbcDeviceOptions.Location = new System.Drawing.Point(9, 143);
            this.tbcDeviceOptions.Name = "tbcDeviceOptions";
            this.tbcDeviceOptions.SelectedIndex = 0;
            this.tbcDeviceOptions.Size = new System.Drawing.Size(323, 224);
            this.tbcDeviceOptions.TabIndex = 16;
            // 
            // tbp_rdbUsb
            // 
            this.tbp_rdbUsb.Controls.Add(this.grpUsb);
            this.tbp_rdbUsb.Location = new System.Drawing.Point(4, 22);
            this.tbp_rdbUsb.Name = "tbp_rdbUsb";
            this.tbp_rdbUsb.Padding = new System.Windows.Forms.Padding(3);
            this.tbp_rdbUsb.Size = new System.Drawing.Size(315, 198);
            this.tbp_rdbUsb.TabIndex = 1;
            this.tbp_rdbUsb.Text = "Usb";
            this.tbp_rdbUsb.UseVisualStyleBackColor = true;
            // 
            // grpUsb
            // 
            this.grpUsb.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpUsb.Controls.Add(this.txtVendorId);
            this.grpUsb.Controls.Add(this.txtProductId);
            this.grpUsb.Controls.Add(this.lblVendorID);
            this.grpUsb.Controls.Add(this.lblProductID);
            this.grpUsb.Location = new System.Drawing.Point(6, 6);
            this.grpUsb.Name = "grpUsb";
            this.grpUsb.Size = new System.Drawing.Size(303, 88);
            this.grpUsb.TabIndex = 17;
            this.grpUsb.TabStop = false;
            this.grpUsb.Text = "Регистры USB устройства";
            // 
            // txtVendorId
            // 
            this.txtVendorId.Location = new System.Drawing.Point(70, 19);
            this.txtVendorId.Name = "txtVendorId";
            this.txtVendorId.Size = new System.Drawing.Size(203, 20);
            this.txtVendorId.TabIndex = 15;
            // 
            // txtProductId
            // 
            this.txtProductId.Location = new System.Drawing.Point(70, 42);
            this.txtProductId.Name = "txtProductId";
            this.txtProductId.Size = new System.Drawing.Size(203, 20);
            this.txtProductId.TabIndex = 16;
            // 
            // lblVendorID
            // 
            this.lblVendorID.AutoSize = true;
            this.lblVendorID.Location = new System.Drawing.Point(6, 22);
            this.lblVendorID.Name = "lblVendorID";
            this.lblVendorID.Size = new System.Drawing.Size(58, 13);
            this.lblVendorID.TabIndex = 14;
            this.lblVendorID.Text = "Vendor ID:";
            // 
            // lblProductID
            // 
            this.lblProductID.AutoSize = true;
            this.lblProductID.Location = new System.Drawing.Point(6, 45);
            this.lblProductID.Name = "lblProductID";
            this.lblProductID.Size = new System.Drawing.Size(61, 13);
            this.lblProductID.TabIndex = 12;
            this.lblProductID.Text = "Product ID:";
            // 
            // tbp_rdbUsbEc
            // 
            this.tbp_rdbUsbEc.Controls.Add(this.grbEcSet);
            this.tbp_rdbUsbEc.Controls.Add(this.grbVirtualCom);
            this.tbp_rdbUsbEc.Location = new System.Drawing.Point(4, 22);
            this.tbp_rdbUsbEc.Name = "tbp_rdbUsbEc";
            this.tbp_rdbUsbEc.Size = new System.Drawing.Size(315, 198);
            this.tbp_rdbUsbEc.TabIndex = 3;
            this.tbp_rdbUsbEc.Text = "UsbEc";
            this.tbp_rdbUsbEc.UseVisualStyleBackColor = true;
            // 
            // grbEcSet
            // 
            this.grbEcSet.Controls.Add(this.nudDac2);
            this.grbEcSet.Controls.Add(this.nudDac1);
            this.grbEcSet.Controls.Add(this.trbDac2);
            this.grbEcSet.Controls.Add(this.lblDac2);
            this.grbEcSet.Controls.Add(this.trbDac1);
            this.grbEcSet.Controls.Add(this.lblDac1);
            this.grbEcSet.Location = new System.Drawing.Point(6, 84);
            this.grbEcSet.Name = "grbEcSet";
            this.grbEcSet.Size = new System.Drawing.Size(303, 100);
            this.grbEcSet.TabIndex = 19;
            this.grbEcSet.TabStop = false;
            this.grbEcSet.Text = "Настройка аппаратуры";
            // 
            // nudDac2
            // 
            this.nudDac2.Location = new System.Drawing.Point(233, 55);
            this.nudDac2.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
            this.nudDac2.Name = "nudDac2";
            this.nudDac2.Size = new System.Drawing.Size(64, 20);
            this.nudDac2.TabIndex = 12;
            this.nudDac2.ValueChanged += new System.EventHandler(this.nudDac2_ValueChanged);
            // 
            // nudDac1
            // 
            this.nudDac1.Location = new System.Drawing.Point(233, 24);
            this.nudDac1.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
            this.nudDac1.Name = "nudDac1";
            this.nudDac1.Size = new System.Drawing.Size(64, 20);
            this.nudDac1.TabIndex = 12;
            this.nudDac1.ValueChanged += new System.EventHandler(this.nudDac1_ValueChanged);
            // 
            // trbDac2
            // 
            this.trbDac2.AutoSize = false;
            this.trbDac2.Location = new System.Drawing.Point(51, 50);
            this.trbDac2.Maximum = 4095;
            this.trbDac2.Name = "trbDac2";
            this.trbDac2.Size = new System.Drawing.Size(176, 25);
            this.trbDac2.TabIndex = 11;
            this.trbDac2.TickFrequency = 256;
            this.trbDac2.Scroll += new System.EventHandler(this.trbDac2_Scroll);
            // 
            // lblDac2
            // 
            this.lblDac2.AutoSize = true;
            this.lblDac2.Location = new System.Drawing.Point(7, 57);
            this.lblDac2.Name = "lblDac2";
            this.lblDac2.Size = new System.Drawing.Size(38, 13);
            this.lblDac2.TabIndex = 0;
            this.lblDac2.Text = "DAC2:";
            // 
            // trbDac1
            // 
            this.trbDac1.AutoSize = false;
            this.trbDac1.Location = new System.Drawing.Point(51, 19);
            this.trbDac1.Maximum = 4095;
            this.trbDac1.Name = "trbDac1";
            this.trbDac1.Size = new System.Drawing.Size(176, 25);
            this.trbDac1.TabIndex = 11;
            this.trbDac1.TickFrequency = 256;
            this.trbDac1.Scroll += new System.EventHandler(this.trbDac1_Scroll);
            // 
            // lblDac1
            // 
            this.lblDac1.AutoSize = true;
            this.lblDac1.Location = new System.Drawing.Point(7, 26);
            this.lblDac1.Name = "lblDac1";
            this.lblDac1.Size = new System.Drawing.Size(38, 13);
            this.lblDac1.TabIndex = 0;
            this.lblDac1.Text = "DAC1:";
            // 
            // grbVirtualCom
            // 
            this.grbVirtualCom.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grbVirtualCom.Controls.Add(this.cmbUsbEcCom);
            this.grbVirtualCom.Controls.Add(this.rdbComUsbEcManual);
            this.grbVirtualCom.Controls.Add(this.rdbComUsbEcAuto);
            this.grbVirtualCom.Location = new System.Drawing.Point(6, 6);
            this.grbVirtualCom.Name = "grbVirtualCom";
            this.grbVirtualCom.Size = new System.Drawing.Size(303, 72);
            this.grbVirtualCom.TabIndex = 18;
            this.grbVirtualCom.TabStop = false;
            this.grbVirtualCom.Text = "Настройки определения номера COM-порта";
            // 
            // cmbUsbEcCom
            // 
            this.cmbUsbEcCom.Enabled = false;
            this.cmbUsbEcCom.FormattingEnabled = true;
            this.cmbUsbEcCom.Location = new System.Drawing.Point(82, 40);
            this.cmbUsbEcCom.Name = "cmbUsbEcCom";
            this.cmbUsbEcCom.Size = new System.Drawing.Size(121, 21);
            this.cmbUsbEcCom.TabIndex = 2;
            // 
            // rdbComUsbEcManual
            // 
            this.rdbComUsbEcManual.AutoSize = true;
            this.rdbComUsbEcManual.Location = new System.Drawing.Point(6, 40);
            this.rdbComUsbEcManual.Name = "rdbComUsbEcManual";
            this.rdbComUsbEcManual.Size = new System.Drawing.Size(70, 17);
            this.rdbComUsbEcManual.TabIndex = 1;
            this.rdbComUsbEcManual.Text = "Вручную:";
            this.rdbComUsbEcManual.UseVisualStyleBackColor = true;
            this.rdbComUsbEcManual.CheckedChanged += new System.EventHandler(this.rdbComUsbEcManual_CheckedChanged);
            // 
            // rdbComUsbEcAuto
            // 
            this.rdbComUsbEcAuto.AutoSize = true;
            this.rdbComUsbEcAuto.Checked = true;
            this.rdbComUsbEcAuto.Location = new System.Drawing.Point(6, 17);
            this.rdbComUsbEcAuto.Name = "rdbComUsbEcAuto";
            this.rdbComUsbEcAuto.Size = new System.Drawing.Size(103, 17);
            this.rdbComUsbEcAuto.TabIndex = 1;
            this.rdbComUsbEcAuto.TabStop = true;
            this.rdbComUsbEcAuto.Text = "Автоматически";
            this.rdbComUsbEcAuto.UseVisualStyleBackColor = true;
            // 
            // tbp_rdbFtdiOld
            // 
            this.tbp_rdbFtdiOld.Location = new System.Drawing.Point(4, 22);
            this.tbp_rdbFtdiOld.Name = "tbp_rdbFtdiOld";
            this.tbp_rdbFtdiOld.Size = new System.Drawing.Size(315, 198);
            this.tbp_rdbFtdiOld.TabIndex = 4;
            this.tbp_rdbFtdiOld.Text = "FtdiOld";
            this.tbp_rdbFtdiOld.UseVisualStyleBackColor = true;
            // 
            // tbp_rdbFtdiNew
            // 
            this.tbp_rdbFtdiNew.Controls.Add(this.lblComNew);
            this.tbp_rdbFtdiNew.Controls.Add(this.dgvAutoDevices);
            this.tbp_rdbFtdiNew.Location = new System.Drawing.Point(4, 22);
            this.tbp_rdbFtdiNew.Name = "tbp_rdbFtdiNew";
            this.tbp_rdbFtdiNew.Padding = new System.Windows.Forms.Padding(3);
            this.tbp_rdbFtdiNew.Size = new System.Drawing.Size(315, 198);
            this.tbp_rdbFtdiNew.TabIndex = 2;
            this.tbp_rdbFtdiNew.Text = "FtdiNew";
            this.tbp_rdbFtdiNew.UseVisualStyleBackColor = true;
            // 
            // dgvAutoDevices
            // 
            this.dgvAutoDevices.AllowUserToAddRows = false;
            this.dgvAutoDevices.AllowUserToDeleteRows = false;
            this.dgvAutoDevices.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvAutoDevices.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgvAutoDevices.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dgvAutoDevices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAutoDevices.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmId,
            this.clmName,
            this.clmOn,
            this.clmVolt});
            this.dgvAutoDevices.Location = new System.Drawing.Point(6, 19);
            this.dgvAutoDevices.MultiSelect = false;
            this.dgvAutoDevices.Name = "dgvAutoDevices";
            this.dgvAutoDevices.RowHeadersVisible = false;
            this.dgvAutoDevices.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAutoDevices.Size = new System.Drawing.Size(303, 173);
            this.dgvAutoDevices.TabIndex = 1;
            // 
            // tbp_rdbBluetooth
            // 
            this.tbp_rdbBluetooth.Controls.Add(this.grpBluetooth);
            this.tbp_rdbBluetooth.Location = new System.Drawing.Point(4, 22);
            this.tbp_rdbBluetooth.Name = "tbp_rdbBluetooth";
            this.tbp_rdbBluetooth.Size = new System.Drawing.Size(315, 198);
            this.tbp_rdbBluetooth.TabIndex = 5;
            this.tbp_rdbBluetooth.Text = "Bt";
            this.tbp_rdbBluetooth.UseVisualStyleBackColor = true;
            // 
            // grpBluetooth
            // 
            this.grpBluetooth.Controls.Add(this.txtBtPin);
            this.grpBluetooth.Controls.Add(this.lblBtPin);
            this.grpBluetooth.Controls.Add(this.txtBtId);
            this.grpBluetooth.Controls.Add(this.lblBtId);
            this.grpBluetooth.Controls.Add(this.rdbBtManual);
            this.grpBluetooth.Controls.Add(this.rdbBtP2);
            this.grpBluetooth.Controls.Add(this.rdbBtP1);
            this.grpBluetooth.Location = new System.Drawing.Point(6, 6);
            this.grpBluetooth.Name = "grpBluetooth";
            this.grpBluetooth.Size = new System.Drawing.Size(303, 155);
            this.grpBluetooth.TabIndex = 0;
            this.grpBluetooth.TabStop = false;
            this.grpBluetooth.Text = "Настройка устройства Bluetooth";
            // 
            // txtBtPin
            // 
            this.txtBtPin.Location = new System.Drawing.Point(60, 114);
            this.txtBtPin.Name = "txtBtPin";
            this.txtBtPin.Size = new System.Drawing.Size(128, 20);
            this.txtBtPin.TabIndex = 17;
            // 
            // lblBtPin
            // 
            this.lblBtPin.AutoSize = true;
            this.lblBtPin.Location = new System.Drawing.Point(26, 117);
            this.lblBtPin.Name = "lblBtPin";
            this.lblBtPin.Size = new System.Drawing.Size(28, 13);
            this.lblBtPin.TabIndex = 16;
            this.lblBtPin.Text = "PIN:";
            // 
            // txtBtId
            // 
            this.txtBtId.Location = new System.Drawing.Point(60, 88);
            this.txtBtId.Name = "txtBtId";
            this.txtBtId.Size = new System.Drawing.Size(128, 20);
            this.txtBtId.TabIndex = 17;
            // 
            // lblBtId
            // 
            this.lblBtId.AutoSize = true;
            this.lblBtId.Location = new System.Drawing.Point(26, 91);
            this.lblBtId.Name = "lblBtId";
            this.lblBtId.Size = new System.Drawing.Size(21, 13);
            this.lblBtId.TabIndex = 16;
            this.lblBtId.Text = "ID:";
            // 
            // rdbBtManual
            // 
            this.rdbBtManual.AutoSize = true;
            this.rdbBtManual.Checked = true;
            this.rdbBtManual.Location = new System.Drawing.Point(6, 65);
            this.rdbBtManual.Name = "rdbBtManual";
            this.rdbBtManual.Size = new System.Drawing.Size(108, 17);
            this.rdbBtManual.TabIndex = 0;
            this.rdbBtManual.TabStop = true;
            this.rdbBtManual.Text = "Задать вручную:";
            this.rdbBtManual.UseVisualStyleBackColor = true;
            this.rdbBtManual.CheckedChanged += new System.EventHandler(this.rdbBtManual_CheckedChanged);
            // 
            // rdbBtP2
            // 
            this.rdbBtP2.AutoSize = true;
            this.rdbBtP2.Location = new System.Drawing.Point(6, 42);
            this.rdbBtP2.Name = "rdbBtP2";
            this.rdbBtP2.Size = new System.Drawing.Size(73, 17);
            this.rdbBtP2.TabIndex = 0;
            this.rdbBtP2.Text = "P6W0002";
            this.rdbBtP2.UseVisualStyleBackColor = true;
            // 
            // rdbBtP1
            // 
            this.rdbBtP1.AutoSize = true;
            this.rdbBtP1.Location = new System.Drawing.Point(6, 19);
            this.rdbBtP1.Name = "rdbBtP1";
            this.rdbBtP1.Size = new System.Drawing.Size(70, 17);
            this.rdbBtP1.TabIndex = 0;
            this.rdbBtP1.Text = "PWAVE6";
            this.rdbBtP1.UseVisualStyleBackColor = true;
            // 
            // rdbBluetooth
            // 
            this.rdbBluetooth.AutoSize = true;
            this.rdbBluetooth.Location = new System.Drawing.Point(29, 120);
            this.rdbBluetooth.Name = "rdbBluetooth";
            this.rdbBluetooth.Size = new System.Drawing.Size(191, 17);
            this.rdbBluetooth.TabIndex = 1;
            this.rdbBluetooth.Text = "Беспроводной блютуз (Bluetooth)";
            this.rdbBluetooth.UseVisualStyleBackColor = true;
            this.rdbBluetooth.CheckedChanged += new System.EventHandler(this.rdbUsb_CheckedChanged);
            // 
            // rdbFtdiNew
            // 
            this.rdbFtdiNew.AutoSize = true;
            this.rdbFtdiNew.Location = new System.Drawing.Point(29, 97);
            this.rdbFtdiNew.Name = "rdbFtdiNew";
            this.rdbFtdiNew.Size = new System.Drawing.Size(187, 17);
            this.rdbFtdiNew.TabIndex = 1;
            this.rdbFtdiNew.Text = "Беспроводной спецсвязь (сеть)";
            this.rdbFtdiNew.UseVisualStyleBackColor = true;
            this.rdbFtdiNew.CheckedChanged += new System.EventHandler(this.rdbUsb_CheckedChanged);
            // 
            // rdbFtdiOld
            // 
            this.rdbFtdiOld.AutoSize = true;
            this.rdbFtdiOld.Location = new System.Drawing.Point(29, 74);
            this.rdbFtdiOld.Name = "rdbFtdiOld";
            this.rdbFtdiOld.Size = new System.Drawing.Size(209, 17);
            this.rdbFtdiOld.TabIndex = 13;
            this.rdbFtdiOld.Text = "Беспроводной спецсвязь (1 прибор)";
            this.rdbFtdiOld.UseVisualStyleBackColor = true;
            this.rdbFtdiOld.CheckedChanged += new System.EventHandler(this.rdbUsb_CheckedChanged);
            // 
            // rdbUsbEc
            // 
            this.rdbUsbEc.AutoSize = true;
            this.rdbUsbEc.Location = new System.Drawing.Point(29, 51);
            this.rdbUsbEc.Name = "rdbUsbEc";
            this.rdbUsbEc.Size = new System.Drawing.Size(98, 17);
            this.rdbUsbEc.TabIndex = 14;
            this.rdbUsbEc.Text = "Проводной ЭК";
            this.rdbUsbEc.UseVisualStyleBackColor = true;
            this.rdbUsbEc.CheckedChanged += new System.EventHandler(this.rdbUsb_CheckedChanged);
            // 
            // rdbUsb
            // 
            this.rdbUsb.AutoSize = true;
            this.rdbUsb.Checked = true;
            this.rdbUsb.Location = new System.Drawing.Point(29, 28);
            this.rdbUsb.Name = "rdbUsb";
            this.rdbUsb.Size = new System.Drawing.Size(112, 17);
            this.rdbUsb.TabIndex = 14;
            this.rdbUsb.TabStop = true;
            this.rdbUsb.Text = "Проводной (USB)";
            this.rdbUsb.UseVisualStyleBackColor = true;
            this.rdbUsb.CheckedChanged += new System.EventHandler(this.rdbUsb_CheckedChanged);
            // 
            // lblMode
            // 
            this.lblMode.AutoSize = true;
            this.lblMode.Location = new System.Drawing.Point(6, 6);
            this.lblMode.Name = "lblMode";
            this.lblMode.Size = new System.Drawing.Size(85, 13);
            this.lblMode.TabIndex = 12;
            this.lblMode.Text = "Режим работы:";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.grpVoiceProp);
            this.tabPage3.Controls.Add(this.pnlSoundResult);
            this.tabPage3.Controls.Add(this.pnlSoundPulse);
            this.tabPage3.Controls.Add(this.chkSoundResult);
            this.tabPage3.Controls.Add(this.chkSoundPulse);
            this.tabPage3.Location = new System.Drawing.Point(124, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(335, 373);
            this.tabPage3.TabIndex = 1;
            this.tabPage3.Text = "Звук";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // grpVoiceProp
            // 
            this.grpVoiceProp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpVoiceProp.Controls.Add(this.pcbMicrophone);
            this.grpVoiceProp.Controls.Add(this.lblVoiceVolume);
            this.grpVoiceProp.Controls.Add(this.lblVoiceVolumeTitle);
            this.grpVoiceProp.Controls.Add(this.trbVoiceVolume);
            this.grpVoiceProp.Controls.Add(this.cmbVoice);
            this.grpVoiceProp.Controls.Add(this.lblVoice);
            this.grpVoiceProp.Location = new System.Drawing.Point(6, 290);
            this.grpVoiceProp.Name = "grpVoiceProp";
            this.grpVoiceProp.Size = new System.Drawing.Size(326, 77);
            this.grpVoiceProp.TabIndex = 16;
            this.grpVoiceProp.TabStop = false;
            this.grpVoiceProp.Text = "Настройка голоса диктора";
            // 
            // pcbMicrophone
            // 
            this.pcbMicrophone.Image = global::TonClock.Properties.Resources.Microphone;
            this.pcbMicrophone.Location = new System.Drawing.Point(6, 22);
            this.pcbMicrophone.Name = "pcbMicrophone";
            this.pcbMicrophone.Size = new System.Drawing.Size(20, 20);
            this.pcbMicrophone.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pcbMicrophone.TabIndex = 20;
            this.pcbMicrophone.TabStop = false;
            // 
            // lblVoiceVolume
            // 
            this.lblVoiceVolume.AutoSize = true;
            this.lblVoiceVolume.Location = new System.Drawing.Point(254, 50);
            this.lblVoiceVolume.Name = "lblVoiceVolume";
            this.lblVoiceVolume.Size = new System.Drawing.Size(24, 13);
            this.lblVoiceVolume.TabIndex = 18;
            this.lblVoiceVolume.Text = "0 %";
            // 
            // lblVoiceVolumeTitle
            // 
            this.lblVoiceVolumeTitle.AutoSize = true;
            this.lblVoiceVolumeTitle.Location = new System.Drawing.Point(32, 50);
            this.lblVoiceVolumeTitle.Name = "lblVoiceVolumeTitle";
            this.lblVoiceVolumeTitle.Size = new System.Drawing.Size(65, 13);
            this.lblVoiceVolumeTitle.TabIndex = 19;
            this.lblVoiceVolumeTitle.Text = "Громкость:";
            // 
            // trbVoiceVolume
            // 
            this.trbVoiceVolume.AutoSize = false;
            this.trbVoiceVolume.Location = new System.Drawing.Point(103, 46);
            this.trbVoiceVolume.Maximum = 100;
            this.trbVoiceVolume.Name = "trbVoiceVolume";
            this.trbVoiceVolume.Size = new System.Drawing.Size(145, 25);
            this.trbVoiceVolume.TabIndex = 17;
            this.trbVoiceVolume.TickFrequency = 10;
            this.trbVoiceVolume.ValueChanged += new System.EventHandler(this.trbVoiceVolume_ValueChanged);
            // 
            // cmbVoice
            // 
            this.cmbVoice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbVoice.FormattingEnabled = true;
            this.cmbVoice.Items.AddRange(new object[] {
            "Женский",
            "Мужской"});
            this.cmbVoice.Location = new System.Drawing.Point(103, 19);
            this.cmbVoice.Name = "cmbVoice";
            this.cmbVoice.Size = new System.Drawing.Size(145, 21);
            this.cmbVoice.TabIndex = 16;
            // 
            // lblVoice
            // 
            this.lblVoice.AutoSize = true;
            this.lblVoice.Location = new System.Drawing.Point(32, 22);
            this.lblVoice.Name = "lblVoice";
            this.lblVoice.Size = new System.Drawing.Size(40, 13);
            this.lblVoice.TabIndex = 15;
            this.lblVoice.Text = "Голос:";
            // 
            // pnlSoundResult
            // 
            this.pnlSoundResult.Controls.Add(this.grpSoundResult);
            this.pnlSoundResult.Location = new System.Drawing.Point(29, 226);
            this.pnlSoundResult.Name = "pnlSoundResult";
            this.pnlSoundResult.Size = new System.Drawing.Size(303, 58);
            this.pnlSoundResult.TabIndex = 11;
            // 
            // grpSoundResult
            // 
            this.grpSoundResult.Controls.Add(this.trbSoundResultVolume);
            this.grpSoundResult.Controls.Add(this.lblSoundResultVolume);
            this.grpSoundResult.Controls.Add(this.lblSoundResultVolumeTitle);
            this.grpSoundResult.Location = new System.Drawing.Point(3, 3);
            this.grpSoundResult.Name = "grpSoundResult";
            this.grpSoundResult.Size = new System.Drawing.Size(297, 50);
            this.grpSoundResult.TabIndex = 16;
            this.grpSoundResult.TabStop = false;
            this.grpSoundResult.Text = "Мелодия при озвучке результатов";
            // 
            // trbSoundResultVolume
            // 
            this.trbSoundResultVolume.AutoSize = false;
            this.trbSoundResultVolume.Location = new System.Drawing.Point(77, 19);
            this.trbSoundResultVolume.Maximum = 100;
            this.trbSoundResultVolume.Name = "trbSoundResultVolume";
            this.trbSoundResultVolume.Size = new System.Drawing.Size(145, 25);
            this.trbSoundResultVolume.TabIndex = 10;
            this.trbSoundResultVolume.TickFrequency = 10;
            this.trbSoundResultVolume.ValueChanged += new System.EventHandler(this.trbSoundResultVolume_ValueChanged);
            // 
            // lblSoundResultVolume
            // 
            this.lblSoundResultVolume.AutoSize = true;
            this.lblSoundResultVolume.Location = new System.Drawing.Point(228, 23);
            this.lblSoundResultVolume.Name = "lblSoundResultVolume";
            this.lblSoundResultVolume.Size = new System.Drawing.Size(24, 13);
            this.lblSoundResultVolume.TabIndex = 11;
            this.lblSoundResultVolume.Text = "0 %";
            // 
            // lblSoundResultVolumeTitle
            // 
            this.lblSoundResultVolumeTitle.AutoSize = true;
            this.lblSoundResultVolumeTitle.Location = new System.Drawing.Point(6, 23);
            this.lblSoundResultVolumeTitle.Name = "lblSoundResultVolumeTitle";
            this.lblSoundResultVolumeTitle.Size = new System.Drawing.Size(65, 13);
            this.lblSoundResultVolumeTitle.TabIndex = 12;
            this.lblSoundResultVolumeTitle.Text = "Громкость:";
            // 
            // pnlSoundPulse
            // 
            this.pnlSoundPulse.Controls.Add(this.grpRecord);
            this.pnlSoundPulse.Controls.Add(this.grpPulse);
            this.pnlSoundPulse.Location = new System.Drawing.Point(29, 29);
            this.pnlSoundPulse.Name = "pnlSoundPulse";
            this.pnlSoundPulse.Size = new System.Drawing.Size(303, 168);
            this.pnlSoundPulse.TabIndex = 10;
            // 
            // grpRecord
            // 
            this.grpRecord.Controls.Add(this.lblSoundRecVolume);
            this.grpRecord.Controls.Add(this.lblSoundRecVolumeTitle);
            this.grpRecord.Controls.Add(this.cmbRec);
            this.grpRecord.Controls.Add(this.lblRec);
            this.grpRecord.Controls.Add(this.trbSoundRecVolume);
            this.grpRecord.Location = new System.Drawing.Point(3, 87);
            this.grpRecord.Name = "grpRecord";
            this.grpRecord.Size = new System.Drawing.Size(297, 77);
            this.grpRecord.TabIndex = 15;
            this.grpRecord.TabStop = false;
            this.grpRecord.Text = "Мелодия при записи фрагмента ПВ";
            // 
            // lblSoundRecVolume
            // 
            this.lblSoundRecVolume.AutoSize = true;
            this.lblSoundRecVolume.Location = new System.Drawing.Point(228, 50);
            this.lblSoundRecVolume.Name = "lblSoundRecVolume";
            this.lblSoundRecVolume.Size = new System.Drawing.Size(24, 13);
            this.lblSoundRecVolume.TabIndex = 11;
            this.lblSoundRecVolume.Text = "0 %";
            // 
            // lblSoundRecVolumeTitle
            // 
            this.lblSoundRecVolumeTitle.AutoSize = true;
            this.lblSoundRecVolumeTitle.Location = new System.Drawing.Point(6, 50);
            this.lblSoundRecVolumeTitle.Name = "lblSoundRecVolumeTitle";
            this.lblSoundRecVolumeTitle.Size = new System.Drawing.Size(65, 13);
            this.lblSoundRecVolumeTitle.TabIndex = 12;
            this.lblSoundRecVolumeTitle.Text = "Громкость:";
            // 
            // cmbRec
            // 
            this.cmbRec.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRec.FormattingEnabled = true;
            this.cmbRec.Items.AddRange(new object[] {
            "Л.В.Бетховен - К Элизе",
            "Ю.Проскурякова - Ты мое счастье"});
            this.cmbRec.Location = new System.Drawing.Point(77, 19);
            this.cmbRec.Name = "cmbRec";
            this.cmbRec.Size = new System.Drawing.Size(214, 21);
            this.cmbRec.TabIndex = 14;
            // 
            // lblRec
            // 
            this.lblRec.AutoSize = true;
            this.lblRec.Location = new System.Drawing.Point(6, 22);
            this.lblRec.Name = "lblRec";
            this.lblRec.Size = new System.Drawing.Size(55, 13);
            this.lblRec.TabIndex = 13;
            this.lblRec.Text = "Мелодия:";
            // 
            // trbSoundRecVolume
            // 
            this.trbSoundRecVolume.AutoSize = false;
            this.trbSoundRecVolume.Location = new System.Drawing.Point(77, 46);
            this.trbSoundRecVolume.Maximum = 100;
            this.trbSoundRecVolume.Name = "trbSoundRecVolume";
            this.trbSoundRecVolume.Size = new System.Drawing.Size(145, 25);
            this.trbSoundRecVolume.TabIndex = 10;
            this.trbSoundRecVolume.TickFrequency = 10;
            this.trbSoundRecVolume.ValueChanged += new System.EventHandler(this.trbSoundRecVolume_ValueChanged);
            // 
            // grpPulse
            // 
            this.grpPulse.Controls.Add(this.trbSoundPulsePitch);
            this.grpPulse.Controls.Add(this.lblSoundPulsePitchTitle);
            this.grpPulse.Controls.Add(this.lblSoundPulsePitch);
            this.grpPulse.Controls.Add(this.lblSoundPulseVolume);
            this.grpPulse.Controls.Add(this.lblSoundPulseVolumeTitle);
            this.grpPulse.Controls.Add(this.trbSoundPulseVolume);
            this.grpPulse.Location = new System.Drawing.Point(3, 3);
            this.grpPulse.Name = "grpPulse";
            this.grpPulse.Size = new System.Drawing.Size(297, 78);
            this.grpPulse.TabIndex = 15;
            this.grpPulse.TabStop = false;
            this.grpPulse.Text = "Озвучка ПВ";
            // 
            // trbSoundPulsePitch
            // 
            this.trbSoundPulsePitch.AutoSize = false;
            this.trbSoundPulsePitch.Location = new System.Drawing.Point(77, 16);
            this.trbSoundPulsePitch.Maximum = 200;
            this.trbSoundPulsePitch.Name = "trbSoundPulsePitch";
            this.trbSoundPulsePitch.Size = new System.Drawing.Size(145, 25);
            this.trbSoundPulsePitch.TabIndex = 10;
            this.trbSoundPulsePitch.TickFrequency = 12;
            this.trbSoundPulsePitch.Scroll += new System.EventHandler(this.trbSoundPulsePitch_Scroll);
            this.trbSoundPulsePitch.ValueChanged += new System.EventHandler(this.trbSoundPulsePitch_ValueChanged);
            // 
            // lblSoundPulsePitchTitle
            // 
            this.lblSoundPulsePitchTitle.AutoSize = true;
            this.lblSoundPulsePitchTitle.Location = new System.Drawing.Point(6, 20);
            this.lblSoundPulsePitchTitle.Name = "lblSoundPulsePitchTitle";
            this.lblSoundPulsePitchTitle.Size = new System.Drawing.Size(35, 13);
            this.lblSoundPulsePitchTitle.TabIndex = 12;
            this.lblSoundPulsePitchTitle.Text = "Нота:";
            // 
            // lblSoundPulsePitch
            // 
            this.lblSoundPulsePitch.AutoSize = true;
            this.lblSoundPulsePitch.Location = new System.Drawing.Point(228, 20);
            this.lblSoundPulsePitch.Name = "lblSoundPulsePitch";
            this.lblSoundPulsePitch.Size = new System.Drawing.Size(13, 13);
            this.lblSoundPulsePitch.TabIndex = 11;
            this.lblSoundPulsePitch.Text = "0";
            // 
            // lblSoundPulseVolume
            // 
            this.lblSoundPulseVolume.AutoSize = true;
            this.lblSoundPulseVolume.Location = new System.Drawing.Point(228, 51);
            this.lblSoundPulseVolume.Name = "lblSoundPulseVolume";
            this.lblSoundPulseVolume.Size = new System.Drawing.Size(24, 13);
            this.lblSoundPulseVolume.TabIndex = 11;
            this.lblSoundPulseVolume.Text = "0 %";
            // 
            // lblSoundPulseVolumeTitle
            // 
            this.lblSoundPulseVolumeTitle.AutoSize = true;
            this.lblSoundPulseVolumeTitle.Location = new System.Drawing.Point(6, 51);
            this.lblSoundPulseVolumeTitle.Name = "lblSoundPulseVolumeTitle";
            this.lblSoundPulseVolumeTitle.Size = new System.Drawing.Size(65, 13);
            this.lblSoundPulseVolumeTitle.TabIndex = 12;
            this.lblSoundPulseVolumeTitle.Text = "Громкость:";
            // 
            // trbSoundPulseVolume
            // 
            this.trbSoundPulseVolume.AutoSize = false;
            this.trbSoundPulseVolume.Location = new System.Drawing.Point(77, 47);
            this.trbSoundPulseVolume.Maximum = 100;
            this.trbSoundPulseVolume.Name = "trbSoundPulseVolume";
            this.trbSoundPulseVolume.Size = new System.Drawing.Size(145, 25);
            this.trbSoundPulseVolume.TabIndex = 10;
            this.trbSoundPulseVolume.TickFrequency = 10;
            this.trbSoundPulseVolume.ValueChanged += new System.EventHandler(this.trbSoundPulseVolume_ValueChanged);
            // 
            // chkSoundResult
            // 
            this.chkSoundResult.AutoSize = true;
            this.chkSoundResult.Checked = true;
            this.chkSoundResult.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSoundResult.Location = new System.Drawing.Point(6, 203);
            this.chkSoundResult.Name = "chkSoundResult";
            this.chkSoundResult.Size = new System.Drawing.Size(175, 17);
            this.chkSoundResult.TabIndex = 7;
            this.chkSoundResult.Text = "Озвучка результатов замера";
            this.chkSoundResult.UseVisualStyleBackColor = true;
            this.chkSoundResult.CheckedChanged += new System.EventHandler(this.chkSoundResult_CheckedChanged);
            // 
            // chkSoundPulse
            // 
            this.chkSoundPulse.AutoSize = true;
            this.chkSoundPulse.Checked = true;
            this.chkSoundPulse.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSoundPulse.Location = new System.Drawing.Point(6, 6);
            this.chkSoundPulse.Name = "chkSoundPulse";
            this.chkSoundPulse.Size = new System.Drawing.Size(201, 17);
            this.chkSoundPulse.TabIndex = 6;
            this.chkSoundPulse.Text = "Звуковое сопровождение сигнала";
            this.chkSoundPulse.UseVisualStyleBackColor = true;
            this.chkSoundPulse.CheckedChanged += new System.EventHandler(this.chkSoundPulse_CheckedChanged);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.chkLevel);
            this.tabPage4.Location = new System.Drawing.Point(124, 4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(335, 373);
            this.tabPage4.TabIndex = 2;
            this.tabPage4.Text = "Индикация";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // chkLevel
            // 
            this.chkLevel.AutoSize = true;
            this.chkLevel.Checked = true;
            this.chkLevel.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkLevel.Location = new System.Drawing.Point(6, 6);
            this.chkLevel.Name = "chkLevel";
            this.chkLevel.Size = new System.Drawing.Size(164, 17);
            this.chkLevel.TabIndex = 6;
            this.chkLevel.Text = "Индикация уровня сигнала";
            this.chkLevel.UseVisualStyleBackColor = true;
            // 
            // tmrDevices
            // 
            this.tmrDevices.Interval = 1000;
            this.tmrDevices.Tick += new System.EventHandler(this.tmrDevices_Tick);
            // 
            // btnApply
            // 
            this.btnApply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnApply.Location = new System.Drawing.Point(12, 406);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(117, 30);
            this.btnApply.TabIndex = 1;
            this.btnApply.Text = "Применить";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // clmId
            // 
            this.clmId.HeaderText = "ID";
            this.clmId.Name = "clmId";
            this.clmId.ReadOnly = true;
            this.clmId.Width = 43;
            // 
            // clmName
            // 
            this.clmName.HeaderText = "Название";
            this.clmName.Name = "clmName";
            this.clmName.Width = 82;
            // 
            // clmOn
            // 
            this.clmOn.HeaderText = "Состояние";
            this.clmOn.Name = "clmOn";
            this.clmOn.ReadOnly = true;
            this.clmOn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.clmOn.Width = 86;
            // 
            // clmVolt
            // 
            this.clmVolt.HeaderText = "Вольт";
            this.clmVolt.Name = "clmVolt";
            this.clmVolt.ReadOnly = true;
            this.clmVolt.Width = 62;
            // 
            // lblComNew
            // 
            this.lblComNew.AutoSize = true;
            this.lblComNew.Location = new System.Drawing.Point(6, 3);
            this.lblComNew.Name = "lblComNew";
            this.lblComNew.Size = new System.Drawing.Size(152, 13);
            this.lblComNew.TabIndex = 13;
            this.lblComNew.Text = "Список конечных устройств:";
            // 
            // frmProperties
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(487, 448);
            this.Controls.Add(this.tbcProperties);
            this.Controls.Add(this.btnApply);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmProperties";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Настройка";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmProperties_FormClosing);
            this.Load += new System.EventHandler(this.frmProperties_Load);
            this.tbcProperties.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tbcDeviceOptions.ResumeLayout(false);
            this.tbp_rdbUsb.ResumeLayout(false);
            this.grpUsb.ResumeLayout(false);
            this.grpUsb.PerformLayout();
            this.tbp_rdbUsbEc.ResumeLayout(false);
            this.grbEcSet.ResumeLayout(false);
            this.grbEcSet.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudDac2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDac1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDac2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbDac1)).EndInit();
            this.grbVirtualCom.ResumeLayout(false);
            this.grbVirtualCom.PerformLayout();
            this.tbp_rdbFtdiNew.ResumeLayout(false);
            this.tbp_rdbFtdiNew.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAutoDevices)).EndInit();
            this.tbp_rdbBluetooth.ResumeLayout(false);
            this.grpBluetooth.ResumeLayout(false);
            this.grpBluetooth.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.grpVoiceProp.ResumeLayout(false);
            this.grpVoiceProp.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pcbMicrophone)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbVoiceVolume)).EndInit();
            this.pnlSoundResult.ResumeLayout(false);
            this.grpSoundResult.ResumeLayout(false);
            this.grpSoundResult.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbSoundResultVolume)).EndInit();
            this.pnlSoundPulse.ResumeLayout(false);
            this.grpRecord.ResumeLayout(false);
            this.grpRecord.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbSoundRecVolume)).EndInit();
            this.grpPulse.ResumeLayout(false);
            this.grpPulse.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trbSoundPulsePitch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trbSoundPulseVolume)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.TabControl tbcProperties;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.RadioButton rdbFtdiOld;
        private System.Windows.Forms.RadioButton rdbUsb;
        private System.Windows.Forms.Label lblMode;
        private System.Windows.Forms.CheckBox chkSoundResult;
        private System.Windows.Forms.CheckBox chkSoundPulse;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.CheckBox chkLevel;
        private System.Windows.Forms.Panel pnlSoundResult;
        private System.Windows.Forms.Label lblSoundResultVolume;
        private System.Windows.Forms.Label lblSoundResultVolumeTitle;
        private System.Windows.Forms.TrackBar trbSoundResultVolume;
        private System.Windows.Forms.Panel pnlSoundPulse;
        private System.Windows.Forms.Label lblSoundPulsePitch;
        private System.Windows.Forms.Label lblSoundPulseVolume;
        private System.Windows.Forms.Label lblSoundPulsePitchTitle;
        private System.Windows.Forms.Label lblSoundPulseVolumeTitle;
        private System.Windows.Forms.TrackBar trbSoundPulsePitch;
        private System.Windows.Forms.TrackBar trbSoundPulseVolume;
        private System.Windows.Forms.TextBox txtProductId;
        private System.Windows.Forms.TextBox txtVendorId;
        private System.Windows.Forms.Label lblProductID;
        private System.Windows.Forms.Label lblVendorID;
        private System.Windows.Forms.ComboBox cmbRec;
        private System.Windows.Forms.Label lblRec;
        private System.Windows.Forms.ComboBox cmbVoice;
        private System.Windows.Forms.Label lblVoice;
        private System.Windows.Forms.GroupBox grpVoiceProp;
        private System.Windows.Forms.Label lblVoiceVolume;
        private System.Windows.Forms.Label lblVoiceVolumeTitle;
        private System.Windows.Forms.TrackBar trbVoiceVolume;
        private System.Windows.Forms.PictureBox pcbMicrophone;
        private System.Windows.Forms.GroupBox grpRecord;
        private System.Windows.Forms.Label lblSoundRecVolume;
        private System.Windows.Forms.Label lblSoundRecVolumeTitle;
        private System.Windows.Forms.TrackBar trbSoundRecVolume;
        private System.Windows.Forms.GroupBox grpPulse;
        private System.Windows.Forms.GroupBox grpUsb;
        private System.Windows.Forms.GroupBox grpSoundResult;
        private System.Windows.Forms.RadioButton rdbFtdiNew;
        private System.Windows.Forms.DataGridView dgvAutoDevices;
        private System.Windows.Forms.Timer tmrDevices;
        private System.Windows.Forms.RadioButton rdbUsbEc;
        private System.Windows.Forms.RadioButton rdbBluetooth;
        private System.Windows.Forms.TabControl tbcDeviceOptions;
        private System.Windows.Forms.TabPage tbp_rdbUsb;
        private System.Windows.Forms.TabPage tbp_rdbFtdiNew;
        private System.Windows.Forms.TabPage tbp_rdbUsbEc;
        private System.Windows.Forms.GroupBox grbVirtualCom;
        private System.Windows.Forms.ComboBox cmbUsbEcCom;
        private System.Windows.Forms.RadioButton rdbComUsbEcManual;
        private System.Windows.Forms.RadioButton rdbComUsbEcAuto;
        private System.Windows.Forms.TabPage tbp_rdbFtdiOld;
        private System.Windows.Forms.TabPage tbp_rdbBluetooth;
        private System.Windows.Forms.GroupBox grbEcSet;
        private System.Windows.Forms.NumericUpDown nudDac2;
        private System.Windows.Forms.NumericUpDown nudDac1;
        private System.Windows.Forms.TrackBar trbDac2;
        private System.Windows.Forms.Label lblDac2;
        private System.Windows.Forms.TrackBar trbDac1;
        private System.Windows.Forms.Label lblDac1;
        private System.Windows.Forms.GroupBox grpBluetooth;
        private System.Windows.Forms.RadioButton rdbBtP1;
        private System.Windows.Forms.RadioButton rdbBtManual;
        private System.Windows.Forms.RadioButton rdbBtP2;
        private System.Windows.Forms.TextBox txtBtPin;
        private System.Windows.Forms.Label lblBtPin;
        private System.Windows.Forms.TextBox txtBtId;
        private System.Windows.Forms.Label lblBtId;
        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Label lblComNew;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmId;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmName;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmOn;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmVolt;
    }
}