﻿namespace TonClock
{
    partial class frmPropBuffer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClose = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.lblScale = new System.Windows.Forms.Label();
            this.numScale = new System.Windows.Forms.NumericUpDown();
            this.chkPoints = new System.Windows.Forms.CheckBox();
            this.chkPrintValues = new System.Windows.Forms.CheckBox();
            this.chkDerivative = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.numScale)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(178, 120);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(117, 30);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Отмена";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(46, 120);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(117, 30);
            this.btnOK.TabIndex = 5;
            this.btnOK.Text = "ОК";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // lblScale
            // 
            this.lblScale.AutoSize = true;
            this.lblScale.Location = new System.Drawing.Point(12, 9);
            this.lblScale.Name = "lblScale";
            this.lblScale.Size = new System.Drawing.Size(177, 13);
            this.lblScale.TabIndex = 4;
            this.lblScale.Text = "Масштаб по горизонтальной оси:";
            // 
            // numScale
            // 
            this.numScale.DecimalPlaces = 2;
            this.numScale.Location = new System.Drawing.Point(195, 7);
            this.numScale.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numScale.Name = "numScale";
            this.numScale.Size = new System.Drawing.Size(77, 20);
            this.numScale.TabIndex = 1;
            this.numScale.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // chkPoints
            // 
            this.chkPoints.AutoSize = true;
            this.chkPoints.Location = new System.Drawing.Point(15, 40);
            this.chkPoints.Name = "chkPoints";
            this.chkPoints.Size = new System.Drawing.Size(267, 17);
            this.chkPoints.TabIndex = 2;
            this.chkPoints.Text = "Показать точки, по которым строятся графики";
            this.chkPoints.UseVisualStyleBackColor = true;
            // 
            // chkPrintValues
            // 
            this.chkPrintValues.AutoSize = true;
            this.chkPrintValues.Location = new System.Drawing.Point(15, 63);
            this.chkPrintValues.Name = "chkPrintValues";
            this.chkPrintValues.Size = new System.Drawing.Size(216, 17);
            this.chkPrintValues.TabIndex = 3;
            this.chkPrintValues.Text = "Печатать реальные значения данных";
            this.chkPrintValues.UseVisualStyleBackColor = true;
            // 
            // chkDerivative
            // 
            this.chkDerivative.AutoSize = true;
            this.chkDerivative.Location = new System.Drawing.Point(15, 86);
            this.chkDerivative.Name = "chkDerivative";
            this.chkDerivative.Size = new System.Drawing.Size(183, 17);
            this.chkDerivative.TabIndex = 4;
            this.chkDerivative.Text = "Рисовать график производной";
            this.chkDerivative.UseVisualStyleBackColor = true;
            // 
            // frmPropBuffer
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(307, 162);
            this.Controls.Add(this.chkDerivative);
            this.Controls.Add(this.chkPrintValues);
            this.Controls.Add(this.chkPoints);
            this.Controls.Add(this.numScale);
            this.Controls.Add(this.lblScale);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPropBuffer";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Настройка";
            ((System.ComponentModel.ISupportInitialize)(this.numScale)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Label lblScale;
        private System.Windows.Forms.NumericUpDown numScale;
        private System.Windows.Forms.CheckBox chkPoints;
        private System.Windows.Forms.CheckBox chkPrintValues;
        private System.Windows.Forms.CheckBox chkDerivative;
    }
}