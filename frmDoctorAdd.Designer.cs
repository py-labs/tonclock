﻿namespace TonClock
{
    partial class frmDoctorAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnOK = new System.Windows.Forms.Button();
            this.picDoctor = new System.Windows.Forms.PictureBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblSurname = new System.Windows.Forms.Label();
            this.txtLastname = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.txtFirstname = new System.Windows.Forms.TextBox();
            this.lblPatronymic = new System.Windows.Forms.Label();
            this.txtPatronymic = new System.Windows.Forms.TextBox();
            this.lblCity = new System.Windows.Forms.Label();
            this.lblOrganization = new System.Windows.Forms.Label();
            this.cmbCity = new System.Windows.Forms.ComboBox();
            this.cmbOrganization = new System.Windows.Forms.ComboBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.lblZvezda = new System.Windows.Forms.Label();
            this.lblZvezdaTitle = new System.Windows.Forms.Label();
            this.lblZvezda1 = new System.Windows.Forms.Label();
            this.lblZvezda2 = new System.Windows.Forms.Label();
            this.lblZvezda3 = new System.Windows.Forms.Label();
            this.lblZvezda4 = new System.Windows.Forms.Label();
            this.lblZvezda5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picDoctor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Location = new System.Drawing.Point(239, 222);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(117, 30);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "Отмена";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(107, 222);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(117, 30);
            this.btnOK.TabIndex = 12;
            this.btnOK.Text = "ОК";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // picDoctor
            // 
            this.picDoctor.Location = new System.Drawing.Point(12, 12);
            this.picDoctor.Name = "picDoctor";
            this.picDoctor.Size = new System.Drawing.Size(24, 24);
            this.picDoctor.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picDoctor.TabIndex = 4;
            this.picDoctor.TabStop = false;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(52, 23);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(35, 13);
            this.lblTitle.TabIndex = 5;
            this.lblTitle.Text = "label1";
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.Location = new System.Drawing.Point(12, 55);
            this.lblSurname.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(59, 13);
            this.lblSurname.TabIndex = 6;
            this.lblSurname.Text = "Фамилия:";
            // 
            // txtLastname
            // 
            this.txtLastname.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLastname.Location = new System.Drawing.Point(107, 52);
            this.txtLastname.Name = "txtLastname";
            this.txtLastname.Size = new System.Drawing.Size(249, 20);
            this.txtLastname.TabIndex = 7;
            this.txtLastname.TextChanged += new System.EventHandler(this.txtLastname_TextChanged);
            this.txtLastname.Validating += new System.ComponentModel.CancelEventHandler(this.txtLastname_Validating);
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(12, 81);
            this.lblName.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(32, 13);
            this.lblName.TabIndex = 6;
            this.lblName.Text = "Имя:";
            // 
            // txtFirstname
            // 
            this.txtFirstname.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFirstname.Location = new System.Drawing.Point(107, 78);
            this.txtFirstname.Name = "txtFirstname";
            this.txtFirstname.Size = new System.Drawing.Size(249, 20);
            this.txtFirstname.TabIndex = 8;
            this.txtFirstname.TextChanged += new System.EventHandler(this.txtFirstname_TextChanged);
            this.txtFirstname.Validating += new System.ComponentModel.CancelEventHandler(this.txtFirstname_Validating);
            // 
            // lblPatronymic
            // 
            this.lblPatronymic.AutoSize = true;
            this.lblPatronymic.Location = new System.Drawing.Point(12, 107);
            this.lblPatronymic.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblPatronymic.Name = "lblPatronymic";
            this.lblPatronymic.Size = new System.Drawing.Size(57, 13);
            this.lblPatronymic.TabIndex = 6;
            this.lblPatronymic.Text = "Отчество:";
            // 
            // txtPatronymic
            // 
            this.txtPatronymic.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPatronymic.Location = new System.Drawing.Point(107, 104);
            this.txtPatronymic.Name = "txtPatronymic";
            this.txtPatronymic.Size = new System.Drawing.Size(249, 20);
            this.txtPatronymic.TabIndex = 9;
            this.txtPatronymic.TextChanged += new System.EventHandler(this.txtPatronymic_TextChanged);
            this.txtPatronymic.Validating += new System.ComponentModel.CancelEventHandler(this.txtPatronymic_Validating);
            // 
            // lblCity
            // 
            this.lblCity.AutoSize = true;
            this.lblCity.Location = new System.Drawing.Point(12, 133);
            this.lblCity.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblCity.Name = "lblCity";
            this.lblCity.Size = new System.Drawing.Size(40, 13);
            this.lblCity.TabIndex = 6;
            this.lblCity.Text = "Город:";
            // 
            // lblOrganization
            // 
            this.lblOrganization.AutoSize = true;
            this.lblOrganization.Location = new System.Drawing.Point(12, 160);
            this.lblOrganization.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblOrganization.Name = "lblOrganization";
            this.lblOrganization.Size = new System.Drawing.Size(77, 13);
            this.lblOrganization.TabIndex = 6;
            this.lblOrganization.Text = "Организация:";
            // 
            // cmbCity
            // 
            this.cmbCity.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbCity.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbCity.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbCity.FormattingEnabled = true;
            this.cmbCity.Location = new System.Drawing.Point(107, 130);
            this.cmbCity.Name = "cmbCity";
            this.cmbCity.Size = new System.Drawing.Size(249, 21);
            this.cmbCity.Sorted = true;
            this.cmbCity.TabIndex = 10;
            this.cmbCity.TextChanged += new System.EventHandler(this.cmbCity_TextChanged);
            this.cmbCity.Validating += new System.ComponentModel.CancelEventHandler(this.cmbCity_Validating);
            // 
            // cmbOrganization
            // 
            this.cmbOrganization.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbOrganization.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cmbOrganization.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbOrganization.FormattingEnabled = true;
            this.cmbOrganization.Location = new System.Drawing.Point(106, 157);
            this.cmbOrganization.Name = "cmbOrganization";
            this.cmbOrganization.Size = new System.Drawing.Size(250, 21);
            this.cmbOrganization.Sorted = true;
            this.cmbOrganization.TabIndex = 11;
            this.cmbOrganization.TextChanged += new System.EventHandler(this.cmbOrganization_TextChanged);
            this.cmbOrganization.Validating += new System.ComponentModel.CancelEventHandler(this.cmbOrganization_Validating);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // lblZvezda
            // 
            this.lblZvezda.AutoSize = true;
            this.lblZvezda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblZvezda.ForeColor = System.Drawing.Color.Red;
            this.lblZvezda.Location = new System.Drawing.Point(337, 193);
            this.lblZvezda.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.lblZvezda.Name = "lblZvezda";
            this.lblZvezda.Size = new System.Drawing.Size(12, 13);
            this.lblZvezda.TabIndex = 15;
            this.lblZvezda.Text = "*";
            // 
            // lblZvezdaTitle
            // 
            this.lblZvezdaTitle.AutoSize = true;
            this.lblZvezdaTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblZvezdaTitle.Location = new System.Drawing.Point(12, 193);
            this.lblZvezdaTitle.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.lblZvezdaTitle.Name = "lblZvezdaTitle";
            this.lblZvezdaTitle.Size = new System.Drawing.Size(325, 13);
            this.lblZvezdaTitle.TabIndex = 14;
            this.lblZvezdaTitle.Text = "Поля, обязательные для заполнения, обозначены звёздочкой";
            // 
            // lblZvezda1
            // 
            this.lblZvezda1.AutoSize = true;
            this.lblZvezda1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblZvezda1.ForeColor = System.Drawing.Color.Red;
            this.lblZvezda1.Location = new System.Drawing.Point(71, 55);
            this.lblZvezda1.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.lblZvezda1.Name = "lblZvezda1";
            this.lblZvezda1.Size = new System.Drawing.Size(12, 13);
            this.lblZvezda1.TabIndex = 16;
            this.lblZvezda1.Text = "*";
            // 
            // lblZvezda2
            // 
            this.lblZvezda2.AutoSize = true;
            this.lblZvezda2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblZvezda2.ForeColor = System.Drawing.Color.Red;
            this.lblZvezda2.Location = new System.Drawing.Point(44, 81);
            this.lblZvezda2.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.lblZvezda2.Name = "lblZvezda2";
            this.lblZvezda2.Size = new System.Drawing.Size(12, 13);
            this.lblZvezda2.TabIndex = 16;
            this.lblZvezda2.Text = "*";
            // 
            // lblZvezda3
            // 
            this.lblZvezda3.AutoSize = true;
            this.lblZvezda3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblZvezda3.ForeColor = System.Drawing.Color.Red;
            this.lblZvezda3.Location = new System.Drawing.Point(69, 107);
            this.lblZvezda3.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.lblZvezda3.Name = "lblZvezda3";
            this.lblZvezda3.Size = new System.Drawing.Size(12, 13);
            this.lblZvezda3.TabIndex = 16;
            this.lblZvezda3.Text = "*";
            // 
            // lblZvezda4
            // 
            this.lblZvezda4.AutoSize = true;
            this.lblZvezda4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblZvezda4.ForeColor = System.Drawing.Color.Red;
            this.lblZvezda4.Location = new System.Drawing.Point(52, 133);
            this.lblZvezda4.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.lblZvezda4.Name = "lblZvezda4";
            this.lblZvezda4.Size = new System.Drawing.Size(12, 13);
            this.lblZvezda4.TabIndex = 16;
            this.lblZvezda4.Text = "*";
            // 
            // lblZvezda5
            // 
            this.lblZvezda5.AutoSize = true;
            this.lblZvezda5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblZvezda5.ForeColor = System.Drawing.Color.Red;
            this.lblZvezda5.Location = new System.Drawing.Point(89, 160);
            this.lblZvezda5.Margin = new System.Windows.Forms.Padding(0, 0, 3, 0);
            this.lblZvezda5.Name = "lblZvezda5";
            this.lblZvezda5.Size = new System.Drawing.Size(12, 13);
            this.lblZvezda5.TabIndex = 16;
            this.lblZvezda5.Text = "*";
            // 
            // frmDoctorAdd
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(380, 264);
            this.Controls.Add(this.lblZvezda5);
            this.Controls.Add(this.lblZvezda4);
            this.Controls.Add(this.lblZvezda3);
            this.Controls.Add(this.lblZvezda2);
            this.Controls.Add(this.lblZvezda1);
            this.Controls.Add(this.lblZvezda);
            this.Controls.Add(this.lblZvezdaTitle);
            this.Controls.Add(this.cmbOrganization);
            this.Controls.Add(this.cmbCity);
            this.Controls.Add(this.lblOrganization);
            this.Controls.Add(this.lblCity);
            this.Controls.Add(this.txtPatronymic);
            this.Controls.Add(this.lblPatronymic);
            this.Controls.Add(this.txtFirstname);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.txtLastname);
            this.Controls.Add(this.lblSurname);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.picDoctor);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmDoctorAdd";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Добавление консультанта";
            this.Shown += new System.EventHandler(this.frmDoctorAdd_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.picDoctor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.PictureBox picDoctor;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.TextBox txtLastname;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtFirstname;
        private System.Windows.Forms.Label lblPatronymic;
        private System.Windows.Forms.TextBox txtPatronymic;
        private System.Windows.Forms.Label lblCity;
        private System.Windows.Forms.Label lblOrganization;
        private System.Windows.Forms.ComboBox cmbCity;
        private System.Windows.Forms.ComboBox cmbOrganization;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label lblZvezda;
        private System.Windows.Forms.Label lblZvezdaTitle;
        private System.Windows.Forms.Label lblZvezda5;
        private System.Windows.Forms.Label lblZvezda4;
        private System.Windows.Forms.Label lblZvezda3;
        private System.Windows.Forms.Label lblZvezda2;
        private System.Windows.Forms.Label lblZvezda1;
    }
}