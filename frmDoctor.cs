﻿using System;
using System.Drawing;
using System.Threading;
using System.Windows.Forms;
using System.Data.OleDb;

namespace TonClock
{
    public partial class frmDoctor : Form
    {
        
        public frmDoctor()
        {
            InitializeComponent();
            this.Icon = Properties.Resources.yagodka;
        }

        private void frmDoctor_Load(object sender, EventArgs e)
        {
            this.Text = ConstGlobals.AssemblyTitle + " - Консультанты";
        }

        internal bool UpdateMainTable(out string strErr, int id = 0)
        {
            btnAdd.Enabled = false;
            btnEdit.Enabled = false;
            btnDelete.Enabled = false;
            btnNext.Enabled = false;
            try
            {
                using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    OleDbCommand myOleDbCommand = new OleDbCommand(
                        "SELECT Doctors.id, Doctors.lastname, Doctors.firstname, Doctors.patronymic, " +
                        "       Cities.cName, Organizations.oName " +
                        "FROM Doctors, Cities, Organizations " +
                        "WHERE Doctors.city = Cities.id " +
                        "  AND Doctors.organization = Organizations.id " +
                        "ORDER BY Doctors.id ASC",
                        myOleDbConnection);
                    OleDbDataReader myReader = myOleDbCommand.ExecuteReader();
                    grdDoctors.Rows.Clear();
                    while (myReader.Read())
                    {
                        grdDoctors.Rows.Add();
                        for (int i = 0; i < myReader.FieldCount; i++)
                            grdDoctors.Rows[grdDoctors.Rows.Count - 1].Cells[i].Value = myReader[i].ToString();
                        if ((id != 0) && ((int)myReader[0] == id))
                        {
                            grdDoctors.Rows[grdDoctors.Rows.Count - 1].Selected = true;
                            grdDoctors.CurrentCell = grdDoctors.Rows[grdDoctors.Rows.Count - 1].Cells[1];
                        }
                    }
                    btnAdd.Enabled = true;
                    btnEdit.Enabled = myReader.HasRows;
                    btnDelete.Enabled = myReader.HasRows;
                    btnNext.Enabled = myReader.HasRows;
                    myReader.Close();
                }
            }
            catch (Exception exc)
            {
                strErr = exc.Message;
                return false;
            }
            StartTimerButton();
            strErr = "";
            return true;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            StopTimerButton();
            if ((grdDoctors.SelectedRows.Count > 0) &&
                (grdDoctors.SelectedRows[0].Cells[0].Value != null))
            {
                frmPatients MyFrmPatients = new frmPatients(
                    int.Parse(grdDoctors.SelectedRows[0].Cells[0].Value.ToString()), this);
                this.Hide();
                MyFrmPatients.Show();
            }
            else
                MessageBox.Show("В таблице нет выделенной строки.\n" +
                    "Выполнение операции не возможно.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            StopTimerButton();
            frmDoctorAdd myFrmDoctorAdd = new frmDoctorAdd();
            myFrmDoctorAdd.ShowDialog();
            if ((myFrmDoctorAdd.DialogResult == DialogResult.OK) && (myFrmDoctorAdd.returnFlagOK()))
            {
                string strError;
                if (!UpdateMainTable(out strError, myFrmDoctorAdd.returnIdRecord()))
                    MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                        strError + "\n\n" +
                        "Пожалуйста, обратитесь к администратору.",
                        "Ошибка",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            StartTimerButton();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            StopTimerButton();
            if ((grdDoctors.SelectedRows.Count > 0) &&
                (grdDoctors.SelectedRows[0].Cells[0].Value != null))
            {
                int id = int.Parse(grdDoctors.SelectedRows[0].Cells[0].Value.ToString());
                frmDoctorAdd myFrmDoctorAdd = new frmDoctorAdd(id, false);
                myFrmDoctorAdd.ShowDialog();
                if ((myFrmDoctorAdd.DialogResult == DialogResult.OK) && (myFrmDoctorAdd.returnFlagOK()))
                {
                    id = myFrmDoctorAdd.returnIdRecord();
                    string strError;
                    if (!UpdateMainTable(out strError, id))
                        MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                            strError + "\n\n" +
                            "Пожалуйста, обратитесь к администратору.",
                            "Ошибка",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
                MessageBox.Show("В таблице нет выделенной строки.\n" +
                    "Выполнение операции не возможно.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            StartTimerButton();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            StopTimerButton();
            if ((grdDoctors.SelectedRows.Count > 0) &&
                (grdDoctors.SelectedRows[0].Cells[0].Value != null))
            {
                DialogResult result = MessageBox.Show("Удалить выделенную запись консультанта " +
                    grdDoctors.SelectedRows[0].Cells[1].Value.ToString() + " " +
                    grdDoctors.SelectedRows[0].Cells[2].Value.ToString()[0] + "." +
                    grdDoctors.SelectedRows[0].Cells[3].Value.ToString()[0] + ". ?\n\n" +
                    "ВНИМАНИЕ: будут так же удалены все пациенты данного консультанта.",
                    "Удаление",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    int id = int.Parse(grdDoctors.SelectedRows[0].Cells[0].Value.ToString());
                    string strError;
                    if (!DbPulseOperations.DeleteDoctor(id, out strError))
                        MessageBox.Show("Возникла ошибка при удалении консультанта:\n" + strError,
                                        "Ошибка",
                                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    try
                    {
                        if (!UpdateMainTable(out strError))
                            throw new Exception(strError);
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show("Возникла ошибка при работе с базой данных:\n" +
                            exc.Message + "\n\n" +
                            "Пожалуйста, обратитесь к администратору.",
                            "Ошибка",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
                MessageBox.Show("В таблице нет выделенной строки.\n" +
                    "Выполнение операции не возможно.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            StartTimerButton();
        }


        private void tmrButton_Tick(object sender, EventArgs e)
        {
            if (btnNext.Enabled)
                btnNext.BackColor = (btnNext.BackColor == DefaultBackColor ? Color.LawnGreen : DefaultBackColor);
            else if (btnAdd.Enabled)
                btnAdd.BackColor = (btnAdd.BackColor == DefaultBackColor ? Color.LawnGreen : DefaultBackColor);
        }

        public void StartTimerButton()
        {
            tmrButton.Start();
        }

        private void StopTimerButton()
        {
            tmrButton.Stop();
            btnNext.BackColor = DefaultBackColor;
            btnAdd.BackColor = DefaultBackColor;
        }

        // О программе (F1)
        private void frmDoctor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 112)
            {
                frmAboutBox myFormAbout = new frmAboutBox();
                myFormAbout.ShowDialog();
            }
        }
        

    }
}
