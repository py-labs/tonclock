﻿namespace TonClock
{
    partial class frmBuffer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblPulseWaveData = new System.Windows.Forms.Label();
            this.menuStripBuffer = new System.Windows.Forms.MenuStrip();
            this.mnuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuPageSetup = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPrintPreview = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPrint = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVario = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuGlobal = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCasp = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuStatist = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlPulseData = new System.Windows.Forms.Panel();
            this.lblWave = new System.Windows.Forms.Label();
            this.pnlPulse = new System.Windows.Forms.Panel();
            this.cmsPulseWave = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuPulseWaveAuto = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlStress = new System.Windows.Forms.Panel();
            this.lblStressPic = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.grdMeasurements = new System.Windows.Forms.DataGridView();
            this.clmIdDb = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmControl = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.clmNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmSosud = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmTop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmLower = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmGlobalTop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmGlobalLower = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmKorotkovTop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmKorotkovLower = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmXgdTop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmXgdLower = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmMtpTop = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmMtpLower = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmPulse = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmStress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmMode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmSignal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmComment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clmComparison = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.btnClearComparator = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lstStatist2 = new System.Windows.Forms.ListBox();
            this.lstStatist1 = new System.Windows.Forms.ListBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.pnlComparison = new System.Windows.Forms.Panel();
            this.pnlComparisonOneHeight = new System.Windows.Forms.Panel();
            this.lblComparisonStatist = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.grpParams = new System.Windows.Forms.GroupBox();
            this.grpS = new System.Windows.Forms.GroupBox();
            this.txtS2 = new System.Windows.Forms.TextBox();
            this.lblS1 = new System.Windows.Forms.Label();
            this.txtS1 = new System.Windows.Forms.TextBox();
            this.lblS3 = new System.Windows.Forms.Label();
            this.lblS2 = new System.Windows.Forms.Label();
            this.btnParamsDB = new System.Windows.Forms.Button();
            this.grpParamsPulse = new System.Windows.Forms.GroupBox();
            this.nudA1_A2 = new System.Windows.Forms.NumericUpDown();
            this.btnParamsPulse = new System.Windows.Forms.Button();
            this.txtFSS = new System.Windows.Forms.TextBox();
            this.lblA1_A2 = new System.Windows.Forms.Label();
            this.lblA2A1 = new System.Windows.Forms.Label();
            this.txtA2A1 = new System.Windows.Forms.TextBox();
            this.lblFSS = new System.Windows.Forms.Label();
            this.txtK = new System.Windows.Forms.TextBox();
            this.txtK2 = new System.Windows.Forms.TextBox();
            this.lblK = new System.Windows.Forms.Label();
            this.lblK2 = new System.Windows.Forms.Label();
            this.txtK1 = new System.Windows.Forms.TextBox();
            this.lblK1 = new System.Windows.Forms.Label();
            this.txtD = new System.Windows.Forms.TextBox();
            this.txtB = new System.Windows.Forms.TextBox();
            this.lblD = new System.Windows.Forms.Label();
            this.txtC = new System.Windows.Forms.TextBox();
            this.lblB = new System.Windows.Forms.Label();
            this.txtA = new System.Windows.Forms.TextBox();
            this.lblC = new System.Windows.Forms.Label();
            this.lblA = new System.Windows.Forms.Label();
            this.txtFSSOpor = new System.Windows.Forms.TextBox();
            this.lblFSSOpor2 = new System.Windows.Forms.Label();
            this.lblFSSOpor = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.tmrButton = new System.Windows.Forms.Timer(this.components);
            this.menuStripBuffer.SuspendLayout();
            this.cmsPulseWave.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdMeasurements)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.grpParams.SuspendLayout();
            this.grpS.SuspendLayout();
            this.grpParamsPulse.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudA1_A2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPulseWaveData
            // 
            this.lblPulseWaveData.AutoSize = true;
            this.lblPulseWaveData.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblPulseWaveData.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblPulseWaveData.Location = new System.Drawing.Point(0, 0);
            this.lblPulseWaveData.Name = "lblPulseWaveData";
            this.lblPulseWaveData.Size = new System.Drawing.Size(165, 15);
            this.lblPulseWaveData.TabIndex = 0;
            this.lblPulseWaveData.Text = "Записанный фрагмент ПВ:";
            // 
            // menuStripBuffer
            // 
            this.menuStripBuffer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFile,
            this.mnuSettings,
            this.mnuHelp});
            this.menuStripBuffer.Location = new System.Drawing.Point(5, 0);
            this.menuStripBuffer.Name = "menuStripBuffer";
            this.menuStripBuffer.Size = new System.Drawing.Size(824, 24);
            this.menuStripBuffer.TabIndex = 1;
            this.menuStripBuffer.Text = "menuStrip1";
            // 
            // mnuFile
            // 
            this.mnuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuOpen,
            this.toolStripSeparator1,
            this.mnuDelete,
            this.toolStripSeparator2,
            this.mnuPageSetup,
            this.mnuPrintPreview,
            this.mnuPrint,
            this.toolStripSeparator5,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Size = new System.Drawing.Size(48, 20);
            this.mnuFile.Text = "&Файл";
            // 
            // mnuOpen
            // 
            this.mnuOpen.Image = global::TonClock.Properties.Resources.openfolderHS;
            this.mnuOpen.Name = "mnuOpen";
            this.mnuOpen.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.mnuOpen.Size = new System.Drawing.Size(233, 22);
            this.mnuOpen.Text = "&Открыть...";
            this.mnuOpen.Click += new System.EventHandler(this.mnuOpen_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(230, 6);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Image = global::TonClock.Properties.Resources.DeleteHS;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.ShortcutKeys = System.Windows.Forms.Keys.Delete;
            this.mnuDelete.Size = new System.Drawing.Size(233, 22);
            this.mnuDelete.Text = "&Удалить замер";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(230, 6);
            // 
            // mnuPageSetup
            // 
            this.mnuPageSetup.Enabled = false;
            this.mnuPageSetup.Image = global::TonClock.Properties.Resources.PrintSetupHS;
            this.mnuPageSetup.Name = "mnuPageSetup";
            this.mnuPageSetup.Size = new System.Drawing.Size(233, 22);
            this.mnuPageSetup.Text = "Пара&метры страницы";
            this.mnuPageSetup.Click += new System.EventHandler(this.mnuPageSetup_Click);
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Enabled = false;
            this.mnuPrintPreview.Image = global::TonClock.Properties.Resources.PrintPreviewHS;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Size = new System.Drawing.Size(233, 22);
            this.mnuPrintPreview.Text = "Пред&варительный просмотр";
            this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // mnuPrint
            // 
            this.mnuPrint.Enabled = false;
            this.mnuPrint.Image = global::TonClock.Properties.Resources.PrintHS;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.mnuPrint.Size = new System.Drawing.Size(233, 22);
            this.mnuPrint.Text = "&Печать...";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(230, 6);
            // 
            // mnuExit
            // 
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.mnuExit.Size = new System.Drawing.Size(233, 22);
            this.mnuExit.Text = "&Закрыть окно";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuSettings
            // 
            this.mnuSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuVario,
            this.mnuGlobal,
            this.mnuCasp,
            this.toolStripSeparator3,
            this.mnuStatist,
            this.toolStripSeparator4,
            this.mnuProperties});
            this.mnuSettings.Name = "mnuSettings";
            this.mnuSettings.Size = new System.Drawing.Size(59, 20);
            this.mnuSettings.Text = "&Сервис";
            // 
            // mnuVario
            // 
            this.mnuVario.Image = global::TonClock.Properties.Resources.graphhs;
            this.mnuVario.Name = "mnuVario";
            this.mnuVario.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.mnuVario.Size = new System.Drawing.Size(214, 22);
            this.mnuVario.Text = "&Вариабельность...";
            this.mnuVario.Click += new System.EventHandler(this.mnuVario_Click);
            // 
            // mnuGlobal
            // 
            this.mnuGlobal.Image = global::TonClock.Properties.Resources.CalculatorHS;
            this.mnuGlobal.Name = "mnuGlobal";
            this.mnuGlobal.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.G)));
            this.mnuGlobal.Size = new System.Drawing.Size(214, 22);
            this.mnuGlobal.Text = "&Глобал...";
            this.mnuGlobal.Visible = false;
            this.mnuGlobal.Click += new System.EventHandler(this.mnuGlobal_Click);
            // 
            // mnuCasp
            // 
            this.mnuCasp.Image = global::TonClock.Properties.Resources.CaspIcon;
            this.mnuCasp.Name = "mnuCasp";
            this.mnuCasp.Size = new System.Drawing.Size(214, 22);
            this.mnuCasp.Text = "CASP...";
            this.mnuCasp.Click += new System.EventHandler(this.mnuCasp_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(211, 6);
            // 
            // mnuStatist
            // 
            this.mnuStatist.Name = "mnuStatist";
            this.mnuStatist.Size = new System.Drawing.Size(214, 22);
            this.mnuStatist.Text = "Режим статистики";
            this.mnuStatist.Click += new System.EventHandler(this.mnuStatist_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(211, 6);
            // 
            // mnuProperties
            // 
            this.mnuProperties.Name = "mnuProperties";
            this.mnuProperties.Size = new System.Drawing.Size(214, 22);
            this.mnuProperties.Text = "&Настройка...";
            this.mnuProperties.Click += new System.EventHandler(this.mnuProperties_Click);
            // 
            // mnuHelp
            // 
            this.mnuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuAbout});
            this.mnuHelp.Name = "mnuHelp";
            this.mnuHelp.Size = new System.Drawing.Size(65, 20);
            this.mnuHelp.Text = "Сп&равка";
            // 
            // mnuAbout
            // 
            this.mnuAbout.Name = "mnuAbout";
            this.mnuAbout.Size = new System.Drawing.Size(158, 22);
            this.mnuAbout.Text = "&О программе...";
            this.mnuAbout.Click += new System.EventHandler(this.mnuAbout_Click);
            // 
            // pnlPulseData
            // 
            this.pnlPulseData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlPulseData.AutoScroll = true;
            this.pnlPulseData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPulseData.Location = new System.Drawing.Point(0, 18);
            this.pnlPulseData.Name = "pnlPulseData";
            this.pnlPulseData.Size = new System.Drawing.Size(818, 142);
            this.pnlPulseData.TabIndex = 2;
            this.pnlPulseData.Scroll += new System.Windows.Forms.ScrollEventHandler(this.pnlPulseData_Scroll);
            this.pnlPulseData.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlPulseData_Paint);
            // 
            // lblWave
            // 
            this.lblWave.AutoSize = true;
            this.lblWave.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblWave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblWave.Location = new System.Drawing.Point(0, 0);
            this.lblWave.Name = "lblWave";
            this.lblWave.Size = new System.Drawing.Size(80, 15);
            this.lblWave.TabIndex = 3;
            this.lblWave.Text = "Средняя ПВ:";
            // 
            // pnlPulse
            // 
            this.pnlPulse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlPulse.AutoScroll = true;
            this.pnlPulse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPulse.ContextMenuStrip = this.cmsPulseWave;
            this.pnlPulse.Location = new System.Drawing.Point(0, 18);
            this.pnlPulse.Name = "pnlPulse";
            this.pnlPulse.Size = new System.Drawing.Size(240, 109);
            this.pnlPulse.TabIndex = 2;
            this.pnlPulse.Scroll += new System.Windows.Forms.ScrollEventHandler(this.pnlPulse_Scroll);
            this.pnlPulse.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlPulse_Paint);
            this.pnlPulse.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlPulse_MouseDown);
            this.pnlPulse.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlPulse_MouseMove);
            this.pnlPulse.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pnlPulse_MouseUp);
            // 
            // cmsPulseWave
            // 
            this.cmsPulseWave.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuPulseWaveAuto});
            this.cmsPulseWave.Name = "cmsPulseWave";
            this.cmsPulseWave.Size = new System.Drawing.Size(290, 26);
            // 
            // mnuPulseWaveAuto
            // 
            this.mnuPulseWaveAuto.Name = "mnuPulseWaveAuto";
            this.mnuPulseWaveAuto.Size = new System.Drawing.Size(289, 22);
            this.mnuPulseWaveAuto.Text = "Автоматическое определение реперов";
            this.mnuPulseWaveAuto.Click += new System.EventHandler(this.mnuPulseWaveAuto_Click);
            // 
            // pnlStress
            // 
            this.pnlStress.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlStress.AutoScroll = true;
            this.pnlStress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlStress.Location = new System.Drawing.Point(0, 18);
            this.pnlStress.Name = "pnlStress";
            this.pnlStress.Size = new System.Drawing.Size(240, 110);
            this.pnlStress.TabIndex = 2;
            this.pnlStress.Scroll += new System.Windows.Forms.ScrollEventHandler(this.pnlStress_Scroll);
            this.pnlStress.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlStress_Paint);
            // 
            // lblStressPic
            // 
            this.lblStressPic.AutoSize = true;
            this.lblStressPic.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblStressPic.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblStressPic.Location = new System.Drawing.Point(0, 0);
            this.lblStressPic.Name = "lblStressPic";
            this.lblStressPic.Size = new System.Drawing.Size(51, 15);
            this.lblStressPic.TabIndex = 3;
            this.lblStressPic.Text = "Стресс:";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblTitle.Location = new System.Drawing.Point(0, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(116, 15);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Замеры пациента:";
            // 
            // grdMeasurements
            // 
            this.grdMeasurements.AllowUserToAddRows = false;
            this.grdMeasurements.AllowUserToDeleteRows = false;
            this.grdMeasurements.AllowUserToResizeRows = false;
            this.grdMeasurements.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grdMeasurements.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdMeasurements.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdMeasurements.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clmIdDb,
            this.clmControl,
            this.clmNumber,
            this.clmDate,
            this.clmTime,
            this.clmSosud,
            this.clmTop,
            this.clmLower,
            this.clmGlobalTop,
            this.clmGlobalLower,
            this.clmKorotkovTop,
            this.clmKorotkovLower,
            this.clmXgdTop,
            this.clmXgdLower,
            this.clmMtpTop,
            this.clmMtpLower,
            this.clmPulse,
            this.clmStress,
            this.clmMode,
            this.clmSignal,
            this.clmComment,
            this.clmComparison});
            this.grdMeasurements.EnableHeadersVisualStyles = false;
            this.grdMeasurements.Location = new System.Drawing.Point(0, 18);
            this.grdMeasurements.MultiSelect = false;
            this.grdMeasurements.Name = "grdMeasurements";
            this.grdMeasurements.RowHeadersWidth = 25;
            this.grdMeasurements.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdMeasurements.Size = new System.Drawing.Size(818, 209);
            this.grdMeasurements.TabIndex = 7;
            this.grdMeasurements.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdMeasurements_CellContentClick);
            this.grdMeasurements.CellPainting += new System.Windows.Forms.DataGridViewCellPaintingEventHandler(this.grdMeasurements_CellPainting);
            this.grdMeasurements.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdMeasurements_CellValidated);
            this.grdMeasurements.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.grdMeasurements_CellValidating);
            this.grdMeasurements.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdMeasurements_CellValueChanged);
            this.grdMeasurements.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.grdMeasurements_ColumnWidthChanged);
            this.grdMeasurements.Scroll += new System.Windows.Forms.ScrollEventHandler(this.grdMeasurements_Scroll);
            this.grdMeasurements.SelectionChanged += new System.EventHandler(this.grdMeasurements_SelectionChanged);
            this.grdMeasurements.Paint += new System.Windows.Forms.PaintEventHandler(this.grdMeasurements_Paint);
            // 
            // clmIdDb
            // 
            this.clmIdDb.HeaderText = "idDb";
            this.clmIdDb.Name = "clmIdDb";
            this.clmIdDb.ReadOnly = true;
            this.clmIdDb.Visible = false;
            // 
            // clmControl
            // 
            this.clmControl.HeaderText = "Контроль тонометром";
            this.clmControl.Name = "clmControl";
            this.clmControl.ReadOnly = true;
            this.clmControl.Visible = false;
            // 
            // clmNumber
            // 
            this.clmNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmNumber.HeaderText = "№";
            this.clmNumber.Name = "clmNumber";
            this.clmNumber.ReadOnly = true;
            this.clmNumber.Width = 43;
            // 
            // clmDate
            // 
            this.clmDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmDate.HeaderText = "Дата";
            this.clmDate.Name = "clmDate";
            this.clmDate.ReadOnly = true;
            this.clmDate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmDate.Width = 39;
            // 
            // clmTime
            // 
            this.clmTime.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmTime.HeaderText = "Время";
            this.clmTime.Name = "clmTime";
            this.clmTime.ReadOnly = true;
            this.clmTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.clmTime.Width = 46;
            // 
            // clmSosud
            // 
            this.clmSosud.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmSosud.HeaderText = "Сосуды";
            this.clmSosud.Name = "clmSosud";
            this.clmSosud.ReadOnly = true;
            this.clmSosud.Width = 70;
            // 
            // clmTop
            // 
            this.clmTop.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmTop.HeaderText = "АД в.";
            this.clmTop.Name = "clmTop";
            this.clmTop.ReadOnly = true;
            this.clmTop.Width = 60;
            // 
            // clmLower
            // 
            this.clmLower.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmLower.HeaderText = "АД н.";
            this.clmLower.Name = "clmLower";
            this.clmLower.ReadOnly = true;
            this.clmLower.Width = 60;
            // 
            // clmGlobalTop
            // 
            this.clmGlobalTop.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmGlobalTop.HeaderText = "АД в.";
            this.clmGlobalTop.Name = "clmGlobalTop";
            this.clmGlobalTop.ReadOnly = true;
            this.clmGlobalTop.Visible = false;
            // 
            // clmGlobalLower
            // 
            this.clmGlobalLower.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmGlobalLower.HeaderText = "АД н.";
            this.clmGlobalLower.Name = "clmGlobalLower";
            this.clmGlobalLower.ReadOnly = true;
            this.clmGlobalLower.Visible = false;
            // 
            // clmKorotkovTop
            // 
            this.clmKorotkovTop.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmKorotkovTop.HeaderText = "АД в.";
            this.clmKorotkovTop.Name = "clmKorotkovTop";
            this.clmKorotkovTop.Visible = false;
            // 
            // clmKorotkovLower
            // 
            this.clmKorotkovLower.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmKorotkovLower.HeaderText = "АД н.";
            this.clmKorotkovLower.Name = "clmKorotkovLower";
            this.clmKorotkovLower.Visible = false;
            // 
            // clmXgdTop
            // 
            this.clmXgdTop.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmXgdTop.HeaderText = "АД в.";
            this.clmXgdTop.Name = "clmXgdTop";
            this.clmXgdTop.Visible = false;
            // 
            // clmXgdLower
            // 
            this.clmXgdLower.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmXgdLower.HeaderText = "АД н.";
            this.clmXgdLower.Name = "clmXgdLower";
            this.clmXgdLower.Visible = false;
            // 
            // clmMtpTop
            // 
            this.clmMtpTop.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmMtpTop.HeaderText = "АД в.";
            this.clmMtpTop.Name = "clmMtpTop";
            this.clmMtpTop.Visible = false;
            // 
            // clmMtpLower
            // 
            this.clmMtpLower.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmMtpLower.HeaderText = "АД н.";
            this.clmMtpLower.Name = "clmMtpLower";
            this.clmMtpLower.Visible = false;
            // 
            // clmPulse
            // 
            this.clmPulse.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmPulse.HeaderText = "Пульс";
            this.clmPulse.Name = "clmPulse";
            this.clmPulse.ReadOnly = true;
            this.clmPulse.Width = 63;
            // 
            // clmStress
            // 
            this.clmStress.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmStress.HeaderText = "Стресс";
            this.clmStress.Name = "clmStress";
            this.clmStress.ReadOnly = true;
            this.clmStress.Width = 68;
            // 
            // clmMode
            // 
            this.clmMode.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.clmMode.HeaderText = "Режим";
            this.clmMode.Name = "clmMode";
            this.clmMode.ReadOnly = true;
            this.clmMode.Width = 67;
            // 
            // clmSignal
            // 
            this.clmSignal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmSignal.HeaderText = "Сигнал";
            this.clmSignal.Name = "clmSignal";
            this.clmSignal.ReadOnly = true;
            this.clmSignal.Width = 68;
            // 
            // clmComment
            // 
            this.clmComment.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clmComment.FillWeight = 200F;
            this.clmComment.HeaderText = "Комментарий";
            this.clmComment.Name = "clmComment";
            // 
            // clmComparison
            // 
            this.clmComparison.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.clmComparison.HeaderText = "Рек.";
            this.clmComparison.Name = "clmComparison";
            this.clmComparison.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.clmComparison.Width = 54;
            // 
            // btnClearComparator
            // 
            this.btnClearComparator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearComparator.Enabled = false;
            this.btnClearComparator.Location = new System.Drawing.Point(0, 220);
            this.btnClearComparator.Name = "btnClearComparator";
            this.btnClearComparator.Size = new System.Drawing.Size(197, 41);
            this.btnClearComparator.TabIndex = 10;
            this.btnClearComparator.Text = "Очистить список волн";
            this.btnClearComparator.UseVisualStyleBackColor = false;
            this.btnClearComparator.Click += new System.EventHandler(this.btnClearComparator_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 375F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel5, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel6, 1, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 24);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(824, 666);
            this.tableLayoutPanel1.TabIndex = 11;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.tableLayoutPanel3);
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Controls.Add(this.lblComparisonStatist);
            this.panel1.Controls.Add(this.btnClearComparator);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(624, 402);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel1.SetRowSpan(this.panel1, 2);
            this.panel1.Size = new System.Drawing.Size(197, 261);
            this.panel1.TabIndex = 8;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.lstStatist2, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.lstStatist1, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 18);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(197, 196);
            this.tableLayoutPanel3.TabIndex = 12;
            this.tableLayoutPanel3.Visible = false;
            // 
            // lstStatist2
            // 
            this.lstStatist2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstStatist2.FormattingEnabled = true;
            this.lstStatist2.Items.AddRange(new object[] {
            "ТонКлок",
            "Глобал",
            "Коротков",
            "ХГД",
            "МТП"});
            this.lstStatist2.Location = new System.Drawing.Point(100, 0);
            this.lstStatist2.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.lstStatist2.Name = "lstStatist2";
            this.lstStatist2.Size = new System.Drawing.Size(97, 196);
            this.lstStatist2.TabIndex = 1;
            // 
            // lstStatist1
            // 
            this.lstStatist1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstStatist1.FormattingEnabled = true;
            this.lstStatist1.Items.AddRange(new object[] {
            "ТонКлок",
            "Глобал",
            "Коротков",
            "ХГД",
            "МТП"});
            this.lstStatist1.Location = new System.Drawing.Point(0, 0);
            this.lstStatist1.Margin = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.lstStatist1.Name = "lstStatist1";
            this.lstStatist1.Size = new System.Drawing.Size(96, 196);
            this.lstStatist1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.pnlComparison, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.pnlComparisonOneHeight, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 18);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(197, 196);
            this.tableLayoutPanel2.TabIndex = 11;
            // 
            // pnlComparison
            // 
            this.pnlComparison.AutoScroll = true;
            this.pnlComparison.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlComparison.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlComparison.Location = new System.Drawing.Point(0, 0);
            this.pnlComparison.Margin = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.pnlComparison.Name = "pnlComparison";
            this.pnlComparison.Size = new System.Drawing.Size(96, 196);
            this.pnlComparison.TabIndex = 8;
            this.pnlComparison.Scroll += new System.Windows.Forms.ScrollEventHandler(this.pnlComparison_Scroll);
            this.pnlComparison.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlComparison_Paint);
            // 
            // pnlComparisonOneHeight
            // 
            this.pnlComparisonOneHeight.AutoScroll = true;
            this.pnlComparisonOneHeight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlComparisonOneHeight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlComparisonOneHeight.Location = new System.Drawing.Point(100, 0);
            this.pnlComparisonOneHeight.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.pnlComparisonOneHeight.Name = "pnlComparisonOneHeight";
            this.pnlComparisonOneHeight.Size = new System.Drawing.Size(97, 196);
            this.pnlComparisonOneHeight.TabIndex = 8;
            this.pnlComparisonOneHeight.Scroll += new System.Windows.Forms.ScrollEventHandler(this.pnlComparisonOneHeight_Scroll);
            this.pnlComparisonOneHeight.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlComparisonOneHeight_Paint);
            this.pnlComparisonOneHeight.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlComparisonOneHeight_MouseDown);
            this.pnlComparisonOneHeight.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlComparisonOneHeight_MouseMove);
            this.pnlComparisonOneHeight.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pnlComparisonOneHeight_MouseUp);
            // 
            // lblComparisonStatist
            // 
            this.lblComparisonStatist.AutoSize = true;
            this.lblComparisonStatist.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblComparisonStatist.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblComparisonStatist.Location = new System.Drawing.Point(0, 0);
            this.lblComparisonStatist.Name = "lblComparisonStatist";
            this.lblComparisonStatist.Size = new System.Drawing.Size(117, 15);
            this.lblComparisonStatist.TabIndex = 9;
            this.lblComparisonStatist.Text = "Реконструкция ПВ:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblWave);
            this.panel2.Controls.Add(this.pnlPulse);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 402);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(240, 127);
            this.panel2.TabIndex = 9;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblStressPic);
            this.panel3.Controls.Add(this.pnlStress);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 535);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(240, 128);
            this.panel3.TabIndex = 10;
            // 
            // panel4
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel4, 3);
            this.panel4.Controls.Add(this.lblTitle);
            this.panel4.Controls.Add(this.grdMeasurements);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(818, 227);
            this.panel4.TabIndex = 11;
            // 
            // panel5
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel5, 3);
            this.panel5.Controls.Add(this.lblPulseWaveData);
            this.panel5.Controls.Add(this.pnlPulseData);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(3, 236);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(818, 160);
            this.panel5.TabIndex = 12;
            // 
            // panel6
            // 
            this.panel6.AutoScroll = true;
            this.panel6.Controls.Add(this.grpParams);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(249, 402);
            this.panel6.Name = "panel6";
            this.tableLayoutPanel1.SetRowSpan(this.panel6, 2);
            this.panel6.Size = new System.Drawing.Size(369, 261);
            this.panel6.TabIndex = 13;
            // 
            // grpParams
            // 
            this.grpParams.Controls.Add(this.grpS);
            this.grpParams.Controls.Add(this.btnParamsDB);
            this.grpParams.Controls.Add(this.grpParamsPulse);
            this.grpParams.Controls.Add(this.txtK);
            this.grpParams.Controls.Add(this.txtK2);
            this.grpParams.Controls.Add(this.lblK);
            this.grpParams.Controls.Add(this.lblK2);
            this.grpParams.Controls.Add(this.txtK1);
            this.grpParams.Controls.Add(this.lblK1);
            this.grpParams.Controls.Add(this.txtD);
            this.grpParams.Controls.Add(this.txtB);
            this.grpParams.Controls.Add(this.lblD);
            this.grpParams.Controls.Add(this.txtC);
            this.grpParams.Controls.Add(this.lblB);
            this.grpParams.Controls.Add(this.txtA);
            this.grpParams.Controls.Add(this.lblC);
            this.grpParams.Controls.Add(this.lblA);
            this.grpParams.Controls.Add(this.txtFSSOpor);
            this.grpParams.Controls.Add(this.lblFSSOpor2);
            this.grpParams.Controls.Add(this.lblFSSOpor);
            this.grpParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpParams.Location = new System.Drawing.Point(0, 0);
            this.grpParams.MinimumSize = new System.Drawing.Size(331, 240);
            this.grpParams.Name = "grpParams";
            this.grpParams.Size = new System.Drawing.Size(369, 261);
            this.grpParams.TabIndex = 7;
            this.grpParams.TabStop = false;
            this.grpParams.Text = "Параметры замера:";
            // 
            // grpS
            // 
            this.grpS.Controls.Add(this.txtS2);
            this.grpS.Controls.Add(this.lblS1);
            this.grpS.Controls.Add(this.txtS1);
            this.grpS.Controls.Add(this.lblS3);
            this.grpS.Controls.Add(this.lblS2);
            this.grpS.Location = new System.Drawing.Point(6, 169);
            this.grpS.Name = "grpS";
            this.grpS.Size = new System.Drawing.Size(143, 86);
            this.grpS.TabIndex = 39;
            this.grpS.TabStop = false;
            this.grpS.Text = "Сосуды";
            // 
            // txtS2
            // 
            this.txtS2.Location = new System.Drawing.Point(61, 53);
            this.txtS2.Name = "txtS2";
            this.txtS2.Size = new System.Drawing.Size(57, 20);
            this.txtS2.TabIndex = 33;
            this.txtS2.Text = "0,75";
            this.txtS2.TextChanged += new System.EventHandler(this.txtFSS_TextChanged);
            this.txtS2.Validating += new System.ComponentModel.CancelEventHandler(this.txtS2_Validating);
            // 
            // lblS1
            // 
            this.lblS1.AutoSize = true;
            this.lblS1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblS1.Location = new System.Drawing.Point(6, 15);
            this.lblS1.Name = "lblS1";
            this.lblS1.Size = new System.Drawing.Size(53, 15);
            this.lblS1.TabIndex = 30;
            this.lblS1.Text = "расшир.";
            // 
            // txtS1
            // 
            this.txtS1.Location = new System.Drawing.Point(61, 27);
            this.txtS1.Name = "txtS1";
            this.txtS1.Size = new System.Drawing.Size(57, 20);
            this.txtS1.TabIndex = 32;
            this.txtS1.Text = "0,42";
            this.txtS1.TextChanged += new System.EventHandler(this.txtFSS_TextChanged);
            this.txtS1.Validating += new System.ComponentModel.CancelEventHandler(this.txtS1_Validating);
            // 
            // lblS3
            // 
            this.lblS3.AutoSize = true;
            this.lblS3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblS3.Location = new System.Drawing.Point(6, 65);
            this.lblS3.Name = "lblS3";
            this.lblS3.Size = new System.Drawing.Size(45, 15);
            this.lblS3.TabIndex = 28;
            this.lblS3.Text = "сжаты";
            // 
            // lblS2
            // 
            this.lblS2.AutoSize = true;
            this.lblS2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblS2.Location = new System.Drawing.Point(6, 40);
            this.lblS2.Name = "lblS2";
            this.lblS2.Size = new System.Drawing.Size(55, 15);
            this.lblS2.TabIndex = 29;
            this.lblS2.Text = "средние";
            // 
            // btnParamsDB
            // 
            this.btnParamsDB.Image = global::TonClock.Properties.Resources.Check_16x16;
            this.btnParamsDB.Location = new System.Drawing.Point(158, 169);
            this.btnParamsDB.Name = "btnParamsDB";
            this.btnParamsDB.Size = new System.Drawing.Size(187, 86);
            this.btnParamsDB.TabIndex = 42;
            this.btnParamsDB.Text = "Принять параметры (пересчитать результаты и сохранить в базе)";
            this.btnParamsDB.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnParamsDB.UseVisualStyleBackColor = false;
            this.btnParamsDB.Click += new System.EventHandler(this.btnParamsDB_Click);
            // 
            // grpParamsPulse
            // 
            this.grpParamsPulse.Controls.Add(this.nudA1_A2);
            this.grpParamsPulse.Controls.Add(this.btnParamsPulse);
            this.grpParamsPulse.Controls.Add(this.txtFSS);
            this.grpParamsPulse.Controls.Add(this.lblA1_A2);
            this.grpParamsPulse.Controls.Add(this.lblA2A1);
            this.grpParamsPulse.Controls.Add(this.txtA2A1);
            this.grpParamsPulse.Controls.Add(this.lblFSS);
            this.grpParamsPulse.Location = new System.Drawing.Point(6, 19);
            this.grpParamsPulse.Name = "grpParamsPulse";
            this.grpParamsPulse.Size = new System.Drawing.Size(143, 144);
            this.grpParamsPulse.TabIndex = 28;
            this.grpParamsPulse.TabStop = false;
            this.grpParamsPulse.Text = "По ПВ";
            // 
            // nudA1_A2
            // 
            this.nudA1_A2.Location = new System.Drawing.Point(61, 118);
            this.nudA1_A2.Name = "nudA1_A2";
            this.nudA1_A2.Size = new System.Drawing.Size(57, 20);
            this.nudA1_A2.TabIndex = 35;
            this.nudA1_A2.ValueChanged += new System.EventHandler(this.nudA1_A2_ValueChanged);
            // 
            // btnParamsPulse
            // 
            this.btnParamsPulse.Location = new System.Drawing.Point(9, 70);
            this.btnParamsPulse.Name = "btnParamsPulse";
            this.btnParamsPulse.Size = new System.Drawing.Size(107, 38);
            this.btnParamsPulse.TabIndex = 31;
            this.btnParamsPulse.Text = "Определить по пульсовой волне";
            this.btnParamsPulse.UseVisualStyleBackColor = false;
            this.btnParamsPulse.Click += new System.EventHandler(this.btnParamsPulse_Click);
            // 
            // txtFSS
            // 
            this.txtFSS.Location = new System.Drawing.Point(61, 18);
            this.txtFSS.Name = "txtFSS";
            this.txtFSS.Size = new System.Drawing.Size(57, 20);
            this.txtFSS.TabIndex = 29;
            this.txtFSS.TextChanged += new System.EventHandler(this.txtFSS_TextChanged);
            this.txtFSS.Validating += new System.ComponentModel.CancelEventHandler(this.txtFSS_Validating);
            // 
            // lblA1_A2
            // 
            this.lblA1_A2.AutoSize = true;
            this.lblA1_A2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblA1_A2.Location = new System.Drawing.Point(8, 118);
            this.lblA1_A2.Name = "lblA1_A2";
            this.lblA1_A2.Size = new System.Drawing.Size(48, 15);
            this.lblA1_A2.TabIndex = 34;
            this.lblA1_A2.Text = "А2 - А1:";
            // 
            // lblA2A1
            // 
            this.lblA2A1.AutoSize = true;
            this.lblA2A1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblA2A1.Location = new System.Drawing.Point(8, 45);
            this.lblA2A1.Name = "lblA2A1";
            this.lblA2A1.Size = new System.Drawing.Size(47, 15);
            this.lblA2A1.TabIndex = 34;
            this.lblA2A1.Text = "А2 / А1:";
            // 
            // txtA2A1
            // 
            this.txtA2A1.Location = new System.Drawing.Point(61, 44);
            this.txtA2A1.Name = "txtA2A1";
            this.txtA2A1.Size = new System.Drawing.Size(57, 20);
            this.txtA2A1.TabIndex = 30;
            this.txtA2A1.TextChanged += new System.EventHandler(this.txtFSS_TextChanged);
            this.txtA2A1.Validating += new System.ComponentModel.CancelEventHandler(this.txtA2A1_Validating);
            // 
            // lblFSS
            // 
            this.lblFSS.AutoSize = true;
            this.lblFSS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblFSS.Location = new System.Drawing.Point(8, 19);
            this.lblFSS.Name = "lblFSS";
            this.lblFSS.Size = new System.Drawing.Size(34, 15);
            this.lblFSS.TabIndex = 33;
            this.lblFSS.Text = "ЧСС:";
            // 
            // txtK
            // 
            this.txtK.Location = new System.Drawing.Point(184, 94);
            this.txtK.Name = "txtK";
            this.txtK.Size = new System.Drawing.Size(57, 20);
            this.txtK.TabIndex = 37;
            this.txtK.Text = "2";
            this.txtK.TextChanged += new System.EventHandler(this.txtFSS_TextChanged);
            this.txtK.Validating += new System.ComponentModel.CancelEventHandler(this.txtK_Validating);
            // 
            // txtK2
            // 
            this.txtK2.Location = new System.Drawing.Point(184, 70);
            this.txtK2.Name = "txtK2";
            this.txtK2.Size = new System.Drawing.Size(57, 20);
            this.txtK2.TabIndex = 36;
            this.txtK2.Text = "2,875";
            this.txtK2.TextChanged += new System.EventHandler(this.txtFSS_TextChanged);
            this.txtK2.Validating += new System.ComponentModel.CancelEventHandler(this.txtK2_Validating);
            // 
            // lblK
            // 
            this.lblK.AutoSize = true;
            this.lblK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblK.Location = new System.Drawing.Point(155, 96);
            this.lblK.Name = "lblK";
            this.lblK.Size = new System.Drawing.Size(16, 15);
            this.lblK.TabIndex = 15;
            this.lblK.Text = "k:";
            // 
            // lblK2
            // 
            this.lblK2.AutoSize = true;
            this.lblK2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblK2.Location = new System.Drawing.Point(155, 71);
            this.lblK2.Name = "lblK2";
            this.lblK2.Size = new System.Drawing.Size(26, 15);
            this.lblK2.TabIndex = 14;
            this.lblK2.Text = "k2:";
            // 
            // txtK1
            // 
            this.txtK1.Location = new System.Drawing.Point(184, 44);
            this.txtK1.Name = "txtK1";
            this.txtK1.Size = new System.Drawing.Size(57, 20);
            this.txtK1.TabIndex = 35;
            this.txtK1.Text = "1,917";
            this.txtK1.TextChanged += new System.EventHandler(this.txtFSS_TextChanged);
            this.txtK1.Validating += new System.ComponentModel.CancelEventHandler(this.txtK1_Validating);
            // 
            // lblK1
            // 
            this.lblK1.AutoSize = true;
            this.lblK1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblK1.Location = new System.Drawing.Point(155, 45);
            this.lblK1.Name = "lblK1";
            this.lblK1.Size = new System.Drawing.Size(26, 15);
            this.lblK1.TabIndex = 13;
            this.lblK1.Text = "k1:";
            // 
            // txtD
            // 
            this.txtD.Location = new System.Drawing.Point(288, 120);
            this.txtD.Name = "txtD";
            this.txtD.Size = new System.Drawing.Size(57, 20);
            this.txtD.TabIndex = 41;
            this.txtD.Text = "1";
            this.txtD.TextChanged += new System.EventHandler(this.txtFSS_TextChanged);
            this.txtD.Validating += new System.ComponentModel.CancelEventHandler(this.txtD_Validating);
            // 
            // txtB
            // 
            this.txtB.Location = new System.Drawing.Point(288, 69);
            this.txtB.Name = "txtB";
            this.txtB.Size = new System.Drawing.Size(57, 20);
            this.txtB.TabIndex = 39;
            this.txtB.Text = "2";
            this.txtB.TextChanged += new System.EventHandler(this.txtFSS_TextChanged);
            this.txtB.Validating += new System.ComponentModel.CancelEventHandler(this.txtB_Validating);
            // 
            // lblD
            // 
            this.lblD.AutoSize = true;
            this.lblD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblD.Location = new System.Drawing.Point(265, 121);
            this.lblD.Name = "lblD";
            this.lblD.Size = new System.Drawing.Size(17, 15);
            this.lblD.TabIndex = 7;
            this.lblD.Text = "d:";
            // 
            // txtC
            // 
            this.txtC.Location = new System.Drawing.Point(288, 95);
            this.txtC.Name = "txtC";
            this.txtC.Size = new System.Drawing.Size(57, 20);
            this.txtC.TabIndex = 40;
            this.txtC.Text = "0,1";
            this.txtC.TextChanged += new System.EventHandler(this.txtFSS_TextChanged);
            this.txtC.Validating += new System.ComponentModel.CancelEventHandler(this.txtC_Validating);
            // 
            // lblB
            // 
            this.lblB.AutoSize = true;
            this.lblB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblB.Location = new System.Drawing.Point(265, 70);
            this.lblB.Name = "lblB";
            this.lblB.Size = new System.Drawing.Size(17, 15);
            this.lblB.TabIndex = 6;
            this.lblB.Text = "b:";
            // 
            // txtA
            // 
            this.txtA.Location = new System.Drawing.Point(288, 44);
            this.txtA.Name = "txtA";
            this.txtA.Size = new System.Drawing.Size(57, 20);
            this.txtA.TabIndex = 38;
            this.txtA.Text = "0,7";
            this.txtA.TextChanged += new System.EventHandler(this.txtFSS_TextChanged);
            this.txtA.Validating += new System.ComponentModel.CancelEventHandler(this.txtA_Validating);
            // 
            // lblC
            // 
            this.lblC.AutoSize = true;
            this.lblC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblC.Location = new System.Drawing.Point(265, 95);
            this.lblC.Name = "lblC";
            this.lblC.Size = new System.Drawing.Size(16, 15);
            this.lblC.TabIndex = 5;
            this.lblC.Text = "c:";
            // 
            // lblA
            // 
            this.lblA.AutoSize = true;
            this.lblA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblA.Location = new System.Drawing.Point(265, 45);
            this.lblA.Name = "lblA";
            this.lblA.Size = new System.Drawing.Size(17, 15);
            this.lblA.TabIndex = 8;
            this.lblA.Text = "a:";
            // 
            // txtFSSOpor
            // 
            this.txtFSSOpor.Location = new System.Drawing.Point(229, 18);
            this.txtFSSOpor.Name = "txtFSSOpor";
            this.txtFSSOpor.Size = new System.Drawing.Size(57, 20);
            this.txtFSSOpor.TabIndex = 34;
            this.txtFSSOpor.Text = "80";
            this.txtFSSOpor.TextChanged += new System.EventHandler(this.txtFSS_TextChanged);
            this.txtFSSOpor.Validating += new System.ComponentModel.CancelEventHandler(this.txtFSSOpor_Validating);
            // 
            // lblFSSOpor2
            // 
            this.lblFSSOpor2.AutoSize = true;
            this.lblFSSOpor2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblFSSOpor2.Location = new System.Drawing.Point(294, 19);
            this.lblFSSOpor2.Name = "lblFSSOpor2";
            this.lblFSSOpor2.Size = new System.Drawing.Size(45, 15);
            this.lblFSSOpor2.TabIndex = 3;
            this.lblFSSOpor2.Text = "уд/мин";
            // 
            // lblFSSOpor
            // 
            this.lblFSSOpor.AutoSize = true;
            this.lblFSSOpor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblFSSOpor.Location = new System.Drawing.Point(155, 19);
            this.lblFSSOpor.Name = "lblFSSOpor";
            this.lblFSSOpor.Size = new System.Drawing.Size(68, 15);
            this.lblFSSOpor.TabIndex = 3;
            this.lblFSSOpor.Text = "ЧСС опор.:";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // tmrButton
            // 
            this.tmrButton.Enabled = true;
            this.tmrButton.Interval = 400;
            this.tmrButton.Tick += new System.EventHandler(this.tmrButton_Tick);
            // 
            // frmBuffer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(834, 695);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.menuStripBuffer);
            this.DoubleBuffered = true;
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStripBuffer;
            this.MinimizeBox = false;
            this.Name = "frmBuffer";
            this.Padding = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Обработка данных";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBuffer_FormClosing);
            this.Load += new System.EventHandler(this.frmBuffer_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmBuffer_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmBuffer_KeyUp);
            this.Resize += new System.EventHandler(this.frmBuffer_Resize);
            this.menuStripBuffer.ResumeLayout(false);
            this.menuStripBuffer.PerformLayout();
            this.cmsPulseWave.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdMeasurements)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.grpParams.ResumeLayout(false);
            this.grpParams.PerformLayout();
            this.grpS.ResumeLayout(false);
            this.grpS.PerformLayout();
            this.grpParamsPulse.ResumeLayout(false);
            this.grpParamsPulse.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudA1_A2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPulseWaveData;
        private System.Windows.Forms.MenuStrip menuStripBuffer;
        private System.Windows.Forms.ToolStripMenuItem mnuFile;
        private System.Windows.Forms.ToolStripMenuItem mnuOpen;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mnuExit;
        private System.Windows.Forms.ToolStripMenuItem mnuSettings;
        private System.Windows.Forms.ToolStripMenuItem mnuProperties;
        private System.Windows.Forms.ToolStripMenuItem mnuHelp;
        private System.Windows.Forms.ToolStripMenuItem mnuAbout;
        private System.Windows.Forms.Panel pnlPulseData;
        private System.Windows.Forms.Label lblWave;
        private System.Windows.Forms.Panel pnlPulse;
        private System.Windows.Forms.Panel pnlStress;
        private System.Windows.Forms.Label lblStressPic;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.DataGridView grdMeasurements;
        private System.Windows.Forms.Button btnClearComparator;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox grpParams;
        private System.Windows.Forms.GroupBox grpS;
        private System.Windows.Forms.TextBox txtS2;
        private System.Windows.Forms.Label lblS1;
        private System.Windows.Forms.TextBox txtS1;
        private System.Windows.Forms.Label lblS3;
        private System.Windows.Forms.Label lblS2;
        private System.Windows.Forms.Button btnParamsDB;
        private System.Windows.Forms.GroupBox grpParamsPulse;
        private System.Windows.Forms.Button btnParamsPulse;
        private System.Windows.Forms.TextBox txtFSS;
        private System.Windows.Forms.Label lblA2A1;
        private System.Windows.Forms.TextBox txtA2A1;
        private System.Windows.Forms.Label lblFSS;
        private System.Windows.Forms.TextBox txtK;
        private System.Windows.Forms.TextBox txtK2;
        private System.Windows.Forms.Label lblK;
        private System.Windows.Forms.Label lblK2;
        private System.Windows.Forms.TextBox txtK1;
        private System.Windows.Forms.Label lblK1;
        private System.Windows.Forms.TextBox txtD;
        private System.Windows.Forms.TextBox txtB;
        private System.Windows.Forms.Label lblD;
        private System.Windows.Forms.TextBox txtC;
        private System.Windows.Forms.Label lblB;
        private System.Windows.Forms.TextBox txtA;
        private System.Windows.Forms.Label lblC;
        private System.Windows.Forms.Label lblA;
        private System.Windows.Forms.TextBox txtFSSOpor;
        private System.Windows.Forms.Label lblFSSOpor2;
        private System.Windows.Forms.Label lblFSSOpor;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblComparisonStatist;
        private System.Windows.Forms.Panel pnlComparison;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Panel pnlComparisonOneHeight;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ToolStripMenuItem mnuPageSetup;
        private System.Windows.Forms.ToolStripMenuItem mnuPrintPreview;
        private System.Windows.Forms.ToolStripMenuItem mnuPrint;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Drawing.Printing.PrintDocument printDocument1;
        private System.Windows.Forms.ToolStripMenuItem mnuVario;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem mnuGlobal;
        private System.Windows.Forms.ToolStripMenuItem mnuStatist;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ListBox lstStatist2;
        private System.Windows.Forms.ListBox lstStatist1;
        private System.Windows.Forms.Timer tmrButton;
        private System.Windows.Forms.ToolStripMenuItem mnuDelete;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ContextMenuStrip cmsPulseWave;
        private System.Windows.Forms.ToolStripMenuItem mnuPulseWaveAuto;
        private System.Windows.Forms.NumericUpDown nudA1_A2;
        private System.Windows.Forms.Label lblA1_A2;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmIdDb;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clmControl;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmSosud;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmTop;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmLower;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmGlobalTop;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmGlobalLower;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmKorotkovTop;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmKorotkovLower;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmXgdTop;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmXgdLower;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmMtpTop;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmMtpLower;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmPulse;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmStress;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmMode;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmSignal;
        private System.Windows.Forms.DataGridViewTextBoxColumn clmComment;
        private System.Windows.Forms.DataGridViewCheckBoxColumn clmComparison;
        private System.Windows.Forms.ToolStripMenuItem mnuCasp;
    }
}