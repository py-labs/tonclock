﻿//
// В классе ClockSettings хранятся настройки формы часов
//

using System.Configuration;

namespace TonClock
{

    public enum DeviceModes
    {
        Usb,        // проводной
        UsbEc,      // проводной эконом
        FtdiOld,    // беспроводной old
        FtdiNew,    // беспроводной new
        Bluetooth   // блютуз
    };

    sealed class ClockSettings : ApplicationSettingsBase
    {
        //Default constructor
        public ClockSettings()
        { }

        //Deep copy constructor
        public ClockSettings(ClockSettings other)
        {
            DeepCopy(other);
        }

        public void DeepCopy(ClockSettings other)
        {
            if (other == null)
                return;
            DeviceMode = other.DeviceMode;
            VendorId = other.VendorId;
            ProductId = other.ProductId;
            UsbEcCom = other.UsbEcCom;
            Dac1 = other.Dac1;
            Dac2 = other.Dac2;
            SrcId = other.SrcId;
            BtId = other.BtId;
            BtPin = other.BtPin;
            FlagSoundPulse = other.FlagSoundPulse;
            FlagSoundResult = other.FlagSoundResult;
            SoundPulseVolume = other.SoundPulseVolume;
            SoundPulsePitch = other.SoundPulsePitch;
            Rec = other.Rec;
            SoundRecVolume = other.SoundRecVolume;
            SoundResultVolume = other.SoundResultVolume;
            Voice = other.Voice;
            VoiceVolume = other.VoiceVolume;
            FlagLevel = other.FlagLevel;
        }


        // ----------------------------- Режим работы -----------------------------

        // Режим работы
        [UserScopedSetting()]
        [DefaultSettingValue("0")]
        public DeviceModes DeviceMode
        {
            get { return (DeviceModes)this["DeviceMode"]; }
            set { this["DeviceMode"] = value; }
        }

        // Регистр Usb - VendorId
        [UserScopedSetting()]
        [DefaultSettingValue("534b")]
        public string VendorId
        {
            get { return (string)this["VendorId"]; }
            set { this["VendorId"] = value; }
        }

        // Регистр Usb - ProductId
        [UserScopedSetting()]
        [DefaultSettingValue("0a01")]
        public string ProductId
        {
            get { return (string)this["ProductId"]; }
            set { this["ProductId"] = value; }
        }

        // Virtual Com Port (пустое поле - авто поиск)
        [UserScopedSetting()]
        [DefaultSettingValue("")]
        public string UsbEcCom
        {
            get { return (string)this["UsbEcCom"]; }
            set { this["UsbEcCom"] = value; }
        }

        // DAC1
        [UserScopedSetting()]
        [DefaultSettingValue("1600")]
        public ushort Dac1
        {
            get { return (ushort)this["Dac1"]; }
            set { this["Dac1"] = value; }
        }

        // DAC2
        [UserScopedSetting()]
        [DefaultSettingValue("3100")]
        public ushort Dac2
        {
            get { return (ushort)this["Dac2"]; }
            set { this["Dac2"] = value; }
        }

        // srcID прибора, с которым будет соединяться (-1 => с 1-м из доступных)
        [UserScopedSetting()]
        [DefaultSettingValue("-1")]
        public int SrcId
        {
            get { return (int)this["SrcId"]; }
            set { this["SrcId"] = value; }
        }

        // Bluetooth - ID
        [UserScopedSetting()]
        [DefaultSettingValue("PWAVE6")]
        public string BtId
        {
            get { return (string)this["BtId"]; }
            set { this["BtId"] = value; }
        }

        // Bluetooth - PIN
        [UserScopedSetting()]
        [DefaultSettingValue("0000")]
        public string BtPin
        {
            get { return (string)this["BtPin"]; }
            set { this["BtPin"] = value; }
        }


        // ----------------------------- Звук -----------------------------

        // Флаг, нужно ли озвучивать пульсовую волну
        [UserScopedSetting()]
        [DefaultSettingValue("true")]
        public bool FlagSoundPulse
        {
            get { return (bool)this["FlagSoundPulse"]; }
            set { this["FlagSoundPulse"] = value; }
        }

        // Флаг, нужно ли озвучивать результаты измерений
        [UserScopedSetting()]
        [DefaultSettingValue("true")]
        public bool FlagSoundResult
        {
            get { return (bool)this["FlagSoundResult"]; }
            set { this["FlagSoundResult"] = value; }
        }

        // Громкость озвучки пульсовой волны
        [UserScopedSetting()]
        [DefaultSettingValue("1")]
        public float SoundPulseVolume
        {
            get { return (float)this["SoundPulseVolume"]; }
            set { this["SoundPulseVolume"] = value; }
        }

        // Нота, используемая при озвучке пульсовой волны
        [UserScopedSetting()]
        [DefaultSettingValue("80")]
        public float SoundPulsePitch
        {
            get { return (float)this["SoundPulsePitch"]; }
            set { this["SoundPulsePitch"] = value; }
        }

        // Мелодия, используемая при записи
        [UserScopedSetting()]
        [DefaultSettingValue("0")]
        public int Rec
        {
            get { return (int)this["Rec"]; }
            set { this["Rec"] = value; }
        }

        // Громкость мелодии, используемой при записи
        [UserScopedSetting()]
        [DefaultSettingValue("1")]
        public float SoundRecVolume
        {
            get { return (float)this["SoundRecVolume"]; }
            set { this["SoundRecVolume"] = value; }
        }

        // Громкость озвучки результатов измерений
        [UserScopedSetting()]
        [DefaultSettingValue("0.2")]
        public float SoundResultVolume
        {
            get { return (float)this["SoundResultVolume"]; }
            set { this["SoundResultVolume"] = value; }
        }

        // Голос диктора
        [UserScopedSetting()]
        [DefaultSettingValue("0")]
        public int Voice
        {
            get { return (int)this["Voice"]; }
            set { this["Voice"] = value; }
        }

        // Громкость голоса
        [UserScopedSetting()]
        [DefaultSettingValue("1")]
        public float VoiceVolume
        {
            get { return (float)this["VoiceVolume"]; }
            set { this["VoiceVolume"] = value; }
        }

        // ----------------------------- Индикация -----------------------------

        // Флаг, что нужна индикация уровня сигнала
        [UserScopedSetting()]
        [DefaultSettingValue("true")]
        public bool FlagLevel
        {
            get { return (bool)this["FlagLevel"]; }
            set { this["FlagLevel"] = value; }
        }

        

    }
}
