﻿namespace TonClock
{
    partial class frmVario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bkwStress = new System.ComponentModel.BackgroundWorker();
            this.menuStripBuffer = new System.Windows.Forms.MenuStrip();
            this.mnuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPageSetup = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPrintPreview = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuPrint = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.prbStress = new System.Windows.Forms.ProgressBar();
            this.pnlStress = new System.Windows.Forms.Panel();
            this.lblStress = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pnlPulseWave = new System.Windows.Forms.Panel();
            this.lblPulseWave = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pnlIntegrAmplit = new System.Windows.Forms.Panel();
            this.lblIntegrAmplit = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pnlIntegrAmplitPhas = new System.Windows.Forms.Panel();
            this.lblIntegrAmplitPhas = new System.Windows.Forms.Label();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.menuStripBuffer.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // bkwStress
            // 
            this.bkwStress.WorkerReportsProgress = true;
            this.bkwStress.WorkerSupportsCancellation = true;
            this.bkwStress.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bkwStress_DoWork);
            this.bkwStress.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bkwStress_ProgressChanged);
            this.bkwStress.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bkwStress_RunWorkerCompleted);
            // 
            // menuStripBuffer
            // 
            this.menuStripBuffer.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuFile});
            this.menuStripBuffer.Location = new System.Drawing.Point(5, 0);
            this.menuStripBuffer.Name = "menuStripBuffer";
            this.menuStripBuffer.Size = new System.Drawing.Size(724, 24);
            this.menuStripBuffer.TabIndex = 1;
            this.menuStripBuffer.Text = "menuStrip1";
            // 
            // mnuFile
            // 
            this.mnuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuPageSetup,
            this.mnuPrintPreview,
            this.mnuPrint,
            this.toolStripSeparator2,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Size = new System.Drawing.Size(48, 20);
            this.mnuFile.Text = "&Файл";
            // 
            // mnuPageSetup
            // 
            this.mnuPageSetup.Enabled = false;
            this.mnuPageSetup.Image = global::TonClock.Properties.Resources.PrintSetupHS;
            this.mnuPageSetup.Name = "mnuPageSetup";
            this.mnuPageSetup.Size = new System.Drawing.Size(233, 22);
            this.mnuPageSetup.Text = "Пара&метры страницы";
            this.mnuPageSetup.Click += new System.EventHandler(this.mnuPageSetup_Click);
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Enabled = false;
            this.mnuPrintPreview.Image = global::TonClock.Properties.Resources.PrintPreviewHS;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Size = new System.Drawing.Size(233, 22);
            this.mnuPrintPreview.Text = "Пред&варительный просмотр";
            this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // mnuPrint
            // 
            this.mnuPrint.Enabled = false;
            this.mnuPrint.Image = global::TonClock.Properties.Resources.PrintHS;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.mnuPrint.Size = new System.Drawing.Size(233, 22);
            this.mnuPrint.Text = "&Печать...";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(230, 6);
            // 
            // mnuExit
            // 
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.mnuExit.Size = new System.Drawing.Size(233, 22);
            this.mnuExit.Text = "&Закрыть окно";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 24);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(724, 433);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.prbStress);
            this.panel1.Controls.Add(this.pnlStress);
            this.panel1.Controls.Add(this.lblStress);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(356, 138);
            this.panel1.TabIndex = 0;
            // 
            // prbStress
            // 
            this.prbStress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.prbStress.Location = new System.Drawing.Point(112, 0);
            this.prbStress.Name = "prbStress";
            this.prbStress.Size = new System.Drawing.Size(244, 15);
            this.prbStress.TabIndex = 5;
            this.prbStress.Visible = false;
            // 
            // pnlStress
            // 
            this.pnlStress.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlStress.AutoScroll = true;
            this.pnlStress.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlStress.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlStress.Location = new System.Drawing.Point(0, 18);
            this.pnlStress.Name = "pnlStress";
            this.pnlStress.Size = new System.Drawing.Size(356, 120);
            this.pnlStress.TabIndex = 3;
            this.pnlStress.Scroll += new System.Windows.Forms.ScrollEventHandler(this.pnlStress_Scroll);
            this.pnlStress.Click += new System.EventHandler(this.pnlStress_Click);
            this.pnlStress.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlStress_Paint);
            // 
            // lblStress
            // 
            this.lblStress.AutoSize = true;
            this.lblStress.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblStress.Location = new System.Drawing.Point(0, 0);
            this.lblStress.Name = "lblStress";
            this.lblStress.Size = new System.Drawing.Size(51, 15);
            this.lblStress.TabIndex = 0;
            this.lblStress.Text = "Стресс:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.pnlPulseWave);
            this.panel2.Controls.Add(this.lblPulseWave);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(365, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(356, 138);
            this.panel2.TabIndex = 1;
            // 
            // pnlPulseWave
            // 
            this.pnlPulseWave.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlPulseWave.AutoScroll = true;
            this.pnlPulseWave.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPulseWave.Location = new System.Drawing.Point(0, 18);
            this.pnlPulseWave.Name = "pnlPulseWave";
            this.pnlPulseWave.Size = new System.Drawing.Size(356, 120);
            this.pnlPulseWave.TabIndex = 4;
            this.pnlPulseWave.Scroll += new System.Windows.Forms.ScrollEventHandler(this.pnlPulseWave_Scroll);
            this.pnlPulseWave.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlPulseWave_Paint);
            // 
            // lblPulseWave
            // 
            this.lblPulseWave.AutoSize = true;
            this.lblPulseWave.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblPulseWave.Location = new System.Drawing.Point(0, 0);
            this.lblPulseWave.Name = "lblPulseWave";
            this.lblPulseWave.Size = new System.Drawing.Size(80, 15);
            this.lblPulseWave.TabIndex = 0;
            this.lblPulseWave.Text = "Средняя ПВ:";
            // 
            // panel3
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel3, 2);
            this.panel3.Controls.Add(this.pnlIntegrAmplit);
            this.panel3.Controls.Add(this.lblIntegrAmplit);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 147);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(718, 138);
            this.panel3.TabIndex = 2;
            // 
            // pnlIntegrAmplit
            // 
            this.pnlIntegrAmplit.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlIntegrAmplit.AutoScroll = true;
            this.pnlIntegrAmplit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlIntegrAmplit.Location = new System.Drawing.Point(0, 18);
            this.pnlIntegrAmplit.Name = "pnlIntegrAmplit";
            this.pnlIntegrAmplit.Size = new System.Drawing.Size(718, 120);
            this.pnlIntegrAmplit.TabIndex = 4;
            this.pnlIntegrAmplit.Scroll += new System.Windows.Forms.ScrollEventHandler(this.pnlIntegrAmplit_Scroll);
            this.pnlIntegrAmplit.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlIntegrAmplit_Paint);
            // 
            // lblIntegrAmplit
            // 
            this.lblIntegrAmplit.AutoSize = true;
            this.lblIntegrAmplit.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblIntegrAmplit.Location = new System.Drawing.Point(0, 0);
            this.lblIntegrAmplit.Name = "lblIntegrAmplit";
            this.lblIntegrAmplit.Size = new System.Drawing.Size(227, 15);
            this.lblIntegrAmplit.TabIndex = 0;
            this.lblIntegrAmplit.Text = "Амплитудная интегралогистограмма:";
            // 
            // panel4
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.panel4, 2);
            this.panel4.Controls.Add(this.pnlIntegrAmplitPhas);
            this.panel4.Controls.Add(this.lblIntegrAmplitPhas);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 291);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(718, 139);
            this.panel4.TabIndex = 3;
            // 
            // pnlIntegrAmplitPhas
            // 
            this.pnlIntegrAmplitPhas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlIntegrAmplitPhas.AutoScroll = true;
            this.pnlIntegrAmplitPhas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlIntegrAmplitPhas.Location = new System.Drawing.Point(0, 18);
            this.pnlIntegrAmplitPhas.Name = "pnlIntegrAmplitPhas";
            this.pnlIntegrAmplitPhas.Size = new System.Drawing.Size(718, 121);
            this.pnlIntegrAmplitPhas.TabIndex = 5;
            this.pnlIntegrAmplitPhas.Scroll += new System.Windows.Forms.ScrollEventHandler(this.pnlIntegrAmplitPhas_Scroll);
            this.pnlIntegrAmplitPhas.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlIntegrAmplitPhas_Paint);
            // 
            // lblIntegrAmplitPhas
            // 
            this.lblIntegrAmplitPhas.AutoSize = true;
            this.lblIntegrAmplitPhas.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblIntegrAmplitPhas.Location = new System.Drawing.Point(0, 0);
            this.lblIntegrAmplitPhas.Name = "lblIntegrAmplitPhas";
            this.lblIntegrAmplitPhas.Size = new System.Drawing.Size(269, 15);
            this.lblIntegrAmplitPhas.TabIndex = 0;
            this.lblIntegrAmplitPhas.Text = "Амплитудо-фазовая интегралогистограмма:";
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // frmVario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 462);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.menuStripBuffer);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStripBuffer;
            this.Name = "frmVario";
            this.Padding = new System.Windows.Forms.Padding(5, 0, 5, 5);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Вариабельность";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmVario_FormClosing);
            this.Load += new System.EventHandler(this.frmVario_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmVario_KeyDown);
            this.Resize += new System.EventHandler(this.frmVario_Resize);
            this.menuStripBuffer.ResumeLayout(false);
            this.menuStripBuffer.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.ComponentModel.BackgroundWorker bkwStress;
        private System.Windows.Forms.MenuStrip menuStripBuffer;
        private System.Windows.Forms.ToolStripMenuItem mnuFile;
        private System.Windows.Forms.ToolStripMenuItem mnuPageSetup;
        private System.Windows.Forms.ToolStripMenuItem mnuPrintPreview;
        private System.Windows.Forms.ToolStripMenuItem mnuPrint;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem mnuExit;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ProgressBar prbStress;
        private System.Windows.Forms.Panel pnlStress;
        private System.Windows.Forms.Label lblStress;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlPulseWave;
        private System.Windows.Forms.Label lblPulseWave;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel pnlIntegrAmplit;
        private System.Windows.Forms.Label lblIntegrAmplit;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel pnlIntegrAmplitPhas;
        private System.Windows.Forms.Label lblIntegrAmplitPhas;
        private System.Drawing.Printing.PrintDocument printDocument1;
    }
}