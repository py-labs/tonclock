﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Linq;
using System.Text;

namespace TonClock
{
    static class Molodimetr
    {

        //*****************************************************************
        //                      Отрисовка данных                          *
        //*****************************************************************

        private static RectangleF DrawBase(Graphics gr, Rectangle rct, string title, int bigRepers = 4, int smallRepers = 0, bool smallRepersDiv = true, bool colorsOrder = true, bool colorsTopBig = true)
        {
            gr.SmoothingMode = SmoothingMode.AntiAlias;
            gr.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
            // выводим заголовок
            if (!string.IsNullOrEmpty(title))
            {
                using (Font fntTitle = new Font("Microsoft Sans Serif", rct.Height/13f, FontStyle.Regular))
                {
                    using (StringFormat sf = new StringFormat())
                    {
                        sf.Alignment = StringAlignment.Center;
                        gr.DrawString(title, fntTitle, Brushes.Black, rct, sf);
                    }
                    int titleHeight = (int) gr.MeasureString(title, fntTitle).Height;
                    rct = new Rectangle(rct.X, rct.Y + titleHeight, rct.Width, rct.Height - titleHeight);
                }
            }
            // определяем квадрат
            if (rct.Width > rct.Height)
            {
                int delta = rct.Width - rct.Height;
                rct = new Rectangle(rct.X + delta/2, rct.Y, rct.Width - delta, rct.Height);
            }
            else if (rct.Width < rct.Height)
            {
                int delta = rct.Height - rct.Width;
                rct = new Rectangle(rct.X, rct.Y + delta / 2, rct.Width, rct.Height - delta);
            }
            // рисуем фон
            gr.DrawImage(Properties.Resources.molodimetr, rct);
            int border = 47*rct.Width/Properties.Resources.molodimetr.Width;
            rct = new Rectangle(rct.X + border, rct.Y + border, rct.Width - border*2, rct.Height - border*2);
            border = (rct.Height/2)/10;
            rct = new Rectangle(rct.X + border, rct.Y + border, rct.Width - border * 2, rct.Height - border * 2);
            PointF pCenter = new PointF(rct.X + rct.Width / 2f, rct.Y + rct.Height / 2f);
            float widthBlocks = border;
            RectangleF rctBlocks = new RectangleF(rct.X + widthBlocks / 2f, rct.Y + widthBlocks / 2f, rct.Width - widthBlocks, rct.Height - widthBlocks);
            Color clr1 = colorsOrder ? Color.Green : Color.Red,
                  clr2 = Color.Blue,
                  clr3 = colorsOrder ? Color.Red : Color.Green;
            float perexodGrad1 = colorsTopBig ? 225 : 243,
                  perexodGrad2 = colorsTopBig ? 315 : 297,
                  radius = rctBlocks.Width / 2f;
            float perexodGrad = 8;
            using (LinearGradientBrush lgb = new LinearGradientBrush(PCircle(pCenter, radius, perexodGrad1 - perexodGrad, true),
                PCircle(pCenter, radius, perexodGrad1 + perexodGrad, true), clr1, clr2))
            {
                using (Pen penBlocks = new Pen(lgb, widthBlocks))
                    gr.DrawArc(penBlocks, rctBlocks, perexodGrad1 - perexodGrad, perexodGrad * 2);
            }
            using (LinearGradientBrush lgb = new LinearGradientBrush(PCircle(pCenter, radius, perexodGrad2 - perexodGrad, true),
                PCircle(pCenter, radius, perexodGrad2 + perexodGrad, true), clr2, clr3))
            {
                using (Pen penBlocks = new Pen(lgb, widthBlocks))
                    gr.DrawArc(penBlocks, rctBlocks, perexodGrad2 - perexodGrad, perexodGrad * 2);
            }
            perexodGrad = 6;
            using (Pen penBlocks = new Pen(clr1, widthBlocks))
            {
                gr.DrawArc(penBlocks, rctBlocks, 135, (perexodGrad1 - perexodGrad) - 135);
                penBlocks.Color = clr2;
                gr.DrawArc(penBlocks, rctBlocks, perexodGrad1 + perexodGrad, (perexodGrad2 - perexodGrad1) - perexodGrad);
                penBlocks.Color = clr3;
                gr.DrawArc(penBlocks, rctBlocks, perexodGrad2 + perexodGrad, 360 - (perexodGrad2 + perexodGrad) + 45);
            }

            //Pen p = new Pen(Color.Black);
            //gr.DrawRectangle(p, rct);

            // рисуем черточки
            //DrawBase(Graphics gr, Rectangle rct, string title, int bigRepers = 4, int smallRepers = 0, bool smallRepersDiv = true, bool colorsOrder = true, bool colorsTopBig = true)
            using (Pen penLines = new Pen(_clrElements, 1))
            {
                gr.DrawArc(penLines, rct, 135, 270);
                penLines.Width = 7f*rct.Width/Properties.Resources.molodimetr.Width;
                if (bigRepers > 1)
                {
                    float angle = 225,
                          repersDist = 270f/(bigRepers - 1);
                    while (angle >= -45)
                    {
                        gr.DrawLine(penLines, PCircle(pCenter, rct.Width / 2f, angle),
                                    PCircle(pCenter, rct.Width / 2f - widthBlocks * 1.8f, angle));
                        if (smallRepers > 0 && (angle - repersDist) >= -45)
                        {
                            using (Pen penLinesSmall = new Pen(_clrElements, 1))
                            {
                                for (int i = 1; i < (smallRepers + 1); i++)
                                {
                                    if (smallRepersDiv && i == (smallRepers + 1)/2)
                                    {
                                        penLinesSmall.Width = penLines.Width/2f;
                                        gr.DrawLine(penLinesSmall,
                                                    PCircle(pCenter, rct.Width / 2f, angle - repersDist / (smallRepers + 1) * i),
                                                    PCircle(pCenter, rct.Width/2f - widthBlocks*1.5f,
                                                            angle - repersDist / (smallRepers + 1) * i));
                                    }
                                    else
                                    {
                                        penLinesSmall.Width = 1;
                                        gr.DrawLine(penLinesSmall,
                                                    PCircle(pCenter, rct.Width / 2f, angle - repersDist / (smallRepers + 1) * i),
                                                    PCircle(pCenter, rct.Width/2f - widthBlocks*1.2f,
                                                            angle - repersDist / (smallRepers + 1) * i));
                                    }
                                }
                            }
                        }
                        angle -= repersDist;
                    }
                }
            }
            //return new Rectangle(rct.X + (int)widthBlocks, rct.Y + (int)widthBlocks, rct.Width - (int)widthBlocks * 2, rct.Height - (int)widthBlocks * 2);
            return rct;
        }

        private static float XCircle(PointF center, float radius, float angle)
        {
            angle *= (float)(Math.PI/180);
            return center.X + radius*(float)Math.Cos(angle);
        }

        private static float YCircle(PointF center, float radius, float angle)
        {
            angle *= (float)(Math.PI / 180);
            return center.Y - radius * (float)Math.Sin(angle);
        }

        private static PointF PCircle(PointF center, float radius, float angle, bool inverse = false)
        {
            if (inverse)
                angle = 360 - angle;
            return new PointF(XCircle(center, radius, angle), YCircle(center, radius, angle));
        }

        //private static Point PCircleRound(PointF center, float radius, float angle)
        //{
        //    return new Point((int) Math.Round(XCircle(center, radius, angle)),
        //                     (int) Math.Round(YCircle(center, radius, angle)));
        //}

        private static Color _clrElements = Color.FromArgb(27, 27, 27);

        private static void DrawArrow(Graphics gr, RectangleF rct, float angle)
        {
            PointF pCenter = new PointF(rct.X + rct.Width / 2, rct.Y + rct.Height / 2);
            PointF[] points = new PointF[]
                                  {
                                      PCircle(pCenter, rct.Width/2 - rct.Width/30, angle - 1),
                                      PCircle(pCenter, rct.Width/2, angle),
                                      PCircle(pCenter, rct.Width/2 - rct.Width/30, angle + 1),
                                      PCircle(pCenter, rct.Width/10, angle + 180 - 15),
                                      PCircle(pCenter, rct.Width/10, angle + 180 + 15)
                                  };
            using (Brush br = new SolidBrush(_clrElements))
            {
                gr.FillPolygon(br, points);
                float rFix = rct.Width / 20;
                gr.FillEllipse(br, new RectangleF(pCenter.X - rFix, pCenter.Y - rFix, rFix * 2, rFix * 2));
            }
        }

        public static void DrawSosud(Graphics gr, Rectangle rctCanvas, int val)
        {
            try
            {
                RectangleF rct = DrawBase(gr, rctCanvas, String.Empty);
                PointF pCenter = new PointF(rct.X + rct.Width / 2, rct.Y + rct.Height / 2);
                using (Font fntText = new Font("Microsoft Sans Serif", rct.Width / 20, FontStyle.Regular))
                {
                    PointF pText = PCircle(pCenter, rct.Width / 2 - rct.Width / 10 - fntText.GetHeight(gr) / 2, 185);
                    //pText.X += rct.Width / 30;
                    gr.DrawString("расшир.", fntText, Brushes.Black, pText);
                    pText = PCircle(pCenter, rct.Width / 2 - rct.Width / 10 - fntText.GetHeight(gr) / 2, 90);
                    pText.X -= gr.MeasureString("средние", fntText).Width / 2;
                    //pText.Y += rct.Width / 30;
                    gr.DrawString("средние", fntText, Brushes.Black, pText);
                    pText = PCircle(pCenter, rct.Width / 2 - rct.Width / 10 - fntText.GetHeight(gr) / 2, -5);
                    pText.X -= gr.MeasureString("сжаты", fntText).Width;// + rct.Width / 40);
                    gr.DrawString("сжаты", fntText, Brushes.Black, pText);
                }
                using (Font fntText = new Font("Microsoft Sans Serif", rct.Width / 15, FontStyle.Regular))
                {
                    //for (float i = 0.2f; i <= 0.8f; i += 0.2f)
                    //{
                    //    PointF pText = PCircle(pCenter, rct.Width / 2 - rct.Width / 10 - rct.Width / 15,
                    //                           225 - 270 / 0.6f * (i - 0.2f));
                    //    string text = i.ToString();
                    //    SizeF mes = gr.MeasureString(text, fntText);
                    //    pText.X -= mes.Width / 2;
                    //    pText.Y -= mes.Height / 2;
                    //    gr.DrawString(text, fntText, Brushes.Black, pText);
                    //}
                    //string strMes = "отн.ед.";
                    string strMes = "сотояние";
                    PointF pTextMes = PCircle(pCenter, rct.Width / 2 - rct.Width / 10, -90);
                    pTextMes.X -= gr.MeasureString(strMes, fntText).Width / 2;
                    pTextMes.Y -= gr.MeasureString(strMes, fntText).Height / 2;
                    gr.DrawString(strMes, fntText, Brushes.Black, pTextMes);
                }
                // рисуем стрелку
                float angle = 90;
                if (val == 1)
                    angle = 180;
                else if (val == 3)
                    angle = 0;
                DrawArrow(gr, rct, angle);
            }
            catch (Exception)
            {
                gr.Clear(Color.Black);
            }
            
        }

        public static void DrawStress(Graphics gr, Rectangle rctCanvas, int val)
        {
            try
            {
                RectangleF rct = DrawBase(gr, rctCanvas, String.Empty, 6, 0, false, true, false);
                PointF pCenter = new PointF(rct.X + rct.Width / 2, rct.Y + rct.Height / 2);
                using (Font fntText = new Font("Microsoft Sans Serif", rct.Width / 15, FontStyle.Regular))
                {
                    for (int i = 0; i <= 5; i++)
                    {
                        PointF pText = PCircle(pCenter, rct.Width/2 - rct.Width/10 - rct.Width/15, 225 - 270/5f*i);
                        string text = i.ToString();
                        SizeF mes = gr.MeasureString(text, fntText);
                        pText.X -= mes.Width / 2;
                        pText.Y -= mes.Height / 2;
                        gr.DrawString(text, fntText, Brushes.Black, pText);
                    }
                    string strMes = "баллов";
                    PointF pTextMes = PCircle(pCenter, rct.Width / 2 - rct.Width / 10, -90);
                    pTextMes.X -= gr.MeasureString(strMes, fntText).Width / 2;
                    pTextMes.Y -= gr.MeasureString(strMes, fntText).Height / 2;
                    gr.DrawString(strMes, fntText, Brushes.Black, pTextMes);
                }
                // рисуем стрелку
                DrawArrow(gr, rct, 225 - 270/5f*val);
            }
            catch (Exception)
            {
                gr.Clear(Color.Black);
            }   
        }

        public static void DrawPulse(Graphics gr, Rectangle rctCanvas, int val)
        {
            try
            {
                RectangleF rct = DrawBase(gr, rctCanvas, "Пульс", 6, 19, true, true, false);
                PointF pCenter = new PointF(rct.X + rct.Width / 2, rct.Y + rct.Height / 2);
                using (Font fntText = new Font("Microsoft Sans Serif", rct.Width / 15, FontStyle.Regular))
                {
                    for (int i = 20; i <= 120; i += 20)
                    {
                        PointF pText = PCircle(pCenter, rct.Width/2 - rct.Width/10 - rct.Width/15,
                                               225 - 270/100f*(i - 20));
                        string text = i.ToString();
                        SizeF mes = gr.MeasureString(text, fntText);
                        pText.X -= mes.Width / 2;
                        pText.Y -= mes.Height / 2;
                        gr.DrawString(text, fntText, Brushes.Black, pText);
                    }
                    string strMes = "уд/мин";
                    PointF pTextMes = PCircle(pCenter, rct.Width / 2 - rct.Width / 10, -90);
                    pTextMes.X -= gr.MeasureString(strMes, fntText).Width / 2;
                    pTextMes.Y -= gr.MeasureString(strMes, fntText).Height / 2;
                    gr.DrawString(strMes, fntText, Brushes.Black, pTextMes);
                }
                // рисуем стрелку
                DrawArrow(gr, rct, 225 - 270/100f*(val - 20));
            }
            catch (Exception)
            {
                gr.Clear(Color.Black);
            }
        }

        public static void DrawAge(Graphics gr, Rectangle rctCanvas, int val)
        {
            try
            {
                RectangleF rct = DrawBase(gr, rctCanvas, "Возраст", 7, 9);
                PointF pCenter = new PointF(rct.X + rct.Width / 2, rct.Y + rct.Height / 2);
                using (Font fntText = new Font("Microsoft Sans Serif", rct.Width / 15, FontStyle.Regular))
                {
                    for (int i = 20; i <= 80; i += 10)
                    {
                        PointF pText = PCircle(pCenter, rct.Width / 2 - rct.Width / 10 - rct.Width / 15,
                                               225 - 270 / 60f * (i - 20));
                        string text = i.ToString();
                        SizeF mes = gr.MeasureString(text, fntText);
                        pText.X -= mes.Width / 2;
                        pText.Y -= mes.Height / 2;
                        gr.DrawString(text, fntText, Brushes.Black, pText);
                    }
                    string strMes = "лет";
                    PointF pTextMes = PCircle(pCenter, rct.Width / 2 - rct.Width / 10, -90);
                    pTextMes.X -= gr.MeasureString(strMes, fntText).Width / 2;
                    pTextMes.Y -= gr.MeasureString(strMes, fntText).Height / 2;
                    gr.DrawString(strMes, fntText, Brushes.Black, pTextMes);
                }
                // рисуем стрелку
                DrawArrow(gr, rct, 225 - 270 / 60f * (val - 20));
            }
            catch (Exception)
            {
                gr.Clear(Color.Black);
            }
        }

        public static void DrawBlood(Graphics gr, Rectangle rctCanvas, int val)
        {
            try
            {
                RectangleF rct = DrawBase(gr, rctCanvas, "Кровоток", 7, 4, false, false);
                PointF pCenter = new PointF(rct.X + rct.Width / 2, rct.Y + rct.Height / 2);
                using (Font fntText = new Font("Microsoft Sans Serif", rct.Width / 15, FontStyle.Regular))
                {
                    for (int i = 60; i <= 90; i += 5)
                    {
                        PointF pText = PCircle(pCenter, rct.Width / 2 - rct.Width / 10 - rct.Width / 15,
                                               225 - 270 / 30f * (i - 60));
                        string text = i.ToString();
                        SizeF mes = gr.MeasureString(text, fntText);
                        pText.X -= mes.Width / 2;
                        pText.Y -= mes.Height / 2;
                        gr.DrawString(text, fntText, Brushes.Black, pText);
                    }
                    string strMes = "мл/мин";
                    PointF pTextMes = PCircle(pCenter, rct.Width / 2 - rct.Width / 10, -90);
                    pTextMes.X -= gr.MeasureString(strMes, fntText).Width / 2;
                    pTextMes.Y -= gr.MeasureString(strMes, fntText).Height / 2;
                    gr.DrawString(strMes, fntText, Brushes.Black, pTextMes);
                }
                // рисуем стрелку
                DrawArrow(gr, rct, 225 - 270 / 30f * (val - 60));
            }
            catch (Exception)
            {
                gr.Clear(Color.Black);
            }
        }


        public static void DrawPressure(Graphics gr, Rectangle rct, int top, int lower, int rctSosudWidth, float fSize)
        {
            gr.SmoothingMode = SmoothingMode.HighQuality;// .AntiAlias;
            gr.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
            float fontSize;
            // выводим заголовок
            //using (Font fntTitle = new Font("Microsoft Sans Serif", rct.Height / 13f, FontStyle.Regular))
            //{
            //    string title = "Давл.";
            //    using (StringFormat sf = new StringFormat())
            //    {
            //        sf.Alignment = StringAlignment.Center;
            //        gr.DrawString(title, fntTitle, Brushes.Black, rct, sf);
            //    }
            //    int titleHeight = (int)gr.MeasureString(title, fntTitle).Height;
            //    rct = new Rectangle(rct.X, rct.Y + titleHeight, rct.Width, rct.Height - titleHeight);
            //    using (Font fntMes = new Font("Microsoft Sans Serif", fntTitle.Size/2, FontStyle.Regular))
            //    {
            //        string mes = "(мм рт.ст.)";
            //        using (StringFormat sf = new StringFormat())
            //        {
            //            sf.Alignment = StringAlignment.Center;
            //            gr.DrawString(mes, fntMes, Brushes.Black, rct, sf);
            //        }
            //        int mesHeight = (int)gr.MeasureString(title, fntMes).Height;
            //        rct = new Rectangle(rct.X, rct.Y + mesHeight, rct.Width, rct.Height - mesHeight);
            //    }
            //}
            using (Font fntMes = new Font("Microsoft Sans Serif", fSize / 2, FontStyle.Regular))
            {
                string mes = "(мм рт.ст.)";
                using (StringFormat sf = new StringFormat())
                {
                    sf.Alignment = StringAlignment.Center;
                    gr.DrawString(mes, fntMes, Brushes.Black, rct, sf);
                }
                int mesHeight = (int)gr.MeasureString(mes, fntMes).Height;
                rct = new Rectangle(rct.X, rct.Y + mesHeight, rct.Width, rct.Height - mesHeight);
            }
            // определим размер шрифта
            using (Font fnt = new Font("Microsoft Sans Serif", 1, FontStyle.Bold))
            {
                fontSize = FontSize(gr, "200", fnt, new SizeF(rct.Width/3f, rct.Height));
            }
            // рисуем столбик
            using (Font fntReg = new Font("Microsoft Sans Serif", fontSize, FontStyle.Regular),
                        fntBold = new Font("Microsoft Sans Serif", fontSize, FontStyle.Bold))
            {
                Rectangle rctPres = new Rectangle(rct.X + rct.Width/3, rct.Y + (int) fntReg.GetHeight(gr)/2, rct.Width/3,
                                                  rct.Height - (int) fntReg.GetHeight(gr));
                if (top > 120)
                    gr.FillRectangle(Brushes.Red,
                                     new Rectangle(rctPres.X, PrecLevel(rctPres, top), rctPres.Width,
                                                   PrecLevel(rctPres, Math.Max(120, lower)) - PrecLevel(rctPres, top)));
                if (top >= 80 && lower <= 120)
                    gr.FillRectangle(Brushes.Blue,
                                     new Rectangle(rctPres.X, PrecLevel(rctPres, Math.Min(120, top)), rctPres.Width,
                                                   PrecLevel(rctPres, Math.Max(80, lower)) - PrecLevel(rctPres, Math.Min(120, top))));
                if (lower < 80)
                    gr.FillRectangle(Brushes.Green,
                                     new Rectangle(rctPres.X, PrecLevel(rctPres, Math.Min(80, top)), rctPres.Width,
                                                   PrecLevel(rctPres, lower) - PrecLevel(rctPres, Math.Min(80, top))));
                int border = 7 * rctSosudWidth / Properties.Resources.molodimetr.Width;
                using (Pen pBorder = new Pen(_clrElements, border))
                {
                    gr.DrawRectangle(pBorder, rctPres);
                    string strVal = top.ToString();
                    gr.DrawLine(pBorder, rctPres.X, PrecLevel(rctPres, top), rctPres.Right, PrecLevel(rctPres, top));
                    gr.DrawString(strVal, fntBold, Brushes.Black, rctPres.X - gr.MeasureString(strVal, fntBold).Width,
                                  PrecLevel(rctPres, top) - gr.MeasureString(strVal, fntBold).Height/2);
                    strVal = lower.ToString();
                    gr.DrawLine(pBorder, rctPres.X, PrecLevel(rctPres, lower), rctPres.Right, PrecLevel(rctPres, lower));
                    gr.DrawString(strVal, fntBold, Brushes.Black, rctPres.X - gr.MeasureString(strVal, fntBold).Width,
                                  PrecLevel(rctPres, lower) - gr.MeasureString(strVal, fntBold).Height/2);
                    pBorder.Width /= 2;
                    for (int iVal = 0; iVal <= 200; iVal += 40)
                    {
                        strVal = iVal.ToString();
                        gr.DrawLine(pBorder, rctPres.X, PrecLevel(rctPres, iVal), rctPres.Right, PrecLevel(rctPres, iVal));
                        gr.DrawString(strVal, fntReg, Brushes.Black, rctPres.Right,
                                      PrecLevel(rctPres, iVal) - gr.MeasureString(strVal, fntReg).Height/2);
                    }
                }
                
            }
        }

        // вычисление уровня столбика давления
        private static int PrecLevel(Rectangle rct, int val)
        {
            if (val > 200)
                return rct.Y;
            if (val < 0)
                return rct.Bottom;
            return rct.Bottom - rct.Height*val/200;
        }


        public static float DrawTopTitle(Graphics gr, Rectangle rct)
        {
            gr.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
            // определим размер шрифта
            float fontSize;
            string[] _titles = new string[] {"Сосуды", "Давление", "Стресс"};
            using (Font fnt = new Font("Microsoft Sans Serif", 1, FontStyle.Regular))
            {
                fontSize = Math.Min(FontSize(gr, _titles[0], fnt, new SizeF(rct.Width*0.4f, rct.Height)),
                                    FontSize(gr, _titles[2], fnt, new SizeF(rct.Width*0.4f, rct.Height)));
            }
            // выводим заголовок
            using (Font fntTitle = new Font("Microsoft Sans Serif", fontSize, FontStyle.Regular))
            {
                using (StringFormat sf = new StringFormat())
                {
                    sf.Alignment = StringAlignment.Center;
                    gr.DrawString(_titles[0], fntTitle, Brushes.Black,
                                  new RectangleF(rct.X, rct.Y, rct.Width*0.4f, rct.Height), sf);
                    gr.DrawString(_titles[2], fntTitle, Brushes.Black,
                                  new RectangleF(rct.X + rct.Width*0.6f, rct.Y, rct.Width*0.4f, rct.Height), sf);
                    gr.DrawString(_titles[1], fntTitle, Brushes.Black,
                                  new RectangleF(rct.X, rct.Y, rct.Width, rct.Height), sf);
                }
            }
            return fontSize;
        }


        public static float DrawLegend(Graphics gr, Rectangle rct)
        {
            // определим размер шрифта
            float fontSize = 0;
            string[] _titles = new string[] { "fitness", "middle", "alarm" };
            using (Font fnt = new Font("Microsoft Sans Serif", 1, FontStyle.Regular))
            {
                for (int i = 0; i < 3; i++)
                {
                    float curSize = FontSize(gr, _titles[i], fnt, new SizeF(rct.Width*0.3f, rct.Height));
                    if (fontSize < 1 || curSize < fontSize)
                        fontSize = curSize;
                }
            }
            // выводим заголовок
            using (Font fntTitle = new Font("Microsoft Sans Serif", fontSize, FontStyle.Regular))
            {
                float blockHeight = fntTitle.GetHeight(gr),
                      blockWidth = blockHeight*2,
                      sep1 = blockHeight,
                      sep2 = blockHeight*3,
                      allWidth = blockWidth*3 + sep1*3 + sep2*2,
                      blockY = rct.Y + rct.Height/2 - blockHeight/2;
                for (int i = 0; i < 3; i++)
                    allWidth += gr.MeasureString(_titles[i], fntTitle).Width;
                float startX = rct.X + rct.Width/2 - allWidth/2;
                Color[] _clrs = new Color[] {Color.Green, Color.Blue, Color.Red};
                for (int i = 0; i < 3; i++)
                {
                    using (Brush br = new SolidBrush(_clrs[i]))
                    {
                        gr.FillRectangle(br, new RectangleF(startX, blockY, blockWidth, blockHeight));
                    }
                    startX += blockWidth + sep1;
                    gr.DrawString(_titles[i], fntTitle, Brushes.Black,
                                  new RectangleF(startX, blockY, rct.Width*0.3f, rct.Height));
                    startX += gr.MeasureString(_titles[i], fntTitle).Width + sep2;
                }
            }
            return fontSize;
        }


        // вычисление размера шрифта
        private static float FontSize(Graphics gr, String text, Font fntText, SizeF s)
        {
            float result = 0.1f;
            Font fntTextNew = null;
            try
            {
                fntTextNew = new Font(fntText.FontFamily, result);
                SizeF fs = gr.MeasureString(text, fntText);
                while (fs.Width < s.Width && fs.Height < s.Height)
                {
                    result += 0.1f;
                    fntTextNew.Dispose();
                    fntTextNew = new Font(fntTextNew.FontFamily, result);
                    fs = gr.MeasureString(text, fntTextNew);
                }
            }
            finally
            {
                if (fntTextNew != null)
                    fntTextNew.Dispose();
            }
            return result;
        }

    }
}
