﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Drawing.Printing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TonClock
{
    public partial class frmStatist : Form
    {
        //*****************************************************************
        //                      Поля и параметры                          *
        //*****************************************************************

        private int[] _selectedRowsDb, _selectedRowsTable, _statist;
        private int _idPatient;
        private string _strPatient;

        private string[] _strTableHeaders,
                         _strTableFooters = new string[]
                                                {
                                                    "Отклонение по среднему арифметическому", "Допуск",
                                                    "Систематическая погрешность (\"стандарт\")", "Допуск"
                                                };


        //*****************************************************************
        //                        Конструкторы                            *
        //*****************************************************************

        public frmStatist(int[] rowsDb, int[] rowsT, int[] s, int idP)
        {
            InitializeComponent();
            this.Icon = Properties.Resources.yagodka;
            _selectedRowsDb = rowsDb;
            _selectedRowsTable = rowsT;
            _statist = s;
            _idPatient = idP;
        }


        //*****************************************************************
        //        Подсчет и отображение статистики, загрузка формы        *
        //*****************************************************************

        // цвет выделенной ячейки
        private readonly Color ColorSelectedCells = Color.FromArgb(255, 220, 220);
        // допуски
        private const byte AdmissionAverage = 5,
                           AdmissionStandard = 8;
        // погрешности
        private double _errorAverageTop,
                       _errorStandardTop,
                       _errorAverageLower,
                       _errorStandardLower;

        private void frmStatist_Load(object sender, EventArgs e)
        {
            try
            {
                using (OleDbConnection myOleDbConnection = new OleDbConnection(ConstGlobals.СonnectionString))
                {
                    myOleDbConnection.Open();
                    // Информация о пациенте
                    OleDbCommand myOleDbCommand = new OleDbCommand(
                        "SELECT lastname, firstname, patronymic, age " +
                        "FROM Patients " +
                        "WHERE id = " + _idPatient,
                        myOleDbConnection);
                    OleDbDataReader myReader = myOleDbCommand.ExecuteReader();
                    if (myReader != null && myReader.Read())
                    {
                        this.Text = ConstGlobals.AssemblyTitle + " - Статистика - " + myReader[0].ToString() + " " +
                                    myReader[1].ToString()[0] + "." + myReader[2].ToString()[0] + "., " +
                                    DbPulseOperations.PrintAge(myReader.GetInt16(3));
                        _strPatient = myReader[0].ToString() + " " +
                                      myReader[1].ToString() + " " + myReader[2].ToString() + ", " +
                                      DbPulseOperations.PrintAge(myReader.GetInt16(3));
                        myReader.Close();
                    }
                    else
                    {
                        if (myReader != null && !myReader.IsClosed)
                            myReader.Close();
                        throw new Exception("В базе не обнаружен пациент с таким идентификатором.");
                    }
                    // Замеры
                    int sumAverageTop = 0,
                        sumStandardTop = 0,
                        sumAverageLower = 0,
                        sumStandardLower = 0,
                        countTop = 0,
                        countLower = 0;
                    grdStatist.ColumnHeadersHeight = grdStatist.ColumnHeadersHeight * 2;
                    grdStatist.Rows.Clear();
                    string commandText1 = "SELECT mesDateTime";
                    for (int i = 0; i < _statist.Length; i++)
                    {
                        switch (_statist[i])
                        {
                            case 1:
                                commandText1 += ", globalTop, globalLower";
                                break;
                            case 2:
                                commandText1 += ", korotkovTop, korotkovLower";
                                break;
                            case 3:
                                commandText1 += ", xgdTop, xgdLower";
                                break;
                            case 4:
                                commandText1 += ", mtpTop, mtpLower";
                                break;
                            default:
                                commandText1 += ", topPressure, lowerPressure";
                                break;
                        }
                    }
                    commandText1 += " FROM Measurements WHERE id = ";
                    for (int i = 0; i < _selectedRowsDb.Length && i < _selectedRowsTable.Length; i++)
                    {
                        myOleDbCommand.CommandText = commandText1 + _selectedRowsDb[i];
                        myReader = myOleDbCommand.ExecuteReader();
                        if (myReader != null && myReader.Read())
                        {
                            grdStatist.Rows.Add();
                            int rowIndex = grdStatist.Rows.Count - 1;
                            grdStatist.Rows[rowIndex].Cells["id"].Value = _selectedRowsTable[i];
                            grdStatist.Rows[rowIndex].Cells["date"].Value = myReader.GetDateTime(0).ToShortDateString();
                            grdStatist.Rows[rowIndex].Cells["time"].Value = myReader.GetDateTime(0).ToShortTimeString();
                            if (!myReader.IsDBNull(1))
                                grdStatist.Rows[rowIndex].Cells["top1"].Value = myReader.GetInt16(1);
                            if (!myReader.IsDBNull(2))
                                grdStatist.Rows[rowIndex].Cells["lower1"].Value = myReader.GetInt16(2);
                            if (!myReader.IsDBNull(3))
                                grdStatist.Rows[rowIndex].Cells["top2"].Value = myReader.GetInt16(3);
                            if (!myReader.IsDBNull(4))
                                grdStatist.Rows[rowIndex].Cells["lower2"].Value = myReader.GetInt16(4);
                            // Обработка значений
                            if (!myReader.IsDBNull(1) && !myReader.IsDBNull(3))
                            {
                                int val = myReader.GetInt16(3) - myReader.GetInt16(1);
                                grdStatist.Rows[rowIndex].Cells["topResult"].Value = val;
                                if (Math.Abs(val) > AdmissionAverage)
                                    grdStatist.Rows[rowIndex].Cells["topResult"].Style.BackColor = ColorSelectedCells;
                                sumAverageTop += val;
                                sumStandardTop += val*val;
                                countTop++;
                            }
                            if (!myReader.IsDBNull(2) && !myReader.IsDBNull(4))
                            {
                                int val = myReader.GetInt16(4) - myReader.GetInt16(2);
                                grdStatist.Rows[rowIndex].Cells["lowerResult"].Value = val;
                                if (Math.Abs(val) > AdmissionAverage)
                                    grdStatist.Rows[rowIndex].Cells["lowerResult"].Style.BackColor = ColorSelectedCells;
                                sumAverageLower += val;
                                sumStandardLower += val * val;
                                countLower++;
                            }
                            myReader.Close();
                        }
                        else
                        {
                            if (myReader != null && !myReader.IsClosed)
                                myReader.Close();
                            throw new Exception("Не удалось прочитать информацию о замере " + i + ".");
                        }
                    }
                    // Обработка результатов
                    _errorAverageTop = (double)sumAverageTop / countTop;
                    _errorStandardTop = Math.Sqrt((double)sumStandardTop / countTop);
                    _errorAverageLower = (double)sumAverageLower / countLower;
                    _errorStandardLower = Math.Sqrt((double)sumStandardLower / countLower);
                    // пишем среднее арифметическое
                    Font fontResult = new Font(grdStatist.DefaultCellStyle.Font, FontStyle.Bold);
                    grdStatist.Rows.Add();
                    grdStatist.Rows[grdStatist.Rows.Count - 1].Cells["topResult"].Value =
                        _errorAverageTop.ToString("0.##", CultureInfo.CreateSpecificCulture("ru-RU"));
                    if (Math.Abs(_errorAverageTop) > AdmissionAverage)
                        grdStatist.Rows[grdStatist.Rows.Count - 1].Cells["topResult"].Style.BackColor = ColorSelectedCells;
                    grdStatist.Rows[grdStatist.Rows.Count - 1].Cells["lowerResult"].Value =
                        _errorAverageLower.ToString("0.##", CultureInfo.CreateSpecificCulture("ru-RU"));
                    if (Math.Abs(_errorAverageLower) > AdmissionAverage)
                        grdStatist.Rows[grdStatist.Rows.Count - 1].Cells["lowerResult"].Style.BackColor = ColorSelectedCells;
                    grdStatist.Rows[grdStatist.Rows.Count - 1].DefaultCellStyle.Font = fontResult;
                    grdStatist.Rows.Add();
                    grdStatist.Rows[grdStatist.Rows.Count - 1].Cells["topResult"].Value = "± " + AdmissionAverage;
                    grdStatist.Rows[grdStatist.Rows.Count - 1].Cells["lowerResult"].Value = "± " + AdmissionAverage;
                    // пишем систематическую погрешность
                    grdStatist.Rows.Add();
                    grdStatist.Rows[grdStatist.Rows.Count - 1].Cells["topResult"].Value =
                        _errorStandardTop.ToString("0.##", CultureInfo.CreateSpecificCulture("ru-RU"));
                    if (Math.Abs(_errorStandardTop) > AdmissionStandard)
                        grdStatist.Rows[grdStatist.Rows.Count - 1].Cells["topResult"].Style.BackColor = ColorSelectedCells;
                    grdStatist.Rows[grdStatist.Rows.Count - 1].Cells["lowerResult"].Value =
                        _errorStandardLower.ToString("0.##", CultureInfo.CreateSpecificCulture("ru-RU"));
                    if (Math.Abs(_errorStandardLower) > AdmissionStandard)
                        grdStatist.Rows[grdStatist.Rows.Count - 1].Cells["lowerResult"].Style.BackColor = ColorSelectedCells;
                    grdStatist.Rows[grdStatist.Rows.Count - 1].DefaultCellStyle.Font = fontResult;
                    grdStatist.Rows.Add();
                    grdStatist.Rows[grdStatist.Rows.Count - 1].Cells["topResult"].Value = AdmissionStandard;
                    grdStatist.Rows[grdStatist.Rows.Count - 1].Cells["lowerResult"].Value = AdmissionStandard;
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show("Внимание! При попытке открытия базы данных произошла ошибка:\n" +
                    exc.Message + "\n\n" +
                    "Дальнейшая работа модуля не возможна.\n\n" +
                    "Пожалуйста, обратитесь к администратору.",
                    "Ошибка",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.DialogResult = DialogResult.Abort;
            }
            // параметры печати
            printDocument1.DefaultPageSettings = new PageSettings
                                                     {Landscape = false, Margins = new Margins(50, 50, 50, 50)};
            printDocument1.DocumentName = "TonClockStatist";
            // заголовки таблицы
            _strTableHeaders = new string[4] { "Замер", String.Empty, String.Empty, "Расчет погрешностей" };
            for (int i = 0; i < _statist.Length; i++)
            {
                switch (_statist[i])
                {
                    case 1:
                        _strTableHeaders[1 + i] = "Глобал";
                        break;
                    case 2:
                        _strTableHeaders[1 + i] = "Коротков";
                        break;
                    case 3:
                        _strTableHeaders[1 + i] = "ХГД";
                        break;
                    case 4:
                        _strTableHeaders[1 + i] = "МТП";
                        break;
                    default:
                        _strTableHeaders[1 + i] = "ТонКлок";
                        break;
                }
            }
        }


        //*****************************************************************
        //                            Меню                                *
        //*****************************************************************

        private void mnuExit_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void frmStatist_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }


        //*****************************************************************
        //                      Прорисовка таблицы                        *
        //*****************************************************************

        private void grdStatist_Paint(object sender, PaintEventArgs e)
        {
            Graphics graph = e.Graphics;
            Font fontHeader = new Font(grdStatist.ColumnHeadersDefaultCellStyle.Font, FontStyle.Bold);
            StringFormat formatHeader = new StringFormat { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Center };
            SolidBrush brushHeaderBack = new SolidBrush(grdStatist.ColumnHeadersDefaultCellStyle.BackColor),
                       brushHeaderText = new SolidBrush(grdStatist.ColumnHeadersDefaultCellStyle.ForeColor);
            Rectangle r1 = grdStatist.GetCellDisplayRectangle(grdStatist.Columns["id"].Index, -1, false);
            r1 = new Rectangle(r1.X + 1, r1.Y + 1, r1.Width +
                               grdStatist.GetCellDisplayRectangle(grdStatist.Columns["date"].Index, -1, false).Width +
                               grdStatist.GetCellDisplayRectangle(grdStatist.Columns["time"].Index, -1, false).Width - 2,
                               r1.Height / 2 - 2);
            graph.FillRectangle(brushHeaderBack, r1);
            graph.DrawString(_strTableHeaders[0], fontHeader, brushHeaderText, r1, formatHeader);
            for (int i = 0; i < 2; i++)
            {
                r1 = grdStatist.GetCellDisplayRectangle(grdStatist.Columns["top" + (i + 1)].Index, -1, false);
                r1 = new Rectangle(r1.X + 1, r1.Y + 1, r1.Width +
                                   grdStatist.GetCellDisplayRectangle(grdStatist.Columns["lower" + (i + 1)].Index, -1, false).Width - 2,
                                   r1.Height / 2 - 2);
                graph.FillRectangle(brushHeaderBack, r1);
                graph.DrawString(_strTableHeaders[1 + i], fontHeader, brushHeaderText, r1, formatHeader);
            }
            r1 = grdStatist.GetCellDisplayRectangle(grdStatist.Columns["topResult"].Index, -1, false);
            r1 = new Rectangle(r1.X + 1, r1.Y + 1, r1.Width +
                               grdStatist.GetCellDisplayRectangle(grdStatist.Columns["lowerResult"].Index, -1, false).Width - 2,
                               r1.Height / 2 - 2);
            graph.FillRectangle(brushHeaderBack, r1);
            graph.DrawString(_strTableHeaders[3], fontHeader, brushHeaderText, r1, formatHeader);
            // Строки внизу
            if (grdStatist.Rows.Count >= 4)
            {
                var textBrush = new SolidBrush(grdStatist.DefaultCellStyle.BackColor);
                formatHeader.Alignment = StringAlignment.Near;
                fontHeader = new Font(grdStatist.DefaultCellStyle.Font, FontStyle.Bold);
                Font fontHeader2 = new Font(fontHeader, FontStyle.Regular);
                r1 = grdStatist.GetCellDisplayRectangle(grdStatist.Columns["id"].Index, grdStatist.Rows.Count - 4, false);
                r1 = new Rectangle(r1.X + 1, r1.Y, r1.Width +
                                   grdStatist.GetCellDisplayRectangle(grdStatist.Columns["date"].Index, grdStatist.Rows.Count - 4, false).Width +
                                   grdStatist.GetCellDisplayRectangle(grdStatist.Columns["time"].Index, grdStatist.Rows.Count - 4, false).Width +
                                   grdStatist.GetCellDisplayRectangle(grdStatist.Columns["top1"].Index, grdStatist.Rows.Count - 4, false).Width +
                                   grdStatist.GetCellDisplayRectangle(grdStatist.Columns["lower1"].Index, grdStatist.Rows.Count - 4, false).Width +
                                   grdStatist.GetCellDisplayRectangle(grdStatist.Columns["top2"].Index, grdStatist.Rows.Count - 4, false).Width +
                                   grdStatist.GetCellDisplayRectangle(grdStatist.Columns["lower2"].Index, grdStatist.Rows.Count - 4, false).Width - 2,
                                   r1.Height - 1);
                graph.FillRectangle(textBrush, r1);
                graph.DrawString(_strTableFooters[0], fontHeader, brushHeaderText, r1, formatHeader);
                r1.Y = grdStatist.GetCellDisplayRectangle(grdStatist.Columns["id"].Index, grdStatist.Rows.Count - 3, false).Y;
                graph.FillRectangle(textBrush, r1);
                graph.DrawString(_strTableFooters[1], fontHeader2, brushHeaderText, r1, formatHeader);
                r1.Y = grdStatist.GetCellDisplayRectangle(grdStatist.Columns["id"].Index, grdStatist.Rows.Count - 2, false).Y;
                graph.FillRectangle(textBrush, r1);
                graph.DrawString(_strTableFooters[2], fontHeader, brushHeaderText, r1, formatHeader);
                r1.Y = grdStatist.GetCellDisplayRectangle(grdStatist.Columns["id"].Index, grdStatist.Rows.Count - 1, false).Y;
                graph.FillRectangle(textBrush, r1);
                graph.DrawString(_strTableFooters[3], fontHeader2, brushHeaderText, r1, formatHeader);
            }
        }

        private void grdStatist_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.ScrollOrientation == ScrollOrientation.HorizontalScroll)
            {
                Rectangle rtHeader = grdStatist.DisplayRectangle;
                rtHeader.Height = grdStatist.ColumnHeadersHeight / 2;
                grdStatist.Invalidate(rtHeader);
            }
            Rectangle r1 = grdStatist.GetCellDisplayRectangle(grdStatist.Columns["id"].Index, grdStatist.Rows.Count - 4, false);
            r1.Width += grdStatist.GetCellDisplayRectangle(grdStatist.Columns["date"].Index, -1, false).Width +
                grdStatist.GetCellDisplayRectangle(grdStatist.Columns["time"].Index, grdStatist.Rows.Count - 4, false).Width +
                grdStatist.GetCellDisplayRectangle(grdStatist.Columns["top1"].Index, grdStatist.Rows.Count - 4, false).Width +
                grdStatist.GetCellDisplayRectangle(grdStatist.Columns["lower1"].Index, grdStatist.Rows.Count - 4, false).Width +
                grdStatist.GetCellDisplayRectangle(grdStatist.Columns["top2"].Index, grdStatist.Rows.Count - 4, false).Width +
                grdStatist.GetCellDisplayRectangle(grdStatist.Columns["lower2"].Index, grdStatist.Rows.Count - 4, false).Width;
            r1.Height += grdStatist.GetCellDisplayRectangle(grdStatist.Columns["id"].Index, grdStatist.Rows.Count - 3, false).Height +
                grdStatist.GetCellDisplayRectangle(grdStatist.Columns["id"].Index, grdStatist.Rows.Count - 2, false).Height +
                grdStatist.GetCellDisplayRectangle(grdStatist.Columns["id"].Index, grdStatist.Rows.Count - 1, false).Height;
            grdStatist.Invalidate(r1);
        }

        private void grdStatist_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            Rectangle rtHeader = grdStatist.DisplayRectangle;
            rtHeader.Height = grdStatist.ColumnHeadersHeight / 2;
            grdStatist.Invalidate(rtHeader);
        }

        private void grdStatist_CellPainting(object sender, DataGridViewCellPaintingEventArgs e)
        {
            if (e.ColumnIndex > -1)
            {
                string colName = grdStatist.Columns[e.ColumnIndex].Name;
                if (colName == "top1" || colName == "top2" || colName == "topResult")
                {
                    e.Paint(e.CellBounds, DataGridViewPaintParts.All); // рисуем полностью ячейку
                    // рисуем линию
                    using (Pen p = new Pen(Color.Black, 1))
                    {
                        e.Graphics.DrawLine(p,
                                            new Point(e.CellBounds.Left - 1, e.CellBounds.Top - 1),
                                            new Point(e.CellBounds.Left - 1, e.CellBounds.Bottom - 1));
                    }
                    e.Handled = true;
                }
            }
            if (grdStatist.Rows.Count >= 4 &&
                (e.RowIndex == grdStatist.Rows.Count - 4 || e.RowIndex == grdStatist.Rows.Count - 2))
            {
                e.Paint(e.CellBounds, DataGridViewPaintParts.All); // рисуем полностью ячейку
                // рисуем линию
                using (Pen p = new Pen(Color.Black, 1))
                {
                    e.Graphics.DrawLine(p,
                                        new Point(e.CellBounds.Left - 1, e.CellBounds.Top - 1),
                                        new Point(e.CellBounds.Right - 1, e.CellBounds.Top - 1));
                }
                e.Handled = true;
            }
        }


        //*****************************************************************
        //                      Печать на принтер                         *
        //*****************************************************************

        private void mnuPageSetup_Click(object sender, EventArgs e)
        {
            PageSetupDialog diag = new PageSetupDialog();
            diag.Document = printDocument1;
            diag.EnableMetric = true;
            diag.ShowDialog();
        }

        private void mnuPrintPreview_Click(object sender, EventArgs e)
        {
            PrintPreviewDialog diag = new PrintPreviewDialog();
            diag.Document = printDocument1;
            diag.Icon = Properties.Resources.yagodka;
            diag.ShowDialog();
        }

        private void mnuPrint_Click(object sender, EventArgs e)
        {
            PrintDialog diag = new PrintDialog();
            diag.Document = printDocument1;
            diag.AllowSelection = false;
            diag.AllowSomePages = false;
            if (diag.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    printDocument1.Print();
                }
                catch (Exception exc)
                {
                    MessageBox.Show("Возникла ошибка при попытке печати:\n" +
                                    exc.Message,
                                    "Ошибка",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        // Параметры печати
        private int curPrintPage, curPrintRow;
        private const float separatorPrint = 10;

        private void printDocument1_BeginPrint(object sender, PrintEventArgs e)
        {
            curPrintPage = 0;
            curPrintRow = 0;
        }

        private void printDocument1_PrintPage(object sender, PrintPageEventArgs e)
        {
            // Cоздаем экземпляр graph класса Graphics:
            Graphics graph = e.Graphics;
            graph.PageUnit = GraphicsUnit.Display;
            graph.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;

            // Определяем печатную область страницы:
            float leftMargin = e.MarginBounds.Left,
                  rightMargin = e.MarginBounds.Right,
                  topMargin = e.MarginBounds.Top,
                  bottomMargin = e.MarginBounds.Bottom,
                  printableWidth = e.MarginBounds.Width,
                  printableHeight = e.MarginBounds.Height;

            // Определим используемые шрифты и кисти
            Font fontHeader1 = new Font("Microsoft Sans Serif", 16, FontStyle.Bold),
                 fontTableHeader = new Font("Microsoft Sans Serif", 10, FontStyle.Bold),
                 fontTable = new Font("Microsoft Sans Serif", 10),
                 fontTableTitle = new Font("Microsoft Sans Serif", 10, FontStyle.Italic);
            StringFormat formatText = new StringFormat
                                          {
                                              Alignment = StringAlignment.Center,
                                              LineAlignment = StringAlignment.Near,
                                              Trimming = StringTrimming.EllipsisWord
                                          };
            SolidBrush brushText = new SolidBrush(Color.Black);

            // Начинаем печать
            curPrintPage++;

            // Выводим заголовок
            string strTableTitle;
            RectangleF rctText = new RectangleF(leftMargin, topMargin, printableWidth, printableHeight);
            if (curPrintPage == 1)
            {
                graph.DrawString(_strPatient, fontHeader1, brushText, rctText, formatText);
                rctText.Y += graph.MeasureString(_strPatient, fontHeader1).Height + separatorPrint;
                formatText.Alignment = StringAlignment.Near;
                strTableTitle = "Таблица \"Статистика\"";
            }
            else
            {
                formatText.Alignment = StringAlignment.Far;
                strTableTitle = "Продолжение таблицы";
            }
            graph.DrawString(strTableTitle, fontTableTitle, brushText, rctText, formatText);
            rctText.Y += graph.MeasureString(strTableTitle, fontTableTitle).Height;
            
            // Таблица
            const float tablePadding = 5;
            Pen penTable1 = new Pen(brushText, 2),
                penTable2 = new Pen(brushText, 1);
            // Ширина столбцов
            float[] columnsWidth = new float[grdStatist.Columns.Count];
            for (int i = 0; i < 3; i++)
            {
                columnsWidth[i] = graph.MeasureString(grdStatist.Columns[i].HeaderText, fontTableHeader).Width +
                                  tablePadding*2;
                for (int j = 0; j < grdStatist.Rows.Count; j++)
                {
                    float newColumnWidth = (grdStatist[i, j].Value != null
                                                ? graph.MeasureString(grdStatist[i, j].Value.ToString(), fontTable).
                                                      Width + tablePadding*2
                                                : 0);
                    if (newColumnWidth > columnsWidth[i])
                        columnsWidth[i] = newColumnWidth;
                }
            }
            for (int i = 3; i < grdStatist.Columns.Count; i++)
            {
                columnsWidth[i] = (printableWidth - (columnsWidth[0] + columnsWidth[1] + columnsWidth[2]))/
                                  (grdStatist.Columns.Count - 3);
            }
            // Печать заголовка
            rctText = new RectangleF(rctText.X, rctText.Y, printableWidth, tablePadding * 2 + fontTableHeader.GetHeight(graph));
            SolidBrush brushFillHeader = new SolidBrush(Color.LightGray);
            formatText.Alignment = StringAlignment.Center;
            formatText.LineAlignment = StringAlignment.Center;
            for (int i = 0; i < _strTableHeaders.Length; i++)
            {
                int j = (i != 0 ? i*2 + 1 : 0);
                rctText.Width = columnsWidth[j] + columnsWidth[j + 1] + (i != 0 ? 0 : columnsWidth[j + 2]);
                graph.FillRectangle(brushFillHeader, rctText);
                graph.DrawString(_strTableHeaders[i], fontTableHeader, brushText, rctText, formatText);
                graph.DrawLines(penTable1,
                                new PointF[]
                                    {
                                        new PointF(rctText.X, rctText.Bottom), new PointF(rctText.X, rctText.Y),
                                        new PointF(rctText.Right, rctText.Y), new PointF(rctText.Right, rctText.Bottom)
                                    });
                rctText.X += rctText.Width;
            }
            rctText = new RectangleF(leftMargin, rctText.Y + rctText.Height, printableWidth, tablePadding * 2 + fontTable.GetHeight(graph));
            for (int i = 0; i < grdStatist.Columns.Count; i++)
            {
                if (graph.MeasureString(grdStatist.Columns[i].HeaderText, fontTable).Width > columnsWidth[i])
                {
                    rctText.Height += fontTable.GetHeight(graph);
                    break;
                }
            }
            for (int i = 0; i < grdStatist.Columns.Count; i++)
            {
                rctText.Width = columnsWidth[i];
                graph.FillRectangle(brushFillHeader, rctText);
                graph.DrawString(grdStatist.Columns[i].HeaderText, fontTable, brushText, rctText, formatText);
                graph.DrawLines(penTable2,
                                new PointF[]
                                    {
                                        new PointF(rctText.X, rctText.Bottom), new PointF(rctText.X, rctText.Y),
                                        new PointF(rctText.Right, rctText.Y), new PointF(rctText.Right, rctText.Bottom)
                                    });
                if (i == 0 || i == 3 || i == 5 || i == 7)
                    graph.DrawLine(penTable1, new PointF(rctText.X, rctText.Bottom), new PointF(rctText.X, rctText.Y));
                else if (i == grdStatist.Columns.Count - 1)
                    graph.DrawLine(penTable1, new PointF(rctText.Right, rctText.Y), new PointF(rctText.Right, rctText.Bottom));
                rctText.X += rctText.Width;
            }
            // Выводим строки таблицы
            rctText = new RectangleF(leftMargin, rctText.Y + rctText.Height, printableWidth, tablePadding * 2 + fontTable.GetHeight(graph));
            while (curPrintRow <= grdStatist.Rows.Count - 1 &&
                rctText.Bottom + separatorPrint + fontTable.GetHeight(graph) <= bottomMargin)
            {
                int curColumn = 0;
                while (curColumn < grdStatist.Columns.Count)
                {
                    rctText.Width = columnsWidth[curColumn];
                    if (curPrintRow >= grdStatist.Rows.Count - 4 && curColumn < grdStatist.Columns.Count - 2)
                        for (int j = curColumn + 1; j < grdStatist.Columns.Count - 2; j++)
                            rctText.Width += columnsWidth[j];
                    graph.FillRectangle(new SolidBrush(grdStatist[curColumn, curPrintRow].Style.BackColor), rctText);
                    Font curFontTable = fontTable;
                    if (curPrintRow == grdStatist.Rows.Count - 4 || curPrintRow == grdStatist.Rows.Count - 2)
                    {
                        curFontTable = new Font(curFontTable, FontStyle.Bold);
                        graph.DrawLine(penTable1, new PointF(rctText.X, rctText.Y), new PointF(rctText.Right, rctText.Y));
                    }
                    string curText = (grdStatist[curColumn, curPrintRow].Value != null
                                          ? grdStatist[curColumn, curPrintRow].Value.ToString()
                                          : String.Empty);
                    if (curPrintRow >= grdStatist.Rows.Count - 4 && curColumn < grdStatist.Columns.Count - 2)
                        curText = _strTableFooters[curPrintRow - (grdStatist.Rows.Count - 4)];
                    graph.DrawString(curText, curFontTable, brushText, rctText, formatText);
                    graph.DrawLines(penTable2,
                                    new PointF[]
                                        {
                                            new PointF(rctText.X, rctText.Bottom), new PointF(rctText.X, rctText.Y),
                                            new PointF(rctText.Right, rctText.Y),
                                            new PointF(rctText.Right, rctText.Bottom)
                                        });
                    if (curColumn == 0 || curColumn == 3 || curColumn == 5 || curColumn == 7)
                        graph.DrawLine(penTable1, new PointF(rctText.X, rctText.Bottom), new PointF(rctText.X, rctText.Y));
                    else if (curColumn == grdStatist.Columns.Count - 1)
                        graph.DrawLine(penTable1, new PointF(rctText.Right, rctText.Y), new PointF(rctText.Right, rctText.Bottom));
                    rctText.X += rctText.Width;
                    if (curPrintRow >= grdStatist.Rows.Count - 4 && curColumn < grdStatist.Columns.Count - 2)
                        curColumn = grdStatist.Columns.Count - 2;
                    else
                        curColumn++;
                }
                rctText.X = leftMargin;
                rctText.Y += rctText.Height;
                curPrintRow++;
            }
            e.HasMorePages = (curPrintRow < grdStatist.Rows.Count);
            if (e.HasMorePages)
                graph.DrawLine(penTable2, new PointF(leftMargin, rctText.Top - penTable2.Width / 2),
                               new PointF(rightMargin, rctText.Top - penTable2.Width / 2));
            else
                graph.DrawLine(penTable1, new PointF(leftMargin, rctText.Top - penTable1.Width/2),
                               new PointF(rightMargin, rctText.Top - penTable1.Width / 2));

            // Номер страницы
            if (!e.HasMorePages && curPrintPage <= 1) return;
            rctText = new RectangleF(leftMargin, bottomMargin - fontTable.GetHeight(graph), printableWidth, fontTable.GetHeight(graph));
            graph.DrawString("Страница " + curPrintPage, fontTable, brushText, rctText, formatText);
        }



    }
}
