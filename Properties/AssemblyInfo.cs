﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Управление общими сведениями о сборке осуществляется с помощью 
// набора атрибутов. Измените значения этих атрибутов, чтобы изменить сведения,
// связанные со сборкой.
[assembly: AssemblyTitle("TonClockCASP")]
[assembly: AssemblyDescription("Программа обеспечивает связь с оборудованием по проводному и беспроводному интерфейсу. Сигнал непрерывно отображается на экране в виде пульсовой волны. После записи фрагмента длительностью 20 секунд программа анализирует волну, и формирует на экране результат диагностики. Для более точного анализа данных возможен доступ в буфер (защищен паролем). С вопросами по работе программы обращайтесь к автору по e-mail: yishome@mail.ru")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Эмдея")]
[assembly: AssemblyProduct("TonClock")]
[assembly: AssemblyCopyright("Copyright ©  2012 Рудченко Павел")]
[assembly: AssemblyTrademark("TonClock is a registered trademark of Emdeya")]
[assembly: AssemblyCulture("")]

// Параметр ComVisible со значением FALSE делает типы в сборке невидимыми 
// для COM-компонентов.  Если требуется обратиться к типу в этой сборке через 
// COM, задайте атрибуту ComVisible значение TRUE для этого типа.
[assembly: ComVisible(false)]

// Следующий GUID служит для идентификации библиотеки типов, если этот проект будет видимым для COM
[assembly: Guid("0e9b97a6-685b-4e74-be0e-26dc7169f3ae")]

// Сведения о версии сборки состоят из следующих четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии 
//      Номер построения
//      Редакция
//
// Можно задать все значения или принять номер построения и номер редакции по умолчанию, 
// используя "*", как показано ниже:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("4.0.6.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
