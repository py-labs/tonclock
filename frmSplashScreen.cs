﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using TonClock.Properties;

namespace TonClock
{
    public partial class frmSplashScreen : Form
    {
        public frmSplashScreen(int ms)
        {
            InitializeComponent();
            msDisplay = ms;
            msStart = Environment.TickCount;
        }


        //*****************************************************************
        //                     Базовые константы                          *
        //*****************************************************************

        // радиус скругления углов формы
        private const int Radius = 20;
        private string strVersionNumber = Assembly.GetExecutingAssembly().GetName().Version.ToString();
        private int msDisplay, msStart, msOpacity;


        //*****************************************************************
        //                     Загрузка и прорисовка                      *
        //*****************************************************************

        private void frmSplashScreen_Load(object sender, EventArgs e)
        {
            GraphicsPath gp = new GraphicsPath();
            int width = this.ClientSize.Width,
                height = this.ClientSize.Height;
            gp.AddLine(Radius, 0, width - Radius, 0);
            gp.AddArc(width - (Radius * 2), 0, Radius * 2, Radius * 2, 270, 90);
            gp.AddLine(width, Radius, width, height - Radius);
            gp.AddArc(width - (Radius * 2), height - (Radius * 2), Radius * 2, Radius * 2, 0, 90);
            gp.AddLine(width - Radius, height, Radius, height);
            gp.AddArc(0, height - (Radius * 2), Radius * 2, Radius * 2, 90, 90);
            gp.AddLine(0, height - Radius, 0, Radius);
            gp.AddArc(0, 0, Radius * 2, Radius * 2, 180, 90);
            gp.CloseFigure();
            Region myRegion = new Region(gp);
            this.Region = myRegion;

            _incrementOpacity = 0.05f;
            tmrOpacity.Start();
        }

        private void frmSplashScreen_Paint(object sender, PaintEventArgs e)
        {
            Graphics graph = e.Graphics;
            graph.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            graph.SmoothingMode = SmoothingMode.HighQuality;
            Color linesColor = Color.FromArgb(85, 140, 50);
            Pen linesPen = new Pen(linesColor, 6);
            SolidBrush textBrush = new SolidBrush(Color.FromArgb(40, 38, 22));
            linesPen.SetLineCap(LineCap.Round, LineCap.Round, DashCap.Round);
            DrawRoundRect(graph, linesPen, 10, 10,
                          this.ClientSize.Width - 20, this.ClientSize.Height - 20, Radius);
            Bitmap bmpLogo = (Bitmap) Properties.Resources.logo_350_350;
            bmpLogo.SetResolution(graph.DpiX, graph.DpiY);
            graph.DrawImage(bmpLogo, new Point(20, 20));
            Font fontHeader = new Font("Microsoft Sans Serif", 22, FontStyle.Bold),
                 fontText = new Font("Microsoft Sans Serif", 14),
                 fontFooter = new Font("Microsoft Sans Serif", 10, FontStyle.Italic);
            float curX = 370, curY = 20;
            string strName = ConstGlobals.AssemblyTitle,
                   strCopyright = "© Эмдея, 2015",
                   strVersion = "Версия: " + strVersionNumber,
                   strWait = "Подождите, идет запуск...";
            graph.DrawString(strName, fontHeader, textBrush, curX, curY);
            curY += fontHeader.GetHeight(graph) + 10;
            graph.DrawLine(linesPen, curX, curY, curX + graph.MeasureString(strName, fontHeader).Width, curY);
            curY += 10;
            graph.DrawString(strCopyright, fontText, textBrush, curX, curY);
            curY += fontText.GetHeight(graph) + 5;
            graph.DrawString(strVersion, fontText, textBrush, curX, curY);
            curX = this.ClientRectangle.Right - 25 - graph.MeasureString(strWait, fontFooter).Width;
            curY = 365 - fontFooter.GetHeight(graph);
            graph.DrawString(strWait, fontFooter, textBrush, curX, curY);
            for (int i = 1; i <= intProgress; i++)
                graph.FillEllipse(new SolidBrush(linesColor), rctProgress.X + (i - 1)*(sizeProgress*2), rctProgress.Y,
                                  sizeProgress, sizeProgress);
            linesColor = Color.FromArgb(50, 85, 140, 50);
            for (int i = intProgress + 1; i * (sizeProgress * 2) <= rctProgress.Width; i++)
                graph.FillEllipse(new SolidBrush(linesColor), rctProgress.X + (i - 1)*(sizeProgress*2), rctProgress.Y,
                                  sizeProgress, sizeProgress);
        }

        private void DrawRoundRect(Graphics g, Pen p, float X, float Y, float width, float height, float radius)
        {
            GraphicsPath gp = new GraphicsPath();
            gp.AddLine(X + radius, Y, X + width - radius, Y);
            gp.AddArc(X + width - (radius * 2), Y, radius * 2, radius * 2, 270, 90);
            gp.AddLine(X + width, Y + radius, X + width, Y + height - radius);
            gp.AddArc(X + width - (radius * 2), Y + height - (radius * 2), radius * 2, radius * 2, 0, 90);
            gp.AddLine(X + width - radius, Y + height, X + radius, Y + height);
            gp.AddArc(X, Y + height - (radius * 2), radius * 2, radius * 2, 90, 90);
            gp.AddLine(X, Y + height - radius, X, Y + radius);
            gp.AddArc(X, Y, radius * 2, radius * 2, 180, 90);
            gp.CloseFigure();
            g.DrawPath(p, gp);
            gp.Dispose();
        }


        //*****************************************************************
        //                         Прозрачность                           *
        //*****************************************************************

        private float _incrementOpacity;

        private void tmrOpacity_Tick(object sender, EventArgs e)
        {
            this.Opacity += _incrementOpacity;
            if (this.Opacity <= 0 || this.Opacity >= 1)
            {
                tmrOpacity.Stop();
                if (this.Opacity >= 1)
                {
                    msOpacity = Environment.TickCount - msStart;
                    msDisplay -= msOpacity;
                    tmrProgress.Interval = (msDisplay / (numAllProgress + 1) > 0 ? msDisplay / (numAllProgress + 1) : 1);
                    tmrProgress.Start();
                }
            }
        }

        public void StartClose()
        {
            tmrProgress.Stop();
            tmrProgress.Dispose();
            _incrementOpacity = -0.05f;
            tmrOpacity.Start();
        }

        public bool CanClose
        {
            get { return !tmrOpacity.Enabled; }
        }


        //*****************************************************************
        //                           Прогресс                             *
        //*****************************************************************

        private const int sizeProgress = 9, numAllProgress = 13;
        Rectangle rctProgress = new Rectangle(366, 330, sizeProgress * 2 * numAllProgress, sizeProgress);
        private int intProgress = 0;

        private void tmrProgress_Tick(object sender, EventArgs e)
        {
            if ((intProgress + 1) * (sizeProgress * 2) <= rctProgress.Width)
            {
                intProgress++;
                this.Invalidate(new Rectangle(rctProgress.X + (intProgress - 1)*(sizeProgress*2), rctProgress.Y,
                                              sizeProgress,
                                              10));
                this.Update();
            }
            else
            {
                tmrProgress.Stop();
                tmrProgress.Dispose();
                //StartClose();
            }
        }

    }
}
