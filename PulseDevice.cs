﻿using System;
using System.IO;
using System.IO.Ports;
using System.Net.Sockets;
using System.Threading;
using InTheHand.Net;
using InTheHand.Net.Bluetooth;
using InTheHand.Net.Sockets;
using UsbLibrary;
using FTD2XX_NET;
using System.Management;

namespace TonClock
{
    // Событие получения дискретных данных от прибора
    public class NewPulseEventArgs : EventArgs
    {
        private readonly ushort _value;

        public NewPulseEventArgs(ushort value)
        {
            _value = value;
        }

        public ushort Value { get { return _value; } }
    }

    public interface IDevicePulse : IDisposable
    {
        DeviceModes DeviceMode { get; }
        void StartDevice();
        void StopDevice();
        event EventHandler<NewPulseEventArgs> NewPulse;
        bool DeviceStarted { get; }
    }


    public abstract class AbstractDevice : IDevicePulse
    {
        public DeviceModes DeviceMode { get; protected set; }
        public abstract void StartDevice();
        public abstract void StopDevice();
        public abstract void Dispose();

        public event EventHandler<NewPulseEventArgs> NewPulse;
        
        protected virtual void OnNewPulse(NewPulseEventArgs e)
        {
            EventHandler<NewPulseEventArgs> temp = NewPulse;
            if (temp != null) temp(this, e);
        }

        public abstract bool DeviceStarted { get; }
    }


    //*****************************************************************
    //                             Usb                                *
    //*****************************************************************

    public class UsbHidDevice : AbstractDevice
    {
        // прибор
        private UsbHidPort _usb;
        private bool _flagConnected = false;

        // команды
        private const byte UsbCommandStart = 3;
        private const byte UsbCommandEnd = 0;

        // конструктор
        public UsbHidDevice(String vendorId, String productId)
        {
            DeviceMode = DeviceModes.Usb;
            if (String.IsNullOrEmpty(vendorId))
                throw new ArgumentNullException("vendorId");
            if (String.IsNullOrEmpty(productId))
                throw new ArgumentNullException("productId");
            int numVendorId = Int32.Parse(vendorId, System.Globalization.NumberStyles.HexNumber),
                numProductId = Int32.Parse(productId, System.Globalization.NumberStyles.HexNumber);
            _usb = new UsbLibrary.UsbHidPort { ProductId = numProductId, VendorId = numVendorId };
            _usb.OnDataRecieved += new UsbLibrary.DataRecievedEventHandler(usb_OnDataRecieved);
            _usb.OnSpecifiedDeviceRemoved += new System.EventHandler(usb_OnSpecifiedDeviceRemoved);
            _usb.OnSpecifiedDeviceArrived += new System.EventHandler(usb_OnSpecifiedDeviceArrived);
            _usb.OnDataSend += new System.EventHandler(usb_OnDataSend);
            _usb.CheckDevicePresent();
        }

        public override bool DeviceStarted
        {
            get { return _usb != null && _usb.SpecifiedDevice != null && _flagConnected; }
        }

        public override void StartDevice()
        {
            if (!DeviceStarted)
                throw new Exception("Не обнаружено устройство USB.");
            PulseSendCommand(UsbCommandStart);
        }

        public override void StopDevice()
        {
            if (_usb != null && _usb.SpecifiedDevice != null)
            {
                PulseSendCommand(UsbCommandEnd);
            }
        }

        public override void Dispose()
        {
            if (_usb != null)
            {
                if (_usb.SpecifiedDevice != null)
                    _usb.SpecifiedDevice.Dispose();
                _usb.Dispose();
                _usb = null;
            }
        }

        private void PulseSendCommand(byte cmd)
        {
            if (_usb != null && _usb.SpecifiedDevice != null)
            {
                byte[] data = new byte[9];
                data[8] = cmd;
                _usb.SpecifiedDevice.SendData(data);
            }
        }

        private void usb_OnDataRecieved(object sender, UsbLibrary.DataRecievedEventArgs args)
        {
            ushort val = (ushort)(args.data[7] + (args.data[8] << 8));
            OnNewPulse(new NewPulseEventArgs(val));
        }

        private void usb_OnSpecifiedDeviceRemoved(object sender, EventArgs e)
        {
            _flagConnected = false;
        }

        private void usb_OnSpecifiedDeviceArrived(object sender, EventArgs e)
        {
            _flagConnected = true;
        }

        private void usb_OnDataSend(object sender, EventArgs e)
        {
            // при отправке данных ниче делать не нужно
        }
    }


    //*****************************************************************
    //                            UsbEc                               *
    //*****************************************************************

    public class UsbEcDevice : AbstractDevice
    {
        // автоматический поиск номера COM-порта по его описанию
        private static string GetComPort(string descr)
        {
            using (ManagementObjectSearcher searcher =
                    new ManagementObjectSearcher("root\\CIMV2",
                    "SELECT * FROM Win32_SerialPort"))
            {

                foreach (ManagementObject queryObj in searcher.Get())
                {
                    if (queryObj["Description"] != null && String.Equals(descr, queryObj["Description"].ToString()))
                        return queryObj["DeviceID"] != null ? queryObj["DeviceID"].ToString() : null;
                }

                return String.Empty;
            }
        }

        // прибор
        private ushort _dac1, _dac2;
        private SerialPort _serial;
        private String _strComPort;
        private byte[] _serialPortBuffer;

        // конструктор
        public UsbEcDevice(String comPort, ushort dac1, ushort dac2)
        {
            DeviceMode = DeviceModes.UsbEc;
            if (String.IsNullOrEmpty(comPort))
                _strComPort = GetComPort("STMicroelectronics Virtual COM Port");
            else
                _strComPort = comPort;
            if (string.IsNullOrEmpty(_strComPort))
                throw new Exception("Не обнаружен виртуальный COM-порт устройства STMicroelectronics.");

            _dac1 = dac1;
            _dac2 = dac2;
            _serial = new SerialPort();
            _serial.DtrEnable = true;
            _serial.ReadTimeout = 20;
            _serial.WriteTimeout = 20;
            _serial.BaudRate = 115200;
            _serial.StopBits = StopBits.Two;
        }

        public override bool DeviceStarted
        {
            get { return _serial != null && _serial.IsOpen; }
        }

        private void OpenSerialPort(string strCom)
        {
            if (_serial.IsOpen)
                _serial.Close();
            _serial.PortName = strCom;
            _serial.Open();
        }

        private void CloseSerialPort()
        {
            if (_serial != null)
            {
                if (_serial.IsOpen)
                {
                    _serial.DiscardInBuffer();
                    _serial.DiscardOutBuffer();
                    _serial.Close();
                }
                _serial.Dispose();
                _serial = null;
            }
            _serialPortBuffer = null;
        }

        private void SendDacPacket(ushort dac, bool flagDac1 = true)
        {
            byte[] dacbuf = new byte[7]; // буферный массив для отправки команды
            dacbuf[0] = 0x7e;
            dacbuf[1] = 4;
            dacbuf[2] = 0x81;
            dacbuf[3] = (byte)(flagDac1 ? 0x31 : 0x32);
            dacbuf[4] = (byte)(0xFF & (dac >> 8));
            dacbuf[5] = (byte)(0xFF & dac);

            PreparePacketToSend(ref dacbuf);
            _serial.Write(dacbuf, 0, dacbuf.Length);
        }

        private void SendDacPacket()
        {
            SendDacPacket(_dac1, true);
            SendDacPacket(_dac2, false);
        }

        public void SetDac(ushort dac1, ushort dac2)
        {
            _dac1 = dac1;
            _dac2 = dac2;
            if (DeviceStarted)
                SendDacPacket();
        }

        public override void StartDevice()
        {
            OpenSerialPort(_strComPort);
            _serial.DataReceived += new SerialDataReceivedEventHandler(serial_DataReceived);

            SendDacPacket();
            
            byte[] start = new byte[6]; // буферный массив для старта
            start[0] = 0x7e;
            start[1] = 3;
            start[2] = 0x81;
            start[3] = 0;
            start[4] = 0x67;
            start[5] = 23;
            _serial.Write(start, 0, start.Length);
        }

        public override void StopDevice()
        {
            if (DeviceStarted)
            {
                byte[] stop = new byte[6]; // буферный массив для окончания изменрений
                stop[0] = 0x7e;
                stop[1] = 3;
                stop[2] = 0x81;
                stop[3] = 0;
                stop[4] = 0x73;
                stop[5] = 11;
                _serial.Write(stop, 0, stop.Length);
            }
            CloseSerialPort();
        }

        public override void Dispose()
        {
            CloseSerialPort();
        }

        private void serial_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            byte[] arrBytes;
            try
            {
                int startBytes = (_serialPortBuffer != null ? _serialPortBuffer.Length : 0),
                    readBytes = _serial.BytesToRead;
                arrBytes = new byte[readBytes + startBytes];
                _serial.Read(arrBytes, startBytes, readBytes);
                if (_serialPortBuffer != null)
                    _serialPortBuffer.CopyTo(arrBytes, 0);
            }
            catch (Exception)
            {
                return;
            }

            int frameStart = 0, frameEnd = 0;
            while ((frameStart < arrBytes.Length) && (frameEnd < arrBytes.Length))
            {
                while ((frameStart < arrBytes.Length) && (arrBytes[frameStart] != 0x7E))
                    frameStart++;
                if (frameStart < arrBytes.Length)   // пакет найден
                {
                    frameEnd = frameStart;
                    while ((frameEnd < arrBytes.Length - 1) && (arrBytes[frameEnd + 1] != 0x7E))
                        frameEnd++;
                    bool flagFrameRight = false;
                    if ((frameEnd < arrBytes.Length - 1) || ((frameEnd < arrBytes.Length) && (arrBytes[frameEnd] != 0x7D)))
                    {
                        byte[] packageBytes = new byte[frameEnd - frameStart + 1];
                        for (int i = frameStart; i <= frameEnd; i++)
                            packageBytes[i - frameStart] = arrBytes[i];
                        flagFrameRight = PackageProcessing(packageBytes);
                    }
                    // если последний пакет найден, но НЕ ПРАВИЛЬНЫЙ, то оставляем в буфере
                    if (flagFrameRight)
                        _serialPortBuffer = null;
                    else if (frameEnd >= arrBytes.Length - 1)
                    {
                        _serialPortBuffer = new byte[arrBytes.Length - frameStart];
                        for (int i = frameStart; i < arrBytes.Length; i++)
                            _serialPortBuffer[i - frameStart] = arrBytes[i];
                        break;
                    }
                    frameStart = frameEnd + 1;
                }
                else
                    _serialPortBuffer = null;
            }
        }

        // подготовка к отправке пакета
        public static void PreparePacketToSend(ref byte[] bytes)
        {
            if (bytes == null || bytes.Length < 3)
                return;
            // ckeksum
            byte sum = 0;
            for (int i = 2; i < bytes.Length - 1; i++)
                sum += bytes[i];
            bytes[bytes.Length - 1] = (byte)(0xFF - sum);
            // Escape (0x7E = 0x7D, 0x5E; 0x7D = 0x7D, 0x5D (0x5E ^ 0x20 = 0x7E))
            int curByte = 1;
            while (curByte < bytes.Length)
            {
                if (bytes[curByte] == 0x7E || bytes[curByte] == 0x7D)
                {
                    byte[] arrNew = new byte[bytes.Length + 1];
                    for (int i = 0; i < curByte; i++)
                        arrNew[i] = bytes[i];
                    arrNew[curByte] = 0x7D;
                    arrNew[curByte + 1] = (byte)(bytes[curByte] ^ 0x20);
                    for (int i = curByte + 2; i < arrNew.Length; i++)
                        arrNew[i] = bytes[i - 1];
                    bytes = arrNew;
                    curByte++;
                }
                curByte++;
            }
        }

        // обработка пакета
        public static void BasePackageProcessing(ref byte[] bytes)
        {
            // удаление Escape (0x7E = 0x7D, 0x5E, 0x7D = 0x7D, 0x5D (0x5E ^ 0x20 = 0x7E))
            int curByte = 0;
            while ((curByte + 1) < bytes.Length)
            {
                if (bytes[curByte] == 0x7D)
                {
                    byte[] arrNew = new byte[bytes.Length - 1];
                    for (int i = 0; i < curByte; i++)
                        arrNew[i] = bytes[i];
                    arrNew[curByte] = (byte) (bytes[curByte + 1] ^ 0x20);
                    for (int i = curByte + 1; i < arrNew.Length; i++)
                        arrNew[i] = bytes[i + 1];
                    bytes = arrNew;
                }
                curByte++;
            }
        }

        // обработка пакета
        private bool PackageProcessing(byte[] bytes)
        {
            BasePackageProcessing(ref bytes);

            if (bytes.Length < 8)
                return false;
            int curByte = 1;
            ushort frameLength = bytes[curByte++]; // длина пакета
            if ((bytes.Length - curByte - 1) != frameLength) // проверка длины пакета
                return false;
            byte sum = 0; // проверка контрольной суммы
            for (int i = curByte; i < bytes.Length; i++)
                sum += bytes[i];
            if (sum != 0xFF)
                return false;
            // для подтверждения
            byte[] ask = null;
            byte frameId = 0;
            // 3.	Байт – тип передаваемого пакета. 0x81 – информационный пакет, 0x89 – пакет подтверждения(ACK).
            if (bytes[curByte++] != 0x81)
                return false;
            //  4.	Байт – вид сообщения.
            // 0x00 – сообщение не требует подтверждения о получении,
            // 0x01- сообщение требует подтверждения о получении,
            // 0x02 - сообщение не требует подтверждения о получении и прибор находится в режиме ожидания
            if (bytes[curByte] == 0x01)
                ask = new byte[] { 0x7E, 3, 0x89, frameId, 0, 0 };
            else if (bytes[curByte] != 0x00 && bytes[curByte] != 0x02)
                return false;
            curByte++;
            // 5.	Для типа пакета 0x81 – 0x70 означает передачу значения напряжения на приборе,
            // 0x44 – передачу данных с АЦП
            if (bytes[curByte++] == 0x44)
            {
                frameId = (byte)(0x80 | bytes[curByte++]); // PackID
                while (curByte < (bytes.Length - 2))
                {
                    ushort val = (ushort) ((0xFFF & (bytes[curByte] << 8)) | bytes[curByte + 1]);
                    OnNewPulse(new NewPulseEventArgs(val));
                    curByte += 2;
                }
            }
            // отправка подтверждения
            if (ask != null && ask.Length > 3)
            {
                ask[3] = frameId;
                PreparePacketToSend(ref ask);
                _serial.Write(ask, 0, ask.Length);
            }
            return true;
        }

    }


    //*****************************************************************
    //                         FtdiDevice                             *
    //*****************************************************************

    public class FtdiDevice : AbstractDevice
    {
        // прибор
        protected SerialPort _serial;
        protected String _strComPort;
        protected byte[] _serialPortBuffer;

        // конструктор
        public FtdiDevice()
        {
            DeviceMode = DeviceModes.FtdiOld;
            _strComPort = FtdiGetComPort();
            if (string.IsNullOrEmpty(_strComPort))
                throw new Exception("Не обнаружен виртуальный COM-порт устройства FTDI.");
            _serial = new System.IO.Ports.SerialPort();
            _serial.DtrEnable = true;
            _serial.ReadTimeout = 20;
            _serial.WriteTimeout = 20;
        }

        public override bool DeviceStarted
        {
            get { return _serial != null && _serial.IsOpen; }
        }

        private static string FtdiGetComPort()
        {
            string strComPort = string.Empty;
            UInt32 ftdiDeviceCount = 0;
            FTDI.FT_STATUS ftStatus = FTDI.FT_STATUS.FT_OK;

            // Create new instance of the FTDI device class
            FTDI myFtdiDevice = new FTDI(false);

            // Determine the number of FTDI devices connected to the machine
            ftStatus = myFtdiDevice.GetNumberOfDevices(ref ftdiDeviceCount);
            // Check status
            if (ftStatus != FTDI.FT_STATUS.FT_OK)
                throw new Exception("Не удалось определить количество подключенных поддерживаемых устройств. Драйвер FTD2XX установлен?");
            if (ftdiDeviceCount == 0)
                throw new Exception("Не обнаружено подключенных поддерживаемых устройств.");

            // Allocate storage for device info list
            FTDI.FT_DEVICE_INFO_NODE[] ftdiDeviceList = new FTDI.FT_DEVICE_INFO_NODE[ftdiDeviceCount];
            // Populate our device list
            ftStatus = myFtdiDevice.GetDeviceList(ftdiDeviceList);
            if (ftStatus != FTDI.FT_STATUS.FT_OK)
                throw new Exception("Не удалось определить список подключенных поддерживаемых устройств.");

            ftStatus = FTDI.FT_STATUS.FT_OTHER_ERROR;
            for (UInt32 i = 0; i < ftdiDeviceCount; i++)
            {
                // Open device in our list by serial number
                ftStatus = myFtdiDevice.OpenByLocation(ftdiDeviceList[i].LocId);
                if (ftStatus != FTDI.FT_STATUS.FT_OK)
                    continue;
                ftStatus = myFtdiDevice.GetCOMPort(out strComPort);
                if (ftStatus != FTDI.FT_STATUS.FT_OK)
                    continue;
                ftStatus = myFtdiDevice.Close();
                if (ftStatus == FTDI.FT_STATUS.FT_OK)
                    break;
            }
            if (ftStatus != FTDI.FT_STATUS.FT_OK)
                throw new Exception("Не удалось подключиться ни к одному приемному устройству.");

            return strComPort;
        }

        private void OpenSerialPort(string strCom)
        {
            if (_serial.IsOpen)
                _serial.Close();
            _serial.PortName = strCom;
            _serial.BaudRate = 9600;
            _serial.StopBits = StopBits.One;
            _serial.Open();
        }

        protected void CloseSerialPort()
        {
            if (_serial != null)
            {
                if (_serial.IsOpen)
                {
                    _serial.DiscardInBuffer();
                    _serial.DiscardOutBuffer();
                    _serial.Close();
                }
                _serial.Dispose();
                _serial = null;
            }
            _serialPortBuffer = null;
        }

        public override void StartDevice()
        {
            OpenSerialPort(_strComPort);
            _serial.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(serial_DataReceived);
        }

        public override void StopDevice()
        {
            CloseSerialPort();
        }

        public override void Dispose()
        {
            CloseSerialPort();
        }

        private void serial_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            byte[] arrBytes;
            try
            {
                int startBytes = (_serialPortBuffer != null ? _serialPortBuffer.Length : 0),
                    readBytes = _serial.BytesToRead;
                arrBytes = new byte[readBytes + startBytes];
                _serial.Read(arrBytes, startBytes, readBytes);
                if (_serialPortBuffer != null)
                    _serialPortBuffer.CopyTo(arrBytes, 0);
            }
            catch (Exception)
            {
                return;
            }

            int frameStart = 0, frameEnd = 0;
            while ((frameStart < arrBytes.Length) && (frameEnd < arrBytes.Length))
            {
                while ((frameStart < arrBytes.Length) && (arrBytes[frameStart] != 0x7E))
                    frameStart++;
                if (frameStart < arrBytes.Length)   // пакет найден
                {
                    frameEnd = frameStart;
                    while ((frameEnd < arrBytes.Length - 1) && (arrBytes[frameEnd + 1] != 0x7E))
                        frameEnd++;
                    bool flagFrameRight = false;
                    if ((frameEnd < arrBytes.Length - 1) || ((frameEnd < arrBytes.Length) && (arrBytes[frameEnd] != 0x7D)))
                    {
                        byte[] packageBytes = new byte[frameEnd - frameStart + 1];
                        for (int i = frameStart; i <= frameEnd; i++)
                            packageBytes[i - frameStart] = arrBytes[i];
                        flagFrameRight = PackageProcessing(packageBytes);
                    }
                    // если последний пакет найден, но НЕ ПРАВИЛЬНЫЙ, то оставляем в буфере
                    if (flagFrameRight)
                        _serialPortBuffer = null;
                    else if (frameEnd >= arrBytes.Length - 1)
                    {
                        _serialPortBuffer = new byte[arrBytes.Length - frameStart];
                        for (int i = frameStart; i < arrBytes.Length; i++)
                            _serialPortBuffer[i - frameStart] = arrBytes[i];
                        break;
                    }
                    frameStart = frameEnd + 1;
                }
                else
                    _serialPortBuffer = null;
            }
        }

        // обработка пакета
        private bool PackageProcessing(byte[] bytes)
        {
            UsbEcDevice.BasePackageProcessing(ref bytes);
            if ((bytes.Length) < 3)
                return false;
            ushort val = (ushort)((bytes[2] << 8) | bytes[1]);
            OnNewPulse(new NewPulseEventArgs(val));
            return true;
        }
    }


    //*****************************************************************
    //                        FtdiNewDevice                           *
    //*****************************************************************

    public class FtdiNewDevice : FtdiDevice
    {
        private int _srcIdCurrent;
        private readonly DevicesList _devicesList;
        // текущее состояние КУ
        private enum EndDeviceOperation
        {
            Waiting,        // ожидание (прием всех broadcast пакетов)
            Connecting,     // подключение к КУ
            Connected,      // КУ подключено
            Disconnecting   // отключение от КУ
        };
        private EndDeviceOperation _edOperation;
        // таймер обновления КУ
        private System.Threading.Timer _tmrEndDevices;

        // конструктор
        public FtdiNewDevice(DevicesList dList, int srcIdCurrent = -1)
            : base()
        {
            DeviceMode = DeviceModes.FtdiNew;
            _edOperation = EndDeviceOperation.Waiting;
            _devicesList = dList;
            _srcIdCurrent = srcIdCurrent;
            OpenSerialPort(_strComPort, false);
            _serial.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(serial_DataReceived);
            _tmrEndDevices = new Timer(new TimerCallback(TimerEndDevices), _devicesList, 100, 1000);
        }

        private void TimerEndDevices(object devices)
        {
            if (!_serial.IsOpen)
                OpenSerialPort(_strComPort, false);
            _devicesList.UpdateTimer();
        }

        private void OpenSerialPort(string strCom, bool showErrors = true)
        {
            try
            {
                if (_serial.IsOpen)
                    _serial.Close();
                _serial.PortName = strCom;
                _serial.BaudRate = 115200;
                _serial.StopBits = StopBits.Two;
                _serial.Open();
            }
            catch
            {
                if (showErrors)
                    throw;
            }
        }

        public override void StartDevice()
        {
            if (_srcIdCurrent == -1)
            {
                _srcIdCurrent = _devicesList.GetFirstConnectedId();
                if (_srcIdCurrent == -1)
                    throw new Exception("Не обнаружено доступных конечных устройств.");
            }
            OpenSerialPort(_strComPort, true);
            _serial.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(serial_DataReceived);
            _edOperation = EndDeviceOperation.Connecting;
        }

        public void StartDevice(int srcIdCurrent)
        {
            _srcIdCurrent = srcIdCurrent;
            StartDevice();
        }

        public override void StopDevice()
        {
            if (_edOperation == EndDeviceOperation.Connected)
                _edOperation = EndDeviceOperation.Disconnecting;
            else
            {
                _edOperation = EndDeviceOperation.Waiting;
                OnEndDeviceStatus(new EndDeviceStatusEventArgs(false));
            }
        }

        public override void Dispose()
        {
            if (_tmrEndDevices != null)
            {
                _tmrEndDevices.Dispose();
                _tmrEndDevices = null;
            }
            CloseSerialPort();
        }

        public bool ConnectedEndDevice
        {
            get { return (_serial.IsOpen && (_edOperation == EndDeviceOperation.Connected)); }
        }

        public int EndDeviceId { get { return _srcIdCurrent; } }

        private void serial_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            byte[] arrBytes;
            try
            {
                int startBytes = (_serialPortBuffer != null ? _serialPortBuffer.Length : 0),
                    readBytes = _serial.BytesToRead;
                arrBytes = new byte[readBytes + startBytes];
                _serial.Read(arrBytes, startBytes, readBytes);
                if (_serialPortBuffer != null)
                    _serialPortBuffer.CopyTo(arrBytes, 0);
            }
            catch (Exception)
            {
                return;
            }

            int frameStart = 0, frameEnd = 0, srcPacketCurId = -1;
            while ((frameStart < arrBytes.Length) && (frameEnd < arrBytes.Length))
            {
                while ((frameStart < arrBytes.Length) && (arrBytes[frameStart] != 0x7E))
                    frameStart++;
                if (frameStart < arrBytes.Length)   // пакет найден
                {
                    frameEnd = frameStart;
                    while ((frameEnd < arrBytes.Length - 1) && (arrBytes[frameEnd + 1] != 0x7E))
                        frameEnd++;
                    bool flagFrameRight = false;
                    if ((frameEnd < arrBytes.Length - 1) || ((frameEnd < arrBytes.Length) && (arrBytes[frameEnd] != 0x7D)))
                    {
                        byte[] packageBytes = new byte[frameEnd - frameStart + 1];
                        for (int i = frameStart; i <= frameEnd; i++)
                            packageBytes[i - frameStart] = arrBytes[i];
                        flagFrameRight = PackageProcessing(packageBytes, out srcPacketCurId);
                    }
                    // если последний пакет найден, но НЕ ПРАВИЛЬНЫЙ, то оставляем в буфере
                    if (flagFrameRight)
                        _serialPortBuffer = null;
                    else if (frameEnd >= arrBytes.Length - 1)
                    {
                        _serialPortBuffer = new byte[arrBytes.Length - frameStart];
                        for (int i = frameStart; i < arrBytes.Length; i++)
                            _serialPortBuffer[i - frameStart] = arrBytes[i];
                        break;
                    }
                    frameStart = frameEnd + 1;
                }
                else
                    _serialPortBuffer = null;
            }

            // отправляем сигнальные пакеты

            if ((srcPacketCurId == _srcIdCurrent) &&
                ((_edOperation == EndDeviceOperation.Connecting) || (_edOperation == EndDeviceOperation.Disconnecting)))
            {
                arrBytes = new byte[] { 0x7E, 0, 7, 1, 0x67, 0, 0, 0, 0xFF, 0x67, 0x31 };
                arrBytes[5] = (byte)(srcPacketCurId >> 8);
                arrBytes[6] = (byte)(0xff & srcPacketCurId);
                switch (_edOperation)
                {
                    case EndDeviceOperation.Connecting:
                        arrBytes[10] = (byte)(0xff & (0x31 - arrBytes[5] - arrBytes[6]));
                        break;
                    case EndDeviceOperation.Disconnecting:
                        arrBytes[4] = arrBytes[9] = 0x73;
                        arrBytes[10] = (byte)(0xff & (0x19 - arrBytes[5] - arrBytes[6]));
                        break;
                }
                _serial.Write(arrBytes, 0, arrBytes.Length);
            }
        }

        private int _lastPacketNumber;

        // обработка пакета
        private bool PackageProcessing(byte[] bytes, out int srcId)
        {
            srcId = -1;
            UsbEcDevice.BasePackageProcessing(ref bytes);

            if ((bytes.Length) < 3)
                return false;
            int curByte = 1;
            ushort frameLength = (ushort)((bytes[curByte++] << 8) | bytes[curByte++]); // длина пакета
            if ((bytes.Length - curByte - 1) != frameLength) // проверка длины пакета
                return false;
            byte sum = 0; // проверка контрольной суммы
            for (int i = curByte; i < bytes.Length; i++)
                sum += bytes[i];
            if (sum != 0xFF)
                return false;
            if (bytes[curByte++] != 0x81) // признак RX (Receive) Packet
                return false;
            srcId = (bytes[curByte++] << 8) | bytes[curByte++]; // адрес КУ
            curByte++; // RSSI – не используем
            // Disconnecting
            if ((_edOperation == EndDeviceOperation.Disconnecting) && (srcId == _srcIdCurrent) && (bytes[curByte] == 0x2))
            {
                _edOperation = EndDeviceOperation.Waiting;
                OnEndDeviceStatus(new EndDeviceStatusEventArgs(false));
            }
            // Connecting
            if ((_edOperation == EndDeviceOperation.Connecting) && (srcId == _srcIdCurrent) && (bytes[curByte] == 0x0))
            {
                _edOperation = EndDeviceOperation.Connected;
                OnEndDeviceStatus(new EndDeviceStatusEventArgs(true));
            }
            // обработка данных пакета
            if (((bytes[curByte] == 0x2) || (bytes[curByte] == 0x0)) && (bytes[curByte + 1] == 0x70))
            {
                // напряжение питания  в сантивольтах
                curByte += 2;
                ushort volt = (ushort)((bytes[curByte] << 8) | bytes[curByte + 1]);
                _devicesList.ReceivedPackageFromDevice((ushort)srcId, volt);
            }
            else if ((bytes[curByte] == 0x0) && (bytes[curByte + 1] == 0x44) && (srcId == _srcIdCurrent))
            {
                curByte += 2;
                if (_lastPacketNumber != bytes[curByte])
                {
                    _lastPacketNumber = bytes[curByte++];
                    while (curByte < (bytes.Length - 2))
                    {
                        ushort val = (ushort)((0xFFF & (bytes[curByte] << 8)) | bytes[curByte + 1]);
                        OnNewPulse(new NewPulseEventArgs(val));
                        curByte += 2;
                    }
                }
            }
            return true;
        }

        // событие подключения или отключения КУ
        public class EndDeviceStatusEventArgs : EventArgs
        {
            private readonly bool _connected;

            public EndDeviceStatusEventArgs(bool connected)
            {
                _connected = connected;
            }

            public bool Connected { get { return _connected; } }
        }

        public event EventHandler<EndDeviceStatusEventArgs> EndDeviceStatus;

        protected virtual void OnEndDeviceStatus(EndDeviceStatusEventArgs e)
        {
            EventHandler<EndDeviceStatusEventArgs> temp = EndDeviceStatus;
            if (temp != null) temp(this, e);
        }

    }


    //*****************************************************************
    //                       BluetoothDevice                          *
    //*****************************************************************

    public class BluetoothDevice : AbstractDevice
    {
        // класс ошибки пакета
        private class PacketException : Exception
        {
            private readonly bool _packetError = false;
            private readonly byte[] _ask = null;
            public bool PacketError { get { return _packetError; } }
            public byte[] Ask { get { return _ask; } }

            //Стандартные конструкторы
            public PacketException() : base() { }
            public PacketException(string message) : base(message) { }
            public PacketException(string message, Exception innerException) : base(message, innerException) { }

            //Конструктор для свойства PacketError
            public PacketException(string message, bool pError, byte[] ask = null)
                : this(message)
            {
                _packetError = pError;
                _ask = ask;
            }
        }

        // прибор
        private string _btId, _btPin;
        private BluetoothClient _client;
        private Stream _stream;
        private int _volt = -1;
        private Thread _thrReadStream;

        // конструктор
        public BluetoothDevice(string strBtId, string strBtPin)
        {
            DeviceMode = DeviceModes.Bluetooth;

            if (!BluetoothRadio.IsSupported)
                throw new Exception("На компьютере не поддерживается Bluetooth.");
            if (BluetoothRadio.PrimaryRadio == null)
                throw new Exception("На компьютере не обнаружен модуль Bluetooth.");
            if (!BluetoothRadio.IsSupported)
                throw new Exception("На компьютере установлен не поддерживаемый модуль Bluetooth.");
            //if (BluetoothRadio.PrimaryRadio.Mode == RadioMode.PowerOff)
                BluetoothRadio.PrimaryRadio.Mode = RadioMode.Connectable;

            _btId = strBtId;
            _btPin = strBtPin;
            _client = new BluetoothClient();
            BluetoothEndPoint btEndPoint = null;
            BluetoothDeviceInfo[] btList = _client.DiscoverDevices(255, true, true, false, false);
            foreach (var btDevice in btList)
            {
                if ((btEndPoint = TryConnectToDevice(btDevice)) != null)
                    break;
            }

            if (btEndPoint == null)
            {
                //BluetoothRadio.PrimaryRadio.Mode = RadioMode.Discoverable;
                btList = _client.DiscoverDevices();
                foreach (var btDevice in btList)
                {
                    if ((btEndPoint = TryConnectToDevice(btDevice)) != null)
                        break;
                }
            }
            
            if (btEndPoint != null)
            {
                //BluetoothRadio.PrimaryRadio.Mode = RadioMode.;
                //Guid spguid3 = BluetoothService.SerialPort;
                //BluetoothListener cli = new BluetoothListener(spguid3);
                //cli.ServiceName = "TonClock BT Service";
                //cli.Start();
                //_client = cli.AcceptBluetoothClient();
                //btEndPoint.
                
                int attempToConnect = 0;
                while (!_client.Connected)
                {
                    try
                    {
                        _client.Connect(btEndPoint);
                    }
                    catch (SocketException)
                    {
                        if ((++attempToConnect) >= 3)
                            throw;
                    }
                }
               
                _stream = _client.GetStream();
                //_stream2 = _client.GetStream();
                _thrReadStream = new Thread(delegate() { ReadStream(_stream); });
                _thrReadStream.IsBackground = true;
                _thrReadStream.Priority = ThreadPriority.AboveNormal;
                _thrReadStream.Start();
                //cli.Stop();
            }
            else
            {
                throw new Exception("Не найдено устройство блютуз (Bluetooth).");
            }
        }

        private BluetoothEndPoint TryConnectToDevice(BluetoothDeviceInfo btDevice)
        {
            btDevice.Refresh();
            if (btDevice.DeviceName.Equals(_btId))
            {
                // Authenticate with the device
                if (!btDevice.Authenticated)
                {
                    // Use pin "0000" for authentication
                    if (!BluetoothSecurity.PairRequest(btDevice.DeviceAddress, _btPin))
                    {
                        throw new Exception("Не удалось произвести сопряжение с устройством Bluetooth.");
                    }
                }
                Guid spguid = new Guid("00001101-0000-1000-8000-00805f9b34fb"); //BluetoothService.SerialPort;
                Guid spguid2 = BluetoothService.SerialPort;
                Guid spguid3 = BluetoothService.RFCommProtocol;

                //ServiceRecord[] records = btDevice.GetServiceRecords(btDevice.InstalledServices[0]);
                //int port = -1;
                //foreach (var serviceRecord in records)
                //{
                //    ServiceElement el1 = ServiceRecordHelper.GetL2CapChannelElement(serviceRecord),
                //                   el2 = ServiceRecordHelper.GetRfcommChannelElement(serviceRecord);
                //    int n1 = ServiceRecordHelper.GetL2CapChannelNumber(serviceRecord),
                //        n2 = ServiceRecordHelper.GetRfcommChannelNumber(serviceRecord);
                //    port = ServiceRecordHelper.GetRfcommChannelNumber(serviceRecord);
                //}

                try
                {
                    ServiceRecord[] records = btDevice.GetServiceRecords(spguid);
                }
                catch
                {
                    BluetoothSecurity.PairRequest(btDevice.DeviceAddress, _btPin);
                }
                
                string val2 = spguid2.ToString();
                string val3 = spguid3.ToString();
                //btDevice.SetServiceState(spguid, true); //(btDevice.InstalledServices[0], true);
                return new BluetoothEndPoint(btDevice.DeviceAddress, spguid);
            }
            return null;
        }

        // передача уровня заряда
        public int GetVoltLevel
        {
            get
            {
                if (_volt < 0)
                    return -1;
                if (_volt < 320)
                    return 0;
                if ((_volt >= 320) && (_volt < 345))
                    return 1;
                if ((_volt >= 345) && (_volt < 370))
                    return 2;
                if ((_volt >= 370) && (_volt < 395))
                    return 3;
                if ((_volt >= 395) && (_volt < 420))
                    return 4;
                return 5;
            }
        }

        public override bool DeviceStarted
        {
            get { return _client != null && _client.Connected; }
        }

        public override void StartDevice()
        {
            if (_stream == null)
                return;
            byte[] start = new byte[6]; // буферный массив для старта
            start[0] = 0x7e;
            start[1] = 3;
            start[2] = 0x81;
            start[3] = 0;
            start[4] = 0x67;
            start[5] = 23;
            _stream.Write(start, 0, start.Length);
            _stream.Flush();
        }

        public override void StopDevice()
        {
            if (_stream == null)
                return;
            byte[] stop = new byte[6]; // буферный массив для окончания изменрений
            stop[0] = 0x7e;
            stop[1] = 3;
            stop[2] = 0x81;
            stop[3] = 0;
            stop[4] = 0x73;
            stop[5] = 11;
            _stream.Write(stop, 0, stop.Length);
            _stream.Flush();
        }

        public override void Dispose()
        {
            if (_stream != null)
            {
                _stream.Flush();
                _stream.Close();
                _stream.Dispose();
                _stream = null;
            }
            //if (_stream2 != null)
            //{
            //    _stream2.Flush();
            //    _stream2.Close();
            //    _stream2.Dispose();
            //    _stream2 = null;
            //}
            if (_client != null)
            {
                _client.Close();
                _client.Dispose();
                _client = null;
            }
        }

        private int _lastPackId = -1;
        private int badPacketCount = 0;

        private void ReadStream(Stream stream)
        {
            int readByte;
            do
            {
                readByte = 0;
                int packetLengthJump = 0;
                try
                {
                    while (readByte >= 0 && readByte != 0x7E)
                    {
                        readByte = ReadByteStream(stream);
                    }
                    // нашли начало пакета
                    if (readByte == 0x7E)
                    {
                        packetLengthJump = ReadByteStream(stream, true) + 1; // длина пакета
                        byte sum = 0; // контрольная сумма
                        // 3.	Байт – тип передаваемого пакета.
                        // 0x81 – информационный пакет, 0x89 – пакет подтверждения(ACK).
                        byte packetType = ReadByteStream(stream, ref packetLengthJump, ref sum);
                        if (packetType != 0x81)
                            throw new PacketException(String.Format("Обрабатываем только информационные пакеты (0x81, байт 0x{0:X}).", packetType), true);
                        // для подтверждения
                        byte[] ask = null;
                        byte packId = 0;
                        // 4.	Байт – вид сообщения. 0x00 – сообщение не требует подтверждения о получении,
                        // 0x01- сообщение требует подтверждения о получении,
                        // 0x02 - сообщение не требует подтверждения о получении и прибор находится в режиме ожидания.
                        byte askFlag = ReadByteStream(stream, ref packetLengthJump, ref sum);
                        if (askFlag == 0x01)
                            ask = new byte[] { 0x7E, 3, 0x89, packId, 0, 0 };
                        else if (askFlag != 0x00 && askFlag != 0x02)
                            throw new PacketException("Неправильный флаг подтверждения (должен быть 0x00, 0x01 или 0x02, а не 0x" +
                                askFlag.ToString("X2") + ").", true);
                        // 5.	Для типа пакета 0x81 – 0x70 означает передачу значения напряжения на приборе, 0x44 – передачу данных с АЦП,
                        // 0x67 – команда включить режим измерений, 0x73 – команда остановить режим измерений.
                        // Для типа пакета 0x89 – байт означает идентификатор пакета, подтверждаемого данным сообщением.
                        // (FrameID = 0x80 | PackID).
                        byte packetTask = ReadByteStream(stream, ref packetLengthJump, ref sum);
                        byte[] dataBytes;
                        bool flagPulse;
                        switch (packetTask)
                        {
                            case 0x70:
                                flagPulse = false;
                                dataBytes = ReadByteArrayStream(2, stream, ref packetLengthJump, ref sum);
                                break;
                            case 0x44:
                                flagPulse = true;
                                packId = (byte)(0x80 | ReadByteStream(stream, ref packetLengthJump, ref sum)); // PackID
                                // подготовка подтверждения
                                if (ask != null && ask.Length > 3)
                                {
                                    ask[3] = packId;
                                    PreparePacketToSend(ref ask);
                                }
                                if (_lastPackId == packId)
                                {
                                    // отправка подтверждения
                                    if (ask != null)
                                    {
                                        stream.Write(ask, 0, ask.Length);
                                    }
                                    throw new PacketException("Пакет уже был обработан (PackID = " + packId + ").",
                                                              true, ask);
                                }
                                _lastPackId = packId;
                                dataBytes = ReadByteArrayStream(packetLengthJump - 1, stream, ref packetLengthJump, ref sum);
                                break;
                            default:
                                throw new PacketException(
                                    "Неправильный флаг назначения (должен быть 0x70 или 0x44, а не 0x" +
                                    packetTask.ToString("X2") + ").", true);
                        }
                        ReadByteStream(stream, ref packetLengthJump, ref sum);
                        if (sum != 0xFF)
                            throw new PacketException("Не выполнена проверка контрольной суммы.", true);
                        // отправка подтверждения
                        if (ask != null)
                        {
                            stream.Write(ask, 0, ask.Length);
                        }
                        // запуск обработки результатов
                        //ThreadPool.QueueUserWorkItem(
                        //    flagPulse ? new WaitCallback(GetPulse) : new WaitCallback(GetVolt), dataBytes);
                        if (flagPulse)
                            GetPulse(dataBytes);
                        else
                            GetVolt(dataBytes);
                    }
                }
                catch (PacketException pe)
                {
                    if (pe.PacketError)
                    {
                        //int jumpedBytes = 0;
                        try
                        {
                            //while (jumpedBytes < packetLengthJump)
                            //{
                            //    byte[] buffer = new byte[packetLengthJump];
                            //    jumpedBytes += stream.Read(buffer, 0, packetLengthJump - jumpedBytes);
                            //}
                            //if (pe.Ask != null)
                            //    stream.Write(pe.Ask, 0, pe.Ask.Length);
                            //stream.Flush();
                            for (int i = 0; i < packetLengthJump; i++)
                                ReadByteStream(stream);
                            //if (pe.Ask != null)
                            //    _stream2.Write(pe.Ask, 0, pe.Ask.Length);
                            //stream.Flush();
                            //byte[] buffer = new byte[packetLengthJump * 2];
                            //int readedBytes = 0;
                            //do
                            //{
                            //    readedBytes += stream.Read(buffer, 0, buffer.Length);
                            //} while (readedBytes < buffer.Length);
                            //stream.Flush();
                            //if (pe.Ask != null)
                            //    stream.Write(pe.Ask, 0, pe.Ask.Length);
                            //stream.Flush();
                            Console.WriteLine("PacketException : " + pe.Message + ", " + pe.PacketError + ", счетчик " + ++badPacketCount
                                 + ", packetLengthJump " + packetLengthJump);
                        }
                        catch (Exception)
                        {
                            readByte = -1;
                        }
                    }
                }
                catch (Exception)
                {
                    readByte = -1;
                }
            } while (readByte >= 0);
        }

        private void GetVolt(Object o)
        {
            byte[] bytes = (byte[])o;
            GetVolt(bytes);
        }

        private void GetVolt(byte[] bytes)
        {
            if (bytes == null || bytes.Length < 2)
                return;
            _volt = (bytes[0] << 8) | bytes[1];
        }

        private void GetPulse(Object o)
        {
            byte[] bytes = (byte[])o;
            GetPulse(bytes);
        }

        private void GetPulse(byte[] bytes)
        {
            if (bytes == null || bytes.Length < 2)
                return;
            int curByte = 0;
            while (curByte < (bytes.Length - 1))
            {
                ushort val = (ushort)((0xFFF & (bytes[curByte] << 8)) | bytes[curByte + 1]);
                OnNewPulse(new NewPulseEventArgs(val));
                curByte += 2;
            }
        }

        private byte[] ReadByteArrayStream(int n, Stream stream, ref int jumpBytes, ref byte sum)
        {
            if (n <= 0)
                return null;
            byte[] bytes = new byte[n];
            for (int i = 0; i < n; i++)
                bytes[i] = ReadByteStream(stream, ref jumpBytes, ref sum);
            return bytes;
        }

        private byte ReadByteStream(Stream stream, bool flagReadingPacket = false)
        {
            int readByte = stream.ReadByte();
            if (readByte == 0x7E)
                if (flagReadingPacket)
                    throw new PacketException("Неожиданное окончание пакета.");
                else
                    return (byte)readByte;
            if (readByte == 0x7D)
            {
                readByte = stream.ReadByte();
                if (readByte >= 0)
                    readByte ^= 0x20;
            }
            return (byte)readByte;
        }

        private byte ReadByteStream(Stream stream, ref int jumpBytes, ref byte sum)
        {
            byte readByte = ReadByteStream(stream, true);
            jumpBytes--;
            sum += readByte;
            return readByte;
        }

        // подготовка к отправке пакета
        public static void PreparePacketToSend(ref byte[] bytes)
        {
            if (bytes == null || bytes.Length < 3)
                return;
            // ckeksum
            byte sum = 0;
            for (int i = 2; i < bytes.Length - 1; i++)
                sum += bytes[i];
            bytes[bytes.Length - 1] = (byte)(0xFF - sum);
            // Escape (0x7E = 0x7D, 0x5E; 0x7D = 0x7D, 0x5D (0x5E ^ 0x20 = 0x7E))
            int curByte = 1;
            while (curByte < bytes.Length)
            {
                if (bytes[curByte] == 0x7E || bytes[curByte] == 0x7D)
                {
                    byte[] arrNew = new byte[bytes.Length + 1];
                    for (int i = 0; i < curByte; i++)
                        arrNew[i] = bytes[i];
                    arrNew[curByte] = 0x7D;
                    arrNew[curByte + 1] = (byte)(bytes[curByte] ^ 0x20);
                    for (int i = curByte + 2; i < arrNew.Length; i++)
                        arrNew[i] = bytes[i - 1];
                    bytes = arrNew;
                    curByte++;
                }
                curByte++;
            }
        }


    }


}
