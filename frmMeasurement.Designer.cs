﻿namespace TonClock
{
    partial class frmMeasurement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.grpInfo = new System.Windows.Forms.GroupBox();
            this.lblFio = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.lblTime = new System.Windows.Forms.Label();
            this.lblControl = new System.Windows.Forms.Label();
            this.lblAge = new System.Windows.Forms.Label();
            this.lblFioTitle = new System.Windows.Forms.Label();
            this.lblDateTitle = new System.Windows.Forms.Label();
            this.lblControlTitle = new System.Windows.Forms.Label();
            this.lblTimeTitle = new System.Windows.Forms.Label();
            this.lblAgeTitle = new System.Windows.Forms.Label();
            this.grpResult = new System.Windows.Forms.GroupBox();
            this.lblStressIndex = new System.Windows.Forms.Label();
            this.lblStressIndexTitle = new System.Windows.Forms.Label();
            this.lblPulse = new System.Windows.Forms.Label();
            this.lblPulseTitle = new System.Windows.Forms.Label();
            this.lblSosud = new System.Windows.Forms.Label();
            this.lblPressure = new System.Windows.Forms.Label();
            this.lblSosudTitle = new System.Windows.Forms.Label();
            this.lblPressureTitle = new System.Windows.Forms.Label();
            this.lblPulseWave = new System.Windows.Forms.Label();
            this.pnlPulseData = new System.Windows.Forms.Panel();
            this.grpInfo.SuspendLayout();
            this.grpResult.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnOK.Location = new System.Drawing.Point(496, 412);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(117, 30);
            this.btnOK.TabIndex = 6;
            this.btnOK.Text = "ОК";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // grpInfo
            // 
            this.grpInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpInfo.Controls.Add(this.lblFio);
            this.grpInfo.Controls.Add(this.lblDate);
            this.grpInfo.Controls.Add(this.lblTime);
            this.grpInfo.Controls.Add(this.lblControl);
            this.grpInfo.Controls.Add(this.lblAge);
            this.grpInfo.Controls.Add(this.lblFioTitle);
            this.grpInfo.Controls.Add(this.lblDateTitle);
            this.grpInfo.Controls.Add(this.lblControlTitle);
            this.grpInfo.Controls.Add(this.lblTimeTitle);
            this.grpInfo.Controls.Add(this.lblAgeTitle);
            this.grpInfo.Location = new System.Drawing.Point(12, 12);
            this.grpInfo.Name = "grpInfo";
            this.grpInfo.Size = new System.Drawing.Size(601, 123);
            this.grpInfo.TabIndex = 20;
            this.grpInfo.TabStop = false;
            this.grpInfo.Text = "Общая информация о замере";
            // 
            // lblFio
            // 
            this.lblFio.AutoSize = true;
            this.lblFio.Location = new System.Drawing.Point(168, 21);
            this.lblFio.Name = "lblFio";
            this.lblFio.Size = new System.Drawing.Size(29, 13);
            this.lblFio.TabIndex = 29;
            this.lblFio.Text = "label";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Location = new System.Drawing.Point(168, 61);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(29, 13);
            this.lblDate.TabIndex = 29;
            this.lblDate.Text = "label";
            // 
            // lblTime
            // 
            this.lblTime.AutoSize = true;
            this.lblTime.Location = new System.Drawing.Point(168, 81);
            this.lblTime.Name = "lblTime";
            this.lblTime.Size = new System.Drawing.Size(29, 13);
            this.lblTime.TabIndex = 29;
            this.lblTime.Text = "label";
            // 
            // lblControl
            // 
            this.lblControl.AutoSize = true;
            this.lblControl.Location = new System.Drawing.Point(168, 101);
            this.lblControl.Name = "lblControl";
            this.lblControl.Size = new System.Drawing.Size(29, 13);
            this.lblControl.TabIndex = 29;
            this.lblControl.Text = "label";
            // 
            // lblAge
            // 
            this.lblAge.AutoSize = true;
            this.lblAge.Location = new System.Drawing.Point(168, 41);
            this.lblAge.Name = "lblAge";
            this.lblAge.Size = new System.Drawing.Size(29, 13);
            this.lblAge.TabIndex = 29;
            this.lblAge.Text = "label";
            // 
            // lblFioTitle
            // 
            this.lblFioTitle.AutoSize = true;
            this.lblFioTitle.Location = new System.Drawing.Point(28, 21);
            this.lblFioTitle.Name = "lblFioTitle";
            this.lblFioTitle.Size = new System.Drawing.Size(134, 13);
            this.lblFioTitle.TabIndex = 28;
            this.lblFioTitle.Text = "Фамилия И.О. пациента:";
            // 
            // lblDateTitle
            // 
            this.lblDateTitle.AutoSize = true;
            this.lblDateTitle.Location = new System.Drawing.Point(28, 61);
            this.lblDateTitle.Name = "lblDateTitle";
            this.lblDateTitle.Size = new System.Drawing.Size(77, 13);
            this.lblDateTitle.TabIndex = 28;
            this.lblDateTitle.Text = "Дата замера:";
            // 
            // lblControlTitle
            // 
            this.lblControlTitle.AutoSize = true;
            this.lblControlTitle.Location = new System.Drawing.Point(28, 101);
            this.lblControlTitle.Name = "lblControlTitle";
            this.lblControlTitle.Size = new System.Drawing.Size(123, 13);
            this.lblControlTitle.TabIndex = 28;
            this.lblControlTitle.Text = "Контроль тонометром:";
            // 
            // lblTimeTitle
            // 
            this.lblTimeTitle.AutoSize = true;
            this.lblTimeTitle.Location = new System.Drawing.Point(28, 81);
            this.lblTimeTitle.Name = "lblTimeTitle";
            this.lblTimeTitle.Size = new System.Drawing.Size(84, 13);
            this.lblTimeTitle.TabIndex = 28;
            this.lblTimeTitle.Text = "Время замера:";
            // 
            // lblAgeTitle
            // 
            this.lblAgeTitle.AutoSize = true;
            this.lblAgeTitle.Location = new System.Drawing.Point(28, 41);
            this.lblAgeTitle.Name = "lblAgeTitle";
            this.lblAgeTitle.Size = new System.Drawing.Size(102, 13);
            this.lblAgeTitle.TabIndex = 28;
            this.lblAgeTitle.Text = "Возраст пациента:";
            // 
            // grpResult
            // 
            this.grpResult.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpResult.Controls.Add(this.lblStressIndex);
            this.grpResult.Controls.Add(this.lblStressIndexTitle);
            this.grpResult.Controls.Add(this.lblPulse);
            this.grpResult.Controls.Add(this.lblPulseTitle);
            this.grpResult.Controls.Add(this.lblSosud);
            this.grpResult.Controls.Add(this.lblPressure);
            this.grpResult.Controls.Add(this.lblSosudTitle);
            this.grpResult.Controls.Add(this.lblPressureTitle);
            this.grpResult.Location = new System.Drawing.Point(12, 292);
            this.grpResult.Name = "grpResult";
            this.grpResult.Size = new System.Drawing.Size(601, 114);
            this.grpResult.TabIndex = 20;
            this.grpResult.TabStop = false;
            this.grpResult.Text = "Результаты замера";
            // 
            // lblStressIndex
            // 
            this.lblStressIndex.AutoSize = true;
            this.lblStressIndex.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblStressIndex.Location = new System.Drawing.Point(207, 90);
            this.lblStressIndex.Name = "lblStressIndex";
            this.lblStressIndex.Size = new System.Drawing.Size(38, 16);
            this.lblStressIndex.TabIndex = 7;
            this.lblStressIndex.Text = "label";
            // 
            // lblStressIndexTitle
            // 
            this.lblStressIndexTitle.AutoSize = true;
            this.lblStressIndexTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblStressIndexTitle.Location = new System.Drawing.Point(28, 90);
            this.lblStressIndexTitle.Name = "lblStressIndexTitle";
            this.lblStressIndexTitle.Size = new System.Drawing.Size(139, 16);
            this.lblStressIndexTitle.TabIndex = 7;
            this.lblStressIndexTitle.Text = "Стрессовый индекс:";
            // 
            // lblPulse
            // 
            this.lblPulse.AutoSize = true;
            this.lblPulse.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblPulse.Location = new System.Drawing.Point(207, 67);
            this.lblPulse.Name = "lblPulse";
            this.lblPulse.Size = new System.Drawing.Size(38, 16);
            this.lblPulse.TabIndex = 7;
            this.lblPulse.Text = "label";
            // 
            // lblPulseTitle
            // 
            this.lblPulseTitle.AutoSize = true;
            this.lblPulseTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblPulseTitle.Location = new System.Drawing.Point(28, 67);
            this.lblPulseTitle.Name = "lblPulseTitle";
            this.lblPulseTitle.Size = new System.Drawing.Size(51, 16);
            this.lblPulseTitle.TabIndex = 7;
            this.lblPulseTitle.Text = "Пульс:";
            // 
            // lblSosud
            // 
            this.lblSosud.AutoSize = true;
            this.lblSosud.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSosud.Location = new System.Drawing.Point(207, 21);
            this.lblSosud.Name = "lblSosud";
            this.lblSosud.Size = new System.Drawing.Size(38, 16);
            this.lblSosud.TabIndex = 7;
            this.lblSosud.Text = "label";
            // 
            // lblPressure
            // 
            this.lblPressure.AutoSize = true;
            this.lblPressure.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblPressure.Location = new System.Drawing.Point(207, 44);
            this.lblPressure.Name = "lblPressure";
            this.lblPressure.Size = new System.Drawing.Size(38, 16);
            this.lblPressure.TabIndex = 7;
            this.lblPressure.Text = "label";
            // 
            // lblSosudTitle
            // 
            this.lblSosudTitle.AutoSize = true;
            this.lblSosudTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblSosudTitle.Location = new System.Drawing.Point(28, 21);
            this.lblSosudTitle.Name = "lblSosudTitle";
            this.lblSosudTitle.Size = new System.Drawing.Size(138, 16);
            this.lblSosudTitle.TabIndex = 7;
            this.lblSosudTitle.Text = "Состояние сосудов:";
            // 
            // lblPressureTitle
            // 
            this.lblPressureTitle.AutoSize = true;
            this.lblPressureTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblPressureTitle.Location = new System.Drawing.Point(28, 44);
            this.lblPressureTitle.Name = "lblPressureTitle";
            this.lblPressureTitle.Size = new System.Drawing.Size(173, 16);
            this.lblPressureTitle.TabIndex = 7;
            this.lblPressureTitle.Text = "Артериальное давление:";
            // 
            // lblPulseWave
            // 
            this.lblPulseWave.AutoSize = true;
            this.lblPulseWave.Location = new System.Drawing.Point(18, 143);
            this.lblPulseWave.Name = "lblPulseWave";
            this.lblPulseWave.Size = new System.Drawing.Size(217, 13);
            this.lblPulseWave.TabIndex = 21;
            this.lblPulseWave.Text = "Записанный фрагмент пульсовой волны:";
            // 
            // pnlPulseData
            // 
            this.pnlPulseData.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlPulseData.AutoScroll = true;
            this.pnlPulseData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPulseData.Location = new System.Drawing.Point(12, 159);
            this.pnlPulseData.Name = "pnlPulseData";
            this.pnlPulseData.Size = new System.Drawing.Size(601, 121);
            this.pnlPulseData.TabIndex = 22;
            this.pnlPulseData.Scroll += new System.Windows.Forms.ScrollEventHandler(this.pnlPulseData_Scroll);
            this.pnlPulseData.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlPulseData_Paint);
            // 
            // frmMeasurement
            // 
            this.AcceptButton = this.btnOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 454);
            this.Controls.Add(this.pnlPulseData);
            this.Controls.Add(this.lblPulseWave);
            this.Controls.Add(this.grpResult);
            this.Controls.Add(this.grpInfo);
            this.Controls.Add(this.btnOK);
            this.MinimizeBox = false;
            this.Name = "frmMeasurement";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Просмотр результатов замера";
            this.Shown += new System.EventHandler(this.frmMeasurement_Shown);
            this.grpInfo.ResumeLayout(false);
            this.grpInfo.PerformLayout();
            this.grpResult.ResumeLayout(false);
            this.grpResult.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.GroupBox grpInfo;
        private System.Windows.Forms.GroupBox grpResult;
        private System.Windows.Forms.Label lblPressureTitle;
        private System.Windows.Forms.Label lblStressIndex;
        private System.Windows.Forms.Label lblStressIndexTitle;
        private System.Windows.Forms.Label lblPulse;
        private System.Windows.Forms.Label lblPulseTitle;
        private System.Windows.Forms.Label lblPressure;
        private System.Windows.Forms.Label lblFio;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.Label lblTime;
        private System.Windows.Forms.Label lblAge;
        private System.Windows.Forms.Label lblFioTitle;
        private System.Windows.Forms.Label lblDateTitle;
        private System.Windows.Forms.Label lblTimeTitle;
        private System.Windows.Forms.Label lblAgeTitle;
        private System.Windows.Forms.Label lblPulseWave;
        private System.Windows.Forms.Panel pnlPulseData;
        private System.Windows.Forms.Label lblSosud;
        private System.Windows.Forms.Label lblSosudTitle;
        private System.Windows.Forms.Label lblControl;
        private System.Windows.Forms.Label lblControlTitle;
    }
}