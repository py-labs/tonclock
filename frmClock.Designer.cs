﻿namespace TonClock
{
    partial class frmClock
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStripMain = new System.Windows.Forms.MenuStrip();
            this.mnuProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSound = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSoundPulse = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSoundPulseOn = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSoundPulseOff = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSoundResult = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSoundResultOn = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSoundResultOff = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTemplates = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTemplatesWaves = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTemplatesStress = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuTemplatesHide = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuService = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuBuffer = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.mnuMolodimetr = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStripMain = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.tmrWatch = new System.Windows.Forms.Timer(this.components);
            this.prbRec = new System.Windows.Forms.ProgressBar();
            this.tmrLight = new System.Windows.Forms.Timer(this.components);
            this.pnlWave = new System.Windows.Forms.Panel();
            this.picTemplates = new System.Windows.Forms.PictureBox();
            this.picWatch = new System.Windows.Forms.PictureBox();
            this.bkwPulseWave = new System.ComponentModel.BackgroundWorker();
            this.bkwSound = new System.ComponentModel.BackgroundWorker();
            this.menuStripMain.SuspendLayout();
            this.statusStripMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picTemplates)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picWatch)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStripMain
            // 
            this.menuStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuProperties,
            this.mnuSound,
            this.mnuTemplates,
            this.mnuService});
            this.menuStripMain.Location = new System.Drawing.Point(0, 0);
            this.menuStripMain.Name = "menuStripMain";
            this.menuStripMain.Size = new System.Drawing.Size(458, 24);
            this.menuStripMain.TabIndex = 1;
            this.menuStripMain.Text = "menuStrip1";
            // 
            // mnuProperties
            // 
            this.mnuProperties.Name = "mnuProperties";
            this.mnuProperties.Size = new System.Drawing.Size(78, 20);
            this.mnuProperties.Text = "&Настройка";
            this.mnuProperties.Click += new System.EventHandler(this.mnuProperties_Click);
            // 
            // mnuSound
            // 
            this.mnuSound.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuSoundPulse,
            this.mnuSoundResult});
            this.mnuSound.Name = "mnuSound";
            this.mnuSound.Size = new System.Drawing.Size(44, 20);
            this.mnuSound.Text = "&Звук";
            // 
            // mnuSoundPulse
            // 
            this.mnuSoundPulse.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuSoundPulseOn,
            this.mnuSoundPulseOff});
            this.mnuSoundPulse.Name = "mnuSoundPulse";
            this.mnuSoundPulse.Size = new System.Drawing.Size(169, 22);
            this.mnuSoundPulse.Text = "Пульсовая волна";
            // 
            // mnuSoundPulseOn
            // 
            this.mnuSoundPulseOn.Name = "mnuSoundPulseOn";
            this.mnuSoundPulseOn.Size = new System.Drawing.Size(131, 22);
            this.mnuSoundPulseOn.Text = "Включен";
            this.mnuSoundPulseOn.Click += new System.EventHandler(this.mnuSoundPulseOn_Click);
            // 
            // mnuSoundPulseOff
            // 
            this.mnuSoundPulseOff.Name = "mnuSoundPulseOff";
            this.mnuSoundPulseOff.Size = new System.Drawing.Size(131, 22);
            this.mnuSoundPulseOff.Text = "Отключен";
            this.mnuSoundPulseOff.Click += new System.EventHandler(this.mnuSoundPulseOff_Click);
            // 
            // mnuSoundResult
            // 
            this.mnuSoundResult.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuSoundResultOn,
            this.mnuSoundResultOff});
            this.mnuSoundResult.Name = "mnuSoundResult";
            this.mnuSoundResult.Size = new System.Drawing.Size(169, 22);
            this.mnuSoundResult.Text = "Комментарий";
            // 
            // mnuSoundResultOn
            // 
            this.mnuSoundResultOn.Name = "mnuSoundResultOn";
            this.mnuSoundResultOn.Size = new System.Drawing.Size(131, 22);
            this.mnuSoundResultOn.Text = "Включен";
            this.mnuSoundResultOn.Click += new System.EventHandler(this.mnuSoundResultOn_Click);
            // 
            // mnuSoundResultOff
            // 
            this.mnuSoundResultOff.Name = "mnuSoundResultOff";
            this.mnuSoundResultOff.Size = new System.Drawing.Size(131, 22);
            this.mnuSoundResultOff.Text = "Отключен";
            this.mnuSoundResultOff.Click += new System.EventHandler(this.mnuSoundResultOff_Click);
            // 
            // mnuTemplates
            // 
            this.mnuTemplates.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuTemplatesWaves,
            this.mnuTemplatesStress,
            this.mnuTemplatesHide});
            this.mnuTemplates.Enabled = false;
            this.mnuTemplates.Name = "mnuTemplates";
            this.mnuTemplates.Size = new System.Drawing.Size(73, 20);
            this.mnuTemplates.Text = "&Шаблоны";
            // 
            // mnuTemplatesWaves
            // 
            this.mnuTemplatesWaves.Name = "mnuTemplatesWaves";
            this.mnuTemplatesWaves.Size = new System.Drawing.Size(172, 22);
            this.mnuTemplatesWaves.Text = "Фитнес-шкала";
            this.mnuTemplatesWaves.Click += new System.EventHandler(this.mnuTemplatesWaves_Click);
            // 
            // mnuTemplatesStress
            // 
            this.mnuTemplatesStress.Name = "mnuTemplatesStress";
            this.mnuTemplatesStress.Size = new System.Drawing.Size(172, 22);
            this.mnuTemplatesStress.Text = "Стресс";
            this.mnuTemplatesStress.Click += new System.EventHandler(this.mnuTemplatesStress_Click);
            // 
            // mnuTemplatesHide
            // 
            this.mnuTemplatesHide.Name = "mnuTemplatesHide";
            this.mnuTemplatesHide.Size = new System.Drawing.Size(172, 22);
            this.mnuTemplatesHide.Text = "Скрыть шаблоны";
            this.mnuTemplatesHide.Click += new System.EventHandler(this.mnuTemplatesHide_Click);
            // 
            // mnuService
            // 
            this.mnuService.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuBuffer,
            this.toolStripSeparator1,
            this.mnuMolodimetr});
            this.mnuService.Name = "mnuService";
            this.mnuService.Size = new System.Drawing.Size(59, 20);
            this.mnuService.Text = "&Сервис";
            // 
            // mnuBuffer
            // 
            this.mnuBuffer.Image = global::TonClock.Properties.Resources.Key_16x16;
            this.mnuBuffer.Name = "mnuBuffer";
            this.mnuBuffer.Size = new System.Drawing.Size(146, 22);
            this.mnuBuffer.Text = "&Буфер";
            this.mnuBuffer.Click += new System.EventHandler(this.mnuBuffer_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(143, 6);
            // 
            // mnuMolodimetr
            // 
            this.mnuMolodimetr.Enabled = false;
            this.mnuMolodimetr.Name = "mnuMolodimetr";
            this.mnuMolodimetr.Size = new System.Drawing.Size(146, 22);
            this.mnuMolodimetr.Text = "&Молодиметр";
            this.mnuMolodimetr.Click += new System.EventHandler(this.mnuMolodimetr_Click);
            // 
            // statusStripMain
            // 
            this.statusStripMain.AutoSize = false;
            this.statusStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.statusStripMain.Location = new System.Drawing.Point(0, 656);
            this.statusStripMain.Name = "statusStripMain";
            this.statusStripMain.Size = new System.Drawing.Size(458, 22);
            this.statusStripMain.TabIndex = 2;
            this.statusStripMain.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // tmrWatch
            // 
            this.tmrWatch.Enabled = true;
            this.tmrWatch.Interval = 500;
            this.tmrWatch.Tick += new System.EventHandler(this.tmrWatch_Tick);
            // 
            // prbRec
            // 
            this.prbRec.Location = new System.Drawing.Point(52, 44);
            this.prbRec.Name = "prbRec";
            this.prbRec.Size = new System.Drawing.Size(348, 15);
            this.prbRec.TabIndex = 5;
            this.prbRec.Visible = false;
            // 
            // tmrLight
            // 
            this.tmrLight.Enabled = true;
            this.tmrLight.Interval = 400;
            this.tmrLight.Tick += new System.EventHandler(this.tmrLight_Tick);
            // 
            // pnlWave
            // 
            this.pnlWave.BackgroundImage = global::TonClock.Properties.Resources.wave_background;
            this.pnlWave.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlWave.Location = new System.Drawing.Point(52, 291);
            this.pnlWave.Name = "pnlWave";
            this.pnlWave.Size = new System.Drawing.Size(100, 180);
            this.pnlWave.TabIndex = 7;
            this.pnlWave.Visible = false;
            this.pnlWave.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlWave_Paint);
            // 
            // picTemplates
            // 
            this.picTemplates.Location = new System.Drawing.Point(0, 56);
            this.picTemplates.Name = "picTemplates";
            this.picTemplates.Size = new System.Drawing.Size(550, 616);
            this.picTemplates.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picTemplates.TabIndex = 6;
            this.picTemplates.TabStop = false;
            this.picTemplates.Visible = false;
            // 
            // picWatch
            // 
            this.picWatch.Image = global::TonClock.Properties.Resources.watch_00;
            this.picWatch.Location = new System.Drawing.Point(15, 27);
            this.picWatch.Name = "picWatch";
            this.picWatch.Size = new System.Drawing.Size(414, 616);
            this.picWatch.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picWatch.TabIndex = 3;
            this.picWatch.TabStop = false;
            this.picWatch.Paint += new System.Windows.Forms.PaintEventHandler(this.picWatch_Paint);
            this.picWatch.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picWatch_MouseDown);
            this.picWatch.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picWatch_MouseMove);
            this.picWatch.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picWatch_MouseUp);
            // 
            // bkwPulseWave
            // 
            this.bkwPulseWave.WorkerSupportsCancellation = true;
            this.bkwPulseWave.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bkwPulseWave_DoWork);
            this.bkwPulseWave.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bkwPulseWave_RunWorkerCompleted);
            // 
            // bkwSound
            // 
            this.bkwSound.WorkerReportsProgress = true;
            this.bkwSound.WorkerSupportsCancellation = true;
            this.bkwSound.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bkwSound_DoWork);
            this.bkwSound.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bkwSound_ProgressChanged);
            // 
            // frmClock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 678);
            this.Controls.Add(this.pnlWave);
            this.Controls.Add(this.picTemplates);
            this.Controls.Add(this.prbRec);
            this.Controls.Add(this.picWatch);
            this.Controls.Add(this.statusStripMain);
            this.Controls.Add(this.menuStripMain);
            this.DoubleBuffered = true;
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStripMain;
            this.MinimizeBox = false;
            this.Name = "frmClock";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Часы-тонометр";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.Load += new System.EventHandler(this.frmClock_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmMain_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmMain_KeyUp);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmMain_MouseMove);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            this.menuStripMain.ResumeLayout(false);
            this.menuStripMain.PerformLayout();
            this.statusStripMain.ResumeLayout(false);
            this.statusStripMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picTemplates)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picWatch)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStripMain;
        private System.Windows.Forms.StatusStrip statusStripMain;
        private System.Windows.Forms.Timer tmrWatch;
        private System.Windows.Forms.PictureBox picWatch;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ProgressBar prbRec;
        private System.Windows.Forms.Timer tmrLight;
        private System.Windows.Forms.PictureBox picTemplates;
        private System.Windows.Forms.Panel pnlWave;
        private System.ComponentModel.BackgroundWorker bkwPulseWave;
        private System.Windows.Forms.ToolStripMenuItem mnuSound;
        private System.Windows.Forms.ToolStripMenuItem mnuSoundPulse;
        private System.Windows.Forms.ToolStripMenuItem mnuSoundPulseOn;
        private System.Windows.Forms.ToolStripMenuItem mnuSoundPulseOff;
        private System.Windows.Forms.ToolStripMenuItem mnuSoundResult;
        private System.Windows.Forms.ToolStripMenuItem mnuSoundResultOff;
        private System.Windows.Forms.ToolStripMenuItem mnuSoundResultOn;
        private System.Windows.Forms.ToolStripMenuItem mnuProperties;
        private System.Windows.Forms.ToolStripMenuItem mnuTemplates;
        private System.Windows.Forms.ToolStripMenuItem mnuTemplatesWaves;
        private System.Windows.Forms.ToolStripMenuItem mnuTemplatesStress;
        private System.Windows.Forms.ToolStripMenuItem mnuTemplatesHide;
        private System.ComponentModel.BackgroundWorker bkwSound;
        private System.Windows.Forms.ToolStripMenuItem mnuService;
        private System.Windows.Forms.ToolStripMenuItem mnuBuffer;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem mnuMolodimetr;
    }
}