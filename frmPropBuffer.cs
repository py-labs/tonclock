﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TonClock
{
    internal partial class frmPropBuffer : Form
    {

        /* ----- констуктор ----- */
        public frmPropBuffer(BufferSettings settings)
        {
            InitializeComponent();
            numScale.Value = (decimal) settings.Scale;
            chkPoints.Checked = settings.FlagPoints;
            chkPrintValues.Checked = settings.FlagPrintValues;
            chkDerivative.Checked = settings.FlagDerivative;
        }

        /* ----- возврат значений в главную форму ----- */
        private void btnOK_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }
        public double returnScale() { return (double) numScale.Value; }
        public bool returnPoints() { return chkPoints.Checked; }
        public bool returnPrintValues() { return chkPrintValues.Checked; }
        public bool returnDerivative() { return chkDerivative.Checked; }

    }
}
