﻿namespace TonClock
{
    partial class frmCasp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblPulse = new System.Windows.Forms.Label();
            this.pnlPulse = new System.Windows.Forms.Panel();
            this.btnCalc = new System.Windows.Forms.Button();
            this.grpParams = new System.Windows.Forms.GroupBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.txtSrpvp = new System.Windows.Forms.TextBox();
            this.lblSrpvp2 = new System.Windows.Forms.Label();
            this.txtTimeA1A4 = new System.Windows.Forms.TextBox();
            this.lblSrpvp = new System.Windows.Forms.Label();
            this.lblTimeA1A42 = new System.Windows.Forms.Label();
            this.lblTimeA1A4 = new System.Windows.Forms.Label();
            this.lblCentralPulse = new System.Windows.Forms.Label();
            this.pnlCentralPulse = new System.Windows.Forms.Panel();
            this.tlpPulse = new System.Windows.Forms.TableLayoutPanel();
            this.btnCalcIa = new System.Windows.Forms.Button();
            this.lblCasp = new System.Windows.Forms.Label();
            this.lblCasp2 = new System.Windows.Forms.Label();
            this.txtCasp = new System.Windows.Forms.TextBox();
            this.lblIa1 = new System.Windows.Forms.Label();
            this.txtIa1 = new System.Windows.Forms.TextBox();
            this.lblIa2 = new System.Windows.Forms.Label();
            this.txtIa2 = new System.Windows.Forms.TextBox();
            this.tlpParams = new System.Windows.Forms.TableLayoutPanel();
            this.grpCentral = new System.Windows.Forms.GroupBox();
            this.tmrButton = new System.Windows.Forms.Timer(this.components);
            this.grpParams.SuspendLayout();
            this.tlpPulse.SuspendLayout();
            this.tlpParams.SuspendLayout();
            this.grpCentral.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblPulse
            // 
            this.lblPulse.AutoSize = true;
            this.lblPulse.Location = new System.Drawing.Point(3, 0);
            this.lblPulse.Name = "lblPulse";
            this.lblPulse.Size = new System.Drawing.Size(77, 15);
            this.lblPulse.TabIndex = 0;
            this.lblPulse.Text = "Лучевая ПВ:";
            // 
            // pnlPulse
            // 
            this.pnlPulse.AutoScroll = true;
            this.pnlPulse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlPulse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPulse.Location = new System.Drawing.Point(3, 18);
            this.pnlPulse.Name = "pnlPulse";
            this.pnlPulse.Size = new System.Drawing.Size(454, 139);
            this.pnlPulse.TabIndex = 3;
            this.pnlPulse.Scroll += new System.Windows.Forms.ScrollEventHandler(this.pnlPulse_Scroll);
            this.pnlPulse.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlPulse_Paint);
            this.pnlPulse.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlPulse_MouseDown);
            this.pnlPulse.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlPulse_MouseMove);
            this.pnlPulse.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pnlPulse_MouseUp);
            // 
            // btnCalc
            // 
            this.btnCalc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCalc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCalc.Image = global::TonClock.Properties.Resources.Check_16x16;
            this.btnCalc.Location = new System.Drawing.Point(3, 163);
            this.btnCalc.Name = "btnCalc";
            this.btnCalc.Size = new System.Drawing.Size(454, 30);
            this.btnCalc.TabIndex = 0;
            this.btnCalc.Text = "Вычислить центральную ПВ";
            this.btnCalc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCalc.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCalc.UseVisualStyleBackColor = false;
            this.btnCalc.Click += new System.EventHandler(this.btnCalc_Click);
            // 
            // grpParams
            // 
            this.grpParams.Controls.Add(this.checkBox1);
            this.grpParams.Controls.Add(this.txtSrpvp);
            this.grpParams.Controls.Add(this.lblSrpvp2);
            this.grpParams.Controls.Add(this.txtTimeA1A4);
            this.grpParams.Controls.Add(this.lblSrpvp);
            this.grpParams.Controls.Add(this.lblTimeA1A42);
            this.grpParams.Controls.Add(this.lblTimeA1A4);
            this.grpParams.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpParams.Location = new System.Drawing.Point(3, 3);
            this.grpParams.Name = "grpParams";
            this.grpParams.Size = new System.Drawing.Size(224, 107);
            this.grpParams.TabIndex = 4;
            this.grpParams.TabStop = false;
            this.grpParams.Text = "Расчетные параметры:";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(62, 74);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(125, 19);
            this.checkBox1.TabIndex = 3;
            this.checkBox1.Text = "Тестовая печать";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            // 
            // txtSrpvp
            // 
            this.txtSrpvp.Location = new System.Drawing.Point(62, 47);
            this.txtSrpvp.Name = "txtSrpvp";
            this.txtSrpvp.ReadOnly = true;
            this.txtSrpvp.Size = new System.Drawing.Size(85, 21);
            this.txtSrpvp.TabIndex = 1;
            // 
            // lblSrpvp2
            // 
            this.lblSrpvp2.AutoSize = true;
            this.lblSrpvp2.Location = new System.Drawing.Point(153, 50);
            this.lblSrpvp2.Name = "lblSrpvp2";
            this.lblSrpvp2.Size = new System.Drawing.Size(25, 15);
            this.lblSrpvp2.TabIndex = 0;
            this.lblSrpvp2.Text = "м/с";
            // 
            // txtTimeA1A4
            // 
            this.txtTimeA1A4.Location = new System.Drawing.Point(62, 20);
            this.txtTimeA1A4.Name = "txtTimeA1A4";
            this.txtTimeA1A4.ReadOnly = true;
            this.txtTimeA1A4.Size = new System.Drawing.Size(85, 21);
            this.txtTimeA1A4.TabIndex = 1;
            // 
            // lblSrpvp
            // 
            this.lblSrpvp.AutoSize = true;
            this.lblSrpvp.Location = new System.Drawing.Point(6, 50);
            this.lblSrpvp.Name = "lblSrpvp";
            this.lblSrpvp.Size = new System.Drawing.Size(50, 15);
            this.lblSrpvp.TabIndex = 0;
            this.lblSrpvp.Text = "СРПВп:";
            // 
            // lblTimeA1A42
            // 
            this.lblTimeA1A42.AutoSize = true;
            this.lblTimeA1A42.Location = new System.Drawing.Point(153, 23);
            this.lblTimeA1A42.Name = "lblTimeA1A42";
            this.lblTimeA1A42.Size = new System.Drawing.Size(13, 15);
            this.lblTimeA1A42.TabIndex = 0;
            this.lblTimeA1A42.Text = "с";
            // 
            // lblTimeA1A4
            // 
            this.lblTimeA1A4.AutoSize = true;
            this.lblTimeA1A4.Location = new System.Drawing.Point(6, 23);
            this.lblTimeA1A4.Name = "lblTimeA1A4";
            this.lblTimeA1A4.Size = new System.Drawing.Size(48, 15);
            this.lblTimeA1A4.TabIndex = 0;
            this.lblTimeA1A4.Text = "A1 - A4:";
            // 
            // lblCentralPulse
            // 
            this.lblCentralPulse.AutoSize = true;
            this.lblCentralPulse.Location = new System.Drawing.Point(3, 196);
            this.lblCentralPulse.Name = "lblCentralPulse";
            this.lblCentralPulse.Size = new System.Drawing.Size(109, 15);
            this.lblCentralPulse.TabIndex = 0;
            this.lblCentralPulse.Text = "Центральная ПВ:";
            // 
            // pnlCentralPulse
            // 
            this.pnlCentralPulse.AutoScroll = true;
            this.pnlCentralPulse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCentralPulse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCentralPulse.Location = new System.Drawing.Point(3, 214);
            this.pnlCentralPulse.Name = "pnlCentralPulse";
            this.pnlCentralPulse.Size = new System.Drawing.Size(454, 139);
            this.pnlCentralPulse.TabIndex = 3;
            this.pnlCentralPulse.Scroll += new System.Windows.Forms.ScrollEventHandler(this.pnlCentralPulse_Scroll);
            this.pnlCentralPulse.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlCentralPulse_Paint);
            this.pnlCentralPulse.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pnlCentralPulse_MouseDown);
            this.pnlCentralPulse.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlCentralPulse_MouseMove);
            this.pnlCentralPulse.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pnlCentralPulse_MouseUp);
            // 
            // tlpPulse
            // 
            this.tlpPulse.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpPulse.ColumnCount = 1;
            this.tlpPulse.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpPulse.Controls.Add(this.btnCalcIa, 0, 5);
            this.tlpPulse.Controls.Add(this.lblPulse, 0, 0);
            this.tlpPulse.Controls.Add(this.lblCentralPulse, 0, 3);
            this.tlpPulse.Controls.Add(this.btnCalc, 0, 2);
            this.tlpPulse.Controls.Add(this.pnlPulse, 0, 1);
            this.tlpPulse.Controls.Add(this.pnlCentralPulse, 0, 4);
            this.tlpPulse.Location = new System.Drawing.Point(12, 12);
            this.tlpPulse.Name = "tlpPulse";
            this.tlpPulse.RowCount = 6;
            this.tlpPulse.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpPulse.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpPulse.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tlpPulse.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tlpPulse.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpPulse.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tlpPulse.Size = new System.Drawing.Size(460, 392);
            this.tlpPulse.TabIndex = 9;
            // 
            // btnCalcIa
            // 
            this.btnCalcIa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnCalcIa.Enabled = false;
            this.btnCalcIa.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnCalcIa.Image = global::TonClock.Properties.Resources.Check_16x16;
            this.btnCalcIa.Location = new System.Drawing.Point(3, 359);
            this.btnCalcIa.Name = "btnCalcIa";
            this.btnCalcIa.Size = new System.Drawing.Size(454, 30);
            this.btnCalcIa.TabIndex = 4;
            this.btnCalcIa.Text = "Вычислить ИА";
            this.btnCalcIa.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCalcIa.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCalcIa.UseVisualStyleBackColor = false;
            this.btnCalcIa.Click += new System.EventHandler(this.btnCalcIa_Click);
            // 
            // lblCasp
            // 
            this.lblCasp.AutoSize = true;
            this.lblCasp.ForeColor = System.Drawing.Color.Red;
            this.lblCasp.Location = new System.Drawing.Point(6, 23);
            this.lblCasp.Name = "lblCasp";
            this.lblCasp.Size = new System.Drawing.Size(41, 15);
            this.lblCasp.TabIndex = 0;
            this.lblCasp.Text = "CASP:";
            // 
            // lblCasp2
            // 
            this.lblCasp2.AutoSize = true;
            this.lblCasp2.Location = new System.Drawing.Point(153, 23);
            this.lblCasp2.Name = "lblCasp2";
            this.lblCasp2.Size = new System.Drawing.Size(61, 15);
            this.lblCasp2.TabIndex = 0;
            this.lblCasp2.Text = "мм рт.ст.";
            // 
            // txtCasp
            // 
            this.txtCasp.Location = new System.Drawing.Point(62, 20);
            this.txtCasp.Name = "txtCasp";
            this.txtCasp.ReadOnly = true;
            this.txtCasp.Size = new System.Drawing.Size(85, 21);
            this.txtCasp.TabIndex = 1;
            // 
            // lblIa1
            // 
            this.lblIa1.AutoSize = true;
            this.lblIa1.Location = new System.Drawing.Point(6, 50);
            this.lblIa1.Name = "lblIa1";
            this.lblIa1.Size = new System.Drawing.Size(33, 15);
            this.lblIa1.TabIndex = 0;
            this.lblIa1.Text = "ИА1:";
            // 
            // txtIa1
            // 
            this.txtIa1.Location = new System.Drawing.Point(62, 47);
            this.txtIa1.Name = "txtIa1";
            this.txtIa1.ReadOnly = true;
            this.txtIa1.Size = new System.Drawing.Size(85, 21);
            this.txtIa1.TabIndex = 1;
            // 
            // lblIa2
            // 
            this.lblIa2.AutoSize = true;
            this.lblIa2.Location = new System.Drawing.Point(6, 77);
            this.lblIa2.Name = "lblIa2";
            this.lblIa2.Size = new System.Drawing.Size(33, 15);
            this.lblIa2.TabIndex = 0;
            this.lblIa2.Text = "ИА2:";
            // 
            // txtIa2
            // 
            this.txtIa2.Location = new System.Drawing.Point(62, 74);
            this.txtIa2.Name = "txtIa2";
            this.txtIa2.ReadOnly = true;
            this.txtIa2.Size = new System.Drawing.Size(85, 21);
            this.txtIa2.TabIndex = 1;
            // 
            // tlpParams
            // 
            this.tlpParams.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpParams.ColumnCount = 2;
            this.tlpParams.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpParams.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpParams.Controls.Add(this.grpParams, 0, 0);
            this.tlpParams.Controls.Add(this.grpCentral, 1, 0);
            this.tlpParams.Location = new System.Drawing.Point(12, 410);
            this.tlpParams.Name = "tlpParams";
            this.tlpParams.RowCount = 1;
            this.tlpParams.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpParams.Size = new System.Drawing.Size(460, 113);
            this.tlpParams.TabIndex = 10;
            // 
            // grpCentral
            // 
            this.grpCentral.Controls.Add(this.lblCasp);
            this.grpCentral.Controls.Add(this.txtIa2);
            this.grpCentral.Controls.Add(this.txtCasp);
            this.grpCentral.Controls.Add(this.lblIa1);
            this.grpCentral.Controls.Add(this.lblIa2);
            this.grpCentral.Controls.Add(this.txtIa1);
            this.grpCentral.Controls.Add(this.lblCasp2);
            this.grpCentral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpCentral.Location = new System.Drawing.Point(233, 3);
            this.grpCentral.Name = "grpCentral";
            this.grpCentral.Size = new System.Drawing.Size(224, 107);
            this.grpCentral.TabIndex = 5;
            this.grpCentral.TabStop = false;
            this.grpCentral.Text = "Центральные:";
            // 
            // tmrButton
            // 
            this.tmrButton.Enabled = true;
            this.tmrButton.Interval = 400;
            this.tmrButton.Tick += new System.EventHandler(this.tmrButton_Tick);
            // 
            // frmCasp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 535);
            this.Controls.Add(this.tlpParams);
            this.Controls.Add(this.tlpPulse);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.KeyPreview = true;
            this.MinimizeBox = false;
            this.Name = "frmCasp";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "CASP";
            this.Load += new System.EventHandler(this.frmCasp_Load);
            this.Shown += new System.EventHandler(this.frmCasp_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmCasp_KeyDown);
            this.Resize += new System.EventHandler(this.frmCasp_Resize);
            this.grpParams.ResumeLayout(false);
            this.grpParams.PerformLayout();
            this.tlpPulse.ResumeLayout(false);
            this.tlpPulse.PerformLayout();
            this.tlpParams.ResumeLayout(false);
            this.grpCentral.ResumeLayout(false);
            this.grpCentral.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblPulse;
        private System.Windows.Forms.Panel pnlPulse;
        private System.Windows.Forms.Button btnCalc;
        private System.Windows.Forms.GroupBox grpParams;
        private System.Windows.Forms.TextBox txtSrpvp;
        private System.Windows.Forms.Label lblSrpvp2;
        private System.Windows.Forms.TextBox txtTimeA1A4;
        private System.Windows.Forms.Label lblSrpvp;
        private System.Windows.Forms.Label lblTimeA1A42;
        private System.Windows.Forms.Label lblTimeA1A4;
        private System.Windows.Forms.Label lblCentralPulse;
        private System.Windows.Forms.Panel pnlCentralPulse;
        private System.Windows.Forms.TableLayoutPanel tlpPulse;
        private System.Windows.Forms.Label lblCasp;
        private System.Windows.Forms.TextBox txtIa2;
        private System.Windows.Forms.Label lblIa1;
        private System.Windows.Forms.TextBox txtIa1;
        private System.Windows.Forms.Label lblCasp2;
        private System.Windows.Forms.Label lblIa2;
        private System.Windows.Forms.TextBox txtCasp;
        private System.Windows.Forms.TableLayoutPanel tlpParams;
        private System.Windows.Forms.GroupBox grpCentral;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button btnCalcIa;
        private System.Windows.Forms.Timer tmrButton;
    }
}