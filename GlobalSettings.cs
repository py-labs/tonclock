﻿//
// В классе GlobalSettings хранятся настройки модуля "глобал"
//

using System.Configuration;

namespace TonClock
{
    sealed class GlobalSettings : ApplicationSettingsBase
    {

        // ----------------------------- Степени полинома -----------------------------

        // Флаг, что масштаб - по ширине окна
        [UserScopedSetting()]
        [DefaultSettingValue("true")]
        public bool FlagScaleWidth
        {
            get { return (bool)this["FlagScaleWidth"]; }
            set { this["FlagScaleWidth"] = value; }
        }


        // Степени полинома

        [UserScopedSetting()]
        [DefaultSettingValue("0.15")]
        public float J
        {
            get { return (float)this["J"]; }
            set { this["J"] = value; }
        }

        [UserScopedSetting()]
        [DefaultSettingValue("0.6")]
        public float N
        {
            get { return (float)this["N"]; }
            set { this["N"] = value; }
        }

        [UserScopedSetting()]
        [DefaultSettingValue("1")]
        public float H
        {
            get { return (float)this["H"]; }
            set { this["H"] = value; }
        }

        [UserScopedSetting()]
        [DefaultSettingValue("0.8")]
        public float I
        {
            get { return (float)this["I"]; }
            set { this["I"] = value; }
        }

        [UserScopedSetting()]
        [DefaultSettingValue("0.1")]
        public float R
        {
            get { return (float)this["R"]; }
            set { this["R"] = value; }
        }


    }
}
